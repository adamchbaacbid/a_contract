<?php
$type='TrueType';
$name='AngsanaNew';
$desc=array('Ascent'=>923,'Descent'=>-239,'CapHeight'=>437,'Flags'=>32,'FontBBox'=>'[-490 -410 791 941]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>750);
$up=-50;
$ut=25;
$cw=array(
	chr(0)=>750,chr(1)=>750,chr(2)=>750,chr(3)=>750,chr(4)=>750,chr(5)=>750,chr(6)=>750,chr(7)=>750,chr(8)=>750,chr(9)=>750,chr(10)=>750,chr(11)=>750,chr(12)=>750,chr(13)=>750,chr(14)=>750,chr(15)=>750,chr(16)=>750,chr(17)=>750,chr(18)=>750,chr(19)=>750,chr(20)=>750,chr(21)=>750,
	chr(22)=>750,chr(23)=>750,chr(24)=>750,chr(25)=>750,chr(26)=>750,chr(27)=>750,chr(28)=>750,chr(29)=>750,chr(30)=>750,chr(31)=>750,' '=>165,'!'=>220,'"'=>270,'#'=>330,'$'=>330,'%'=>550,'&'=>513,'\''=>119,'('=>220,')'=>220,'*'=>330,'+'=>372,
	','=>165,'-'=>220,'.'=>165,'/'=>183,'0'=>330,'1'=>330,'2'=>330,'3'=>330,'4'=>330,'5'=>330,'6'=>330,'7'=>330,'8'=>330,'9'=>330,':'=>183,';'=>183,'<'=>372,'='=>372,'>'=>372,'?'=>293,'@'=>608,'A'=>477,
	'B'=>440,'C'=>440,'D'=>477,'E'=>403,'F'=>367,'G'=>477,'H'=>477,'I'=>220,'J'=>257,'K'=>477,'L'=>403,'M'=>587,'N'=>477,'O'=>477,'P'=>367,'Q'=>477,'R'=>440,'S'=>367,'T'=>403,'U'=>477,'V'=>477,'W'=>623,
	'X'=>477,'Y'=>477,'Z'=>403,'['=>220,'\\'=>183,']'=>220,'^'=>310,'_'=>330,'`'=>220,'a'=>293,'b'=>330,'c'=>293,'d'=>330,'e'=>293,'f'=>220,'g'=>330,'h'=>330,'i'=>183,'j'=>183,'k'=>330,'l'=>183,'m'=>513,
	'n'=>330,'o'=>330,'p'=>330,'q'=>330,'r'=>220,'s'=>257,'t'=>183,'u'=>330,'v'=>330,'w'=>477,'x'=>330,'y'=>330,'z'=>293,'{'=>317,'|'=>132,'}'=>317,'~'=>357,chr(127)=>750,chr(128)=>330,chr(129)=>750,chr(130)=>750,chr(131)=>750,
	chr(132)=>750,chr(133)=>750,chr(134)=>750,chr(135)=>750,chr(136)=>750,chr(137)=>750,chr(138)=>750,chr(139)=>750,chr(140)=>750,chr(141)=>750,chr(142)=>750,chr(143)=>750,chr(144)=>750,chr(145)=>750,chr(146)=>750,chr(147)=>750,chr(148)=>750,chr(149)=>750,chr(150)=>750,chr(151)=>750,chr(152)=>750,chr(153)=>750,
	chr(154)=>750,chr(155)=>750,chr(156)=>750,chr(157)=>750,chr(158)=>750,chr(159)=>750,chr(160)=>165,chr(161)=>750,chr(162)=>750,chr(163)=>750,chr(164)=>750,chr(165)=>750,chr(166)=>750,chr(167)=>750,chr(168)=>750,chr(169)=>750,chr(170)=>750,chr(171)=>750,chr(172)=>750,chr(173)=>750,chr(174)=>750,chr(175)=>750,
	chr(176)=>750,chr(177)=>750,chr(178)=>750,chr(179)=>750,chr(180)=>750,chr(181)=>750,chr(182)=>750,chr(183)=>750,chr(184)=>750,chr(185)=>750,chr(186)=>750,chr(187)=>750,chr(188)=>750,chr(189)=>750,chr(190)=>750,chr(191)=>750,chr(192)=>750,chr(193)=>750,chr(194)=>750,chr(195)=>750,chr(196)=>750,chr(197)=>750,
	chr(198)=>750,chr(199)=>750,chr(200)=>750,chr(201)=>750,chr(202)=>750,chr(203)=>750,chr(204)=>750,chr(205)=>750,chr(206)=>750,chr(207)=>750,chr(208)=>750,chr(209)=>750,chr(210)=>750,chr(211)=>750,chr(212)=>750,chr(213)=>750,chr(214)=>750,chr(215)=>750,chr(216)=>750,chr(217)=>750,chr(218)=>750,chr(219)=>750,
	chr(220)=>750,chr(221)=>750,chr(222)=>750,chr(223)=>750,chr(224)=>750,chr(225)=>750,chr(226)=>750,chr(227)=>750,chr(228)=>750,chr(229)=>750,chr(230)=>750,chr(231)=>750,chr(232)=>750,chr(233)=>750,chr(234)=>750,chr(235)=>750,chr(236)=>750,chr(237)=>750,chr(238)=>750,chr(239)=>750,chr(240)=>750,chr(241)=>750,
	chr(242)=>750,chr(243)=>750,chr(244)=>750,chr(245)=>750,chr(246)=>750,chr(247)=>750,chr(248)=>750,chr(249)=>750,chr(250)=>750,chr(251)=>750,chr(252)=>750,chr(253)=>750,chr(254)=>750,chr(255)=>750);
$enc='cp874';
$diff='130 /.notdef /.notdef /.notdef 134 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 142 /.notdef 152 /.notdef /.notdef /.notdef /.notdef /.notdef 158 /.notdef /.notdef 161 /kokaithai /khokhaithai /khokhuatthai /khokhwaithai /khokhonthai /khorakhangthai /ngonguthai /chochanthai /chochingthai /chochangthai /sosothai /chochoethai /yoyingthai /dochadathai /topatakthai /thothanthai /thonangmonthothai /thophuthaothai /nonenthai /dodekthai /totaothai /thothungthai /thothahanthai /thothongthai /nonuthai /bobaimaithai /poplathai /phophungthai /fofathai /phophanthai /fofanthai /phosamphaothai /momathai /yoyakthai /roruathai /ruthai /lolingthai /luthai /wowaenthai /sosalathai /sorusithai /sosuathai /hohipthai /lochulathai /oangthai /honokhukthai /paiyannoithai /saraathai /maihanakatthai /saraaathai /saraamthai /saraithai /saraiithai /sarauethai /saraueethai /sarauthai /sarauuthai /phinthuthai /.notdef /.notdef /.notdef /.notdef /bahtthai /saraethai /saraaethai /saraothai /saraaimaimuanthai /saraaimaimalaithai /lakkhangyaothai /maiyamokthai /maitaikhuthai /maiekthai /maithothai /maitrithai /maichattawathai /thanthakhatthai /nikhahitthai /yamakkanthai /fongmanthai /zerothai /onethai /twothai /threethai /fourthai /fivethai /sixthai /seventhai /eightthai /ninethai /angkhankhuthai /khomutthai /.notdef /.notdef /.notdef /.notdef';
$file='cdfb2ac2091924bae9c196850805dd05_angsa.z';
$originalsize=109760;
?>
