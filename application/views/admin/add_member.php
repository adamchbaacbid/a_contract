
<script>

    function ajax_add_member(){

        var employee_id_member = $('#employee_id_member').val();
        var name_member = $('#name_member').val();
        var position_member = $('#position_member').val();
        var email_member = $('#email_member').val();
        var status_member = $('#status_member').val();
        var tel_member = $('#tel_member').val();
        var agency_member = $('#agency_member').val();

        if(employee_id_member==""){
            alert('กรุณาระบุรหัสพนักงาน');
            $('#employee_id_member').focus();
        }else{
            if(name_member==""){
                alert('กรุณาระบุ ชื่อ นามสกุล ของผู้ใช้งาน');
                $('#name_member').focus();
            }else{
                if(position_member==""){
                    alert('กรุณาระบุตำแหน่ง');
                    $('#position_member').focus();
                }else{
                    if(email_member==""){
                        alert('กรุณาระบุอีเมล');
                        $('#email_member').focus();
                    }else{
                        if(tel_member==""){
                            alert('กรุณาระบุสถานนะ');
                            $('#tel_member').focus();
                        }else{

                            $.post( "<?=site_url('index.php/member/add_member_ajax') ?>", {
                                employee_id_member:employee_id_member,
                                name_member:name_member,
                                position_member:position_member,
                                status_member:status_member,
                                email_member:email_member,
                                tel_member:tel_member,
                                agency_member:agency_member

                            }).done(function( msg ) {
                                alert( msg );
                                location.reload();
                            });
                        }

                    }
                }
            }
        }
    }


</script>




<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-user-plus fa-2x text-gray-300"></i>  เพิ่มผู้ใช้งาน</h1>
    <p class="mb-4"></p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เพิ่มบัญชีผู้ใช้งาน</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="employee_id_member">รหัสพนักงาน</label>
                            <input type="number" id="employee_id_member" name="employee_id_member" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==7) return false;" placeholder="รหัสพนักงาน" class="form-control"/>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="name_member">ชื่อ นามสกุล </label>
                            <input type="text" id="name_member" placeholder="" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <label for="position_member">ตำแหน่ง</label>
                            <input type="text" id="position_member" placeholder="ตำแหน่ง"  class="form-control"/>
                        </td>
                        <td >
                            <label for="email_member">อีเมล</label>
                            <input type="text" id="email_member" placeholder="@baac.or.th"  class="form-control"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="agency_member">ส่วนงาน</label>
                            <select  id="agency_member" class="form-control">
                                <option value="สำนักตรวจสอบเทคโนโลยีและสารสนเทศ">สำนักตรวจสอบเทคโนโลยีและสารสนเทศ</option>
                                <option value="สำนักจัดซื้อจัดจ้างพัสดุ">สำนักจัดซื้อจัดจ้างพัสดุ</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="tel_member">โทร</label>
                            <input type="text" id="tel_member" placeholder=""  class="form-control"/>
                        </td>
                        <td>
                            <label for="status_member">สถานะ</label>
                            <select  id="status_member" class="form-control">
                                <option value="admin">Admin</option>
                                <option value="audit user">audit user</option>
                                <option value="purchase user">purchase user</option>
                                <option value="contract user">contract user</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #00CC00">*** รหัสผ่าน และ username เข้าใช้งานครั้งแรกจะเป็นรหัสพนักงาน</td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button class="btn btn-primary" onclick="ajax_add_member()">เพื่มผู้ใช้งาน</button>
                            <button type="reset" class="btn btn-danger" onclick="history.back()" >ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->
