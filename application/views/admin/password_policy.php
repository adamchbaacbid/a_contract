
<script>

    function ajax_add_member(){

        var id_member = $('#id_member').val();
        var name_member = $('#name_member').val();
        var position_member = $('#position_member').val();
        var username = $('#username').val();
        var status_member = $('#status_member').val();

        if(id_member==""){
            alert('กรุณาระบุรหัสพนักงาน');
            $('#id_member').focus();
        }else{
            if(name_member==""){
                alert('กรุณาระบุ ชื่อ นามสกุล ของผู้ใช้งาน');
                $('#name_member').focus();
            }else{
                if(position_member==""){
                    alert('กรุณาเลือกตำแหน่ง');
                    $('#position_member').focus();
                }else{
                    if(username==""){
                        alert('กรุณาระบุชื่อเข้าใช้งาน');
                        $('#username').focus();
                    }else{
                        if(status_member==""){
                            alert('กรุณาระบุสถานนะ');
                            $('#status_member').focus();
                        }else{

                            $.post( "<?=site_url('member/add_member_ajax') ?>", {
                                id_member:id_member,
                                name_member:name_member,
                                position_member:position_member,
                                username:username,
                                status_member:status_member

                            }).done(function( test ) {
                                alert( test );
                                location.reload();
                            });
                        }

                    }
                }
            }
        }


    }


</script>




<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-user-plus fa-2x text-gray-300"></i>  เพิ่มผู้ใช้งาน</h1>
    <p class="mb-4"></p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เพิ่มบัญชีผู้ใช้งาน</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="no_bid">รหัสพนักงาน</label>
                            <input type="number" id="id_member" name="id_member" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==7) return false;" placeholder="รหัสพนักงาน" class="form-control"/>
                        </td>
                        <td>
                            <label for="date_bid">ชื่อ นามสกุล </label>
                            <input type="text" id="name_member" placeholder="" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="position_member">ตำแหน่ง</label>
                            <select  id="position_member" class="form-control">
                                <option value="ผู้บริหารทีม">ผู้บริหารทีม</option>
                                <option value="พนักงานตรวจสอบคอมพิวเตอร์9">พนักงานตรวจสอบคอมพิวเตอร์9</option>
                                <option value="พนักงานตรวจสอบคอมพิวเตอร์8">พนักงานตรวจสอบคอมพิวเตอร์8</option>
                                <option value="พนักงานตรวจสอบคอมพิวเตอร์7">พนักงานตรวจสอบคอมพิวเตอร์7</option>
                                <option value="พนักงานตรวจสอบคอมพิวเตอร์6">พนักงานตรวจสอบคอมพิวเตอร์6</option>
                                <option value="พนักงานตรวจสอบคอมพิวเตอร์5">พนักงานตรวจสอบคอมพิวเตอร์5</option>
                                <option value="พนักงานตรวจสอบคอมพิวเตอร์4">พนักงานตรวจสอบคอมพิวเตอร์4</option>
                                <option value="อื่นๆ">อื่นๆ</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="username">ชื่อสำหรับเข้าใช้งานระบบ</label>
                            <input type="text" id="username" placeholder="username"  class="form-control"/>
                        </td>
                        <td>
                            <label for="status_member">สถานะ</label>
                            <select  id="status_member" class="form-control">
                                <option value="admin">Admin</option>
                                <option value="user">User</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #00CC00">*** รหัสผ่านเข้าใช้งานครั้งแรกจะเป็น 1234</td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button type="submit" class="btn btn-success" onclick="ajax_add_member()">เพื่มผู้ใช้งาน</button>
                            <button type="reset" class="btn btn-danger" onclick="history.back()" >ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->
