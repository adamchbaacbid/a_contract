


<script>
    function delete_member(username){
       
        var confirm1 = confirm("ต้องการลบผู้ใช้งาน Username "+username+" ใช่หรือไม่");
 
        if(confirm1 == true){
            $.post( "<?=site_url('index.php/admin/delete_member') ?>", {
                username:username
            })
                .done(function( data ) {
                    alert( data );
                    location.reload();
                });
        } 
    }
</script>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-user-friends fa-2x text-gray-300"></i> ผู้ใช้งานทั้งหมด</h1>
    <p class="mb-4"></p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">รายชื่อผูใช้งานทั้งหมด</h6>

        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Username</th>
                        <th scope="col">ชื่อ</th>
                        <th scope="col">ตำแหน่ง</th>
                        <th scope="col">สิทธิ</th>
                        <th scope="col">สถานะ</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $i=1;
                    foreach ($member as $row){
                        $username = $row->user_member;

                        echo"
                        <tr>
                        <th scope=\"row\">$i</th>
                        <td>$row->user_member</td>
                        <td>$row->name_member</td>
                        <td>$row->position_member</td>
                        <td>$row->status_member</td>
                        <td>$row->privilege_member</td>
                        <td><a href='#' onclick='delete_member(\"$username\")' title = 'ลบ' > <i class='fas fa-trash'></i></a></td>
                        
                        </tr>
                        
                        
                        
                        
                        
                        ";
                        $i++;
                    }


                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->
