<?php
$data = $this->session->userdata('data');
$session = $data['msg'];

if(isset($session)){
    $user_member =$data['member'];

    if(isset($user_member)){

    }else{
        redirect(base_url('index.php/login'));
    }

}else{
    redirect(base_url('index.php/login_controller/login'));
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ระบบ Procurement Contract</title>
    <link rel="shortcut icon" href="<?=base_url('theme/img/logo2.png') ?>" />





    <!-- Custom fonts for this template-->

    <link href="<?php echo base_url('theme/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('theme/css/css.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('theme/css/sb-admin-2.min.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="<?php echo base_url('theme/vendor/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">




</head>

<body id="page-top" >



<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('index.php/admin/index'); ?>">
            <div class="sidebar-brand-icon ">
                <img class="img-profile rounded-circle" src="<?=base_url('theme/img/logo2.png') ?>" width="50%">
            </div>
            <div class="sidebar-brand-text mx-3 "></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">



        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/admin/index'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>สรุปผู้ใช้งานระบบ</span></a>
        </li>


        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            จัดการ
        </div>


        <!-- Nav Item - Utilities Collapse Menu  policy-->
        <li class="nav-item" >
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#policy" aria-expanded="true" aria-controls="policy">
                <i class="fas fa-fw fa-wrench"></i>
                <span>จัดการระบบ</span>
            </a>
            <div id="policy" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" >
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">ตั้งค่าระบบ:</h6>
                    <a class="collapse-item" href="<?=base_url('index.php/admin/password_policy') ?>">password policy</a>
                    <a class="collapse-item" href="<?=base_url('index.php/admin/show_log') ?>">Audit LOG</a>
                </div>
            </div>
        </li>


        <!-- Nav Item - Utilities Collapse Menu  ผู้ใช้ระบบ-->
        <li class="nav-item" >
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-wrench"></i>
                <span>จัดการผู้ใช้งาน</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" >
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">จัดการผู้ใช้งานระบบ:</h6>
                    <a class="collapse-item" href="<?=base_url('index.php/admin/add_member') ?>">เพิ่มผู้ใช้งาน</a>
                    <a class="collapse-item" href="<?=base_url('index.php/admin/show_all_member') ?>">ผู้ใช้งานมทั้งหมด</a>
                </div>
            </div>
        </li>


        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-primary topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar brand Navbar -->
                <ul class="navbar-nav ml-auto">
                    <h1 class=" text-white text-shadow " >ระบบ A - Contract Management</h1>
                </ul>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-white small"><?=$data['name']; ?></span>
                            <i class="fas fa-user-circle fa-2x text-white"></i>
                            <!--img class="img-profile rounded-circle" src="<?=base_url('theme/img/user.png') ?>"-->
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="<?=site_url('index.php/admin/edit_profile') ?>">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                แก้ไขข้อมูล
                            </a>
                            <a class="dropdown-item" href="<?=site_url('index.php/admin/edit_password') ?>">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                เปลี่ยนรหัสผ่าน
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                ลงชื่อออก
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->











