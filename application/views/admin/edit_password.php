<?php

$member = $this->session->userdata('member');
$data = $member;

?>
<script>

    function ajax_edit_password(){
        var pass_member_old = $('#pass_member_old').val();
        var pass_member_new = $('#pass_member_new').val();
        var pass_member_renew = $('#pass_member_renew').val();
        



        if(pass_member_old==""){
            alert('กรุณาระบุรหัสผ่านเดิม');
            $('#pass_member_old').focus();
        }else{
            if(pass_member_new==""){
                alert('กรุณาระบุรหัสผ่านใหม่');
                $('#pass_member_new').focus();
            }else{
                if(pass_member_renew==""){
                    alert('ยืนยันรหัสผ่านใหม่');
                    $('#pass_member_renew').focus();
                }else{

                   
                        $.post( "<?=site_url('index.php/login_controller/check_old_password_ajax') ?>", {
                                pass_member_old:pass_member_old
                            }).done(function( test ) {

                                if(test=="pass") {


                                   $.post("<?=site_url('index.php/login_controller/edit_password_ajax') ?>", {
                                        pass_member_new: pass_member_new

                                    }).done(function (test) {

                                        alert(test);
                                        location.reload();
                                    });

                                }else {
                                    alert('รหัสผ่านเดิมไม่ถูกต้อง');
                                    $('#pass_member_old').focus();
                                }

                            });

                }
            }
        }


    }

    function check_new_password() {
        var pass_member_new = $('#pass_member_new').val();
        var pass_member_renew = $('#pass_member_renew').val();

        if(pass_member_new!=pass_member_renew){
            $('#button_edit').prop('disabled', true);

            $('#msg').addClass('alert alert-warning').html('รหัสผ่านใหม่ไม่ตรงกัน');
        }else{
            $('#button_edit').prop('disabled', false);
            $('#msg').removeClass('alert alert-warning').html('');
        }
    }

    function check_pass(){
        var code = $('#pass_member_new').val();
        checkpassword(code);
    }

    function checkpassword(password) {
            var strengthbar = document.getElementById("meter");
            var display = document.getElementsByClassName("textbox")[0];
            var display2 = document.getElementsByClassName("textbox2")[0];
            var display3 = document.getElementsByClassName("textbox3")[0];
            var display4 = document.getElementsByClassName("textbox4")[0];
            var display5 = document.getElementsByClassName("textbox5")[0];
            var strength = 0;
            var small = " ต้องมีตัวภาษาอังกฤษพิมพ์เล็ก ";
            var large  = " ต้องมีตัวภาษาอังกฤษพิมพ์ใหญ่ ";
            var num  = " ต้องมีตัวเลข ";
            var char  = " ต้องมีสัญลักษณ์$@#&! ";

            if (password.match(/[a-z]+/)) {
                strength += 1;
                display2.innerHTML = "" ;

            }else{
                display2.innerHTML = small ;
            }
            if (password.match(/[A-Z]+/)) {
                strength += 1;
                display3.innerHTML = "" ;
            }else{
                display3.innerHTML = large ;
            }
            if (password.match(/[0-9]+/)) {
                strength += 1;
                display4.innerHTML = "" ;
            }else{
                display4.innerHTML = num ;
            }
            if (password.match(/[$@#&!]+/)) {
                strength += 1;
                display5.innerHTML = "" ;
            }else {
                display5.innerHTML = char ;
            }

            if (password.length < 8) {

                display.innerHTML = "ตัวอักษรอย่างน้อย 8 ตัว";
            }else {
                display.innerHTML = "";
            }



            switch (strength) {
                case 0:
                    strengthbar.value = 0;
                    $('#button_change').prop('disabled', true);
                    break;

                case 1:
                    strengthbar.value = 25;
                    $('#button_change').prop('disabled', true);
                    break;

                case 2:
                    strengthbar.value = 50;
                    $('#button_change').prop('disabled', true);
                    break;

                case 3:
                    strengthbar.value = 75;
                    $('#button_change').prop('disabled', true);
                    break;

                case 4:
                    strengthbar.value = 100;
                    $('#button_change').prop('disabled', true);

                    break;
            }
    }


        


</script>




<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-cogs fa-2x text-gray-300"></i>  เปลี่ยนรหัสผ่าน</h1>
    <p class="mb-4"></p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เปลี่ยนรหัสผ่าน</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive col-lg-6 col-sm-12" >
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="pass_member">รหัสผ่านเดิม</label>
                            <input type="password" id="pass_member_old" name="pass_member_old"   class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div style="color:Tomato;" >                                    
                                    <div class="textbox text-center"> ตัวอักษรอย่างน้อย 8 ตัว </div>
                                    <div class="textbox2 text-center"> ต้องมีตัวภาษาอังกฤษพิมพ์เล็ก </div>
                                    <div class="textbox3 text-center"> ต้องมีตัวภาษาอังกฤษพิมพ์ใหญ่ </div>
                                    <div class="textbox4 text-center"> ต้องมีตัวเลข </div>
                                    <div class="textbox5 text-center"> ต้องมีสัญลักษณ์$@#&! </div>
                                    <div align="center" hidden><progress max="100" value="0" id="meter"  ></progress></div>

                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="pass_member_new">รหัสผ่านใหม่</label>
                            <input type="password" id="pass_member_new" name="pass_member_new" onkeyup="check_pass()"    class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="pass_member_renew">รหัสผ่านใกม่อีกครั้ง </label>
                            <input type="password" id="pass_member_renew" onkeyup="check_new_password()" name="pass_member_renew"   class="form-control"/>
                            <div  role="alert" id="msg">

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <button type="submit" class="btn btn-success" id="button_edit" onclick="ajax_edit_password()" disabled>ยืนยัน เปลี่ยนรหัสผ่าน</button>
                            <button type="reset" class="btn btn-danger" onclick="window.location.replace('<?=base_url()?>')" >ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->
