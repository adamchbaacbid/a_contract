
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Custom fonts for this template-->

    <link href="<?php echo base_url('theme/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('theme/css/css.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('theme/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
    <script>
        function on_submit(){

            if($('#username').val()==""){
                alert('กรุณาใส่ชื่อผู้ใช้งาน');
            }else{
                if($('#pass').val()==""){
                    alert('กรุณาใส่รหัสผ่าน');
                }else {
                    $('#form_login').submit();
                }
            }
        }
    </script>
</head>

<body class="bg-gradient-primary">
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block "><img src="<?=base_url('theme/img/login.jpg') ?>"  width="500" height="500"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4  text-gray-900 mb-4 ">ระบบ Procurement Contract</h1>
                                </div>
                                <form class="user" id="form_login"  method="post" action="<?=base_url('index.php/login_controller/do_login') ?>" >
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user"name="username" id="username" aria-describedby="emailHelp" placeholder="ชื่อผู้ใช้งาน...">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user"name="pass" id="pass" placeholder="รหัสผ่าน..">
                                    </div>
                                    <button class="btn btn-primary btn-user btn-block text-white" type="submit" >ลงชื่อเข้าใช้</button>



                                </form>
                                <div ><a href="<?=base_url('index.php/login_controller/forgot_password') ?>">ลืมรหัสผ่าน</a></div>
                                <hr>
                                <div style="color: #00CC00;font-size: small">*** เข้าใช้งานครั้งแรก รหัสผ่าน และ username จะเป็นรหัสพนักงาน</div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url('') ;?>theme/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('') ;?>theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('') ;?>theme/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url('') ;?>theme/js/sb-admin-2.min.js"></script>

</body>

</html>
