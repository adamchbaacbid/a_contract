
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Custom fonts for this template-->

    <link href="<?php echo base_url('theme/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('theme/css/css.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('theme/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
    <script>
        window.onload = function(){
            $('#button_change').prop('disabled', true);
        };

        function on_submit(){

            var new_password = $('#new_password').val();
            var confirm_password =  $('#confirm_password').val();

            if(new_password ==""){
                alert('กรุณาระบุรหัสผ่านใหม่');
            }else{
                if(confirm_password ==""){
                    alert('กรุณายืนยันรหัสผ่าน');
                }else {
                    if(new_password == confirm_password) {
                        $('#form_change').submit();
                    }else{
                        alert('ยืนยันรหัสผ่านไม่ถูกต้อง');
                        $('#confirm_password').focus();
                    }
                }
            }
        }


        function check_new_password() {
            var new_password = $('#new_password').val();
            var confirm_password = $('#confirm_password').val();

            if(new_password!=confirm_password){
                $('#button_change').prop('disabled', true);
                $('#msg').addClass('alert alert-warning').html('ยืนยันรหัสผ่านไม่ตรงกัน');
            }else{
                $('#button_change').prop('disabled', false);
                $('#msg').removeClass('alert alert-warning').html('');
            }
        }





        function check_pass(){
            var code = $('#new_password').val();
            checkpassword(code);
        }




        function checkpassword(password) {
            var strengthbar = document.getElementById("meter");
            var display = document.getElementsByClassName("textbox")[0];
            var display2 = document.getElementsByClassName("textbox2")[0];
            var display3 = document.getElementsByClassName("textbox3")[0];
            var display4 = document.getElementsByClassName("textbox4")[0];
            var display5 = document.getElementsByClassName("textbox5")[0];
            var strength = 0;
            var small = " ต้องมีตัวภาษาอังกฤษพิมพ์เล็ก ";
            var large  = " ต้องมีตัวภาษาอังกฤษพิมพ์ใหญ่ ";
            var num  = " ต้องมีตัวเลข ";
            var char  = " ต้องมีสัญลักษณ์$@#&! ";

            if (password.match(/[a-z]+/)) {
                strength += 1;
                display2.innerHTML = "" ;

            }else{
                display2.innerHTML = small ;
            }
            if (password.match(/[A-Z]+/)) {
                strength += 1;
                display3.innerHTML = "" ;
            }else{
                display3.innerHTML = large ;
            }
            if (password.match(/[0-9]+/)) {
                strength += 1;
                display4.innerHTML = "" ;
            }else{
                display4.innerHTML = num ;
            }
            if (password.match(/[$@#&!]+/)) {
                strength += 1;
                display5.innerHTML = "" ;
            }else {
                display5.innerHTML = char ;
            }

            if (password.length < 8) {

                display.innerHTML = "ตัวอักษรอย่างน้อย 8 ตัว";
            }else {
                display.innerHTML = "";
            }



            switch (strength) {
                case 0:
                    strengthbar.value = 0;
                    $('#button_change').prop('disabled', true);
                    break;

                case 1:
                    strengthbar.value = 25;
                    $('#button_change').prop('disabled', true);
                    break;

                case 2:
                    strengthbar.value = 50;
                    $('#button_change').prop('disabled', true);
                    break;

                case 3:
                    strengthbar.value = 75;
                    $('#button_change').prop('disabled', true);
                    break;

                case 4:
                    strengthbar.value = 100;
                    $('#button_change').prop('disabled', true);

                    break;
            }
        }

    </script>
</head>

<body class="bg-gradient-primary">
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"><img src="<?=base_url('theme/img/resetpass.jpg') ?>"  width="500" height="500"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4  text-gray-900 mb-4 ">เปลี่ยนรหัสผ่าน</h1>
                                </div>
                                <div style="color:Tomato;">
                                    <div class="textbox text-center" > ตัวอักษรอย่างน้อย 8 ตัว </div>
                                    <div class="textbox2 text-center"> ต้องมีตัวภาษาอังกฤษพิมพ์เล็ก </div>
                                    <div class="textbox3 text-center"> ต้องมีตัวภาษาอังกฤษพิมพ์ใหญ่ </div>
                                    <div class="textbox4 text-center"> ต้องมีตัวเลข </div>
                                    <div class="textbox5 text-center"> ต้องมีสัญลักษณ์$@#&! </div>
                                    <div align="center" hidden><progress max="100" value="0" id="meter"  ></progress></div>

                                </div>

                                <form class="user" id="form_change"  method="post" action="<?=base_url('index.php/login_controller/first_chang_pass') ?>" >
                                    <div class="form-group">
                                        <input  type="password" onkeyup="check_pass()"  autocomplete="off" class="form-control form-control-user" name="new_password" id="new_password"  placeholder="รหัสผ่านใหม่">
                                    </div>
                                    <div class="form-group">
                                        <input id="user_member" name="user_member" value="<?php echo $user_member ; ?>" hidden>
                                        <input type="password"  onkeyup="check_new_password()" class="form-control form-control-user"name="confirm_password" id="confirm_password" placeholder="ยืนยันรหัสผ่าน">

                                    </div>
                                    <button class="btn btn-primary btn-user btn-block text-white" id="button_change" type="submit" >เปลี่ยนรหัสผ่าน</button>

                                </form>

                                <hr>
                                <div  role="alert" id="msg">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url('') ;?>theme/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('') ;?>theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('') ;?>theme/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url('') ;?>theme/js/sb-admin-2.min.js"></script>

</body>

</html>
