<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Forgot Password</title>

  <link href="<?php echo base_url('theme/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('theme/css/css.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('theme/css/sb-admin-2.min.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="<?php echo base_url('theme/vendor/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">

    <script>
        function on_submit(){

            if($('#email').val()==""){
                alert('กรุณาระบุอีเมล์');
            }else{
                $('#form_forgot_password').submit();
            }
        }
    </script>

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block "><img src="<?=base_url('theme/img/forgot_password.jpg') ?>"  width="500" height="500"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">ลืมรหัสผ่าน</h1>
                    <p class="mb-4">ใส่อีเมล์ ระบบจะส่งรหัสผ่านชั่วคราวให้ผ่านทางอีเมล์ เมื่อได้รับรหัสผ่านชั่วคราวแล้วต้องทำการเปลี่ยนหัสผ่านใหม่เพื่อเข้าใช้งาน</p>
                  </div>
                  <form class="user" id="form_forgot_password"  method="post" action="<?=base_url('index.php/login_controller/forgot_password_ajax') ?>">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                    </div>
                    <button class="btn btn-primary btn-user btn-block text-white" onclick="on_submit()" >Reset Password</button>
                    
                  </form>
                  
                  <hr>
                  <div class="text-center">
                    <a class="small" href="<?php echo base_url(); ?>">ย้อนกลับ</a>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url('') ;?>theme/vendor/jquery/jquery.min.js"></script>

<script src="<?php echo base_url('') ;?>theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('') ;?>theme/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url('') ;?>theme/js/sb-admin-2.min.js"></script>

</body>

</html>
