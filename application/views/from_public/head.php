
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Procurement Platform</title>
    <link rel="shortcut icon" href="<?=base_url('theme/img/logo2.png') ?>" />





    <!-- Custom fonts for this template-->

    <link href="<?php echo base_url('theme/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('theme/css/css.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('theme/css/sb-admin-2.min.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="<?php echo base_url('theme/vendor/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">




</head>

<body id="page-top" >



<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>">
            <div class="sidebar-brand-icon ">
                <img class="img-profile rounded-circle" src="<?=base_url('theme/img/logo2.png') ?>" width="50%">
            </div>
            <div class="sidebar-brand-text mx-3 "></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>">
                <i class="fas fa-fw fa-home"></i>
                <span>หน้าหลัก</span></a>
        </li>


        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            ประเภทงานจัดซื้อจัดจ้าง
        </div>

        <!-- Nav Item - Pages Collapse Menu  จัดการ-->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <i class="fas fa-fw fa-cog"></i>
                <span>ทรัพย์สิน</span>
            </a>
            <div id="collapseOne" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header"  >ประเภทงาน :  </h6>
                    <a class="collapse-item" href="<?php echo base_url('index.php/public_con/show_car_rental'); ?>" >เช่ารถยนต์ (วิธีคัดเลือก)</a>
                    <a class="collapse-item" href="#">เช่าอาคาร (ที่ทำการ สนง.)</a>
                </div>
            </div>
        </li>

        <!-- Nav Item - Pages Collapse Menu  จัดการ-->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>ทั่วไป</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">ประเภทงาน :  </h6>
                    <a class="collapse-item" href="<?php echo base_url('index.php/public_con/show_atc'); ?>" >ซื้อของรางวัล อทช.</a>
                    <a class="collapse-item" href="#">คชจ.ในการบริหารงาน</a>


                </div>

            </div>
        </li>


        <!-- Nav Item - Pages Collapse Menu  จัดการ-->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
                <i class="fas fa-fw fa-cog"></i>
                <span>IT</span>
            </a>
            <div id="collapsethree" class="collapse" aria-labelledby="headingthree" data-parent="#accordionSidebar">

                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">ประเภทงาน :  </h6>


                </div>
            </div>
        </li>




        <!-- Nav Item - Utilities Collapse Menu  ผู้ใช้ระบบ-->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-wrench"></i>
                <span>ก่อสร้าง/ปรับปรุง</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" >
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">ประเภทงาน :  </h6>

                </div>
            </div>
        </li>


        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-primary topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar brand Navbar -->
                <ul class="navbar-nav ml-auto">
                    <h1 class=" text-white text-shadow " >Procurement Platform </h1>
                </ul>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-white small"></span>
                            <i class="fas fa-user-circle fa-2x text-white"></i>
                            <!--img class="img-profile rounded-circle" src="<?=base_url('theme/img/user.png') ?>"-->
                        </a>

                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->











