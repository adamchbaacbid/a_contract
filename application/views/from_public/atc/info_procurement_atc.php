
<style>

    a {
        color:inherit;
        text-decoration: none;
    }
</style>
<script>

    function show_modal_add_step(step) {

        $('#modal_add_step'+step).modal('show');
    }







    window.onload = function(){
        ajax_show_info_procurement();
    }

    function ajax_add_car_rental_step1() {
        var branch = "<?php echo $id_branch; ?>";
        var year_procurement = "<?php echo $year_procurement; ?>";
        var time_procurement = "<?php echo $time_procurement; ?>";
        var select_step1 = $('#select_step1').val();
        var description_step1 = $('#description_step1').val();
        var name_employee_step1 = $('#name_employee_step1').val();


        if(branch==""){
            alert('กรุณาเลือกสาขา');
            $('#branch').focus();
        }else{
            if(year_procurement==""){
                alert('กรุณาระบุปีบัญชี');
                $('#year_procurement').focus();
            }else{
                if(time_procurement==""){
                    alert('กรุณาระบุครั้งที่');
                    $('#time_procurement').focus();
                }else{
                    $.post( "<?=site_url('index.php/public_con/ajax_add_car_rental_step1') ?>", {
                        branch:branch,
                        select_step1:select_step1,
                        description_step1:description_step1,
                        name_employee_step1:name_employee_step1,
                        year_procurement:year_procurement,
                        time_procurement:time_procurement
                    }).done(function( test ) {
                        alert( test );
                        if(test=='บันทึกสำเร็จ'){
                            ajax_show_info_procurement();
                            $('#modal_add_step1').modal('hide');
                        }
                    });
                }
            }
        }
    }

    function ajax_add_atc_step2() {

        var select_step2 = $('#select_step2').val();
        var description_step2 = $('#description_step2').val();
        var name_employee_step2 = $('#name_employee_step2').val();
        //var id_procurement_type = "<?php echo $id_procurement_type; ?>";
        var branch = "<?php echo $id_branch; ?>";
        var year_procurement = "<?php echo $year_procurement; ?>";
        var time_procurement = "<?php echo $time_procurement; ?>";

        if(branch==""){
            alert('กรุณาเลือกสาขา');
            $('#branch').focus();
        }else{
            if(year_procurement==""){
                alert('กรุณาระบุปีบัญชี');
                $('#year_procurement').focus();
            }else{
                if(time_procurement==""){
                    alert('กรุณาระบุครั้งที่');
                    $('#time_procurement').focus();
                }else{
                    $.post( "<?=site_url('index.php/public_con/ajax_add_atc_step2') ?>", {
                        branch:branch,
                        select_step2:select_step2,
                        description_step2:description_step2,
                        name_employee_step2:name_employee_step2,
                        year_procurement:year_procurement,
                        time_procurement:time_procurement
                    }).done(function( test ) {
                        alert( test );
                        if(test=='บันทึกสำเร็จ'){
                            ajax_show_info_procurement();
                            $('#modal_add_step2').modal('hide');
                        }
                    });
                }
            }
        }
    }

    function ajax_add_atc_step3() {

        var select_step3 = $('#select_step3').val();
        var description_step3 = $('#description_step3').val();
        var name_employee_step3 = $('#name_employee_step3').val();
        //var id_procurement_type = "<?php echo $id_procurement_type; ?>";
        var branch = "<?php echo $id_branch; ?>";
        var year_procurement = "<?php echo $year_procurement; ?>";
        var time_procurement = "<?php echo $time_procurement; ?>";

        if(branch==""){
            alert('กรุณาเลือกสาขา');
            $('#branch').focus();
        }else{
            if(year_procurement==""){
                alert('กรุณาระบุปีบัญชี');
                $('#year_procurement').focus();
            }else{
                if(time_procurement==""){
                    alert('กรุณาระบุครั้งที่');
                    $('#time_procurement').focus();
                }else{
                    $.post( "<?=site_url('index.php/public_con/ajax_add_atc_step3') ?>", {
                        branch:branch,
                        select_step3:select_step3,
                        description_step3:description_step3,
                        name_employee_step3:name_employee_step3,
                        year_procurement:year_procurement,
                        time_procurement:time_procurement
                    }).done(function( test ) {
                        alert( test );
                        if(test=='บันทึกสำเร็จ'){
                            ajax_show_info_procurement();
                            $('#modal_add_step3').modal('hide');
                        }
                    });
                }
            }
        }
    }

    function ajax_add_atc_step(step) {
        

        var description_step = $('#description_step'+step).val();
        var name_employee_step = $('#name_employee_step'+step).val();
        var branch = "<?php echo $id_branch; ?>";
        var year_procurement = "<?php echo $year_procurement; ?>";
        var time_procurement = "<?php echo $time_procurement; ?>";

        if(branch==""){
            alert('กรุณาเลือกสาขา');
            $('#branch').focus();
        }else{
            if(year_procurement==""){
                alert('กรุณาระบุปีบัญชี');
                $('#year_procurement').focus();
            }else{
                if(time_procurement==""){
                    alert('กรุณาระบุครั้งที่');
                    $('#time_procurement').focus();
                }else{
                    $.post( "<?=site_url('index.php/public_con/ajax_add_atc_step') ?>", {
                        branch:branch,
                        description_step:description_step,
                        name_employee_step:name_employee_step,
                        year_procurement:year_procurement,
                        time_procurement:time_procurement,
                        step:step
                    }).done(function( test ) {
                        alert( test );
                        if(test=='บันทึกสำเร็จ'){
                            ajax_show_info_procurement();
                            $('#modal_add_step'+step).modal('hide');
                        }
                    });
                }
            }
        }
    }




    function ajax_show_info_procurement(){
        var id_branch = "<?php echo $id_branch; ?>";
        var id_procurement_type = "<?php echo $id_procurement_type; ?>";
        var year_procurement = "<?php echo $year_procurement; ?>";
        var time_procurement = "<?php echo $time_procurement; ?>";


        $.post( "<?=site_url('index.php/public_con/ajax_show_info_procurement') ?>", {
            id_branch: id_branch,
            id_procurement_type:id_procurement_type,
            year_procurement:year_procurement,
            time_procurement:time_procurement
        })
            .done(function (response) {
                var arr = JSON.parse(response);

                for (var key in arr) {
                    var step_procurement = arr[key].step_procurement;
                    var condition_procurement = arr[key].condition_procurement;
                    var name_branch = arr[key].name_branch;
                    var year_procurement = arr[key].year_procurement;
                    var time_procurement = arr[key].time_procurement;
                    var province_branch = arr[key].province_branch;
                    var name_employees = arr[key].name_employees;
                    if(step_procurement==11){
                        var step_now =  'เสร็จสมบูรณ์';
                    }else{
                        var step_now =  parseInt(step_procurement,10)+1;
                    }

                    //alert(step_procurement);
                    $('#msg_head_branch').html('ประเภทงาน :  ซื้อของรางวัล อทช. (วิธีพิเศษ) '+' ปีบัญชี '+year_procurement+' ครั้งที่ '+time_procurement+'      อยู่ขั้นตอนที่ '+step_now);
                    $('#msg_head_step').html(name_branch+' สังกัด'+province_branch);
                    if(step_procurement==1){
                        $( "tr:contains('1.')" ).css("color","#D8D8D8");
                        $('#td1_1_4').html(name_employees);

                        $( "tr:contains('2.')" ).css("color","#909090");
                        $('#tr2_0').show();
                        $('#condition_2_1').css("color","red");
                        $('#condition_2_2').css("color","red");
                        $('#tr2_1').hide();
                        $('#tr2_2').hide();
                        $('#tr2_3').hide();
                        $('#button_2_1').hide();
                        $('#button_2_2').hide();
                        $('#button_2_0').show();

                        $( "tr:contains('3.')" ).css("color","#E8E8E8");
                        $('#button_3_1').hide();
                        $('#button_3_2').hide();
                        $('#button_3_0').hide();
                        $('#tr3_0').show();
                        $('#tr3_1').hide();
                        $('#tr3_2').hide();
                        $('#tr3_3').hide();
                        $('#tr3_4').hide();
                        $('#tr3_5').hide();
                        $('#tr3_6').hide();
                        $('#tr3_7').hide();


                        $( "tr:contains('4.')" ).css("color","#E8E8E8");
                        $('#button_4').hide();
                        $( "tr:contains('5.')" ).css("color","#E8E8E8");
                        $('#button_5').hide();
                        $( "tr:contains('6.')" ).css("color","#E8E8E8");
                        $('#button_6').hide();
                        $( "tr:contains('7.')" ).css("color","#E8E8E8");
                        $('#button_7').hide();
                        $( "tr:contains('8.')" ).css("color","#E8E8E8");
                        $('#button_8').hide();
                        $( "tr:contains('9.')" ).css("color","#E8E8E8");
                        $('#button_9').hide();
                        $( "tr:contains('10.')" ).css("color","#E8E8E8");
                        $('#button_10').hide();
                        $( "tr:contains('11')" ).css("color","#E8E8E8");
                        $('#button_11').hide();
                        $( "tr:contains('12')" ).css("color","#E8E8E8");
                        $('#button_12').hide();
                        $( "tr:contains('13')" ).css("color","#E8E8E8");
                        $('#button_13').hide();
                        $( "tr:contains('14')" ).css("color","#E8E8E8");
                        $('#button_14').hide();

                    }else{
                        if(step_procurement==2){
                            $( "tr:contains('1.')" ).css("color","#D8D8D8");
                            $('#button_1_1').hide();
                            $('#button_1_2').hide();
                            $('#td2_1_4').html(name_employees);
                            $('#td2_2_4').html(name_employees);


                            $( "tr:contains('2.')" ).css("color","#D8D8D8");
                            $('#tr2_0').hide();
                            $('#tr2_1').show();
                            $('#tr2_2').show();
                            $('#tr2_3').show();
                            $('#button_2_1').hide();
                            $('#button_2_2').hide();
                            $('#button_2_0').hide();
                            $('#condition_2_1').css("color","#FFB6C1");
                            $('#condition_2_2').css("color","#FFB6C1");


                            $( "tr:contains('3.')" ).css("color","#909090");
                            $('#button_3_1').hide();
                            $('#button_3_2').hide();
                            $('#button_3_0').show();
                            $('#tr3_0').show();
                            $('#tr3_1').hide();
                            $('#tr3_2').hide();
                            $('#tr3_3').hide();
                            $('#tr3_4').hide();
                            $('#tr3_5').hide();
                            $('#tr3_6').hide();
                            $('#tr3_7').hide();


                            $( "tr:contains('4.')" ).css("color","#E8E8E8");
                            $('#button_4').hide();
                            $( "tr:contains('5.')" ).css("color","#E8E8E8");
                            $('#button_5').hide();
                            $( "tr:contains('6.')" ).css("color","#E8E8E8");
                            $('#button_6').hide();
                            $( "tr:contains('7.')" ).css("color","#E8E8E8");
                            $('#button_7').hide();
                            $( "tr:contains('8.')" ).css("color","#E8E8E8");
                            $('#button_8').hide();
                            $( "tr:contains('9.')" ).css("color","#E8E8E8");
                            $('#button_9').hide();
                            $( "tr:contains('10.')" ).css("color","#E8E8E8");
                            $('#button_10').hide();
                            $( "tr:contains('11')" ).css("color","#E8E8E8");
                            $('#button_11').hide();
                            $( "tr:contains('12')" ).css("color","#E8E8E8");
                            $('#button_12').hide();
                            $( "tr:contains('13')" ).css("color","#E8E8E8");
                            $('#button_13').hide();
                            $( "tr:contains('14')" ).css("color","#E8E8E8");
                            $('#button_14').hide();


                            if(condition_procurement=='กรณีมีการแต่งตั้งคณะกรรมการร่าง TOR'){
                                $('#td2_2_1').hide();
                                $('#td2_2_2').hide();
                                $('#td2_2_3').hide();
                                $('#td2_2_4').hide();
                                $('#td2_1_1').show();
                                $('#td2_1_2').show();
                                $('#td2_1_3').show();
                                $('#td2_1_4').show();
                                $('#td2_1_5').show();
                                $('#td2_1_6').show();
                                $('#td2_1_7').show();
                                $('#td2_1_8').show();
                            }else{
                                $('#td2_1_1').hide();
                                $('#td2_1_2').hide();
                                $('#td2_1_3').hide();
                                $('#td2_1_4').hide();
                                $('#td2_1_5').hide();
                                $('#td2_1_6').hide();
                                $('#td2_1_7').hide();
                                $('#td2_1_8').hide();
                                $('#td2_2_1').show();
                                $('#td2_2_2').show();
                                $('#td2_2_3').show();
                                $('#td2_2_4').show();
                            }
                        }else{
                            if(step_procurement==3){
                                //alert(condition_procurement);
                                $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                $('#button_1_1').hide();
                                $('#button_1_2').hide();
                                $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                $('#tr2_0').hide();
                                $('#tr2_1').show();
                                $('#tr2_2').show();
                                $('#tr2_3').show();
                                $('#button_2_1').hide();
                                $('#button_2_2').hide();
                                $('#button_2_0').hide();
                                $('#condition_2_1').css("color","#FFB6C1");
                                $('#condition_2_2').css("color","#FFB6C1");
                                $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                $('#button_3_0').hide();
                                $('#button_3_1').hide();
                                $('#button_3_2').hide();
                                $('#td3_1_4').html(name_employees);
                                $('#td3_2_4').html(name_employees);

                                $( "tr:contains('4.')" ).css("color","#909090");
                                $('#button_4').show();


                                $( "tr:contains('5.')" ).css("color","#E8E8E8");
                                $('#button_5').hide();
                                $( "tr:contains('6.')" ).css("color","#E8E8E8");
                                $('#button_6').hide();
                                $( "tr:contains('7.')" ).css("color","#E8E8E8");
                                $('#button_7').hide();
                                $( "tr:contains('8.')" ).css("color","#E8E8E8");
                                $('#button_8').hide();
                                $( "tr:contains('9.')" ).css("color","#E8E8E8");
                                $('#button_9').hide();
                                $( "tr:contains('10.')" ).css("color","#E8E8E8");
                                $('#button_10').hide();
                                $( "tr:contains('11')" ).css("color","#E8E8E8");
                                $('#button_11').hide();
                                $( "tr:contains('12')" ).css("color","#E8E8E8");
                                $('#button_12').hide();
                                $( "tr:contains('13')" ).css("color","#E8E8E8");
                                $('#button_13').hide();
                                $( "tr:contains('14')" ).css("color","#E8E8E8");
                                $('#button_14').hide();

                                if(condition_procurement=='กรณีมีการแต่งตั้งคณะกรรมการร่าง TOR'){
                                    $('#tr3_0').hide();
                                    $('#tr3_1').show();
                                    $('#tr3_2').show();
                                    $('#tr3_3').show();
                                    $('#tr3_4').show();
                                    $('#tr3_5').show();
                                    $('#tr3_6').hide();
                                    $('#tr3_7').hide();

                                }else{
                                    $('#tr3_0').hide();
                                    $('#tr3_1').hide();
                                    $('#tr3_2').hide();
                                    $('#tr3_3').hide();
                                    $('#tr3_4').hide();
                                    $('#tr3_5').hide();
                                    $('#tr3_6').show();
                                    $('#tr3_7').show();
                                }

                            }else{
                                if(step_procurement==4){
                                    $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                    $('#button_1_1').hide();
                                    $('#button_1_2').hide();
                                    $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                    $('#tr2_0').hide();
                                    $('#tr2_1').show();
                                    $('#tr2_2').show();
                                    $('#tr2_3').show();
                                    $('#button_2_1').hide();
                                    $('#button_2_2').hide();
                                    $('#button_2_0').hide();
                                    $('#condition_2_1').css("color","#FFB6C1");
                                    $('#condition_2_2').css("color","#FFB6C1");
                                    $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                    $('#button_3_0').hide();
                                    $('#button_3_1').hide();
                                    $('#button_3_2').hide();
                                    $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                    $('#button_4').hide();
                                    $('#td4').html(name_employees);


                                    $( "tr:contains('5.')" ).css("color","#909090");
                                    $('#button_5').show();

                                    $( "tr:contains('6.')" ).css("color","#E8E8E8");
                                    $('#button_6').hide();
                                    $( "tr:contains('7.')" ).css("color","#E8E8E8");
                                    $('#button_7').hide();
                                    $( "tr:contains('8.')" ).css("color","#E8E8E8");
                                    $('#button_8').hide();
                                    $( "tr:contains('9.')" ).css("color","#E8E8E8");
                                    $('#button_9').hide();
                                    $( "tr:contains('10.')" ).css("color","#E8E8E8");
                                    $('#button_10').hide();
                                    $( "tr:contains('11')" ).css("color","#E8E8E8");
                                    $('#button_11').hide();

                                }else{
                                    if(step_procurement==5){
                                        $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                        $('#button_1_1').hide();
                                        $('#button_1_2').hide();
                                        $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                        $('#tr2_0').hide();
                                        $('#tr2_1').show();
                                        $('#tr2_2').show();
                                        $('#tr2_3').show();
                                        $('#button_2_1').hide();
                                        $('#button_2_2').hide();
                                        $('#button_2_0').hide();
                                        $('#condition_2_1').css("color","#FFB6C1");
                                        $('#condition_2_2').css("color","#FFB6C1");
                                        $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                        $('#button_3_0').hide();
                                        $('#button_3_1').hide();
                                        $('#button_3_2').hide();
                                        $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                        $('#button_4').hide();
                                        $( "tr:contains('5.')" ).css("color","#D8D8D8");
                                        $('#button_5').hide();
                                        $('#td5').html(name_employees);

                                        $( "tr:contains('6.')" ).css("color","#909090");
                                        $('#button_6').show();

                                        $( "tr:contains('7.')" ).css("color","#E8E8E8");
                                        $('#button_7').hide();
                                        $( "tr:contains('8.')" ).css("color","#E8E8E8");
                                        $('#button_8').hide();
                                        $( "tr:contains('9.')" ).css("color","#E8E8E8");
                                        $('#button_9').hide();
                                        $( "tr:contains('10.')" ).css("color","#E8E8E8");
                                        $('#button_10').hide();
                                        $( "tr:contains('11')" ).css("color","#E8E8E8");
                                        $('#button_11').hide();

                                    }else{
                                        if(step_procurement==6){
                                            $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                            $('#button_1_1').hide();
                                            $('#button_1_2').hide();
                                            $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                            $('#tr2_0').hide();
                                            $('#tr2_1').show();
                                            $('#tr2_2').show();
                                            $('#tr2_3').show();
                                            $('#button_2_1').hide();
                                            $('#button_2_2').hide();
                                            $('#button_2_0').hide();
                                            $('#condition_2_1').css("color","#FFB6C1");
                                            $('#condition_2_2').css("color","#FFB6C1");
                                            $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                            $('#button_3_0').hide();
                                            $('#button_3_1').hide();
                                            $('#button_3_2').hide();
                                            $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                            $('#button_4').hide();
                                            $( "tr:contains('5.')" ).css("color","#D8D8D8");
                                            $('#button_5').hide();
                                            $( "tr:contains('6.')" ).css("color","#D8D8D8");
                                            $('#button_6').hide();
                                            $('#td6').html(name_employees);

                                            $( "tr:contains('7.')" ).css("color","#909090");
                                            $('#button_7').show();

                                            $( "tr:contains('8.')" ).css("color","#E8E8E8");
                                            $('#button_8').hide();
                                            $( "tr:contains('9.')" ).css("color","#E8E8E8");
                                            $('#button_9').hide();
                                            $( "tr:contains('10.')" ).css("color","#E8E8E8");
                                            $('#button_10').hide();
                                            $( "tr:contains('11')" ).css("color","#E8E8E8");
                                            $('#button_11').hide();

                                        }else{
                                            if(step_procurement==7){
                                                $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                                $('#button_1_1').hide();
                                                $('#button_1_2').hide();
                                                $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                                $('#tr2_0').hide();
                                                $('#tr2_1').show();
                                                $('#tr2_2').show();
                                                $('#tr2_3').show();
                                                $('#button_2_1').hide();
                                                $('#button_2_2').hide();
                                                $('#button_2_0').hide();
                                                $('#condition_2_1').css("color","#FFB6C1");
                                                $('#condition_2_2').css("color","#FFB6C1");
                                                $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                                $('#button_3_0').hide();
                                                $('#button_3_1').hide();
                                                $('#button_3_2').hide();
                                                $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                                $('#button_4').hide();
                                                $( "tr:contains('5.')" ).css("color","#D8D8D8");
                                                $('#button_5').hide();
                                                $( "tr:contains('6.')" ).css("color","#D8D8D8");
                                                $('#button_6').hide();
                                                $( "tr:contains('7.')" ).css("color","#D8D8D8");
                                                $('#button_7').hide();
                                                $('#td7').html(name_employees);

                                                $( "tr:contains('8.')" ).css("color","#909090");
                                                $('#button_8').show();

                                                $( "tr:contains('9.')" ).css("color","#E8E8E8");
                                                $('#button_9').hide();
                                                $( "tr:contains('10.')" ).css("color","#E8E8E8");
                                                $('#button_10').hide();
                                                $( "tr:contains('11')" ).css("color","#E8E8E8");
                                                $('#button_11').hide();

                                            }else{
                                                if(step_procurement==8){
                                                    $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                                    $('#button_1_1').hide();
                                                    $('#button_1_2').hide();
                                                    $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                                    $('#tr2_0').hide();
                                                    $('#tr2_1').show();
                                                    $('#tr2_2').show();
                                                    $('#tr2_3').show();
                                                    $('#button_2_1').hide();
                                                    $('#button_2_2').hide();
                                                    $('#button_2_0').hide();
                                                    $('#condition_2_1').css("color","#FFB6C1");
                                                    $('#condition_2_2').css("color","#FFB6C1");
                                                    $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                                    $('#button_3_0').hide();
                                                    $('#button_3_1').hide();
                                                    $('#button_3_2').hide();
                                                    $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                                    $('#button_4').hide();
                                                    $( "tr:contains('5.')" ).css("color","#D8D8D8");
                                                    $('#button_5').hide();
                                                    $( "tr:contains('6.')" ).css("color","#D8D8D8");
                                                    $('#button_6').hide();
                                                    $( "tr:contains('7.')" ).css("color","#D8D8D8");
                                                    $('#button_7').hide();
                                                    $( "tr:contains('8.')" ).css("color","#D8D8D8");
                                                    $('#button_8').hide();
                                                    $('#td8').html(name_employees);

                                                    $( "tr:contains('9.')" ).css("color","#909090");
                                                    $('#button_9').show();

                                                    $( "tr:contains('10.')" ).css("color","#E8E8E8");
                                                    $('#button_10').hide();
                                                    $( "tr:contains('11')" ).css("color","#E8E8E8");
                                                    $('#button_11').hide();

                                                }else{
                                                    if(step_procurement==9){
                                                        $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                                        $('#button_1_1').hide();
                                                        $('#button_1_2').hide();
                                                        $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                                        $('#tr2_0').hide();
                                                        $('#tr2_1').show();
                                                        $('#tr2_2').show();
                                                        $('#tr2_3').show();
                                                        $('#button_2_1').hide();
                                                        $('#button_2_2').hide();
                                                        $('#button_2_0').hide();
                                                        $('#condition_2_1').css("color","#FFB6C1");
                                                        $('#condition_2_2').css("color","#FFB6C1");
                                                        $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                                        $('#button_3_0').hide();
                                                        $('#button_3_1').hide();
                                                        $('#button_3_2').hide();
                                                        $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                                        $('#button_4').hide();
                                                        $( "tr:contains('5.')" ).css("color","#D8D8D8");
                                                        $('#button_5').hide();
                                                        $( "tr:contains('6.')" ).css("color","#D8D8D8");
                                                        $('#button_6').hide();
                                                        $( "tr:contains('7.')" ).css("color","#D8D8D8");
                                                        $('#button_7').hide();
                                                        $( "tr:contains('8.')" ).css("color","#D8D8D8");
                                                        $('#button_8').hide();
                                                        $( "tr:contains('9.')" ).css("color","#D8D8D8");
                                                        $('#button_9').hide();
                                                        $('#td9').html(name_employees);

                                                        $( "tr:contains('10.')" ).css("color","#909090");
                                                        $('#button_10').show();

                                                        $( "tr:contains('11')" ).css("color","#E8E8E8");
                                                        $('#button_11').hide();

                                                    }else{
                                                        if(step_procurement==10){
                                                            $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                                            $('#button_1_1').hide();
                                                            $('#button_1_2').hide();
                                                            $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                                            $('#tr2_0').hide();
                                                            $('#tr2_1').show();
                                                            $('#tr2_2').show();
                                                            $('#tr2_3').show();
                                                            $('#button_2_1').hide();
                                                            $('#button_2_2').hide();
                                                            $('#button_2_0').hide();
                                                            $('#condition_2_1').css("color","#FFB6C1");
                                                            $('#condition_2_2').css("color","#FFB6C1");
                                                            $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                                            $('#button_3_0').hide();
                                                            $('#button_3_1').hide();
                                                            $('#button_3_2').hide();
                                                            $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                                            $('#button_4').hide();
                                                            $( "tr:contains('5.')" ).css("color","#D8D8D8");
                                                            $('#button_5').hide();
                                                            $( "tr:contains('6.')" ).css("color","#D8D8D8");
                                                            $('#button_6').hide();
                                                            $( "tr:contains('7.')" ).css("color","#D8D8D8");
                                                            $('#button_7').hide();
                                                            $( "tr:contains('8.')" ).css("color","#D8D8D8");
                                                            $('#button_8').hide();
                                                            $( "tr:contains('9.')" ).css("color","#D8D8D8");
                                                            $('#button_9').hide();
                                                            $( "tr:contains('10.')" ).css("color","#D8D8D8");
                                                            $('#button_10').hide();
                                                            $('#td10').html(name_employees);

                                                            $( "tr:contains('11')" ).css("color","#909090");
                                                            $('#button_11').show();


                                                        }else{
                                                            if(step_procurement==11){
                                                                $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                                                $('#button_1_1').hide();
                                                                $('#button_1_2').hide();
                                                                $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                                                $('#tr2_0').hide();
                                                                $('#tr2_1').show();
                                                                $('#tr2_2').show();
                                                                $('#tr2_3').show();
                                                                $('#button_2_1').hide();
                                                                $('#button_2_2').hide();
                                                                $('#button_2_0').hide();
                                                                $('#condition_2_1').css("color","#FFB6C1");
                                                                $('#condition_2_2').css("color","#FFB6C1");
                                                                $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                                                $('#button_3_0').hide();
                                                                $('#button_3_1').hide();
                                                                $('#button_3_2').hide();
                                                                $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                                                $('#button_4').hide();
                                                                $( "tr:contains('5.')" ).css("color","#D8D8D8");
                                                                $('#button_5').hide();
                                                                $( "tr:contains('6.')" ).css("color","#D8D8D8");
                                                                $('#button_6').hide();
                                                                $( "tr:contains('7.')" ).css("color","#D8D8D8");
                                                                $('#button_7').hide();
                                                                $( "tr:contains('8.')" ).css("color","#D8D8D8");
                                                                $('#button_8').hide();
                                                                $( "tr:contains('9.')" ).css("color","#D8D8D8");
                                                                $('#button_9').hide();
                                                                $( "tr:contains('10.')" ).css("color","#D8D8D8");
                                                                $('#button_10').hide();
                                                                $( "tr:contains('11')" ).css("color","#D8D8D8");
                                                                $('#button_11').hide();
                                                                $('#td11').html(name_employees);


                                                                $('#tr15').show().css("color","red");

                                                            }else{
                                                                $( "tr:contains('1.')" ).css("color","#D8D8D8");
                                                                $('#button_1_1').hide();
                                                                $('#button_1_2').hide();
                                                                $( "tr:contains('2.')" ).css("color","#D8D8D8");
                                                                $('#tr2_0').hide();
                                                                $('#tr2_1').show();
                                                                $('#tr2_2').show();
                                                                $('#tr2_3').show();
                                                                $('#button_2_1').hide();
                                                                $('#button_2_2').hide();
                                                                $('#button_2_0').hide();
                                                                $('#condition_2_1').css("color","#FFB6C1");
                                                                $('#condition_2_2').css("color","#FFB6C1");
                                                                $( "tr:contains('3.')" ).css("color","#D8D8D8");
                                                                $('#button_3_0').hide();
                                                                $('#button_3_1').hide();
                                                                $('#button_3_2').hide();
                                                                $( "tr:contains('4.')" ).css("color","#D8D8D8");
                                                                $('#button_4').hide();
                                                                $( "tr:contains('5.')" ).css("color","#D8D8D8");
                                                                $('#button_5').hide();
                                                                $( "tr:contains('6.')" ).css("color","#D8D8D8");
                                                                $('#button_6').hide();
                                                                $( "tr:contains('7.')" ).css("color","#D8D8D8");
                                                                $('#button_7').hide();
                                                                $( "tr:contains('8.')" ).css("color","#D8D8D8");
                                                                $('#button_8').hide();
                                                                $( "tr:contains('9.')" ).css("color","#D8D8D8");
                                                                $('#button_9').hide();
                                                                $( "tr:contains('10.')" ).css("color","#D8D8D8");
                                                                $('#button_10').hide();
                                                                $( "tr:contains('11')" ).css("color","#D8D8D8");
                                                                $('#button_11').hide();



                                                                $('#tr15').hide()

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
    }


</script>

<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>












<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-gift fa-2x text-gray-300"></i> ซื้อของรางวัล อทช. (วิธีพิเศษ)</h1>
    <p class="mb-4">ขั้นตอนการใช้แบบฟอร์ม ของการซื้อของรางวัล อทช. มีทั้งหมด 11 ขั้นตอน</p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><p id="msg_head_branch">ประเภทงาน :  ซื้อของรางวัล อทช. (วิธีพิเศษ)</p>
                <div id="msg_head_step"></div>
            </h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td>
                            <div  class="col-12">
                                <table class="table table-borderless" width='100%' id="table_atc">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>ขั้นตอนวิธีปฏิบัติงาน</th>
                                            <th>เอกสารแบบพิมพ์</th>
                                            <th>ตัวอย่าง</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row" id="td1_1_1">1.จัดทำแผนการจัดซื้อจัดจ้างประจำปี </th>
                                        <td id="td1_1_2">ได้รับการยกเว้นไม่ต้องทำแผนการจัดซื้อจัดจ้าง</td>
                                        <td id="td1_1_3"></td>
                                        <td id="td1_1_4"></td>
                                    </tr>
                                    <tr id="tr2_1">
                                        <th scope="row" id="td2_1_1">2.จัดทำร่างขอบเขตของงาน TOR <div  id="condition_2_1">(กรณีมีการแต่งตั้งคณะกรรมการร่าง TOR)</div></th>
                                        <td id="td2_1_2">2.1 คำสั่งแต่งตั้งคณะกรรมการ  </td>
                                        <td id="td2_1_3">ตัวอย่าง</td>
                                        <td id="td2_1_4"><button id="button_2_1" name="button_2_1" onclick="show_modal_add_step(2)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr2_2">
                                        <th scope="row" id="td2_1_5"></th>
                                        <td id="td2_1_6">2.2 ร่าง TOR/คุณลักษณะเฉพาะของพัสดุ</td>
                                        <td id="td2_1_7">ตัวอย่าง</td>
                                        <td id="td2_1_8"></td>
                                    </tr>
                                    <tr id="tr2_3">
                                        <th scope="row" id="td2_2_1">2.จัดทำร่างขอบเขตของงาน TOR <div id="condition_2_2">(กรณีไม่มีการแต่งตั้งคณะกรรมการร่าง TOR)</div></th>
                                        <td id="td2_2_2">2.1 ร่าง TOR /คุณลักษณะเฉพาะของพัสดุ</td>
                                        <td id="td2_2_3">ตัวอย่าง</td>
                                        <td id="td2_2_4"><button id="button_2_2" name="button_2_2" onclick="show_modal_add_step(2)" class="btn btn-primary">Download</button></td>
                                    </tr >
                                    <tr id="tr2_0">
                                        <th scope="row" id="td2_0_1">2.จัดทำร่างขอบเขตของงาน TOR </th>
                                        <td id="td2_0_2"></td>
                                        <td id="td2_0_3">ตัวอย่าง</td>
                                        <td id="td2_0_4"><button id="button_2_0" name="button_2_0" onclick="show_modal_add_step(2)" class="btn btn-primary">Download</button></td>
                                    </tr >
                                    <tr id="tr3_1">
                                        <th scope="row" id="td3_1_1">3.จัดทำราคากลาง <div  id="condition_3_1">(กรณีมีการแต่งตั้งคณะกรรมการร่าง TOR)</div></th>
                                        <td id="td3_1_2">3.1 คำสั่งแต่งตั้งคณะกรรมการ</td>
                                        <td id="td3_1_3">ตัวอย่าง</td>
                                        <td id="td3_1_4"><button id="button_3_1" name="button_3_1" onclick="show_modal_add_step(3)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr3_2">
                                        <th scope="row" id="td3_1_5"></th>
                                        <td id="td3_1_6">3.2 รายงานการประชุม</td>
                                        <td id="td3_1_7">ตัวอย่าง</td>
                                        <td id="td3_1_8"></td>
                                    </tr>
                                    <tr id="tr3_4">
                                        <th scope="row" id="td3_1_9"></th>
                                        <td id="td3_1_10">3.3 บันทึกสรุปผล</td>
                                        <td id="td3_1_11">ตัวอย่าง</td>
                                        <td id="td3_1_12"></td>
                                    </tr>
                                    <tr id="tr3_5" >
                                        <th scope="row" id="td3_1_13"></th>
                                        <td id="td3_1_14">3.4 ตารางแสดงวงเงินงบประมาณที่ได้รับจัดสรรและรายละเอียดค่าใช้จ่ายการจัดซื้อจัดจ้างที่มิใช่งานก่อสร้าง (บก.06)</td>
                                        <td id="td3_1_15">ตัวอย่าง</td>
                                        <td id="td3_1_16"></td>
                                    </tr>
                                    <tr id="tr3_6">
                                        <th scope="row" id="td3_2_1">3.จัดทำราคากลาง <div  id="condition_3_2">(กรณีไม่มีการแต่งตั้งคณะกรรมการร่าง TOR)</div></th>
                                        <td id="td3_2_2">3.1 บันทึกสรุปผล</td>
                                        <td id="td3_2_3">ตัวอย่าง</td>
                                        <td id="td3_2_4"><button id="button_3_2" name="button_3_2" onclick="show_modal_add_step(3)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr3_7">
                                        <th scope="row" id="td3_2_5"></th>
                                        <td id="td3_2_6">3.2 ตารางแสดงวงเงินงบประมาณที่ได้รับจัดสรรและรายละเอียดค่าใช้จ่ายการจัดซื้อจัดจ้างที่มิใช่งานก่อสร้าง (บก.06)</td>
                                        <td id="td3_2_7">ตัวอย่าง</td>
                                        <td id="td3_2_8"></td>
                                    </tr>
                                    <tr id="tr3_0">
                                        <th scope="row" id="td3_0_1">3.จัดทำราคากลาง </th>
                                        <td id="td3_0_2"></td>
                                        <td id="td3_0_3"></td>
                                        <td id="td3_0_4"><button id="button_3_0" name="button_3_0" onclick="show_modal_add_step(3)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr  id="tr4_1">
                                        <th scope="row">4.จัดทำรายงานขอซื้อ</th>
                                        <td>4.1 รายงานขอซื้อ</td>
                                        <td><a target="_blank" href="#" ">ตัวอย่าง</a></td>
                                        <td id="td4"><button id="button_4" name="button_4" onclick="show_modal_add_step(4)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr4_2">
                                        <th scope="row"></th>
                                        <td>4.2 คำสั่งแต่งตั้งคณะกรรมการซื้อโดยวิธีพิเศษ และคณะกรรมการตรวจรับพัสดุ</td>
                                        <td>ตัวอย่าง</td>
                                        <td></td>
                                    </tr>
                                    <tr id="tr5">
                                        <th scope="row">5.จัดทำหนังสือเชิญชวน</th>
                                        <td>หนังสือเชิญชวน</td>
                                        <td>ตัวอย่าง</td>
                                        <td id="td5"><button id="button_5" name="button_5" onclick="show_modal_add_step(5)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr6">
                                        <th scope="row">6.ต่อรองราคา</th>
                                        <td>บันทึกข้อตกลงและต่อรองราคา</td>
                                        <td>ตัวอย่าง</td>
                                        <td id="td6"><button id="button_6" name="button_6" onclick="show_modal_add_step(6)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr7">
                                        <th scope="row">7.ขออนุมัติซื้อ</th>
                                        <td>บันทึกรายงานผลการดำเนินการ (ขออนุมัติซื้อ)</td>
                                        <td>ตัวอย่าง</td>
                                        <td id="td7"><button id="button_7" name="button_7" onclick="show_modal_add_step(7)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr8">
                                        <th scope="row">8.ประกาศผลผู้ชนะ</th>
                                        <td>ประกาศผลผู้ชนะการเสนอราคา</td>
                                        <td>ตัวอย่าง</td>
                                        <td id="td8"><button id="button_8" name="button_8" onclick="show_modal_add_step(8)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr9">
                                        <th scope="row">9.สนองรับราคา</th>
                                        <td>หนังสือสนองรับราคา</td>
                                        <td>ตัวอย่าง</td>
                                        <td id="td9"><button id="button_9" name="button_9" onclick="show_modal_add_step(9)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr10">
                                        <th scope="row">10.ลงนามใบสั่งซื้อ</th>
                                        <td>ใบสั่งซื้อ</td>
                                        <td>ตัวอย่าง</td>
                                        <td id="td10"><button id="button_10" name="button_10" onclick="show_modal_add_step(10)" class="btn btn-primary">Download</button></td>
                                    </tr>
                                    <tr id="tr11">
                                        <th scope="row">11 บริหารสัญญา</th>
                                        <td>บันทึกตรวจรับพัสดุ</td>
                                        <td>ตัวอย่าง</td>
                                        <td id="td11"><button id="button_11" name="button_11" onclick="show_modal_add_step(11)" class="btn btn-primary">Download</button></td>
                                    </tr>

                                    <tr id="tr15" style="display: none">
                                        <th scope="row" colspan="4" style="text-align:center " >เสร็จสมบูรณ์</th>

                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>































    <!-- start modal add step form ####################################################################################-->

    <div id="modal_add_step1" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <h4 class="modal-title">ขั้นตอนที่ 1 จัดทำแผนการจัดซื้อจัดจ้างประจำปี</h4>
                </div>
                <div class="modal-body" id="myModalBody">
                    
                        <div class="form-group">
                            <div class="table-responsive">
                                <table width="100%" cellpadding="5" >

                                    <tr>
                                        <th scope="row" id="td1_1_1">1.จัดทำแผนการจัดซื้อจัดจ้างประจำปี </th>
                                        <td id="td1_1_2">ได้รับการยกเว้นไม่ต้องทำแผนการจัดซื้อจัดจ้าง</td>
                                    </tr>
                                    <tr>

                                        <td align="right" id="td_step1_4" colspan="2">
                                            <button type="submit" class="btn btn-primary" id="add_step1" name="add_step1" onclick="ajax_add_atc_step1()">ขั้นตอนถัดไป</button>
                                            <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal sub form ####################################################################################-->

<!-- start modal add step form ####################################################################################-->

<div id="modal_add_step2" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 2 จัดทำร่างขอบเขตของงาน TOR</h4>
            </div>
            <div class="modal-body" id="myModalBody">

                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td></td>
                                    <td>
                                        <label for="select_step2">การแต่งตั้งคณะกรรมการร่าง TOR</label>
                                        <select  id="select_step2" name="select_step2" class="form-control">
                                            <option value="กรณีมีการแต่งตั้งคณะกรรมการร่าง TOR">กรณีมีการแต่งตั้งคณะกรรมการร่าง TOR</option>
                                            <option value="กรณีไม่มีการแต่งตั้งคณะกรรมการร่าง TOR">กรณีไม่มีการแต่งตั้งคณะกรรมการร่าง TOR</option>
                                        </select>
                                    </td>
                                </tr>
                                <script>
                                    $(document).ready(function (e) {

                                        $("#select_step2").change(function () {
                                            var select_step2 = $("#select_step2").val();
                                            if(select_step2=="กรณีไม่มีการแต่งตั้งคณะกรรมการร่าง TOR"){
                                                $('#td_step2_1').hide();
                                                $('#td_step2_2').show();
                                            }else {
                                                $('#td_step2_1').show();
                                                $('#td_step2_2').hide();
                                            }
                                        });
                                    });
                                </script>
                                <tr>
                                    <td  colspan="2" id="td_step2_1" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/car/2_2_1.doc'); ?>">1. คำสั่งแต่งตั้งคณะกรรมการ  </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/car/2_2_1.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/car/2_2_2.doc'); ?>">2. ร่าง TOR/คุณลักษณะเฉพาะของพัสดุ</a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/car/2_2_2.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td  colspan="2" id="td_step2_2" style="display: none">
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/car/2_2_2.doc'); ?>"> ร่าง TOR /คุณลักษณะเฉพาะของพัสดุ</a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/car/2_2_2.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step2">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step2" name="description_step2" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step2">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step2" name="name_employee_step2" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step2" name="add_step2" onclick="ajax_add_atc_step2()">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>

                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->


<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step3" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 3 จัดทำราคากลาง</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td ></td>
                                    <td>
                                        <label for="select_step3">การแต่งตั้งคณะกรรมการร่าง TOR</label>
                                        <select  id="select_step3" name="select_step3" class="form-control">
                                            <option value="กรณีมีการแต่งตั้งคณะกรรมการร่าง TOR">กรณีมีการแต่งตั้งคณะกรรมการร่าง TOR</option>
                                            <option value="กรณีไม่มีการแต่งตั้งคณะกรรมการร่าง TOR">กรณีไม่มีการแต่งตั้งคณะกรรมการร่าง TOR</option>
                                        </select>
                                    </td>
                                </tr>
                                <script>
                                    $(document).ready(function (e) {

                                        $("#select_step3").change(function () {
                                            var select_step3 = $("#select_step3").val();
                                            if(select_step3=="กรณีไม่มีการแต่งตั้งคณะกรรมการร่าง TOR"){
                                                $('#td_step3_1_1').hide();
                                                $('#td_step3_2_1').show();

                                            }else {
                                                $('#td_step3_1_1').show();
                                                $('#td_step3_2_1').hide();

                                            }
                                        });
                                    });
                                </script>
                                <tr>
                                    <td id="td_step3_1_1" colspan="2">
                                        <table width="100%" class="table-bordered mb-12">
                                            <thead class="thead-light">
                                            <tr >
                                                <th  width="50%"  scope="col">แบบฟอร์ม </th>
                                                <th  width="50%"  scope="col">ตัวอย่าง</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/form/atc/3_3_1.doc'); ?>">1. คำสั่งแต่งตั้งคณะกรรมการ </a></td>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/ex/atc/3_3_1.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                            <tr>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/form/atc/3_3_2.doc'); ?>">2. รายงานการประชุม </a></td>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/ex/atc/3_3_2.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                            <tr>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/form/atc/3_3_3.doc'); ?>">3. บันทึกสรุปผล</a></td>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/ex/atc/3_3_3.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                            <tr>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/form/atc/3_3_4.doc'); ?>">4. ตารางแสดงวงเงินงบประมาณที่ได้รับจัดสรรและรายละเอียดค่าใช้จ่ายการจัดซื้อจัดจ้างที่มิใช่งานก่อสร้าง (บก.06)</a></td>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/ex/atc/3_3_4.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="td_step3_2_1" style="display: none" colspan="2">
                                        <table width="100%" class="table-bordered mb-12">
                                            <thead class="thead-light">
                                            <tr >
                                                <th  width="50%"  scope="col">แบบฟอร์ม </th>
                                                <th  width="50%"  scope="col">ตัวอย่าง</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/form/atc/3_3_3.doc'); ?>">1. บันทึกสรุปผล</a></td>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/ex/atc/3_3_3.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                            <tr>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/form/atc/3_3_4.doc'); ?>">2. ตารางแสดงวงเงินงบประมาณที่ได้รับจัดสรรและรายละเอียดค่าใช้จ่ายการจัดซื้อจัดจ้างที่มิใช่งานก่อสร้าง (บก.06)</a></td>
                                                <td ><a target="_blank" href="<?php echo base_url('doc/ex/atc/3_3_4.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step3">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step3" name="description_step3" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step3">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step3" name="name_employee_step3" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step3" name="add_step3" onclick="ajax_add_atc_step3()">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->


<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step4" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 4 จัดทำรายงานขอเช่า</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td  colspan="2" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/4_4_1.doc'); ?>">1. รายงานขอซื้อ </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/4_4_1.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/4_4_2.doc'); ?>">2. คำสั่งแต่งตั้งคณะกรรมการซื้อโดยวิธีพิเศษ และคณะกรรมการตรวจรับพัสดุ </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/4_4_2.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step4">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step4" name="description_step4" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step4">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step4" name="name_employee_step4" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step4" name="add_step4" onclick="ajax_add_atc_step(4)">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->


<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step5" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 5 จัดทำหนังสือเชิญชวน</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td  colspan="2" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/5.doc'); ?>"> หนังสือเชิญชวน </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/5.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step5">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step5" name="description_step5" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step5">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step5" name="name_employee_step5" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step5" name="add_step5" onclick="ajax_add_atc_step(5)">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->

<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step6" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 6 ต่อรองราคา</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td  colspan="2" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/6.doc'); ?>"> บันทึกข้อตกลงและต่อรองราคา</a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/6.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>


                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step6">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step6" name="description_step6" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step6">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step6" name="name_employee_step6" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step6" name="add_step6" onclick="ajax_add_atc_step(6)">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->

<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step7" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 7 ขออนุมัติซื้อ</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td  colspan="2" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/7.doc'); ?>"> บันทึกรายงานผลการดำเนินการ (ขออนุมัติซื้อ) </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/7.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>


                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step7">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step7" name="description_step7" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step7">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step7" name="name_employee_step7" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step7" name="add_step7" onclick="ajax_add_atc_step(7)">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->

<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step8" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 8 ประกาศผลผู้ชนะ</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td  colspan="2" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/8.doc'); ?>"> ประกาศผลผู้ชนะการเสนอราคา </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/8.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>


                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step8">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step8" name="description_step8" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step8">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step8" name="name_employee_step8" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step8" name="add_step8" onclick="ajax_add_atc_step(8)">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->


<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step9" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 9 สนองรับราคา</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td  colspan="2" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/9.doc'); ?>"> หนังสือสนองรับราคา (ขออนุมัติเช่า) </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/9.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>


                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step9">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step9" name="description_step9" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step9">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step9" name="name_employee_step9" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step9" name="add_step9" onclick="ajax_add_atc_step(9)">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->



<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step10" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 10 ลงนามใบสั่งซื้อ</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td  colspan="2" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/10.doc'); ?>"> ใบสั่งซื้อ </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/10.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step10">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step10" name="description_step10" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step10">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step10" name="name_employee_step10" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step10" name="add_step10" onclick="ajax_add_atc_step(10)">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->


<!-- start modal add step form ####################################################################################-->
<div id="modal_add_step11" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">ขั้นตอนที่ 11 บริหารสัญญา</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                
                    <div class="form-group">
                        <div class="table-responsive">
                            <table width="100%" cellpadding="5" >
                                <tr>
                                    <td  colspan="2" >
                                        <table width="100%" class="table-bordered mb-12">
                                            <tr >
                                                <th  width="50%" align="center">แบบฟอร์ม </th>
                                                <th  width="50%" align="center">ตัวอย่าง</th>
                                            </tr>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo base_url('doc/form/atc/11.doc'); ?>"> บันทึกตรวจรับพัสดุ </a></td>
                                                <td><a target="_blank" href="<?php echo base_url('doc/ex/atc/11.pdf'); ?>"> เอกสารตัวอย่าง </a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" >
                                        <label for="description_step11">รายละเอียด</label>
                                        <textarea class="form-control" rows="5" id="description_step11" name="description_step11" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td i>
                                        <label for="name_employee_step11">ชื่อพนักงานผู้จัดทำ</label>
                                        <input type="text" id="name_employee_step11" name="name_employee_step11" class="form-control" >
                                    </td>
                                    <td align="right" >
                                        <button type="submit" class="btn btn-primary" id="add_step11" name="add_step11" onclick="ajax_add_atc_step(11)">ขั้นตอนถัดไป</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end of modal sub form ####################################################################################-->
