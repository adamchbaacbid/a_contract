


<script>
    function ajax_add_car_rental_step1() {
        var branch = $('#input_branch').val();
        var select_step1 = $('#select_step1').val();
        var description_step1 = $('#description_step1').val();
        var name_employee_step1 = $('#name_employee_step1').val();
        var year_procurement = $('#year_procurement').val();
        var time_procurement = $('#time_procurement').val();



        if(branch==""){
            alert('กรุณาเลือกสาขา');
            $('#branch').focus();
        }else{

            $.post( "<?=site_url('index.php/public_con/check_name_branch') ?>", {
                branch:branch
            }).done(function( test ) {
                if(test>0){
                    if(year_procurement==""){
                        alert('กรุณาระบุปีบัญชี');
                        $('#year_procurement').focus();
                    }else{
                        if(time_procurement==""){
                            alert('กรุณาระบุครั้งที่');
                            $('#time_procurement').focus();
                        }else{
                            $.post( "<?=site_url('index.php/public_con/ajax_add_car_rental_step1') ?>", {
                                branch:branch,
                                select_step1:select_step1,
                                description_step1:description_step1,
                                name_employee_step1:name_employee_step1,
                                year_procurement:year_procurement,
                                time_procurement:time_procurement
                            }).done(function( test ) {
                                alert( test );
                                if(test=='บันทึกสำเร็จ'){
                                    info_procurement();
                                }
                            });
                        }
                    }
                }else{
                    alert('ไม่พบชื่อสาขาในระบบ กรุณาเลือกสาขาอีกครั้ง');
                    $('#input_branch').focus();
                }
            });
        }
    }


    function info_procurement(){
        // alert(id_branch);
        $('#form_info_procurement').submit();
    }








</script>

<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>

<script>
    $(document).ready(function (e) {
        $("#input_branch").change(function () {
            var branch = $("#input_branch").val();
            var time_procurement = $("#time_procurement2").val();
            var year_procurement = $("#year_procurement2").val();
            $('#id_branch').val(branch)


            if(branch==""){

                $("#input_branch").focus();
                $('#step1').hide();
            }else {
                $("#time_procurement2").focus();
                if(time_procurement==''){
                    $("#time_procurement2").focus();
                    $('#step1').hide();
                }else{
                    $("#year_procurement2").focus();
                    if(year_procurement==''){
                        $("#year_procurement2").focus();
                        $('#step1').hide();
                    }else{
                        $('#step1').show();
                    }
                }
            }
        });
    });

    $(document).ready(function (e) {
        $("#time_procurement2").change(function () {
            var branch = $("#input_branch").val();
            var time_procurement = $("#time_procurement2").val();
            var year_procurement = $("#year_procurement2").val();
            $('#time_procurement').val(time_procurement);

            if(branch==""){

                $("#input_branch").focus();
                $('#step1').hide();
            }else {
                $("#time_procurement2").focus();
                if(time_procurement==''){
                    $("#time_procurement2").focus();
                    $('#step1').hide();
                }else{
                    $("#year_procurement2").focus();
                    if(year_procurement==''){
                        $("#year_procurement2").focus();
                        $('#step1').hide();
                    }else{
                        $('#step1').show();
                    }
                }
            }
        });
    });

    $(document).ready(function (e) {
        $("#year_procurement2").change(function () {
            var branch = $("#input_branch").val();
            var time_procurement = $("#time_procurement2").val();
            var year_procurement = $("#year_procurement2").val();
            $('#year_procurement').val(year_procurement);

            if(branch==""){

                $("#input_branch").focus();
                $('#step1').hide();
            }else {
                $("#time_procurement2").focus();
                if(time_procurement==''){
                    $("#time_procurement2").focus();
                    $('#step1').hide();
                }else{
                    $("#year_procurement2").focus();
                    if(year_procurement==''){
                        $("#year_procurement2").focus();
                        $('#step1').hide();
                    }else{

                        $('#step1').show();
                    }
                }
            }
        });
    });




    $(document).ready(function (e) {
        $('#input_branch').focus(function() {

            $.post("<?=site_url('index.php/public_con/show_name_branch') ?>", {
            })
                .done(function (response) {
                    //alert(response);

                    var arr = JSON.parse(response);
                    //alert(arr);

                    $.each(arr, function(id,name) {

                        var option = $('<option value="'+name+'"></option>');
                        $('#branch_list').append(option);


                    });
                });

        });
    });

    $(document).ready(function (e) {
        $('#input_branch').focusout(function() {
            $("#branch_list").empty();
        });
    });

</script>









<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-car fa-2x text-gray-300"></i> เช่ารถยนต์ (วิธีคัดเลือก)</h1>
    <p class="mb-4">ขั้นตอนการใช้แบบฟอร์ม ของการเช่ารถยนต์ มีทั้งหมด 14 ขั้นตอน</p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เลือกสาขาของท่าน</h6>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td>
                            <label for="input_branch">เลือกสาขา</label>
                            <div class="input-group mb-3">
                                <input list="branch_list" id="input_branch" name="input_branch" placeholder="" class="form-control">
                                <datalist id="branch_list">
                                </datalist>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="year_procurement2">ปีบัญชี</label>
                            <select  id="year_procurement2" name="year_procurement2" class="form-control">
                                <option value="">เลือกปีบัญชี</option>
                                <option value="2563">2563</option>
                                <option value="2564">2564</option>
                                <option value="2565">2565</option>
                                <option value="2566">2566</option>
                            </select>
                        </td>
                        <td>
                            <label for="time_procurement2">ครั้งที่</label>
                            <select  id="time_procurement2" name="time_procurement2" class="form-control">
                                <option value="">เลือกครั้งที่</option>
                                <option value="1">ครั้งที่ 1</option>
                                <option value="2">ครั้งที่ 2</option>
                                <option value="3">ครั้งที่ 3</option>
                                <option value="4">ครั้งที่ 4</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="step1" style="display: none">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ขั้นตอนที่ 1 จัดทำแผนการจัดซื้อจัดจ้างประจำปี</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td>
                            <label for="select_step1">เลือกงบประมาณ</label>
                            <select  id="select_step1" name="select_step1" class="form-control">
                                <option value="งบประมาณเกิน 500,000 บาท">งบประมาณเกิน 500,000 บาท</option>
                                <option value="งบประมาณไม่เกิน 500,000 บาท">งบประมาณไม่เกิน 500,000 บาท</option>
                            </select>
                        </td>
                    </tr>

                    <script>
                        $(document).ready(function (e) {

                            $("#select_step1").change(function () {
                                var select_step1 = $("#select_step1").val();
                                if(select_step1=="งบประมาณไม่เกิน 500,000 บาท"){
                                    $('#td_step1_1').hide();
                                    $('#td_step1_2').hide();
                                    $('#td_step1_3').hide();
                                    $('#td_step1_4').hide();
                                    $('#td_step1_msg').show();
                                    $('#td_step1_msg1').show();

                                }else {
                                    $('#td_step1_1').show();
                                    $('#td_step1_2').show();
                                    $('#td_step1_3').show();
                                    $('#td_step1_4').show();
                                    $('#td_step1_msg').hide();
                                    $('#td_step1_msg1').hide();
                                }
                            });
                        });
                    </script>


                    <tr>
                        <td  colspan="2" id="td_step1_1">
                                <table width="100%" class="table-bordered mb-12">
                                    <tr >
                                        <th  width="50%" align="center">แบบฟอร์ม </th>
                                        <th  width="50%" align="center">ตัวอย่าง</th>
                                    </tr>
                                    <tr>
                                        <td><a target="_blank" href="<?php echo base_url('doc/ex/car/1.pdf'); ?>"> ให้ปฏิบัติตามหนังสือซักซ้อม สจพ. </a></td>
                                        <td><a target="_blank" href="<?php echo base_url('doc/ex/car/1.pdf'); ?>"> เอกสารตัวอย่าง </a></td>

                                    </tr>
                                </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="td_step1_2">
                            <label for="description_step1">รายละเอียด</label>
                            <textarea class="form-control" rows="5" id="description_step1" name="description_step1"  ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td id="td_step1_3">
                            <label for="name_employee_step1">ชื่อพนักงานผู้บันทึก</label>
                            <input type="text" id="name_employee_step1" name="name_employee_step1" class="form-control" >
                        </td>
                        <td align="right" id="td_step1_4">
                            <button type="submit" class="btn btn-primary" id="add_step1" name="add_step1" onclick="ajax_add_car_rental_step1()">ขั้นตอนถัดไป</button>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="td_step1_msg" style="display: none">
                            <div style="color:red" class="alert alert-warning" role="alert" align="center " style="color: red"> ไม่ต้องจัดทำแผนการจัดซื้อจัดจ้าง</div>
                        </td>

                    </tr>
                    <tr>
                        <td align="right" id="td_step1_msg1" style="display: none">
                            <button type="submit" class="btn btn-primary" id="add_step1" name="add_step1" onclick="ajax_add_car_rental_step1()">ขั้นตอนถัดไป</button>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  style="display: none">
                            <form id='form_info_procurement' action='<?php echo base_url('index.php/public_con/info_procurement1'); ?>' target='_blank' method='post' >
                                <input id='id_branch' name='id_branch' value='' type='hidden' >
                                <input id='id_procurement_type' name='id_procurement_type' value='1' type='hidden' >
                                <input id='year_procurement' name='year_procurement' value='' type='hidden' >
                                <input id='time_procurement' name='time_procurement' value='' type='hidden' >

                            </form>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>











