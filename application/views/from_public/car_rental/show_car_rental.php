<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script>

    window.onload = function(){
        num_branch_car_rental();
    }

    function num_branch_car_rental(){
        $.post( "<?=site_url('index.php/public_con/num_branch_car_rental') ?>", {
        }) .done(function( data ) {
            $('#msg_num_branch').html(' ทำไปแล้วจำนวน '+data+' จังหวัด');

            });
    }

    function edit_bid(no_bid){
        $('#form_edit_bid'+no_bid).submit();
    }

    function info_procurement(id_branch,id_procurement_type,year_procurement,time_procurement){
       // alert(id_branch);
        $('#form_info_procurement'+id_branch+id_procurement_type+year_procurement+time_procurement).submit();
    }


    $(document).ready(function (e) {
        $('#input_branch').focus(function() {


            $.post("<?=site_url('index.php/public_con/show_name_branch') ?>", {
            })
                .done(function (response) {
                    //alert(response);

                    var arr = JSON.parse(response);
                    //alert(arr);
                    $.each(arr, function(id,name) {
                        var option = $('<option value="'+name+'"></option>');
                        $('#branch_list').append(option);
                    });
                });

        });
    });

    $(document).ready(function (e) {
        $('#input_branch').focusout(function() {
            $("#branch_list").empty();
        });
    });

</script>


<?php


function datethai($strDate){
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"> <i class="fas fa-table fa-2x text-gray-300"></i> รายการงานเช่ารถยนต์</h1>
    <p class="mb-4"><a  href="<?php echo base_url('index.php/public_con/add_car_rental'); ?>" ><button  class="btn btn-primary" id="add_car_rental" name="add_car_rental" onclick="">เพิ่มงานเช่ารถยนต์</button></a>
    <div id="msg_num_branch"></div>
    </p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">รายการงานเช่ารถยนต์ </h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">สาขา</th>
                        <th scope="col">สังกัด</th>
                        <th scope="col">ประเภทงานจัดซือจัดจ้าง</th>
                        <th scope="col">ปีบัญชี</th>
                        <th scope="col">ครั้งที่</th>
                        <th scope="col">อยู่ในขั้นตอนที่</th>
                        <th scope="col"></th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $i=1;



                    foreach ($data as $row){
                        $row = $row['0'];
                        if($row->step_procurement==14){
                            $step_now = "เสร็จสมบูรณ์";
                        }else{
                            $step_now = $row->step_procurement+1;
                        }
                        echo "<tr>
                                <td scope='row' width='5%'>$i</td>
                                <td  width='20%'><a href='#'  onclick='info_procurement($row->id_branch,$row->id_procurement_type,$row->year_procurement,$row->time_procurement)' >$row->name_branch</a></td>
                                <td  width='25%'><a href='#'  onclick='info_procurement($row->id_branch,$row->id_procurement_type,$row->year_procurement,$row->time_procurement)' >$row->province_branch</a></td>
                                <td  width='20%'><a href='#'  onclick='info_procurement($row->id_branch,$row->id_procurement_type,$row->year_procurement,$row->time_procurement)' >$row->name_procurement_type</a></td>
                                <td  width='10%'><a href='#'  onclick='info_procurement($row->id_branch,$row->id_procurement_type,$row->year_procurement,$row->time_procurement)' >$row->year_procurement</a></td>
                                <td  width='10%'><a href='#'  onclick='info_procurement($row->id_branch,$row->id_procurement_type,$row->year_procurement,$row->time_procurement)' >ครั้งที่ $row->time_procurement</a></td>
                                <td  width='10%'><a href='#'  onclick='info_procurement($row->id_branch,$row->id_procurement_type,$row->year_procurement,$row->time_procurement)' >ขั้นตอนที่ $step_now</a></td>
                                <td>
                                 <form id='form_info_procurement$row->id_branch$row->id_procurement_type$row->year_procurement$row->time_procurement' action='".base_url("index.php/public_con/info_procurement")."' target='_blank' method='post' >
                                    <input id='id_branch' name='id_branch' value='$row->id_branch' type='hidden' >
                                    <input id='id_procurement_type' name='id_procurement_type' value='$row->id_procurement_type' type='hidden' >
                                    <input id='year_procurement' name='year_procurement' value='$row->year_procurement' type='hidden' >
                                    <input id='time_procurement' name='time_procurement' value='$row->time_procurement' type='hidden' >
                                    <a href='#'  onclick='info_procurement($row->id_branch,$row->id_procurement_type,$row->year_procurement,$row->time_procurement)' title='รายละเอียด' class='btn btn-info '>
                                        <i class='fas fa-info-circle'></i>
                                    </a>
                                    </form>
</td>
                             </tr>" ;
                        $i++;
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




