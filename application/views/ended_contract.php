<script>
    function delete_bid(no_bid){

        var r = confirm("ต้องการลบ บันทึกเลขที่ สตท/"+no_bid+" ใช่หรือไม่");

        if(r== true){
            $.post( "<?=site_url('bid/delete_bid') ?>", {
                no_bid:no_bid
            })
                .done(function( data ) {
                    alert( data );
                    location.reload();
                });
        }
    }

    function edit_bid(no_bid){
        $('#form_edit_bid'+no_bid).submit();
    }

    function info_bid(no_bid){
        $('#form_info_bid'+no_bid).submit();
    }

</script>


<?php


function datethai($strDate){
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"> <i class="fas fa-times-circle fa-2x text-gray-300"></i> โครงการที่หมดสัญญาแล้ว</h1>
    <p class="mb-4">เป็นโครงการที่หมดสัญญาแล้วทั้งหมด </p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-danger">โครงการที่หมดสัญญาแล้วทั้งหมด</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-warning" id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">สถานะ</th>
                        <th scope="col">วันที่ประกวดราคา</th>
                        <th scope="col">เรื่อง</th>
                        <th scope="col">วันที่หมดสัญญส</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;
                    $y=$data;

                    foreach ($y as $row){

                        if($row->dif<='0'&&$row->period_bid!='0000-00-00'){
                            echo "<tr>
                                <td scope='row' width='5%'>$i</td>
                                <td  width='15%'>$row->status_bid</td>                                
                                <td  width='15%'>".datethai($row->date_audit)."</td>
                                <td  width='50%'>$row->subject_bid</td>
                                <td width='15%'> ".datethai($row->period_bid)."</td>
                                <td >
                                
                                       <form id='form_info_bid$row->no_bid' action='".base_url("index.php/bid/info_bid")."' method='post' >
                                    <input id='no_bid' name='no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='#'  onclick='info_bid($row->no_bid)' title='รายละเอียด' class='btn btn-info '>
                                        <i class='fas fa-info-circle'></i>
                                    </a>
                                    </form>
                                </td>
                                <td>
                                    <form id='form_edit_bid$row->no_bid' action='".base_url("index.php/bid/edit_bid")."' method='post' >
                                    <input id='no_bid' name='no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='# ' onclick='edit_bid($row->no_bid)' title='แก้ไข' class='btn btn-success '  >
                                        <i class='fas fa-edit'></i>
                                    </a>
                                    </form>
                                </td>
                                
                             </tr>" ;
                            $i++;
                        }


                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




