<script>
    function delete_audit(no_bid){

        var r = confirm("ต้องการลบ การสังเกตการณ์"+no_bid+" ใช่หรือไม่");

        if(r== true){
            $.post( "<?=site_url('index.php/audit/delete_audit') ?>", {
                no_bid:no_bid
            })
                .done(function( data ) {
                    alert( data );
                    location.reload();
                });
        }
    }

    function edit_audit(no_bid){
        $('#form_edit_audit'+no_bid).submit();
    }

    function info_audit(no_bid){
        $('#form_info_audit'+no_bid).submit();
    }

</script>


<?php

    $session = $this->session->userdata('data');
    $privilege_member = $session['privilege_member'];

    if($privilege_member=="user"){
        $hid = 'style="display: none"';
    }else{
        $hid = "";
    }


function datethai($strDate){
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"> <i class="fas fa-table fa-2x text-gray-300"></i> การสังเกตการณ์ทั้งหมด</h1>
    <p class="mb-4">รายละเอียดการสังเกตการณ์</p>



    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ทั้งหมด <?php echo $num_audit ; ?> โครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">เลขที่</th>
                        <th scope="col">วันที่สังเกตการณ์</th>
                        <th scope="col">จัดซื้อจัดจ้าง</th>
                        <th scope="col">ผู้สังเกตการณ์</th>
                        <th scope="col"></th>
                        <th scope="col" <?php echo $hid ; ?> ></th>
                        <th scope="col" <?php echo $hid ; ?>></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;

                    foreach ($info_audit as $row){
                        echo "<tr>
                                <td scope='row' width='5%'>$i</td>
                                <td  width='10%'>$row->no_bid</td>                                
                                <td  width='15%'>".datethai($row->date_audit)."</td>
                                <td  width='50%'>$row->name_bid</td>
                                <td  width='20%'></td>
                                <td >
                                       <form id='form_info_audit$row->no_bid' action='".base_url("index.php/audit/info_audit")."' target='_blank' method='post' >
                                    <input id='info_no_bid' name='info_no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='#'  onclick='info_audit($row->no_bid)' title='รายละเอียด' class='btn btn-info '>
                                        <i class='fas fa-info-circle'></i>
                                    </a>
                                    </form>
                                </td>
                                <td $hid>
                                    <form id='form_edit_audit$row->no_bid' action='".base_url("index.php/audit/edit_audit")."' target='_blank' method='post' >
                                    <input id='no_bid' name='no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='# '  onclick='edit_audit($row->no_bid)' title='แก้ไข' class='btn btn-success '  >
                                        <i class='fas fa-edit'></i>
                                    </a>
                                    </form>
                                </td>
                                <td $hid>
                                    <a href='#' title='ลบ'  onclick='delete_audit($row->no_bid)' class='btn btn-danger '>
                                        <i class='fas fa-trash'></i>
                                    </a>
                                </td>
                             </tr>" ;
                        $i++;

                    }


                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




