<?php
$member = $this->session->userdata('data');
$name = $member['name'];

$arr = $bid['bid'];
$data = $arr['0'];
?>
<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script>
    function format_price(n) { ///////function for price
        return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+" บาท";
    }

    window.onload = function(){
        show_attract_ajax();
        show_vender_ajax();
        show_committee_bid_ajax();
    };


    function ajax_edit_bid() {
        var input_name_project = $('#input_name_project').val();
        var status_bid =$('#status_bid').val();

        var no_bid = $('#no_bid').val();
        var date_bid = $('#date_bid').val();
        var subject_bid =$('#subject_bid').val();
        var no_announce =$('#no_announce').val();
        var date_announce =$('#date_announce').val();
        var type_bid =$('#type_bid').val();
        var cost_bid =$('#cost_bid').val();
        var price_base_bid =$('#price_base_bid').val();
        var price_bid =$('#price_bid').val();
        var month_send_draft_contract =$('#month_send_draft_contract').val();
        var month_approve_bid =$('#month_approve_bid').val();
        var no_approve_bid = $('#no_approve_bid').val();
        var no_egp = $('#no_egp').val();
        var no_contract = $('#no_contract').val();
        var date_contract = $('#date_contract').val();
        var member = $('#member').val();
        var date_receive_bid = $('#date_receive_bid').val();

        var no_direct_committee = $('#no_direct_committee').val();
        var date_direct_committee = $('#date_direct_committee').val();
        var subject_direct_committee = $('#subject_direct_committee').val();

        $.post( "<?=site_url('index.php/audit/edit_bid_button') ?>", {
            input_name_project:input_name_project,
            status_bid:status_bid,

            no_bid:no_bid,
            date_bid:date_bid,
            subject_bid:subject_bid,
            no_announce:no_announce,
            date_announce:date_announce,
            type_bid:type_bid,
            cost_bid:cost_bid,
            price_base_bid:price_base_bid,
            price_bid:price_bid,
            month_send_draft_contract:month_send_draft_contract,
            month_approve_bid:month_approve_bid,
            no_approve_bid:no_approve_bid,
            no_egp:no_egp,
            date_contract:date_contract,
            no_contract:no_contract,
            member:member,
            date_receive_bid:date_receive_bid,
            no_direct_committee:no_direct_committee,
            date_direct_committee:date_direct_committee,
            subject_direct_committee:subject_direct_committee
        }).done(function( test ) {
            alert( test );
            location.reload();
        });
    }





    function show_committee_bid_ajax(){
        var no_bid =  $('#no_bid').val();
        $.post( "<?=site_url('index.php/audit/show_committee_bid_ajax') ?>", {
            no_bid:no_bid
        })
            .done(function( response ) {
                $('#show_committee').html(response);
            });
    }

    function show_modal_committee_bid() {
        reset_committee_bid_form();
        $('#model_committee').modal('show');
        show_committee_bid_ajax()
    }

    function reset_committee_bid_form() {
        $('#name_committee_bid').val('');
        $('#position_committee_bid').val('');
        show_committee_bid_ajax()
    }

    function add_committee_bid() {
        var name_committee_bid =  $('#name_committee_bid').val();
        var position_committee_bid =  $('#position_committee_bid').val();
        var no_bid =  $('#no_bid').val();
        var status_committee_bid = $('#status_committee_bid').val();
        if(no_bid==""){
            alert('กรุณาระบุเลขที่จัดซื้อจัดจ้าง');
            $('#no_bid').focus();
        }else{
            if(name_committee_bid==""){
                alert('กรุณาระบุชื่อคณะกรรมการ');
                $('#name_committee_bid').focus();
            }else{
                if(position_committee_bid==""){
                    alert('กรุณาระบุตำแหน่ง');
                    $('#position_committee_bid').focus();
                }else{
                    $.post("<?=site_url('index.php/audit/add_committee_bid_ajax') ?>", {
                        name_committee_bid: name_committee_bid,
                        position_committee_bid: position_committee_bid,
                        no_bid: no_bid,
                        status_committee_bid:status_committee_bid
                    })
                        .done(function (response) {
                            alert(response);

                            if(response=='เพิ่มสำเร็จ'){
                                show_committee_bid_ajax();
                            }
                        });
                }
            }
        }
    }

    function delete_committee_bid_ajax(id_committee_bid ,name_committee_bid) {

        var c = confirm("ต้องการที่จะลบ "+name_committee_bid+"  ใช่หรือไม่");
        if(c==true){
            $.post( "<?=site_url('index.php/audit/delete_committee_bid_ajax') ?>", {
                id_committee_bid:id_committee_bid,
                name_committee_bid:name_committee_bid
            })
                .done(function( response ) {
                    alert(response);
                    show_committee_bid_ajax();
                    //$('#show_committee').focus();
                });
        }
    }



    $(document).ready(function (e) {
        $('#ddd').on('click', function () {
            var no_bid =  $('#no_bid').val();
            var att = $('#attachment_bid').val();
            if(att==""){
                alert("กรุณาเลือกไฟล์ ก่อนอัพโหลด");
            }else {
                var file_data = $('#attachment_bid').prop('files')[0];
                var form_data = new FormData();

                if(no_bid == ""){
                    alert("กรุณาใส่เลขบันทึกสังเกตุการณ์ ก่อนอัพโหลดไฟล์");
                    $('#no_bid').focus();
                }else {
                    form_data.append('file', file_data);
                    form_data.append('no_bid', no_bid);

                    $.ajax({
                        url: '<?php echo base_url("/index.php/audit/upload_file"); ?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',

                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                            show_attract_ajax();
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                            show_attract_ajax();
                        }
                    });
                }
            }
        });
    });

    function show_attract_ajax(){
        var no_bid =  $('#no_bid').val();
        $.post( "<?=site_url('index.php/audit/show_attract_ajax') ?>", {
            no_bid:no_bid
        })
            .done(function( response ) {
                $('#show_attract').html(response);
            });
    }

    function delete_attract_ajax(id_attract){
        var c = confirm("ต้องการที่จะลบไฟล์ ใช่หรือไม่");
        if(c==true){
            $.post( "<?=site_url('index.php/audit/delete_attract') ?>", {
                id_attract:id_attract
            })
                .done(function( response ) {
                    alert(response);
                    show_attract_ajax();
                    $('#attachment_bid').focus();
                });
        }
    }


    $(document).ready(function (e) {
        $('#name_vender').focus(function() {
            $.post("<?=site_url('index.php/audit/show_name_vender') ?>", {
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    //alert(response);
                    $.each(arr, function(id,name) {
                        var option = $('<option value="'+name+'"></option>');
                        $('#vender_list').append(option);
                    });
                });
        });
    });

    $(document).ready(function (e) {
        $('#name_vender').focusout(function() {
            $("#vender_list").empty();
        });
    });

    function show_modal_sign_up_vender() {
        reset_sign_up_form();
        $('#model_sign_up_vender').modal('show');
    }

    function reset_sign_up_form() {
        $('#name_sign_up_vender').val('');
        $('#capital_vender').val('');
    }

    function sign_up_vender(){
        var name_sign_up_vender = $('#name_sign_up_vender').val();
        var capital_vender = $('#capital_vender').val();

        if(name_sign_up_vender==""){
            alert('กรุณาระบุชื่อบริษัท');
            $('#name_sign_up_vender').focus();
        }else {
            if(capital_vender==""){
                alert('กรุณาระบุชื่อบริษัท');
                $('#capital_vender').focus();
            }else{
                $.post( "<?=site_url('index.php/audit/sign_up_vender') ?>", {
                    name_sign_up_vender:name_sign_up_vender,
                    capital_vender:capital_vender
                })
                    .done(function( response ) {
                        //$('#msg_vender').html(response);
                        alert(response);
                        $('#model_sign_up_vender').modal('hide');
                    });
            }
        }
    }


    function show_vender_ajax(){
        $('#msg_vender').empty();
        var no_bid =  $('#no_bid').val();
        if(no_bid==""){
            alert('ไม่พบ เลขที่จัดซื้อจัดจ้าง');
            $('#no_bid').focus();
        }else {
            //alert(no_bid);
            $.post("<?=site_url('index.php/audit/show_vender_ajax') ?>", {
                no_bid: no_bid
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    var price_base_bid = "<?php echo $data->price_base_bid; ?>";

                    var i = 1;
                    var status = "";
                    var msg_vender = $('#msg_vender'), container;

                    msg_vender.append("<thead><tr><th>ลำดับ</th><th>บริษัท</th><th>ราคาที่ยืน</th><th>ราคาหลังการต่อรอง</th><th>จำนวนที่ลดลง</th><th>ลดจากราคากลางร้อยละ</th><th>สถานะ</th><th></th></tr></thead>");
                    for (var key in arr) {
                        var send = arr[key].sender_active_vender;
                        var pass = arr[key].pass_active_vender;
                        var win = arr[key].win_active_vender;


                        var price_active_vender = arr[key].price_active_vender;
                        var price = arr[key].chaffer_active_vender;

                        if(price==0){
                            var un = price_base_bid - price_active_vender*1;
                            var sale = ((price_base_bid - price_active_vender)*100)/price_base_bid ;
                        }else{
                            var un = price_base_bid - price*1;
                            var sale = ((price_base_bid - price)*100)/price_base_bid ;
                        }
                        price = price*1;
                        price_active_vender = price_active_vender*1;
                        un = un * 1;

                        var p_a_v = "-" ;
                        var p = "-";
                        var u = "-";
                        var s = "-";

                        if (send == 0 && pass == 0 && win == 0) {
                            status = "ไม่ได้ยื่นซอง";
                            var p_a_v = "-" ;
                            var p = "-";
                            var u = "-";
                            var s = "-";
                        } else {
                            if (send == 1 && pass == 0 && win == 0) {
                                status = "คุณสมบัติไม่ผ่าน";
                                var p_a_v = "-" ;
                                var p = "-";
                                var u = "-";
                                var s = "-";
                            } else {
                                if (send == 1 && pass == 1 && win == 0) {
                                    status = "ไม่ชนะ";

                                    var p_a_v = format_price(price_active_vender) ;
                                    var p = "-";
                                    var u = "-";
                                    var s = "-";
                                } else {
                                    if (send == 1 && pass == 1 && win == 1) {
                                        status = "ชนะ";

                                        var p_a_v = format_price(price_active_vender) ;
                                        var p = format_price(price);
                                        var u = format_price(un);
                                        var s = sale.toFixed(2);

                                    }
                                }
                            }
                        }


                        container = $('<tr></tr>');
                        msg_vender.append(container);
                        container.append('<td>' + i + '</td>');
                        container.append('<td>' + arr[key].name_vender + '</td>');
                        container.append('<td>' + p_a_v + '</td>');
                        container.append('<td>' + p + '</td>');
                        container.append('<td>' + u +'</td>');
                        container.append('<td>' + s + '</td>');
                        container.append('<td>' + status + '</td>');
                        container.append('<td><a href="#" onclick="delete_active_vender_ajax('+arr[key].id_active_vender+')" class="btn btn-danger"><i class="fas fa-trash"></i></td>');
                        i++;
                    }
                });
        }

    }

    $(document).ready(function (e) {
        $('#price_base_bid').blur(function() {
            show_vender_ajax();
        });
    });

    function delete_active_vender_ajax(id_vender){
        var c = confirm("ต้องการที่จะลบบริษัท ใช่หรือไม่");
        if(c==true){
            $.post( "<?=site_url('index.php/audit/delete_active_vender') ?>", {
                id_vender:id_vender
            })
                .done(function( response ) {
                    alert(response);
                    show_vender_ajax();
                    $('#button_vender').focus();
                });
        }
    }



    function show_modal_vender() {
        reset_form();
        $('#model_vender').modal('show');
    }


    function add_vender() {
        var name_vender = $('#name_vender').val();
        var sender = $('#sender_vender').val();
        var pricebid = $('#pricebid').val();
        var win = $('#win_vender').val();
        var price_under_bid = $('#price_under_bid').val();
        var no_bid = $('#no_bid').val();
        var pass_vender = $('#pass_vender').val();

        if(no_bid==''){
            alert("กรุณาใส่เลขที่จัดซื้อจัดจ้าง");
            $('#no_bid').focus();
        }else{
            if(name_vender==""){
                alert("กรุณาใส่ชื่อบริษัทที่ซื้อซอง");
                $('#name_vender').focus();
            }else{
                if(sender==""){
                    alert('กรุณาเลือกสถานะการยื่นเสนอราคา');
                    $('#sender_vender').focus();
                }else{
                    var form_data = new FormData();
                    form_data.append('name_vender',name_vender);
                    form_data.append('sender',sender);
                    form_data.append('pricebid',pricebid);
                    form_data.append('win',win);
                    form_data.append('price_under_bid',price_under_bid);
                    form_data.append('no_bid',no_bid);
                    form_data.append('pass_vender',pass_vender);

                    $.ajax({
                        url: '<?php echo base_url("/index.php/audit/add_vender"); ?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            alert(response);
                            show_vender_ajax();
                            //$('#msg_vender').html(response); // display success response from the server
                        },
                        error: function (response) {
                            alert('save error');
                            //$('#msg_vender').html(response); // display error response from the server
                        }
                    });
                    reset_form();
                }
            }
        }
    }

    function reset_form() {
        $('#name_vender').val('');
        $('#sender_vender').val('0');
        $('#pricebid').val('');
        $('#win_vender').val('0');
        $('#price_under_bid').val('');
        $('#pass_vender').val('0');
        document.getElementById("pass").style.display = "none";
        document.getElementById("pricebid_div").style.display = "none";
        document.getElementById("win").style.display = "none";
        document.getElementById("price_under_bid_div").style.display = "none";
    }

    $(document).ready(function (e) {
        $('#sender_vender').click(function() {
            var value_sender = $('#sender_vender').val();
            if(value_sender == "1"){
                document.getElementById("pass").style.display = " block";
            }else{
                $('#pass_vender').val('0');
                $('#pricebid').val('');
                $('#win_vender').val('0');
                $('#price_under_bid').val('');
                document.getElementById("pass").style.display = "none";
                document.getElementById("pricebid_div").style.display = "none";
                document.getElementById("win").style.display = "none";
                document.getElementById("price_under_bid_div").style.display = "none";
            }
        });
    });

    $(document).ready(function (e) {
        $('#pass_vender').click(function() {
            var value_sender = $('#pass_vender').val();
            if(value_sender == "1"){
                document.getElementById("pricebid_div").style.display = " block";
                document.getElementById("win").style.display = "block";
            }else{
                $('#pricebid').val('');
                $('#win_vender').val('0');
                $('#price_under_bid').val('');
                document.getElementById("pricebid_div").style.display = "none";
                document.getElementById("win").style.display = "none";
                document.getElementById("price_under_bid_div").style.display = "none";
            }
        });
    });

    $(document).ready(function (e) {
        $('#win_vender').click(function() {
            var value_win = $('#win_vender').val();
            if(value_win == "1"){
                document.getElementById("price_under_bid_div").style.display = " block";
            }else{
                $('#price_under_bid').val('');
                document.getElementById("price_under_bid_div").style.display = "none";
            }
        });
    });













    function show_modal_project() {
        reset_project();
        $('#modal_project').modal('show');
    }

    function reset_project() {
        $('#name_project').val('');
        $('#no_project').val('');
        $('#date_project').val('');
        $('#owner_project').val('');
        $('#cost_project').val('');
        $('#objective_project').val('');
    }

    function add_project() {
        var name_project =  $('#name_project').val();
        var name =  $('#name_project').val();
        var no_project = $('#no_project').val();
        var date_project =$('#date_project').val();
        var owner_project = $('#owner_project').val();
        var cost_project = $('#cost_project').val();
        var objective_project = $('#objective_project').val();

        if(name_project==""){
            alert('กรุณาระบุชื่อโครงการ');
            $('#name_project').focus();
        }else{
            if(owner_project==""){
                alert('กรุณาระบุเจ้าของโครงการ');
                $('#owner_project').focus();
            }else{
                if(cost_project==""){
                    alert('กรุณาระบุงบประมาณโครงการ');
                    $('#cost_project').focus();
                }else{
                    if(objective_project==""){
                        alert('กรุณาระบุวัตถุประสงค์ของโครงการ');
                        $('#objective_project').focus();
                    }else {

                        $.post("<?=site_url('index.php/audit/add_project') ?>", {
                            name_project: name_project,
                            no_project: no_project,
                            date_project: date_project,
                            owner_project: owner_project,
                            cost_project: cost_project,
                            objective_project: objective_project
                        })
                            .done(function (response) {
                                alert(response);

                                //show_sub_bid();
                                if(response=='เพิ่มสำเร็จ'){
                                    $('#input_project').val(name);
                                }
                            });
                    }
                }

            }
        }
    }

    function add_input_name_project() {
        var input_project = $('#input_project').val();
        if(input_project==""){
            alert('กรุณาระบุชื่อโครงการ หรือเพิ่มโครงการ ก่อนการเลือกโครงการ');
        }else{
            $.post("<?=site_url('index.php/audit/check_name_project') ?>", {
                input_project:input_project
            })
                .done(function (response) {
                    if(response=="yes"){
                        $('#input_name_project').val(input_project);
                        $('#input_project').val("");
                        //$('#card_bid').show();
                        document.getElementById("card_bid").style.display = "block";
                        document.getElementById("card_vender").style.display = "block";
                        document.getElementById("card_attach").style.display = "block";
                        document.getElementById("card_submit").style.display = "block";
                    }else{
                        alert('ไม่พบชื่อโครงการ กรุณาเพิ่มโครงการ หรือเลือกโครงการ');
                        $('#button_project').focus();
                    }
                });
        }
    }

    $(document).ready(function (e) {
        $('#input_project').focus(function() {
            $.post("<?=site_url('index.php/audit/show_project') ?>", {
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    //alert(response);
                    $.each(arr, function(id,name) {
                        var option = $('<option value="'+name+'"></option>');
                        $('#project_list').append(option);
                    });
                });
        });
    });

    $(document).ready(function (e) {
        $('#input_project').focusout(function() {
            $("#project_list").empty();
        });
    });











</script>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-save fa-2x text-gray-300"></i> บันทึกการจัดซื้อจัดจ้างพัสดุ</h1>
    <p class="mb-4">บันทึกัดซื้อจัดจ้างพัสดุ หลังจากที่ทำรายงาน และออกเลขหนังสือแล้ว</p>



    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_submit" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">โครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <pre><?php //print_r($data) ;  ?></pre>
                            <label for="input_name_project">ชื่อโครงการ  </label>
                            <div class="input-group mb-3">
                                <input  type="text" id="input_name_project" value="<?php echo $data->name_project; ?>"  name="input_name_project"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="cost_project" >งบประมาณโครงการ</label>
                            <input type="text" id="cost_project" name="cost_project" value="<?php echo number_format($data->cost_project); ?> " class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="owner_project" >ผู้รับผิดชอบโครงการ</label>
                            <input type="text" id="owner_project" name="owner_project" value="<?php echo $data->owner_project; ?> " class="form-control" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="objective_project">วัตถุประสงค์โครงการ</label>
                            <textarea id="objective_project" name="objective_project" class="form-control" disabled ><?php echo $data->objective_project; ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div id="card_bid" class="card shadow mb-4" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">บันทึก การจัดซื้อจัดจ้าง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td></td>
                        <td>
                            <label for="status_bid">สถานนะการดำเนินการ</label>
                            <select  id="status_bid" name="status_bid" class="form-control">
                                <option value="<?php echo $data->status_bid; ?>"><?php echo $data->status_bid; ?></option>
                                <option value="ขออนุมัติจัดซื้อจัดจ้าง">ขออนุมัติจัดซื้อจัดจ้าง</option>
                                <option value="ขายเอกสาร">ขายเอกสาร</option>
                                <option value="จัดทำหนังสือเชิญชวน">จัดทำหนังสือเชิญชวน</option>
                                <option value="ชะลอการจ้าง">ชะลอการจ้าง</option>
                                <option value="แต่งตั้งคณะกรรมการ">แต่งตั้งคณะกรรมการ</option>
                                <option value="ดำเนินการประกวดราคา">ดำเนินการประกวดราคา</option>
                                <option value="ประกาศผู้ชนะการเสนอราคา">ประกาศผู้ชนะการเสนอราคา</option>
                                <option value="พิจารณาข้อเสนอ">พิจารณาข้อเสนอ</option>
                                <option value="ยกเลิกโครงการ">ยกเลิกโครงการ</option>
                                <option value="ลงนามสัญญาแล้ว">ลงนามสัญญาแล้ว</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="year_bid">ปีบัญชี</label>
                            <input type="text"  id="year_bid" name="year_bid" class="form-control" value="ปีบัญชี <?php echo $data->fiscal_year_bid; ?>" disabled>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <label for="no_bid">เลขที่จัดซื้อจัดจ้าง</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text">เลขที่</span>
                                <input type="text" id="no_bid"  name="no_bid" maxlength="7" value="<?php echo $data->no_bid; ?>"  placeholder="" class="form-control" disabled/>

                            </div>
                        </td>
                        <td width="50%">
                            <label for="date_bid">วันที่จัดซื้อจัดจ้าง</label>
                            <div class="input-group mb-3">
                                <input type="date" id="date_bid" name="date_bid" placeholder="" value="<?php echo $data->date_bid; ?>" class="form-control"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  >
                            <p>
                                <label for="subject_bid">ชื่อการจัดซื้อจัดจ้าง</label>
                                <input type="text" id="subject_bid" name="subject_bid" placeholder="" value="<?php echo $data->name_bid; ?>" class="form-control"/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_announce">เลขที่ประกาศ</label>
                            <input type="text" id="no_announce" name="no_announce" value="<?php echo $data->no_announce_bid; ?>" class="form-control" >
                        </td>
                        <td >
                            <label for="date_announce">ลงวันที่ประกาศ</label>
                            <input type="date" id="date_announce" name="date_announce" placeholder="" value="<?php echo $data->date_announce_bid; ?>" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="type_bid"  >วิธีจัดหา</label>
                            <div class="input-group mb-3">
                                <select  id="type_bid" name="type_bid" class="form-control">
                                    <option value="<?php echo $data->type_bid; ?>"><?php echo $data->type_bid; ?></option>
                                    <option value="ประกวดราคา">ประกวดราคา</option>
                                    <option value="e-bidding">e-bidding</option>
                                    <option value="วิธีคัดเลือก">วิธีคัดเลือก</option>
                                    <option value="เฉพาะเจาะจง">เฉพาะเจาะจง</option>
                                    <option value="เปรียบเทียบ">เปรียบเทียบ</option>
                                    <option value="พิเศษ">พิเศษ</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <label for="cost_bid">งบประมาณ</label>
                            <div class="input-group mb-3">
                                <input type="number" id="cost_bid" name="cost_bid" placeholder="จำนวนเงิน " value="<?php echo $data->cost_bid; ?>" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="price_base_bid">ราคากลาง</label>
                            <div class="input-group mb-3">
                                <input type="number" id="price_base_bid" name="price_base_bid" placeholder="จำนวนเงิน " value="<?php echo $data->price_base_bid; ?>" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </td>
                        <td>
                            <label for="price_bid">ราคาจริง</label>
                            <div class="input-group mb-3">
                                <input type="number" id="price_bid" name="price_bid" placeholder="จำนวนเงิน " value="<?php echo $data->price_bid; ?>" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="month_send_draft_contract">เดือน ส่งจัดทำร่างสัญญา</label>
                            <input type="month" id="month_send_draft_contract" name="month_send_draft_contract" placeholder="" value="<?php echo $data->send_draft_contract; ?>" class="form-control"/>
                        </td>
                        <td>
                            <label for="month_approve_bid">เดือน อนุมัติจัดซื้อจัดจ้าง</label>
                            <input type="month" id="month_approve_bid" name="month_approve_bid" placeholder="" value="<?php echo $data->approve_bid; ?>" class="form-control"/>

                        </td>
                    </tr>
                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="no_approve_bid">เลขที่สัญญาการอนุมัติ</label>
                            <input type="text" id="no_approve_bid" name="no_approve_bid" placeholder="" value="<?php echo $data->no_approve_bid; ?>" class="form-control"/>
                        </td>
                        <td>
                            <label for="no_egp">e-gp</label>
                            <input type="text" id="no_egp" name="no_egp" placeholder="" value="<?php echo $data->no_egp; ?>"  class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_contract">เลขที่สัญญา</label>
                            <input type="text" id="no_contract" name="no_contract" value="<?php echo $data->no_contract; ?>" class="form-control" >
                        </td>
                        <td >
                            <label for="date_contract">ลงวันที่สัญญา</label>
                            <input type="date" id="date_contract" name="date_contract" placeholder="" value="<?php echo $data->date_contract; ?>" class="form-control"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_committee"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">คณะกรรมการประกวดราคา</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td >
                            <label for="no_direct_committee">คำสั่ง ที่</label>
                            <input type="text" id="no_direct_committee" name="no_direct_committee" value="<?php echo $data->no_direct_committee; ?>" class="form-control" >
                        </td>
                        <td >
                            <label for="date_direct_committee">ลงวันที่</label>
                            <input type="date" id="date_direct_committee" name="date_direct_committee" value="<?php echo $data->date_direct_committee; ?>" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="subject_direct_committee">เรื่อง</label>
                            <input type="text" id="subject_direct_committee" name="subject_direct_committee" value="<?php echo $data->subject_direct_committee; ?>" class="form-control" >

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button id="button_committee_bid" class="btn btn-primary" data-toggle="modal" onclick="show_modal_committee_bid()">เพิ่มคณะกรรมการประกวดราคา</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="show_committee">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_vender" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนบริษัทที่ซื้อซอง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <label for="vender" >บริษัท ที่ซื้อซอง</label>
                            <button id="button_vender" class="btn btn-primary" data-toggle="modal" onclick="show_modal_vender()">เพิ่มรายละเอียดการซื้อซอง</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div  class="col-12">
                                <table class='table' width='100%' id="msg_vender">
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_attach" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนเอกสารแนบ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">

                    <tr>
                        <td colspan="2">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="attachment_bid" name="attachment_bid" accept=".pdf,.doc,.docx" aria-describedby="ddd">
                                    <label class="custom-file-label" for="attachment_bid">เลือกเอกสารแนบ</label>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="ddd">อัพโหลด</button>
                                </div>

                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div id="show_attract">
                                <div  style="color:red" class="alert alert-warning" role="alert" >***** ยังไม่มีการอัพโหลด</div>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_submit" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ผู้รับผิดชอบ การจัดซื้อจัดจ้าง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="member" >ผู้รับผิดชอบ</label>
                            <input type="text" id="member" name="member" value="<?php echo $name ;?> " class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="date_receive_bid">วันที่รับงาน</label>
                            <input type="date" id="date_receive_bid" name="date_receive_bid" value="<?php echo $data->date_receive_bid; ?>" class="form-control"/>
                        </td>
                    </tr>

                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button type="submit" class="btn btn-success" onclick="ajax_edit_bid()">แก้ไข</button>
                            <button type="reset" class="btn btn-danger" onclick="window.close()">ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->






<!-- start modal vender form ####################################################################################-->

<div id="model_vender" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title">บริษัทที่ซื้อซอง </h4>
            </div>

            <div class="modal-body" id="myModalBody">

                <form id="all_vender" role="form" >
                    <div class="form-group">

                        <div class="input-group mb-3">
                            <div class="input-group">
                                <input list="vender_list" id="name_vender" name="name_vender" placeholder="ชื่อบริษัท" class="form-control">
                                <datalist id="vender_list">
                                </datalist>
                                <input type="button" class="btn btn-primary" onclick="show_modal_sign_up_vender()" value="ลงทะเบียน บริษัท">
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="sender_vender">สถานะการยื่นเสนอราคา</label>
                            </div>
                            <select class="custom-select" id="sender_vender" name="sender_vender" >
                                <option value="1" >ยื่นเสนอราคา</option>
                                <option value="0"  selected>ไม่ยื่นเสนอราคา</option>
                            </select>
                        </div>
                        <div class="input-group mb-3" id="pass" style="display: none">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="pass_vender">คุณสมบัติ</label>
                                <select class="custom-select" id="pass_vender" name="pass_vender" >
                                    <option value="1" >ผ่าน</option>
                                    <option value="0"  selected>ไม่ผ่าน</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3" style="display: none"  id="pricebid_div">
                            <div class="input-group-prepend">
                                <input type="number"  min="1" step="any" id="pricebid" name="pricebid" placeholder="ราคาที่ยื่นเสนอ" class="form-control" />
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                        <div class="input-group mb-3" id="win" style="display: none" >
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="win_vender">สถานะ</label>
                                <select class="custom-select" id="win_vender" name="win_vender" >
                                    <option value="1" >ชนะการประกวด</option>
                                    <option value="0" selected>ไม่ชนะการประกวดราคา</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3" style="display: none"  id="price_under_bid_div">
                            <div class="input-group-prepend">
                                <input  type="number" min="1" step="any" id="price_under_bid" name="price_under_bid" placeholder="ราคาหลังการต่อรอง" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_vender" name="add_vender" onclick="add_vender()" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->


<!-- start modal project form ####################################################################################-->

<div id="modal_project" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">เพิ่มโครงการ</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="project" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input   type="text" id="name_project" name="name_project" placeholder="ชื่อโครงการ" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="no_project" name="no_project" placeholder="เลขที่โครงการ" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="date" id="date_project" name="date_project" placeholder="ลงวันที่" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="owner_project" name="owner_project" placeholder="เจ้าของเรื่อง" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="number" id="cost_project" name="cost_project" placeholder="งบประมาณของโครงการ" class="form-control"/>
                            <span class="input-group-text">บาท</span>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text">วัตถุประสงค์ของโครงการ</span>
                            <textarea class="form-control" aria-label="With textarea" id="objective_project" name="objective_project" ></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_sub" name="add_sub" data-dismiss="modal" aria-hidden="true" onclick="add_project()">เพิ่ม</button>

                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_project()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal sub form ####################################################################################-->


<!-- start modal sig up vender form ####################################################################################-->

<div id="model_sign_up_vender" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ลงทะเบียนบริษัท </h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="sign_up_vender_form" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="name_sign_up_vender">ชื่อบริษัท</label>
                                <input type="text" id="name_sign_up_vender" name="name_sign_up_vender" class="form-control">
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="capital_vender">ทุนการจดทะเบียน</label>
                                <input type="number" id="capital_vender" name="capital_vender" class="form-control">
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_vender" name="add_vender" onclick="sign_up_vender()" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sign_up_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->

<!-- start modal sig up vender form ####################################################################################-->
<div id="model_committee" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">คณะกรรมการประกวดราคา</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="committee_bid_form" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="name_committee_bid">ชื่อ</label>
                                <input type="text" id="name_committee_bid" name="name_committee_bid" class="form-control">
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="position_committee_bid">ตำแหน่งคณะกรรมการ</label>
                                <select  id="position_committee_bid" name="position_committee_bid" class="form-control">
                                    <option value="กรรมการ">กรรมการ</option>
                                    <option value="ประธานกรรมการ">ประธานกรรมการ</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="status_committee_bid">สถานะวันประกวดราคา</label>
                                <select  id="status_committee_bid" name="status_committee_bid" class="form-control">
                                    <option value="ๅ" selected>มา</option>
                                    <option value="0">ไม่มา</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_committee_bid" name="add_committee_bid" onclick="add_committee_bid()" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_committee_bid_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->





