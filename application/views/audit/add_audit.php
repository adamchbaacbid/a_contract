<script>
    function delete_bid(no_bid){

        var r = confirm("ต้องการลบ บันทึกเลขที่ สตท/"+no_bid+" ใช่หรือไม่");

        if(r== true){
            $.post( "<?=site_url('index.php/audit/delete_bid') ?>", {
                no_bid:no_bid
            })
                .done(function( data ) {
                    alert( data );
                    location.reload();
                });
        }
    }

    function select_bid(no_bid){
        $('#form_select_bid'+no_bid).submit();
    }

    function add_audit2(no_bid){
        $('#form_info_bid'+no_bid).submit();
    }

</script>


<?php


function datethai($strDate){
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"> <i class="fas fa-plus-circle fa-2x text-gray-300"></i> เพิ่มการสังเกตการณ์</h1>
    <p class="mb-4">ขั้นตอนที่ 1 ค้นหาการจัดซื้อจ้างที่จะสังเกตการณ์ แล้วเลือก <i class='fas fa-plus '></i> เพื่อเพิ่มการสังเกตการณ์ </p>



    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">สถานะ</th>
                        <th scope="col">วันที่จัดซื้อจัดจ้าง</th>
                        <th scope="col">โครงการ</th>
                        <th scope="col">เจ้าของโครงการ</th>
                        <th scope="col"></th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;

                    foreach ($bid as $row){
                        echo "<tr>
                                <td scope='row' width='5%'>$i</td>
                                <td  width='15%'>$row->status_bid</td>                                
                                <td  width='15%'>".datethai($row->date_bid)."</td>
                                <td  width='50%'>$row->name_bid</td>
                                <td  width='15%'>$row->owner_project</td>
                                <td >
                                       <form id='form_info_bid$row->no_bid' action='".base_url("index.php/audit/add_audit2")."' target='_blank' method='post' >
                                    <input id='info_no_bid' name='info_no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='#'  onclick='add_audit2($row->no_bid)' title='เลือก ' class='btn btn-info '>
                                        <i class='fas fa-plus'></i>
                                    </a>
                                    </form>
                                </td>
                                
                                
                             </tr>" ;
                        $i++;
                    }


                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




