
<script>
    function delete_project(id_project){

        var r = confirm("ต้องการลบ โครงการนี้ ใช่หรือไม่");

        if(r== true){
            $.post( "<?=site_url('index.php/audit/delete_project') ?>", {
                id_project:id_project
            })
                .done(function( data ) {
                    alert( data );
                    location.reload();
                });
        }
    }

    function edit_project(id_project){
        $('#form_edit_project'+id_project).submit();
    }

    function info_project(id_project){
        $('#form_info_project'+id_project).submit();
    }

</script>


<?php

    $session = $this->session->userdata('data');
    $privilege_member = $session['privilege_member'];

    if($privilege_member=="user"){
        $hid = 'style="display: none"';
    }else{
        $hid = "";
    }


function datethai($strDate){
    if($strDate=="0000-00-00"){
        return "ไม่ได้ระบุวันที่";
    }else{
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"> <i class="fas fa-table fa-2x text-gray-300"></i> โครงการการจัดซื้อจัดจ้างทั้งหมด</h1>
    <p class="mb-4">โครงการการจัดซื้อจัดจ้าง </p>



    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">โครงการทั้งหมด จำนวน  <?php echo $num ; ?> โครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>

                        <th scope="col">วันที่จัดซื้อจัดจ้าง</th>
                        <th scope="col">โครงการ</th>
                        <th scope="col">งบประมาณ</th>
                        <th scope="col">เจ้าของโครงการ</th>
                        <th scope="col"></th>
                        <th scope="col" <?php echo $hid ;?>></th>
                        <th scope="col" <?php echo $hid ;?>></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;

                    foreach ($all_project as $row){
                        echo "<tr>
                                <td scope='row' width='5%'>$i</td>
                                                               
                                <td  width='15%'>".datethai($row->date_project)."</td>
                                <td  width='50%'>$row->name_project</td>
                                <td  width='15%'>$row->cost_project</td> 
                                <td  width='15%'>$row->owner_project</td>
                                <td >
                                    <form id='form_info_project$row->id_project' action='".base_url("index.php/audit/info_project")."' target='_blank' method='post' >
                                    <input id='info_id_project' name='info_id_project' value='$row->id_project' type='hidden' >
                                    <a href='#'  onclick='info_project($row->id_project)' title='รายละเอียด' class='btn btn-info '>
                                        <i class='fas fa-info-circle'></i>
                                    </a>
                                    </form>
                                      
                                </td>
                                <td $hid>
                                    <form id='form_edit_project$row->id_project' action='".base_url("index.php/audit/edit_project")."' target='_blank' method='post' >
                                    <input id='id_project' name='id_project' value='$row->id_project' type='hidden' >
                                    <a href='# ' onclick='edit_project($row->id_project)' title='แก้ไข' class='btn btn-success '  >
                                        <i class='fas fa-edit'></i>
                                    </a>
                                    </form>
                                </td>
                                <td $hid>
                                    <a href='#' title='ลบ' onclick='delete_project($row->id_project)' class='btn btn-danger '>
                                        <i class='fas fa-trash'></i>
                                    </a>
                                </td>
                             </tr>" ;
                        $i++;

                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




