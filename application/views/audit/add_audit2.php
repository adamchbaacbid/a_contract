<?php
$member = $this->session->userdata('data');
$name = $member['name'];

$arr = $bid['bid'];
$data = $arr['0'];

$name_member = $bid['name_member'];


function datethai($strDate){
    if($strDate=="0000-00-00"){
        return "ไม่ได้ระบุวัน";
    }else{
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

}
?>
<script >
    function format_price(n) { ///////function for price
        return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+" บาท";
    }

    window.onload = function(){
        show_attract_ajax();
        show_vender_ajax();
    };


    function show_attract_ajax(){
        var no_bid =  $('#no_bid').val();
        //alert(no_bid);
        $.post( "<?=site_url('index.php/audit/show_attract_ajax') ?>", {
            no_bid:no_bid
        })
            .done(function( response ) {
                $('#show_attract').html(response);
                $('#delete_attract').hide();
            });
    }

    function show_vender_ajax(){
        $('#msg_vender').empty();
        var no_bid =  $('#no_bid').val();
        if(no_bid==""){
            alert('ไม่พบ เลขที่จัดซื้อจัดจ้าง');
            $('#no_bid').focus();
        }else {
            //alert(no_bid);
            $.post("<?=site_url('index.php/audit/show_vender_ajax') ?>", {
                no_bid: no_bid
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    var price_base_bid = $('#price_base_bid').val();

                    var i = 1;
                    var status = "";
                    var msg_vender = $('#msg_vender'), container;

                    msg_vender.append("<thead><tr><th>ลำดับ</th><th>บริษัท</th><th>ราคาที่ยืน</th><th>ราคาหลังการต่อรอง</th><th>ลดจากราคากลางร้อยละ</th><th>สถานะ</th></tr></thead>");
                    for (var key in arr) {

                        var send = arr[key].sender_active_vender;
                        var pass = arr[key].pass_active_vender;
                        var win = arr[key].win_active_vender;
                        if (send == 0 && pass == 0 && win == 0) {
                            status = "ไม่ได้ยื่นซอง";
                        } else {
                            if (send == 1 && pass == 0 && win == 0) {
                                status = "คุณสมบัติไม่ผ่าน";
                            } else {
                                if (send == 1 && pass == 1 && win == 0) {
                                    status = "ไม่ชนะ";
                                } else {
                                    if (send == 1 && pass == 1 && win == 1) {
                                        status = "ชนะ";
                                    }
                                }
                            }
                        }

                        var price = arr[key].chaffer_active_vender;
                        var sale = ((price_base_bid - price) * 100) / price_base_bid;
                        var price_active_vender = arr[key].price_active_vender;

                        price = price * 1;
                        price_active_vender = price_active_vender * 1;

                        container = $('<tr></tr>');
                        msg_vender.append(container);
                        container.append('<td>' + i + '</td>');
                        container.append('<td>' + arr[key].name_vender + '</td>');
                        container.append('<td>' + format_price(price_active_vender) + '</td>');
                        container.append('<td>' + format_price(price) + '</td>');
                        container.append('<td>' + sale.toFixed(2) + '</td>');
                        container.append('<td>' + status + '</td>');
                         i++;
                    }
                });
        }

    }

    function minimize(card_name) {
        var card = document.getElementById(card_name).style.display;
        if(card=="none"){
            var card = document.getElementById(card_name).style.display = "block";
            $('html, body').animate({ scrollTop: $('#'+card_name).offset().top }, 'slow');
        }else{
            var card = document.getElementById(card_name).style.display = "none";
            $('html, body').animate({ scrollTop: $('#'+card_name).offset().top }, 'slow');
        }
    }

    function add_audit(){
        var no_bid = "<?php echo $data->no_bid ; ?>";
        var date_audit = $('#date_audit').val();
        var place_audit = $('#place_audit').val();
        var name_audit1 = $('#name_audit1').val();
        var name_audit2 = $('#name_audit2').val();
        var name_audit3 = $('#name_audit3').val();
        var name_audit4 = $('#name_audit4').val();
        var issue_audit = $('#issue_audit').val();
        var condition_audit = $('#condition_audit').val();

        if(date_audit==""){
            alert('กรุณาระบุวันที่สังเกตการณ์');
            $('#date_audit').focus();
        }else{
            if(name_audit1==""&&name_audit2==""&&name_audit3==""&&name_audit4==""){
                alert('กรุณาระบุผู้สังเกตการณ์');
                $('#name_audit1').focus();
            }else{
                $.post( "<?=site_url('index.php/audit/add_audit_ajax') ?>", {
                    date_audit:date_audit,
                    place_audit:place_audit,
                    name_audit1:name_audit1,
                    name_audit2:name_audit2,
                    name_audit3:name_audit3,
                    name_audit4:name_audit4,
                    issue_audit:issue_audit,
                    condition_audit:condition_audit,
                    no_bid:no_bid
                })
                    .done(function(response) {
                        alert(response);
                        if(response=='บันทึกการสังเกตการณ์ สำเร็จ '){
                            window.close();
                        }
                    });
            }
        }

    }





</script>



<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-plus fa-2x text-gray-300"></i> เพิ่มการสังเกตการณ์</h1>
    <p class="mb-4">ขั้นตอนที่ 2 เพิ่มข้อมูลการสังเกตการณ์ แล้วบันทึก </p>


    <!-- DataTales Example -->
    <div class="card shadow mb-1"  >
        <div class="card-header py-3">
            <div class="container">
                <div class="row">
                    <div class="col col-11">
                        <h6 class="m-0 font-weight-bold text-primary">โครงการ <?php echo $data->name_project; ?></h6>
                    </div>
                    <div class="col col-1" align="right">
                        <a  href='#' title="ย่อ/ขยาย" onclick="minimize('card_project')"><i class='fas fa-window-minimize' ></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body" id="card_project" style="display: none">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">

                    <tr>
                        <td>
                            <label for="cost_project" >งบประมาณโครงการ</label>
                            <input type="text" id="cost_project" name="cost_project" value="<?php echo $data->cost_project; ?> " class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="owner_project" >ผู้รับผิดชอบโครงการ</label>
                            <input type="text" id="owner_project" name="owner_project" value="<?php echo $data->owner_project; ?> " class="form-control" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="objective_project">วัตถุประสงค์โครงการณ์</label>
                            <textarea id="objective_project" name="objective_project" class="form-control" disabled ><?php echo $data->objective_project; ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div  class="card shadow mb-1"  >
        <div class="card-header py-3">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h6 class="m-0 font-weight-bold text-primary">การจัดซื้อจัดจ้าง</h6>
                    </div>
                    <div class="col" align="right">
                        <a  href='#' title="ย่อ/ขยาย" onclick="minimize('card_bid')"><i class='fas fa-window-minimize' ></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body" id="card_bid" style="display: none" >
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td></td>
                        <td>
                            <label for="status_bid">สถานนะการดำเนินการ </label>
                            <div class="input-group mb-3">
                                <input type="text" id="status_bid" value="<?php echo $data->status_bid; ?>"  name="status_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="year_bid">ปีบัญชี </label>
                            <div class="input-group mb-3">
                                <input type="text" id="year_bid" value="ปีบัญชี <?php echo $data->fiscal_year_bid; ?>"  name="year_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <label for="no_bid">เลขที่จัดซื้อจัดจ้าง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_bid" value="<?php echo $data->no_bid; ?>"  name="no_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td width="50%">
                            <label for="date_bid">วันที่จัดซื้อจัดจ้าง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="date_bid" value="วันที่ <?php echo datethai($data->date_bid); ?>"  name="date_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  >
                            <label for="subject_bid">ชื่อการจัดซื้อจัดจ้าง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="subject_bid" value="วันที่ <?php echo $data->name_bid; ?>"  name="subject_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_announce">เลขที่ประกาศ </label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_announce" value="เลขที่ <?php echo $data->no_announce_bid; ?>"  name="no_announce"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td >

                            <label for="date_announce">ลงวันที่ประกาศ</label>
                            <input type="text" id="date_announce" value="วันที่ <?php echo datethai($data->date_announce_bid); ?>" name="date_announce" placeholder="" class="form-control" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="type_bid">วิธีจัดหา </label>
                            <div class="input-group mb-3">
                                <input type="text" id="type_bid" value="<?php echo $data->type_bid; ?>"  name="type_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>
                            <label for="cost_bid">งบประมาณ </label>
                            <div class="input-group mb-3">
                                <input type="text" id="cost_bid" value="<?php echo $data->cost_bid; ?>"  name="cost_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="price_base_bid">ราคากลาง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="price_base_bid" value="<?php echo $data->price_base_bid; ?>"  name="price_base_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>
                            <label for="price_bid">ราคาจริง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="price_bid" value="<?php echo $data->price_bid; ?>"  name="price_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="month_send_draft_contract">เดือน ส่งจัดทำร่างสัญญา </label>
                            <div class="input-group mb-3">
                                <input type="text" id="month_send_draft_contract" value="<?php echo $data->send_draft_contract; ?>"  name="month_send_draft_contract"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>
                            <label for="month_approve_bid">เดือน อนุมัติจัดซื้อจัดจ้าง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="month_approve_bid" value="<?php echo $data->approve_bid; ?>"  name="month_approve_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="no_send_draft_contract">เลขที่สัญญาการอนุมัติ</label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_send_draft_contract" value="<?php echo $data->no_approve_bid; ?>"  name="no_send_draft_contract"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>
                            <label for="no_egp">e-gp</label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_egp" value="<?php echo $data->no_egp; ?>"  name="no_egp"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_contract">เลขที่สัญญา</label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_contract" value="<?php echo $data->no_contract; ?>"  name="no_contract"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td >
                            <label for="date_contract">ลงวันที่สัญญา</label>
                            <div class="input-group mb-3">
                                <input type="text" id="date_contract" value="<?php echo datethai($data->date_contract); ?>"  name="date_contract"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-1"   >
        <div class="card-header py-3">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h6 class="m-0 font-weight-bold text-primary">ส่วนบริษัทที่ซื้อซอง</h6>
                    </div>
                    <div class="col" align="right">
                        <a  href='#' title="ย่อ/ขยาย" onclick="minimize('card_vender')"><i class='fas fa-window-minimize' ></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body" id="card_vender" style="display: none">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <div  class="col-12">
                                <table class='table' width='100%' id="msg_vender">
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-1"  >
        <div class="card-header py-3">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h6 class="m-0 font-weight-bold text-primary">ส่วนเอกสารแนบ</h6>
                    </div>
                    <div class="col" align="right">
                        <a  href='#' title="ย่อ/ขยาย" onclick="minimize('card_attach')"><i class='fas fa-window-minimize' ></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body" id="card_attach" style="display: none">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <div id="show_attract" >
                                <div  style="color:red" class="alert alert-warning" role="alert">***** ยังไม่มีการอัพโหลด</div>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4"  >
        <div class="card-header py-3">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h6 class="m-0 font-weight-bold text-primary">ผู้รับผิดชอบ การจัดซื้อจัดจ้าง</h6>
                    </div>
                    <div class="col" align="right">
                        <a  href='#' title="ย่อ/ขยาย" onclick="minimize('card_submit')"><i class='fas fa-window-minimize' ></i></a>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-body" id="card_submit" style="display: none">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="member" >ผู้รับผิดชอบ</label>
                            <input type="text" id="member" name="member" value="<?php echo $name_member ;?> " class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="date_receive_bid">วันที่รับงาน</label>
                            <input type="text" id="date_receive_bid" value="<?php echo datethai($data->date_receive_bid); ?>" name="date_receive_bid" class="form-control" disabled/>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>




    <!-- DataTales Example -->
    <div class="card shadow mb-4"  >
        <div class="card-header py-3">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h6 class="m-0 font-weight-bold text-primary">เพิ่มการสังเกตการณ์</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body" id="card_audit">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">

                    <tr>
                        <td>
                            <label for="date_audit">วันที่สังเกตการณ์</label>
                            <input type="date" id="date_audit" class="form-control" >
                        </td>
                        <td>
                            <label for="place_audit">สถานที่สังเกตการณ์</label>
                            <select class="form-control" id="place_audit">
                                <option value="ห้องประกวดราคา P401 สำนักจัดซื้อจัดจ้างพัสดุ ธ.ก.ส. สำนักงานใหญ่ อาคารบางเขน">ห้องประกวดราคา P401 สำนักจัดซื้อจัดจ้างพัสดุ ธ.ก.ส. สำนักงานใหญ่ อาคารบางเขน</option>
                                <option value="ห้องประกวดราคา P402 สำนักจัดซื้อจัดจ้างพัสดุ ธ.ก.ส. สำนักงานใหญ่ อาคารบางเขน">ห้องประกวดราคา P402 สำนักจัดซื้อจัดจ้างพัสดุ ธ.ก.ส. สำนักงานใหญ่ อาคารบางเขน</option>
                                <option value="ห้องประกวดราคา P403 สำนักจัดซื้อจัดจ้างพัสดุ ธ.ก.ส. สำนักงานใหญ่ อาคารบางเขน">ห้องประกวดราคา P403 สำนักจัดซื้อจัดจ้างพัสดุ ธ.ก.ส. สำนักงานใหญ่ อาคารบางเขน</option>
                                <option value="ห้องประกวดราคา P404 สำนักจัดซื้อจัดจ้างพัสดุ ธ.ก.ส. สำนักงานใหญ่ อาคารบางเขน">ห้องประกวดราคา P404 สำนักจัดซื้อจัดจ้างพัสดุ ธ.ก.ส. สำนักงานใหญ่ อาคารบางเขน</option>
                                <option value="ไม่ได้เข้าสังเกตการณ์">ไม่ได้เข้าสังเกตการณ์</option>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="direction_audit">ตามบันทึก</label>
                            <input type="text" id="direction_audit" class="form-control" >
                        </td>
                        <td>
                            <label for="date_direction_audit">ลงวันที่</label>
                            <input type="date" id="date_direction_audit" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="issue_audit" >ประเด็น</label>
                            <textarea id="issue_audit" name="issue_audit" class="form-control"  ></textarea>
                        </td>
                        <td>
                            <label for="condition_audit">ข้อตกลงเพิ่มเติม</label>
                            <textarea id="condition_audit" name="condition_audit" class="form-control"  ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="name_audit1" >ผู้สังเกตการณ์</label>
                            <select class="form-control" id="name_audit1">
                                <option value="4001210">นายนิคม คุ้มตลอด</option>
                                <option value="5300507">นายประมุข ปาประโคน</option>
                                <option value="5200244">นายศานติ วิทยเวช</option>
                                <option value="5401173" >นายณัฐพงษ์ ภู่ระย้า</option>
                                <option value="5801142">นายสุธนัตต์ มงคลชาติ</option>
                                <option value="6200002">นายอาดัม จิกิตศิลปิน</option>
                                <option value="" selected>เลือกผู้สังเกตการณ์</option>
                            </select>
                        </td>
                        <td>
                            <label for="name_audit2" >ผู้สังเกตการณ์</label>
                            <select class="form-control" id="name_audit2">
                                <option value="4001210">นายนิคม คุ้มตลอด</option>
                                <option value="5300507">นายประมุข ปาประโคน</option>
                                <option value="5200244">นายศานติ วิทยเวช</option>
                                <option value="5401173" >นายณัฐพงษ์ ภู่ระย้า</option>
                                <option value="5801142">นายสุธนัตต์ มงคลชาติ</option>
                                <option value="6200002">นายอาดัม จิกิตศิลปิน</option>
                                <option value="" selected>เลือกผู้สังเกตการณ์</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="name_audit3" >ผู้สังเกตการณ์</label>
                            <select class="form-control" id="name_audit3">
                                <option value="4001210">นายนิคม คุ้มตลอด</option>
                                <option value="5300507">นายประมุข ปาประโคน</option>
                                <option value="5200244">นายศานติ วิทยเวช</option>
                                <option value="5401173" >นายณัฐพงษ์ ภู่ระย้า</option>
                                <option value="5801142">นายสุธนัตต์ มงคลชาติ</option>
                                <option value="6200002">นายอาดัม จิกิตศิลปิน</option>
                                <option value="" selected>เลือกผู้สังเกตการณ์</option>
                            </select>
                        </td>
                        <td>
                            <label for="name_audit4" >ผู้สังเกตการณ์</label>
                            <select class="form-control" id="name_audit4">
                                <option value="4001210">นายนิคม คุ้มตลอด</option>
                                <option value="5300507">นายประมุข ปาประโคน</option>
                                <option value="5200244">นายศานติ วิทยเวช</option>
                                <option value="5401173" >นายณัฐพงษ์ ภู่ระย้า</option>
                                <option value="5801142">นายสุธนัตต์ มงคลชาติ</option>
                                <option value="6200002">นายอาดัม จิกิตศิลปิน</option>
                                <option value="" selected>เลือกผู้สังเกตการณ์</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button class="btn btn-primary" type="button" onclick="add_audit()">บันทึก</button> <button  class="btn btn-danger" onclick="window.close()" >ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->








