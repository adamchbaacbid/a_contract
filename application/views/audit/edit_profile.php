<?php


$data = $member;


?>
<script>

    function ajax_edit_member(){

        var id_member = $('#id_member').val();
        var name_member = $('#name_member').val();
        var position_member = $('#position_member').val();
        var username = $('#username').val();
        var agency_member = $('#agency_member').val();
        var email_member = $('#email_member').val();
        var tell_member = $('#tell_member').val();



        if(id_member==""){
            alert('กรุณาระบุรหัสพนักงาน');
            $('#id_member').focus();
        }else{
            if(name_member==""){
                alert('กรุณาระบุ ชื่อ นามสกุล ของผู้ใช้งาน');
                $('#name_member').focus();
            }else{
                if(position_member==""){
                    alert('กรุณาเลือกตำแหน่ง');
                    $('#position_member').focus();
                }else{

                            $.post( "<?=site_url('index.php/login_controller/edit_member_ajax') ?>", {
                                id_member:id_member,
                                name_member:name_member,
                                position_member:position_member,
                                username:username,
                                agency_member:agency_member,
                                email_member:email_member,
                                tell_member:tell_member


                            }).done(function( test ) {
                                alert( test );
                                location.reload();
                            });
                }
            }
        }


    }


</script>




<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-user fa-2x text-gray-300"></i>  แก้ไขข้อมูลส่วนตัว</h1>
    <p class="mb-4"></p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">แก้ไขข้อมูล</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="id_member">รหัสพนักงาน</label>
                            <input type="number" id="id_member" name="id_member" value="<?=$data['employee_id_member'] ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==7) return false;" placeholder="รหัสพนักงาน" class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="name_member">ชื่อ นามสกุล </label>
                            <input type="text" id="name_member" placeholder="" value="<?=$data['name_member'] ?>" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="position_member">ตำแหน่ง</label>
                            <input type="text" id="position_member" class="form-control"  value="<?=$data['position_member'] ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="agency_member">ส่วนงาน</label>
                            <input type="text" id="agency_member" class="form-control"  value="<?=$data['agency_member'] ?>">

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="email_member">อีเมล์</label>
                            <input type="text" id="email_member" class="form-control"  value="<?=$data['email_member'] ?>">

                        </td>
                        <td>
                            <label for="tell_member">เบอร์โทร</label>
                            <input type="text" id="tell_member" class="form-control"  value="<?=$data['tell_member'] ?>">

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="username">ชื่อสำหรับเข้าใช้งานระบบ</label>
                            <input type="text" id="username"  value="<?=$data['user_member'] ?>"  class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="status_member">สถานะ</label>
                            <input type="text" id="status_member" value="<?=$data['status_member'] ?>"    class="form-control" disabled/>

                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button type="submit" class="btn btn-success" onclick="ajax_edit_member()">แก้ไข</button>
                            <button type="reset" class="btn btn-danger" onclick="history.back()" >ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->
