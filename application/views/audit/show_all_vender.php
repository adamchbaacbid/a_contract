
<script>

    function de_vender(id_vender,name_vender){
        var r = confirm("ต้องการลบ"+name_vender+" ใช่หรือไม่");
        if(r== true){
            $.post( "<?=site_url('index.php/audit/delete_vender') ?>", {
                id_vender:id_vender,
                name_vender:name_vender
            })
                .done(function( data ) {
                    alert( data );
                    if(data=='ลบเรียบร้อยแล้ว'){
                        location.reload();
                    }
                });
        }
    }



    function edit_vender(id_vender){
        $('#form_edit_vender'+id_vender).submit();
    }


</script>


<?php

    $session = $this->session->userdata('data');
    $privilege_member = $session['privilege_member'];

    if($privilege_member=="user"){
        $hid = 'style="display: none"';
    }else{
        $hid = "";
    }

function datethai($strDate){
    if($strDate=="0000-00-00"){
        return "ไม่ได้ระบุวันที่";
    }else{
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"> <i class="fas fa-table fa-2x text-gray-300"></i> โครงการการจัดซื้อจัดจ้างทั้งหมด</h1>
    <p class="mb-4">โครงการการจัดซื้อจัดจ้าง </p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">โครงการทั้งหมด จำนวน  <?php echo $numshow_committee_bid_ajax ; ?> โครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">ชื่อบริษัท</th>
                        <th scope="col">งบประมาณจัดตั้งบริษัท</th>
                        <th scope="col" <?php echo $hid ; ?>></th>
                        <th scope="col" <?php echo $hid ; ?>></th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;

                    foreach ($all_vender as $row){
                        $id_vender = $row->id_vender;
                        $name_vender = $row->name_vender ;

                        echo "<tr>
                                <td scope='row' width='5%'>$i</td>                                                         
                                <td  width='50%'>$name_vender</td>
                                <td  width='40%'>$row->capital_vender บาท</td> 
                                <td $hid >
                                    <form id='form_edit_vender$id_vender' action='".base_url("index.php/audit/edit_vender")."' target='_blank' method='post' >
                                    <input id='id_vender' name='id_vender' value='$id_vender' type='hidden' >
                                    <a href='# ' onclick='edit_vender($id_vender)' title='แก้ไข' class='btn btn-success '  >
                                        <i class='fas fa-edit'></i>
                                    </a>
                                    </form>
                                </td>
                                <td $hid>                                
                                    <a href='#' title='ลบ' onclick=\"de_vender('$id_vender','$name_vender')\" class='btn btn-danger '>
                                        <i class='fas fa-trash'></i>
                                    </a>
                                </td>
                             </tr>" ;
                        $i++;

                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




