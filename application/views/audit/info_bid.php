<?php
$member = $this->session->userdata('data');
$name = $member['name'];

$arr = $bid['bid'];
$data = $arr['0'];
$member_receive = $bid['name_member'];



function datethai($strDate){
    if($strDate=="0000-00-00"){
        return "ไม่ได้ระบุวัน";
    }else{
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

}
?>
<script >
    function format_price(n) { ///////function for price
        return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+" บาท";
    }

    window.onload = function(){
        show_attract_ajax();
        show_vender_ajax();
        show_committee_bid_ajax();
    };

    function show_committee_bid_ajax(){
        var no_bid =  $('#no_bid').val();
        var edit = "edit";
        $.post( "<?=site_url('index.php/audit/show_committee_bid_ajax') ?>", {
            no_bid:no_bid,
            edit:edit
        })
            .done(function( response ) {
                $('#show_committee').html(response);
            });
    }


    function show_attract_ajax(){
        var no_bid =  $('#no_bid').val();
        var info = "info";
        //alert(no_bid);
        $.post( "<?=site_url('index.php/audit/show_attract_ajax') ?>", {
            no_bid:no_bid,
            info:info
        })
            .done(function( response ) {
                $('#show_attract').html(response);
                $('#delete_attract').hide();
            });
    }

    function show_vender_ajax(){
        $('#msg_vender').empty();
        var no_bid =  $('#no_bid').val();
        if(no_bid==""){
            alert('ไม่พบ เลขที่จัดซื้อจัดจ้าง');
            $('#no_bid').focus();
        }else {
            //alert(no_bid);
            $.post("<?=site_url('index.php/audit/show_vender_ajax') ?>", {
                no_bid: no_bid
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    var price_base_bid = "<?php echo $data->price_base_bid; ?>";

                    var i = 1;
                    var status = "";
                    var msg_vender = $('#msg_vender'), container;

                    msg_vender.append("<thead><tr><th>ลำดับ</th><th>บริษัท</th><th>ราคาที่ยืน</th><th>ราคาหลังการต่อรอง</th><th>จำนวนที่ลดลง</th><th>ลดจากราคากลางร้อยละ</th><th>สถานะ</th></tr></thead>");
                    for (var key in arr) {
                        var send = arr[key].sender_active_vender;
                        var pass = arr[key].pass_active_vender;
                        var win = arr[key].win_active_vender;


                        var price_active_vender = arr[key].price_active_vender;
                        var price = arr[key].chaffer_active_vender;

                        if(price==0){
                            var un = price_base_bid - price_active_vender*1;
                            var sale = ((price_base_bid - price_active_vender)*100)/price_base_bid ;
                        }else{
                            var un = price_base_bid - price*1;
                            var sale = ((price_base_bid - price)*100)/price_base_bid ;
                        }
                        price = price*1;
                        price_active_vender = price_active_vender*1;
                        un = un * 1;

                        var p_a_v = "-" ;
                        var p = "-";
                        var u = "-";
                        var s = "-";

                        if (send == 0 && pass == 0 && win == 0) {
                            status = "ไม่ได้ยื่นซอง";
                            var p_a_v = "-" ;
                            var p = "-";
                            var u = "-";
                            var s = "-";
                        } else {
                            if (send == 1 && pass == 0 && win == 0) {
                                status = "คุณสมบัติไม่ผ่าน";
                                var p_a_v = "-" ;
                                var p = "-";
                                var u = "-";
                                var s = "-";
                            } else {
                                if (send == 1 && pass == 1 && win == 0) {
                                    status = "ไม่ชนะ";

                                    var p_a_v = format_price(price_active_vender) ;
                                    var p = "-";
                                    var u = "-";
                                    var s = "-";
                                } else {
                                    if (send == 1 && pass == 1 && win == 1) {
                                        status = "ชนะ";

                                        var p_a_v = format_price(price_active_vender) ;
                                        var p = format_price(price);
                                        var u = format_price(un);
                                        var s = sale.toFixed(2);

                                    }
                                }
                            }
                        }


                        container = $('<tr></tr>');
                        msg_vender.append(container);
                        container.append('<td>' + i + '</td>');
                        container.append('<td>' + arr[key].name_vender + '</td>');
                        container.append('<td>' + p_a_v + '</td>');
                        container.append('<td>' + p + '</td>');
                        container.append('<td>' + u +'</td>');
                        container.append('<td>' + s + '</td>');
                        container.append('<td>' + status + '</td>');
                         i++;
                    }
                });
        }

    }
</script>





<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-book-reader fa-2x text-gray-300"></i> แสดงการจัดซื้อจัดจ้างพัสดุ</h1>
    <p class="mb-4"></p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_submit" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">โครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <pre><?php //print_r($data) ;  ?></pre>
                            <label for="input_name_project">ชื่อโครงการ  </label>
                            <div class="input-group mb-3">
                                <input  type="text" id="input_name_project" value="<?php echo $data->name_project; ?>"  name="input_name_project"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="cost_project" >งบประมาณโครงการ</label>
                            <input type="text" id="cost_project" name="cost_project" value="<?php echo number_format($data->cost_project); ?>  บาท " class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="owner_project" >ผู้รับผิดชอบโครงการ</label>
                            <input type="text" id="owner_project" name="owner_project" value="<?php echo $data->owner_project; ?> " class="form-control" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="objective_project">วัตถุประสงค์โครงการ</label>
                            <textarea id="objective_project" name="objective_project" class="form-control" disabled ><?php echo $data->objective_project; ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div id="card_bid" class="card shadow mb-4"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">การจัดซื้อจัดจ้าง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td></td>
                        <td>
                            <label for="status_bid">สถานนะการดำเนินการ </label>
                            <div class="input-group mb-3">
                                <input type="text" id="status_bid" value="<?php echo $data->status_bid; ?>"  name="status_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="year_bid">ปีบัญชี </label>
                            <div class="input-group mb-3">
                                <input type="text" id="year_bid" value="ปีบัญชี <?php echo $data->fiscal_year_bid; ?>"  name="year_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <label for="no_bid">เลขที่จัดซื้อจัดจ้าง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_bid" value="<?php echo $data->no_bid; ?>"  name="no_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td width="50%">
                            <label for="date_bid">วันที่จัดซื้อจัดจ้าง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="date_bid" value="<?php echo datethai($data->date_bid); ?>"  name="date_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  >
                            <label for="subject_bid">ชื่อการจัดซื้อจัดจ้าง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="subject_bid" value="<?php echo $data->name_bid; ?>"  name="subject_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_announce">เลขที่ประกาศ </label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_announce" value="เลขที่ <?php echo $data->no_announce_bid; ?>"  name="no_announce"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td >

                            <label for="date_announce">ลงวันที่ประกาศ</label>
                            <input type="text" id="date_announce" value="<?php echo datethai($data->date_announce_bid); ?>" name="date_announce" placeholder="" class="form-control" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="type_bid">วิธีจัดหา </label>
                            <div class="input-group mb-3">
                                <input type="text" id="type_bid" value="<?php echo $data->type_bid; ?>"  name="type_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>
                            <label for="cost_bid">งบประมาณ </label>
                            <div class="input-group mb-3">
                                <input type="text" id="cost_bid" value="<?php echo number_format($data->cost_bid); ?>  บาท"  name="cost_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="price_base_bid">ราคากลาง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="price_base_bid" value="<?php echo number_format($data->price_base_bid); ?> บาท"  name="price_base_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>
                            <label for="price_bid">ราคาจริง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="price_bid" value="<?php echo number_format($data->price_bid); ?>  บาท"  name="price_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="month_send_draft_contract">เดือน ส่งจัดทำร่างสัญญา </label>
                            <div class="input-group mb-3">
                                <input type="text" id="month_send_draft_contract" value="<?php echo $data->send_draft_contract; ?>"  name="month_send_draft_contract"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>
                            <label for="month_approve_bid">เดือน อนุมัติจัดซื้อจัดจ้าง </label>
                            <div class="input-group mb-3">
                                <input type="text" id="month_approve_bid" value="<?php echo $data->approve_bid; ?>"  name="month_approve_bid"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="no_send_draft_contract">เลขที่สัญญาการอนุมัติ</label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_send_draft_contract" value="<?php echo $data->no_approve_bid; ?>"  name="no_send_draft_contract"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td>
                            <label for="no_egp">e-gp</label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_egp" value="<?php echo $data->no_egp; ?>"  name="no_egp"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_contract">เลขที่สัญญา</label>
                            <div class="input-group mb-3">
                                <input type="text" id="no_contract" value="<?php echo $data->no_contract; ?>"  name="no_contract"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                        <td >
                            <label for="date_contract">ลงวันที่สัญญา</label>
                            <div class="input-group mb-3">
                                <input type="text" id="date_contract" value="<?php echo datethai($data->date_contract); ?>"  name="date_contract"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_committee"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">คณะกรรมการประกวดราคา</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td >
                            <label for="no_direct_committee">คำสั่ง ที่</label>
                            <input type="text" id="no_direct_committee" name="no_direct_committee" value="<?php echo $data->no_direct_committee; ?>" class="form-control" disabled>
                        </td>
                        <td >
                            <label for="date_direct_committee">ลงวันที่</label>
                            <input type="text" id="date_direct_committee" name="date_direct_committee" value="<?php echo datethai($data->date_direct_committee); ?>" class="form-control" disabled>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="subject_direct_committee">เรื่อง</label>
                            <input type="text" id="subject_direct_committee" name="subject_direct_committee" value="<?php echo $data->subject_direct_committee; ?>" class="form-control" disabled>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div id="show_committee">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_vender"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนบริษัทที่ซื้อซอง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <div  class="col-12">
                                <table class='table' width='100%' id="msg_vender">
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_attach" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนเอกสารแนบ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <div id="show_attract" >
                                <div  style="color:red" class="alert alert-warning" role="alert">***** ยังไม่มีการอัพโหลด</div>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_submit" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ผู้รับผิดชอบ การจัดซื้อจัดจ้าง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="member" >ผู้รับผิดชอบ</label>
                            <input type="text" id="member" name="member" value="<?php echo $member_receive ;?> " class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="date_receive_bid">วันที่รับงาน</label>
                            <input type="text" id="date_receive_bid" value="<?php echo datethai($data->date_receive_bid); ?>" name="date_receive_bid" class="form-control" disabled/>
                        </td>
                    </tr>
                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button  class="btn btn-danger" onclick="window.close()" >ปิด</button>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->








