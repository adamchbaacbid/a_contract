<?php
$session = $this->session->userdata('data');
$privilege_member = $session['privilege_member'];

if($privilege_member=="user"){
    $hid = 'style="display: none"';
}else{
    $hid = "";
}
?>
<script>

    function date_thai(date){
        var day = date.substr(8,9);
        var month = date.substr(5,2);
        var year = date.substr(0,4);
        day = +day;
        year = +year;
        month = +month-1;

        now = new Date();
        var thday = new Array ("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัส","ศุกร์","เสาร์");
        var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
        var thm = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        var datethai = day+" "+thm[month]+" "+(year+543);

        return datethai;
    }

    function format_price(n) { ///////function for price
        return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+" บาท";
    }

    function info_bid_button(no_bid){

        $('#form_info_'+no_bid).submit();
    }
    function edit_bid(no_bid){

        $('#form_edit_'+no_bid).submit();
    }

    function search_bid(){
        var name_vender = $('#name_vender').val();
        var before = $('#before').val();
        var after = $('#after').val();

        $.post( "<?=site_url('index.php/audit/show_bid_by_vender_ajax') ?>", {
            name_vender:name_vender,
            before:before,
            after:after
        })
            .done(function( response ) {
                var arr = JSON.parse(response);

                $('#dataTable').empty();
                var msg_table = $('#dataTable'), container;
                var i = 1 ;

                msg_table.append("<thead>\n" +
                    "<tr>\n" +
                    "<th scope=\"col\">#</th>\n" +
                    "<th scope=\"col\">วันที่จัดซื้อจัดจ้าง</th>\n" +
                    "<th scope=\"col\">โครงการ</th>\n" +
                    "<th scope=\"col\">ราคากลาง</th>\n" +
                    "<th scope=\"col\">ราคาเสนอครั้งสุดท้าย</th>\n" +
                    "<th scope=\"col\">สถานะ</th>\n" +
                    "<th scope=\"col\"></th>\n" +
                    "<th scope=\"col\"></th>\n" +
                    "</tr>\n" +
                    "</thead>");

                body = $('<tbody></tbody>');
                msg_table.append(body);

                for (var key in arr) {
                    var url_edit = '<?php echo base_url('index.php/audit/edit_bid');?>';
                    var url_info = '<?php echo base_url('index.php/audit/info_bid');?>';

                    container = $('<tr></tr>');
                    body.append(container);
                    container.append('<form action="'+url_edit+'" id="form_edit_'+arr[key].no_bid+'" target="_blank" method="post"><input type="text" id="no_bid" name="no_bid" value="'+arr[key].no_bid+'" hidden> </form>');
                    container.append('<form action="'+url_info+'" id="form_info_'+arr[key].no_bid+'" target="_blank" method="post"><input type="text" id="info_no_bid" name="info_no_bid" value="'+arr[key].no_bid+'" hidden> </form>');
                    var d = arr[key].date_bid;
                    d = date_thai(d);
                    var price_base_bid = arr[key].price_base_bid;
                    price_base_bid = +price_base_bid;
                    var price_bid = arr[key].price_bid;
                    price_bid = +price_bid;

                    container.append('<td scope=\'row\' width=\'5%\'>' + i + '</td>');
                    container.append('<td  width=\'15%\'>' + d + '</td>');
                    container.append('<td  width=\'40%\' >' + arr[key].name_project + '</td>');
                    container.append('<td  width=\'15%\'>' + format_price(price_base_bid) + '</td>');
                    container.append('<td  width=\'15%\'>' + format_price(price_bid) + '</td>');
                    container.append('<td>' + arr[key].status_bid + '</td>');
                    container.append('<td><a href="#" onclick="info_bid_button('+arr[key].no_bid+')"> <i class="fas fa-info-circle"></i></a></td>');
                    container.append('<td><a href="#" <?php echo $hid; ?> onclick="edit_bid('+arr[key].no_bid+')"> <i class="fas fa-edit"></i></a></td>');
                    i++;
                }
                i=i-1;

                $('#show_name_vender').html(name_vender+' (มีจำนวน '+i+' โครงการ)');
            });
    }

    function info_bid(no_bid){
        $('#form_info_bid'+no_bid).submit();
    }

    function show_name_vender() {
        $("#vender_list").empty();
        $.post("<?=site_url('index.php/audit/show_name_vender') ?>", {
        })
            .done(function (response) {
                var arr = JSON.parse(response);
                //alert(response);
                $.each(arr, function(id,name) {
                    var option = $('<option value="'+name+'"></option>');
                    $('#vender_list').append(option);
                });
            });
    }




</script>




<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-search fa-2x text-gray-300"></i> ค้นหาการจัดซื้อจัดจ้าง</h1>
    <p class="mb-4"></p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> ค้นหาการจัดซื้อจัดจ้าง ตามชื่อบริษัท  </h6>
        </div>
        <div class="card-body">
            <div class="row ">
                <div class=" col-sm-12 col-lg-6">
                    <div class="input-group">
                        <input list="vender_list" id="name_vender" onfocus="show_name_vender()" name="name_vender"  placeholder="ชื่อบริษัท" class="form-control">
                        <datalist id="vender_list">
                        </datalist>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" onclick="search_bid()" type="button" id="search_bid">ค้นหา</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div  class=" col-6">
                    จากวันวันที่ <input type="date" class="form-control" id="before">
                </div>
                <div  class=" col-6">
                    ถึง <input type="date" class="form-control" id="after">
                </div>

            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3" >
            <h6 class="m-0 font-weight-bold text-primary"  > ผลการค้นหา : <span id="show_name_vender" class="m-0 font-weight-bold text-danger"></span></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive" >
                <table class="table table-striped " id="dataTable"  width="100%" cellspacing="0" >
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">วันที่ประกวดราคา</th>
                            <th scope="col">โครงการ</th>
                            <th scope="col">ราคากลาง</th>
                            <th scope="col">ราคาเสนอครั้งสุดท้าย</th>
                            <th scope="col">ลดลง</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr><td colspan='8' align='center'>ยังไม่มีการค้นหา</td></tr>
                    </tbody>
                </table>
                <div ><pre id="test_json"></pre></div>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




