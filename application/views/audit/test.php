<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>area > datasets | Chart.js sample</title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('') ;?>theme/vendor/chart.js/style.css">

    <script src="<?php echo base_url('') ;?>theme/vendor/chart.js/Chart.min.js"></script>
    <script src="<?php echo base_url('') ;?>theme/vendor/chart.js/utils.js"></script>
    <script src="<?php echo base_url('') ;?>theme/vendor/chart.js/analyser.js"></script>
</head>
<body>
<div class="content">
    <div class="wrapper">
        <canvas id="chart-0"></canvas>
    </div>
    <div class="toolbar">
        <button onclick="togglePropagate(this)">Propagate</button>
        <button onclick="toggleSmooth(this)">Smooth</button>
        <button onclick="randomize(this)">Randomize</button>
    </div>
    <div id="chart-analyser" class="analyser"></div>
</div>

<script>
    var presets = window.chartColors;
    var utils = Samples.utils;
    var inputs = {
        min: 20,
        max: 10000000,
        count: 12,//เดือน
        decimals: 2,
        continuity: 1
    };

    function generateData1() {
        //return [10,20,30,40,50,60,70,80,90,10,90,80];
        return utils.numbers(inputs);
    }

    function generateData2() {
        return [39.64,66.6,82.61,79.78,29.45,77.81,74.73,36.01,98.94,51.16,32.94,92.66];
    }

    function generateData3() {
        return [100,30,58,78,29.45,77.81,74.73,36.01,98.94,51.16,32.94,92.66];
    }

    function generateLabels() {
        return ['เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม','มกราคม','กุมภาพันธ์','มีนาคม'];
        //return utils.months({count: inputs.count});
    }

    //utils.srand(42);

    var data = {
        labels: generateLabels(),
        datasets: [ {
            backgroundColor: utils.transparentize(presets.orange),
            borderColor: presets.orange,
            data: generateData1(),
            label: '2561',
            fill: false
        }, {
            backgroundColor: utils.transparentize(presets.yellow),
            borderColor: presets.yellow,
            data: generateData2(),
            label: '2562',
            fill: false
        }, {
            backgroundColor: utils.transparentize(presets.green),
            borderColor: presets.green,
            data: generateData3(),
            label: '2563',
            fill: false
        }]
    };

    var options = {
        maintainAspectRatio: false,
        spanGaps: false,
        elements: {
            line: {
                tension: 0.000001
            }
        },
        scales: {
            y: {
                stacked: true,
            }
        },
        plugins: {
            filler: {
                propagate: false
            },
            'samples-filler-analyser': {
                target: 'chart-analyser'
            }
        }
    };

    var chart = new Chart('chart-0', {
        type: 'line',
        data: data,
        options: options
    });

    // eslint-disable-next-line no-unused-vars
    function togglePropagate(btn) {
        var value = btn.classList.toggle('btn-on');
        chart.options.plugins.filler.propagate = value;
        chart.update();
    }

    // eslint-disable-next-line no-unused-vars
    function toggleSmooth(btn) {
        var value = btn.classList.toggle('btn-on');
        chart.options.elements.line.tension = value ? 0.4 : 0.000001;
        chart.update();
    }

    // eslint-disable-next-line no-unused-vars
    function randomize() {
        chart.data.datasets.forEach(function(dataset) {
            dataset.data = generateData();
        });
        chart.update();
    }
</script>


</body>
</html>