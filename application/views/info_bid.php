

<?php  $bid = $bid['0'];

$cost_bid = $bid->cost_bid;

function datethai($strDate){
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}
?>



<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-book-reader fa-2x text-gray-300"></i> แสดงบันทึกการสังเกตุการณ์จัดซื้อจัดจ้างพัสดุ</h1>
    <p class="mb-4"></p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">แสดงบันทึก การสังเกตการณ์</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button  class="btn btn-danger" onclick="history.back()" >ปิด</button>

                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td></td>
                        <td>
                            <label for="status_bid">สถานะการสังเกตุการณ์</label>
                            <input disabled type="text" id="status_bid"  name="status_bid" value="<?php echo $bid->status_bid ; ?>" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <label for="no_bid">บันทึกเลขที่</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text">ICT/</span>
                                <input disabled type="text" id="no_bid"  name="no_bid" maxlength="6" max="6" value="<?php echo $bid->no_bid ; ?>" class="form-control"/>
                            </div>
                        </td>
                        <td width="50%">
                            <label for="date_bid">วันที่ประกวดราคา</label>
                            <input type="text" id="date_bid" name="date_bid" value="<?php echo datethai($bid->date_bid) ; ?>" class="form-control" disabled>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  >
                            <p>
                                <label for="subject_bid">ชื่อโครงการ</label>
                                <input type="text" id="subject_bid" name="subject_bid"value="<?php echo $bid->subject_bid ; ?>" class="form-control" disabled/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="year_bid">ปีบัญชี</label>
                            <input type="text" class="form-control" id="year_bid" value="<?php echo $bid->fiscal_year_bid ; ?>" disabled>

                        </td>
                        <td>
                            <label for="tri_bid">ไตรมาส</label>
                            <input type="text" id="tri_bid" class="form-control" value="ไตรมาส <?php echo $bid->tri_bid ; ?>" disabled>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="type_bid"  >ประเภทการประกวดราคา</label>
                            <input type="text" class="form-control" id="type_bid" value="<?php echo $bid->type_bid ; ?>" disabled>
                        </td>
                        <td>
                            <label for="cost_bid">ราคากลาง</label>
                            <input type="text" id="cost_bid" name="cost_bid" placeholder="จำนวนเงิน " value="<?php echo number_format($bid->cost_bid) ; ?> บาท" class="form-control" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="issues_bid">ประเด็นที่การจัดซื้อจัดจ้างพัสดุ</label>
                            <textarea disabled class="form-control" rows="5" id="issues_bid" name="issues_bid" ><?php echo $bid->issues_bid ; ?></textarea>
                        </td>
                        <td>
                            <label for="condition_bid">ข้อตกลงเพิ่มเติม</label>
                            <textarea disabled class="form-control" rows="5" id="condition_bid" name="condition_bid"><?php echo $bid->condition_bid ; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="type_bid"  >แยกจ่ายตามทรัพย์สิน และMA</label>
                            <input id="type_bid" type="text" class="form-control" value="<?php echo $bid->devide_bid ; ?>" disabled>

                        </td>
                        <td>
                        </td>
                    </tr>
                    <?php
                    $devide = $bid->devide_bid;
                    if($devide=='ไม่แยก'){
                        $style = "display: none";
                    }else{
                        $style = "display: inline";
                    }
                    ?>
                    <tr id="tr_asset"  >

                        <td >
                            <div id="td_asset" style="<?=$style ?>">
                                <label for="price_asset"  >ราคาทรัพย์สิน</label>
                                <input disabled type="text" id="price_asset" value="<?php echo number_format($bid->price_asset) ; ?> บาท" class="form-control">
                            </div>
                        </td>
                        <td >
                            <div id="td_ma" style="<?=$style ?>">
                                <label for="price_ma"  >ราคา MA</label>
                                <input disabled type="text" id="price_ma" value="<?php echo number_format($bid->price_ma) ; ?> บาท" class="form-control">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <label for="period_bid">วันที่หมดสัญญา</label>
                            <input type="text" id="period_bid" name="period_bid" value="<?php echo $bid->period_bid ; ?>" class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="no_contract">เลขที่สัญญา</label>
                            <input type="text" id="no_contract" name="no_contract" value="<?php echo $bid->no_contract ; ?>" class="form-control" >
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนที่มาการแยกประกวดราคาย่อย</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <div id="show_sub">

                    <!-- Divider -->
                    <br>
                    <hr class="sidebar-divider my-0">
                    <br>
                    <!-- END Divider -->

                    <div id="msg_sub_bid">
                        <?php

                        if(isset($sub_bid)){
                            if($sub_bid) {

                                foreach ($sub_bid as $row) {

                                    $cost_sub = $row->cost_sub_bid;
                                    $no_bid_for_sub = $row->no_bid;
                                    $no_sub_bid = $row->no_sub_bid;

                                    echo '
                            <div class="card shadow mb-4">
                            <div class="card-header py-2">
                            <div class="row">
                                <div class="col-sm-12"><h6 class="m-0 font-weight-bold text-black-50">' . $row->no_sub_bid . ' ' . $row->name_sub_bid . '<input id="id_sub_bid" value="' . $row->id_sub_bid . '" hidden> </h6></div>
                                
                                 
                            </div>
                            </div>
                            <div class="card-body">
                                <div  align="center">
                                    <table class="table-responsive" width="100%">
                                        <tr>
                                            <td align="right" width="40%">ราคากลาง : </td>
                                            <td width="60%">' . number_format($cost_sub, 2) . ' บาท</td>
                                        </tr>
                                        <tr>
                                            <td align="right"  valign="top">ประเด็นที่การจัดซื้อจัดจ้างพัสดุ : </td>
                                            <td  valign="top">' . $row->issues_sub_bid . '</td>
                                        </tr>
                                        <tr>
                                            <td align="right"  valign="top">ข้อตกลงเพิ่มเติม : </td>
                                            <td  valign="top">' . $row->condition_sub_bid . '</td>
                                        </tr>
                                        <tr>
                                            <td align="right">สถานะ : </td>
                                            <td>' . $row->status_sub_bid . '</td>
                                        </tr>
                                        <tr>
                                            <td align="right">แยกตามค่าอุปกรณื และ MA : </td>
                                            <td>' . $row->devide_sub_bid . '</td>
                                        </tr>
                                    </table>
                                </div>
                                <br>
                                <div class="col-12">
                                    <label for="vender" >บริษัท ที่ยื่นซอง' . $row->name_sub_bid . '</label>
                                    
                                    <table class=\'table table-bordered\'>
                                             <thead >
                                                <tr >
                                                  <th >บริษัท</th>
                                                  <th>สถานะ</th>
                                                  <th >ราคาที่ยืน</th>
                                                  <th >ราคาหลังการต่อรอง</th>
                                                  <th >ลดจากราคากลาง</th>
                                                  <th >ลดลง</th>                                                  
                                                </tr>
                                             </thead>
                                             <tbody >    
                                    ';

                                    if (isset($vender_sub_bid)) {
                                        if($vender_sub_bid){
                                            $id_vender = "";

                                            foreach ($vender_sub_bid as $vender) {

                                                while ($vender->no_sub_bid == $no_sub_bid && $vender->id_vender != $id_vender) {
                                                    $id_vender = $vender->id_vender;
                                                    $price_vender = $vender->price_vender;
                                                    $price = $cost_sub - $price_vender;
                                                    $per = ($price * 100) / $cost_sub;
                                                    $no_vender = $vender->id_vender;
                                                    if ($vender->win_vender == 1) {
                                                        $status = "ชนะการประกวดราคา";
                                                    } elseif ($vender->sender_vender == 1 && $vender->price_vender == 0 && $vender->pass_vender == 0) {
                                                        $status = "ไม่ผ่านคุณสมบัติ";
                                                    } elseif ($vender->sender_vender == 1 && $vender->price_vender != 0) {
                                                        $status = "ยื่นซอง";
                                                    } elseif ($vender->sender_vender == 0) {
                                                        $status = "ไม่ได้ยื่นซอง";
                                                    }

                                                    echo "
                                                         <tr >
                                                           <td >" . $vender->name_vender . "</td>
                                                           <td>$status</td>
                                                           <td >" . number_format($vender->price_vender, 2) . "</td>
                                                           <td >" . number_format($vender->price_under_vender, 2) . "</td>
                                                           <td >" . number_format($price, 2) . "</td>
                                                           <td >" . number_format($per, 2) . "%</td>
                                                           
                                                         </tr>
                                                    ";
                                                }
                                            }
                                        }else {
                                        echo "
                                             <tr >
                                               <td colspan='7'>ไม่พบบริษัท</td>
                                             </tr>
                                              ";
                                        }

                                    } else {
                                        echo "
                                         <tr >
                                           <td colspan='7'>ไม่พบบริษัท</td>
                                         </tr>
                                          ";
                                    }

                                    echo '
                                                </tbody>
                                          </table>
                                </div>


                                <!-- Divider -->
                                <br>
                                <hr class="sidebar-divider my-0">
                                <br>
                                <!-- END Divider -->
                            </div>
                        </div>            
            ';

                                }
                            }
                        }else{
                            echo "ไม่มีประกวดราคาแยกย่อย";
                        }

                        ?>

                    </div>

                </div>
            </div>
        </div>
    </div>




    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนบริษัท</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td colspan="2">
                            <label for="msg_vender" >บริษัท ที่ซื้อซอง</label>
                            <div id="msg_vender" class="col-12">
                                <table class='table' width="100%">
                                    <thead>
                                    <tr>
                                        <th>บริษัท</th>
                                        <th>สถานะ</th>
                                        <th>ราคาที่ยืน</th>
                                        <th>ราคาหลังการต่อรอง</th>
                                        <th>ลดจากราคากลาง</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i="1";

                                    foreach ($vender_all_bid as $row) {
                                        $no_bid = $row->no_bid;
                                        $name_vender = $row->name_vender;
                                        $price_vender = $row->price_vender;
                                        $price_under_vender = $row->price_under_vender;
                                        $no_vender = $row->id_vender;
                                        if($price_under_vender=='0'){
                                            $sub_price = ($cost_bid - $price_vender);
                                        }else{
                                            $sub_price = ($cost_bid - $price_under_vender);
                                        }
                                        if($cost_bid==0){
                                            $per = 0;
                                        }else{
                                            $per = ($sub_price*100)/$cost_bid;
                                        }
                                        if($row->win_vender==1){
                                            $status = "ชนะการประกวดราคา";
                                        }elseif ($row->sender_vender==1&&$row->price_vender==0&&$row->pass_vender==0){
                                            $status = "ไม่ผ่านคุณสมบัติ";
                                        }elseif ($row->sender_vender==1&&$row->price_vender!=0){
                                            $status = "ยื่นซอง";
                                        }elseif($row->sender_vender==0){
                                            $status = "ไม่ได้ยื่นซอง";
                                        }
                                        $date_sender_vender = $row->date_sender_vender ;

                                        echo "
                                            <tr>
                                                <td width='40%'>$name_vender</td>
                                                <td width='15%'>$status</td>
                                                <td width='15%'>".number_format($price_vender,2)." บาท</td>
                                                <td width='15%'>".number_format($price_under_vender,2)." บาท</td>
                                                <td width='15%'>".number_format($sub_price,2)." บาท ".number_format($per, 2, '.', '')."%</td>                                                
                                                
                                            </tr>            
                                         ";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนเอกสารแนบ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >

                    <tr>
                        <td colspan="2">
                            <div id="show_attract">
                                <table cellpadding='5' class='table col-6' >
                                    <tr>
                                        <td>ลำดับ</td>
                                        <td>ชื่อไฟล์</td>
                                    </tr>
                                    <?php
                                    foreach ($attract as $row){
                                        $path = base_url($row->path_attract);
                                        echo "
                                            <tr>
                                                <td>$i</td>
                                                <td><a href='$path' target='_blank'>$row->oldname_attract</a></td>                                                                
                                            </tr>        
                            
                                        ";
                                        $i++;
                                    }
                                    ?>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>



    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนผูตรวจสอบ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="member_audit1">ผู้ตรวจสอบ</label>
                            <input type="text" id="member_audit1" class="form-control" value="<?php echo $bid->name1_audit ; ?>" disabled>
                        </td>
                        <td>
                            <label for="member_audit2"></label>
                            <input type="text" id="member_audit2" class="form-control" value="<?php echo $bid->name2_audit ; ?>" disabled>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="member_audit3" class="form-control" value="<?php echo $bid->name3_audit ; ?>" disabled>
                        </td>
                        <td>
                            <input type="text" id="member_audit4" class="form-control" value="<?php echo $bid->name4_audit ; ?>" disabled>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="date_audit">วันที่สังเกตการณ์</label>
                            <input type="text" id="date_audit" name="date_audit" value="<?php echo datethai($bid->date_audit) ; ?>" class="form-control" disabled/>
                        </td>
                    </tr>
                </table >
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button  class="btn btn-danger" onclick="history.back()" >ปิด</button>

                        </td>
                    </tr>
                </table>
            </div>
        </div>




    </div>
</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->



