
<!-- Begin Page Content -->
<div class="container-fluid" >

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">

        <h1 class="h3 mb-0 text-gray-800">รายงานสรุป</h1>

    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a href="<?php echo base_url('index.php/audit/show_bid'); ?>">จำนวนการจัดซื้อจัดจ้างทั้งหมด</a> </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_num_bid">โครงการ</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">ราคากลางทั้งหมด</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_price_all_bid"> บาท</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">จำนวนเงินในการจัดซื้อจัดจ้าง</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_all_price">บาท </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">จำนวนการจัดซื้อจัดจ้างที่ ยกเลิกโครงการ</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_fail_bid"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-book-reader fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->

    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">กราฟแสดง จำนวนค่าใช้จ่ายแยกตามปีบัญชีต่อเดือน </h6>

                </div>

                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="chart-0"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <!-- Area Chart -->
        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">กราฟแสดง จำนวนค่าใช้จ่ายแยกตามปีบัญชีต่อไตรไตรมาส </h6>

                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pie Chart -->
        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">อัตราส่วนค่าใช้จ่าย แยกตามปีบัญชี</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <canvas id="myPieChart"></canvas>
                    </div>
                    <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <i class="fas fa-circle text-primary"></i> ปีบัญชี 2561
                    </span>
                        <span class="mr-2">
                      <i class="fas fa-circle text-success"></i> ปีบัญชี 2562
                    </span>
                        <span class="mr-2">
                      <i class="fas fa-circle text-info"></i> ปีบัญชี 2563
                    </span>
                    </div>
                </div>

            </div>
        </div>
    </div>





</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->




<script >

    function format_price(n) { ///////function for price
        return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+" บาท";
    }






    function show_num_bid(){
        $.post( "<?=site_url('index.php/chart/show_num_bid') ?>", {})
            .done(function( response ) {

               $('#show_num_bid').html(response);
            });
    }
    
    function show_all_price() {
        $.post( "<?=site_url('index.php/chart/show_all_price') ?>", {})
            .done(function( response ) {
                var p = response*1;
                var price = format_price(p) ;
                $('#show_all_price').html(price);
            });
    }

    function show_price_all_bid(){
        $.post( "<?=site_url('index.php/chart/show_price_all_bid') ?>", {})
            .done(function( response ) {
                var p = response*1;
                var price = format_price(p) ;
                $('#show_price_all_bid').html(price);
            });
    }

    function show_fail_bid(){
        $.post( "<?=site_url('index.php/chart/show_fail_bid') ?>", {})
            .done(function( response ) {
                $('#show_fail_bid').html(response);
            });
    }

    <!-- DATA area Chart  -->

    // Set new default font family and font color to mimic Bootstrap's default styling
    //Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
   // Chart.defaults.global.defaultFontColor = '#858796';

    function number_format(number, decimals, dec_point, thousands_sep) {
        // *     example: number_format(1234.56, 2, ',', ' ');
        // *     return: '1 234,56'
        number = (number + '').replace(',', '').replace(' ', '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }






    function area_chart(year) {

        $.ajax({
            type: "POST",
            url: "<?=site_url('index.php/chart/show_area_chart') ?>",
            data:  {year:year} ,
            success: function(result) {
                var result2561 = result.y2561;
                var tri1_2561 = result2561.total_tri_1;
                var tri2_2561 = result2561.total_tri_2;
                var tri3_2561 = result2561.total_tri_3;
                var tri4_2561 = result2561.total_tri_4;
                var datatri2561 = [tri1_2561,tri2_2561,tri3_2561,tri4_2561];

                var result2562 = result.y2562;
                var tri1_2562 = result2562.total_tri_1;
                var tri2_2562 = result2562.total_tri_2;
                var tri3_2562 = result2562.total_tri_3;
                var tri4_2562 = result2562.total_tri_4;
                var datatri2562 = [tri1_2562,tri2_2562,tri3_2562,tri4_2562];

                var result2563 = result.y2563;
                var tri1_2563 = result2563.total_tri_1;
                var tri2_2563 = result2563.total_tri_2;
                var tri3_2563 = result2563.total_tri_3;
                var tri4_2563 = result2563.total_tri_4;
                var datatri2563 = [tri1_2563,tri2_2563,tri3_2563,tri4_2563];

                var labeltri = [" ไตรไตรมาส 1","ไตรไตรมาส 2","ไตรไตรมาส 3","ไตรไตรมาส 4"]

                // Area Chart Example
                var ctx = document.getElementById("myAreaChart");
                var myLineChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: labeltri,
                        datasets: [
                            {
                            label: "ปีบัญชี 2561",
                            lineTension: 0.3,
                            backgroundColor: "rgba(112, 33, 225, 0.05)",
                            borderColor: "rgba(112, 33, 225, 1)",
                            pointRadius: 3,
                            pointBackgroundColor: "rgba(78, 115, 223, 1)",
                            pointBorderColor: "rgba(78, 115, 223, 1)",
                            pointHoverRadius: 3,
                            pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                            pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                            pointHitRadius: 10,
                            pointBorderWidth: 2,
                            data:datatri2561 ,
                            },
                            {
                                label: "ปีบัญชี 2562",
                                lineTension: 0.3,
                                backgroundColor: "rgba(0,255,0,0.3)",
                                borderColor: "rgba(0,255,0,1)",
                                pointRadius: 3,
                                pointBackgroundColor: "rgba(0,255,0,1)",
                                pointBorderColor: "rgba(0,255,0,1)",
                                pointHoverRadius: 3,
                                pointHoverBackgroundColor: "rgba(0,255,0,1)",
                                pointHoverBorderColor: "rgba(0,255,0,1)",
                                pointHitRadius: 10,
                                pointBorderWidth: 2,
                                data:datatri2562 ,
                            },
                            {
                                label: "ปีบัญชี 2563",
                                lineTension: 0.3,
                                backgroundColor: "rgba(255,0,0,0.3)",
                                borderColor: "rgba(255,0,0,1)",
                                pointRadius: 3,
                                pointBackgroundColor: "rgba(255,0,0,0.1)",
                                pointBorderColor: "rgba(255,0,0,1)",
                                pointHoverRadius: 3,
                                pointHoverBackgroundColor: "rgba(255,0,0,1)",
                                pointHoverBorderColor: "rgba(255,0,0,1)",
                                pointHitRadius: 10,
                                pointBorderWidth: 2,
                                data:datatri2563 ,
                            }
                        ],
                    },
                    options: {
                        maintainAspectRatio: false,
                        layout: {
                            padding: {
                                left: 10,
                                right: 25,
                                top: 25,
                                bottom: 0
                            }
                        },
                        scales: {
                            xAxes: [{
                                time: {
                                    unit: 'date'
                                },
                                gridLines: {
                                    display: false,
                                    drawBorder: false
                                },
                                ticks: {
                                    maxTicksLimit: 7
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    maxTicksLimit: 5,
                                    padding: 10,
                                    // Include a dollar sign in the ticks
                                    callback: function(value, index, values) {
                                        return '‎฿' + number_format(value);
                                    }
                                },
                                gridLines: {
                                    color: "rgb(234, 236, 244)",
                                    zeroLineColor: "rgb(234, 236, 244)",
                                    drawBorder: false,
                                    borderDash: [2],
                                    zeroLineBorderDash: [2]
                                }
                            }],
                        },
                        legend: {
                            display: false
                        },
                        tooltips: {
                            backgroundColor: "rgb(255,255,255)",
                            bodyFontColor: "#858796",
                            titleMarginBottom: 10,
                            titleFontColor: '#6e707e',
                            titleFontSize: 14,
                            borderColor: '#dddfeb',
                            borderWidth: 1,
                            xPadding: 15,
                            yPadding: 15,
                            displayColors: false,
                            intersect: false,

                            caretPadding: 10,
                            callbacks: {
                                label: function(tooltipItem, chart) {
                                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                    return datasetLabel + ': ' + number_format(tooltipItem.yLabel)+' บาท';
                                }
                            }
                        }
                    }
                });



            }
        });
    }
    <!-- END of area Chart  -->







    <!-- DATA of pie Chart  -->

    // Pie Chart Example
    function pie_chart(year) {

        $.ajax({
            type: "POST",
            url: "<?=base_url('index.php/chart/show_area_chart_by_month') ?>",
            data:  {year:year} ,
            success: function(result) {

                var result2561 = result.y2561;
                var april2561 = result2561.total_april;
                var may2561 = result2561.total_may;
                var june2561 = result2561.total_june;
                var july2561 = result2561.total_july;
                var august2561 = result2561.total_august;
                var september2561 = result2561.total_september;
                var october2561 = result2561.total_october;
                var november2561 = result2561.total_november;
                var december2561 = result2561.total_december;
                var january2561 = result2561.total_january;
                var february2561 = result2561.total_february;
                var march2561 = result2561.total_march;
                var y2561 = (april2561+ may2561+ june2561+ july2561+ august2561+ september2561+ october2561+ november2561+ december2561+ january2561+ february2561+ march2561);

                var result2562 = result.y2562;
                var april2562 = result2562.total_april;
                var may2562 = result2562.total_may;
                var june2562 = result2562.total_june;
                var july2562 = result2562.total_july;
                var august2562 = result2562.total_august;
                var september2562 = result2562.total_september;
                var october2562 = result2562.total_october;
                var november2562 = result2562.total_november;
                var december2562 = result2562.total_december;
                var january2562 = result2562.total_january;
                var february2562 = result2562.total_february;
                var march2562 = result2562.total_march;
                var y2562 = (april2562+ may2562+ june2562+ july2562+ august2562+ september2562+ october2562+ november2562+ december2562+ january2562+ february2562+ march2562);

                var result2563 = result.y2563;
                var april2563 = result2563.total_april;
                var may2563 = result2563.total_may;
                var june2563 = result2563.total_june;
                var july2563 = result2563.total_july;
                var august2563 = result2563.total_august;
                var september2563 = result2563.total_september;
                var october2563 = result2563.total_october;
                var november2563 = result2563.total_november;
                var december2563 = result2563.total_december;
                var january2563 = result2563.total_january;
                var february2563 = result2563.total_february;
                var march2563 = result2563.total_march;
                var y2563 = (april2563+ may2563+ june2563+ july2563+ august2563+ september2563+ october2563+ november2563+ december2563+ january2563+ february2563+ march2563);

                var ctx = document.getElementById("myPieChart");
                var myPieChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ["ปีบัญชี 2561", "ปีบัญชี 2562", "ปีบัญชี 2563"],
                        datasets: [{
                            data: [y2561, y2562, y2563],
                            backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc'],
                            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
                            hoverBorderColor: "rgba(234, 236, 244, 1)",
                        }],
                    },
                    options: {
                        maintainAspectRatio: false,
                        tooltips: {
                            backgroundColor: "rgb(255,255,255)",
                            bodyFontColor: "#858796",
                            borderColor: '#dddfeb',
                            borderWidth: 1,
                            xPadding: 15,
                            yPadding: 15,
                            displayColors: false,
                            caretPadding: 10,



                        },
                        legend: {
                            display: false
                        },
                        cutoutPercentage: 80,
                    },
                });


            }
        })






    }
    <!-- END of pai Chart  -->



</script>




<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ;?>theme/vendor/chart.js/style.css">

<script src="<?php echo base_url('') ;?>theme/vendor/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url('') ;?>theme/vendor/chart.js/utils.js"></script>
<script src="<?php echo base_url('') ;?>theme/vendor/chart.js/analyser.js"></script>
<script>


    window.onload = function(){

        show_num_bid();
        show_price_all_bid();
        show_fail_bid();
        show_all_price();
        area_chart(2563);
        generateData1(2563);
        pie_chart(2563);


    };


    var presets = window.chartColors;
    var utils = Samples.utils;

    function generateData1(year) {
       $.ajax({
            type: "POST",
            url: "<?=base_url('index.php/chart/show_area_chart_by_month') ?>",
            data:  {year:year} ,
            success: function(result) {

                var result2561 = result.y2561;
                var april2561 = result2561.total_april;
                var may2561 = result2561.total_may;
                var june2561 = result2561.total_june;
                var july2561 = result2561.total_july;
                var august2561 = result2561.total_august;
                var september2561 = result2561.total_september;
                var october2561 = result2561.total_october;
                var november2561 = result2561.total_november;
                var december2561 = result2561.total_december;
                var january2561 = result2561.total_january;
                var february2561 = result2561.total_february;
                var march2561 = result2561.total_march;
                var y2561 = [april2561,may2561,june2561,july2561,august2561,september2561,october2561,november2561,december2561,january2561,february2561,march2561];


                var result2562 = result.y2562;
                var april2562 = result2562.total_april;
                var may2562 = result2562.total_may;
                var june2562 = result2562.total_june;
                var july2562 = result2562.total_july;
                var august2562 = result2562.total_august;
                var september2562 = result2562.total_september;
                var october2562 = result2562.total_october;
                var november2562 = result2562.total_november;
                var december2562 = result2562.total_december;
                var january2562 = result2562.total_january;
                var february2562 = result2562.total_february;
                var march2562 = result2562.total_march;
                var y2562 = [april2562,may2562,june2562,july2562,august2562,september2562,october2562,november2562,december2562,january2562,february2562,march2562];


                var result2563 = result.y2563;
                var april2563 = result2563.total_april;
                var may2563 = result2563.total_may;
                var june2563 = result2563.total_june;
                var july2563 = result2563.total_july;
                var august2563 = result2563.total_august;
                var september2563 = result2563.total_september;
                var october2563 = result2563.total_october;
                var november2563 = result2563.total_november;
                var december2563 = result2563.total_december;
                var january2563 = result2563.total_january;
                var february2563 = result2563.total_february;
                var march2563 = result2563.total_march;
                var y2563 = [april2563,may2563,june2563,july2563,august2563,september2563,october2563,november2563,december2563,january2563,february2563,march2563];


                var data = {
                    labels: ['เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม','มกราคม','กุมภาพันธ์','มีนาคม'],
                    datasets: [ {
                        backgroundColor: utils.transparentize(presets.orange),
                        borderColor: presets.orange,
                        data: y2561 ,
                        label: 'ปีบัญชี 2561',
                        fill: false
                    }, {
                        backgroundColor: utils.transparentize(presets.yellow),
                        borderColor: presets.yellow,
                        data: y2562,
                        label: 'ปีบัญชี 2562',
                        fill: false
                    }, {
                        backgroundColor: utils.transparentize(presets.green),
                        borderColor: presets.green,
                        data: y2563,
                        label: 'ปีบัญชี 2563',
                        fill: false
                    }]
                };

                var options = {
                    maintainAspectRatio: false,
                    spanGaps: false,
                    elements: {
                        line: {
                            tension: 0.000001
                        }
                    },
                    scales: {
                        xAxes: [{
                            time: {
                                unit: 'date'
                            },
                            gridLines: {
                                display: false,
                                drawBorder: false
                            },
                            ticks: {
                                maxTicksLimit: 12
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                maxTicksLimit: 5,
                                padding: 10,
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return '‎฿' + number_format(value);
                                }
                            },
                            gridLines: {
                                color: "rgb(234, 236, 244)",
                                zeroLineColor: "rgb(234, 236, 244)",
                                drawBorder: false,
                                borderDash: [2],
                                zeroLineBorderDash: [2]
                            }
                        }],
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        },
                        'samples-filler-analyser': {
                            target: 'chart-analyser'
                        }
                    },
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        titleMarginBottom: 10,
                        titleFontColor: '#6e707e',
                        titleFontSize: 14,
                        borderColor: '#dddfeb',
                        borderWidth: 1,
                        xPadding: 15,
                        yPadding: 15,
                        displayColors: false,
                        intersect: false,

                        caretPadding: 10,
                        callbacks: {
                            label: function(tooltipItem, chart) {
                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                return datasetLabel + ': ' + number_format(tooltipItem.yLabel)+' บาท';
                            }
                        }
                    }
                };

                var chart = new Chart('chart-0', {
                    type: 'line',
                    data: data,
                    options: options
                });


            }
        });




    }



    // eslint-disable-next-line no-unused-vars
    function toggleSmooth(btn) {
        var value = btn.classList.toggle('btn-on');
        chart.options.elements.line.tension = value ? 0.4 : 0.000001;
        chart.update();
    }


</script>




