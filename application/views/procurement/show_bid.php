<script>
    function delete_bid(no_bid){

        var r = confirm("ต้องการลบ เลขที่จัดซื้อจัดจ้าง "+no_bid+" ใช่หรือไม่");

        if(r== true){
            $.post( "<?=site_url('index.php/purchase/delete_bid') ?>", {
                no_bid:no_bid
            })
                .done(function( data ) {
                    alert( data );
                    location.reload();
                });
        }
    }

    function edit_bid(no_bid){
        $('#form_edit_bid'+no_bid).submit();
    }

    function info_bid(no_bid){
        $('#form_info_bid'+no_bid).submit();
    }

</script>


<?php
$member = $this->session->userdata('data');
$my_member = $member['member'];


function datethai($strDate){
    if($strDate=="0000-00-00"){
        return "ไม่ได้ระบุวันที่";
    }else{
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"> <i class="fas fa-table fa-2x text-gray-300"></i> ตารางการจัดซื้อจัดจ้างทั้งหมด</h1>
    <p class="mb-4"></p>



    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">โครงการทั้งหมด  <?php echo $num ; ?> โครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">สถานะ</th>
                        <th scope="col">วันที่จัดซื้อจัดจ้าง</th>
                        <th scope="col">โครงการ</th>
                        <th scope="col">เจ้าของโครงการ</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;


                    foreach ($data as $row){

                        $member_receive = $row->id_member ;

                        if($member_receive==$my_member){
                            $display = 'style="display: block"';
                        }else{
                            $display = 'style="display: none"';
                        }


                        echo "<tr>
                                <td scope='row' width='5%'>$i</td>
                                <td  width='15%'>$row->status_bid</td>                                
                                <td  width='15%'>".datethai($row->date_bid)."</td>
                                <td  width='50%'>$row->name_project</td>
                                <td  width='15%'>$row->owner_project</td>
                                <td >
                                       <form id='form_info_bid$row->no_bid' action='".base_url("index.php/purchase/info_bid")."' target='_blank' method='post' >
                                    <input id='info_no_bid' name='info_no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='#'  onclick='info_bid($row->no_bid)' title='รายละเอียด' class='btn btn-info '>
                                        <i class='fas fa-info-circle'></i>
                                    </a>
                                    </form>
                                </td>
                                <td><div $display >
                                    <form id='form_edit_bid$row->no_bid' action='".base_url("index.php/purchase/edit_bid")."' target='_blank' method='post' >
                                    <input id='no_bid' name='no_bid' value='$row->no_bid' type='hidden' >
                                    <input id='edit' name='edit' value='edit' type='hidden'>
                                    <a href='# ' onclick='edit_bid($row->no_bid)' title='แก้ไข' class='btn btn-success '  >
                                        <i class='fas fa-edit'></i>
                                    </a>
                                    </form>
                                    </div>
                                </td>
                                <td>
                                    <div $display >
                                    <a href='#' title='ลบ' onclick='delete_bid($row->no_bid)' class='btn btn-danger '>
                                        <i class='fas fa-trash'></i>
                                    </a>
                                    </div>
                                </td>
                             </tr>" ;
                        $i++;

                    }


                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




