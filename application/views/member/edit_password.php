<?php

$member = $this->session->userdata('member');
$data = $member;

?>
<script>

    function ajax_edit_password(){
        var pass_member_old = $('#pass_member_old').val();
        var pass_member_new = $('#pass_member_new').val();
        var pass_member_renew = $('#pass_member_renew').val();



        if(pass_member_old==""){
            alert('กรุณาระบุรหัสผ่านเดิม');
            $('#pass_member_old').focus();
        }else{
            if(pass_member_new==""){
                alert('กรุณาระบุรหัสผ่านใหม่');
                $('#pass_member_new').focus();
            }else{
                if(pass_member_renew==""){
                    alert('ยืนยันรหัสผ่านใหม่');
                    $('#pass_member_renew').focus();
                }else{

                            $.post( "<?=site_url('login_controller/check_old_password_ajax') ?>", {
                                pass_member_old:pass_member_old
                            }).done(function( test ) {

                                if(test=="pass") {


                                   $.post("<?=site_url('login_controller/edit_password_ajax') ?>", {
                                        pass_member_new: pass_member_new

                                    }).done(function (test) {

                                        alert(test);
                                        location.reload();
                                    });

                                }else {
                                    alert('รหัสผ่านเดิมไม่ถูกต้อง');
                                    $('#pass_member_old').focus();
                                }

                            });

                }
            }
        }


    }

    function check_new_password() {
        var pass_member_new = $('#pass_member_new').val();
        var pass_member_renew = $('#pass_member_renew').val();

        if(pass_member_new!=pass_member_renew){
            $('#button_edit').prop('disabled', true);

            $('#msg').addClass('alert alert-warning').html('รหัสผ่านใหม่ไม่ตรงกัน');
        }else{
            $('#button_edit').prop('disabled', false);
            $('#msg').removeClass('alert alert-warning').html('');
        }
    }


</script>




<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-cogs fa-2x text-gray-300"></i>  เปลี่ยนรหัสผ่าน</h1>
    <p class="mb-4"></p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เปลี่ยนรหัสผ่าน</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive col-lg-6 col-sm-12" >
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="pass_member">รหัสผ่านเดิม</label>
                            <input type="password" id="pass_member_old" name="pass_member_old"   class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="pass_member_new">รหัสผ่านใหม่</label>
                            <input type="password" id="pass_member_new" name="pass_member_new"   class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="pass_member_renew">รหัสผ่านใกม่อีกครั้ง </label>
                            <input type="password" id="pass_member_renew" onkeyup="check_new_password()" name="pass_member_renew"   class="form-control"/>
                            <div  role="alert" id="msg">

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <button type="submit" class="btn btn-success" id="button_edit" onclick="ajax_edit_password()" disabled>ยืนยัน เปลี่ยนรหัสผ่าน</button>
                            <button type="reset" class="btn btn-danger" onclick="window.location.replace('<?=base_url()?>')" >ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->
