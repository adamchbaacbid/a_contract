<script>

    function search_bid(){
        var name_vender = $('#name_vender').val();
        var before = $('#before').val();
        var after = $('#after').val();

        $.post( "<?=site_url('bid/show_bid_by_vender_ajax') ?>", {
            name_vender:name_vender,
            before:before,
            after:after
        })
            .done(function( response ) {
                $('#msg_table').html(response);
            });

    }

    function info_bid(no_bid){
        $('#form_info_bid'+no_bid).submit();
    }



</script>


<?php


function datethai($strDate){
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-search fa-2x text-gray-300"></i> ค้นหาการสังเกตการณ์การจัดซื้อจัดจ้างพัสดุ</h1>
    <p class="mb-4"></p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> ค้นหาการสังเกตการณ์จัดซื้อจัดจ้างพัสดุ ตามชื่อบริษัท  </h6>
        </div>
        <div class="card-body">
            <div class="row ">
                <div class=" col-sm-12 col-lg-6">
                    <div class="input-group">
                        <input list="vender_list" id="name_vender" name="name_vender" placeholder="ชื่อบริษัท" class="form-control">
                        <datalist id="vender_list">
                            <?php $all_vender = $this->session->userdata('win_vender');
                            foreach ($all_vender as $row) {
                                echo '<option value="'.$row->name_vender.'">';
                            }?>
                        </datalist>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" onclick="search_bid()" type="button" id="search_bid">ค้นหา</button>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row">
                <div  class=" col-6">
                    จากวันวันที่ <input type="date" class="form-control" id="before">
                </div>
                <div  class=" col-6">
                    ถึง <input type="date" class="form-control" id="after">
                </div>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3" >
            <h6 class="m-0 font-weight-bold text-primary"  > ผลการค้นหา </h6>
        </div>
        <div class="card-body">
            <div class="table-responsive" id="msg_table">
                <table class="table table-striped "  width="100%" cellspacing="0" >
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">วันที่ประกวดราคา</th>
                            <th scope="col">โครงการ</th>
                            <th scope="col">ราคากลาง</th>
                            <th scope="col">ราคาเสนอครั้งสุดท้าย</th>
                            <th scope="col">ลดลง</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr><td colspan='8' align='center'>ยังไม่มีการค้นหา</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




