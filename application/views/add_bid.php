<script>
    function ajax_add_bid() {
        var no_bid = $('#no_bid').val();
        var date_bid = $('#date_bid').val();
        var year_bid =$('#year_bid').val();
        var subject_bid =$('#subject_bid').val();
        var type_bid =$('#type_bid').val();
        var tri_bid =$('#tri_bid').val();
        var cost_bid =$('#cost_bid').val();
        var date_audit =$('#date_audit').val();
        var issues_bid =$('#issues_bid').val();
        var condition_bid =$('#condition_bid').val();
        var period_bid =$('#period_bid').val();
        var member_audit1 = $('#member_audit1').val();
        var member_audit2 = $('#member_audit2').val();
        var member_audit3 = $('#member_audit3').val();
        var member_audit4 = $('#member_audit4').val();
        var devide_bid = $('#devide_bid').val();
        var price_asset = $('#price_asset').val();
        var price_ma = $('#price_ma').val();
        var status_bid = $('#status_bid').val();
        var no_contract = $('#no_contract').val();




        if(no_bid==""){
            alert('กรุณาระบุเลขบันทึก ก่อนการบันทึก');
            $('#no_bid').focus();
        }else{
            if(date_bid==""){
                alert('กรุณาระบุวันที่ออกบันทึก ก่อนการบันทึก');
                $('#date_bid').focus();
            }else{
                if(year_bid==""){
                    alert('กรุณาระบุปีบัญชี');
                    $('#year_bid').focus();
                }else{
                    if(subject_bid==""){
                        alert('กรุณาระบุชื่อเรื่องบันทึก');
                        $('#subject_bid').focus();
                    }else{
                        if(type_bid==""){
                            alert("กรุณาระบุประเภทการจัดซื้อจัดจ้างพัสดุ");
                            $('#type_bid').focus();
                        }else{
                            if(tri_bid==""){
                                alert('กรุณาระบุไตรมาส');
                                $('#tri_bid').focus();
                            }else{
                                if(cost_bid==""){
                                    alert('กรุณาระบุงบประมาณ');
                                    $('#cost_bid').focus();
                                }else{
                                    if(date_audit==""){
                                        alert('กรุณาระบุวันที่สังเกตุการณ์');
                                        $('#date_audit').focus();
                                    }else{
                                        if(member_audit1==""&&member_audit2==""&&member_audit3==""&&member_audit4==""){
                                            alert('กรุณาระบุผู้ตรวจสอบ');
                                            $('#member_audit1').focus();
                                        }else{

                                            $.post( "<?=site_url('bid/check_vender_by_no_bid') ?>", {
                                                no_bid:no_bid
                                            }).done(function( test ) {
                                                if(test<"1"){
                                                    alert('โปรดระบุบริษัทที่ซื้อซอง');
                                                    $('#button_vender').focus();
                                                }else{

                                                    $.post( "<?=site_url('bid/add_bid_button') ?>", {
                                                        no_bid:no_bid,
                                                        date_bid:date_bid,
                                                        year_bid:year_bid,
                                                        subject_bid:subject_bid,
                                                        type_bid:type_bid,
                                                        tri_bid:tri_bid,
                                                        cost_bid:cost_bid,
                                                        date_audit:date_audit,
                                                        issues_bid:issues_bid,
                                                        condition_bid:condition_bid,
                                                        period_bid:period_bid,
                                                        member_audit1:member_audit1,
                                                        member_audit2:member_audit2,
                                                        member_audit3:member_audit3,
                                                        member_audit4:member_audit4,
                                                        devide_bid:devide_bid,
                                                        price_asset:price_asset,
                                                        price_ma:price_ma,
                                                        status_bid:status_bid,
                                                        no_contract:no_contract
                                                    }).done(function( test ) {
                                                        alert( test );
                                                        location.reload();
                                                    });
                                                }
                                            });


                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


    }


</script>

<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#ddd').on('click', function () {
            var no_bid =  $('#no_bid').val();
            var att = $('#attachment_bid').val();
            if(att==""){
                alert("กรุณาเลือกไฟล์ ก่อนอัพโหลด");
            }else {
                var file_data = $('#attachment_bid').prop('files')[0];
                var form_data = new FormData();

                if(no_bid == ""){
                    alert("กรุณาใส่เลขบันทึกสังเกตุการณ์ ก่อนอัพโหลดไฟล์");
                    $('#no_bid').focus();

                }else {

                    form_data.append('file', file_data);
                    form_data.append('no_bid', no_bid);

                    $.ajax({
                        url: '<?php echo base_url("/index.php/bid/upload_file"); ?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',

                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                            show_attract_ajax();
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                            show_attract_ajax();
                        }
                    });
                }
            }
        });
    });


    function show_attract_ajax(){
        var no_bid =  $('#no_bid').val();

        $.post( "<?=site_url('bid/show_attract_ajax') ?>", {
            no_bid:no_bid
        })
            .done(function( response ) {
                $('#show_attract').html(response);
            });
    }


    function show_vender_ajax(){
        var no_bid =  $('#no_bid').val();
        var cost_bid = $('#cost_bid').val();
        $.post( "<?=site_url('bid/show_vender_ajax') ?>", {
            no_bid:no_bid,
            cost_bid:cost_bid
        })
            .done(function( response ) {
                $('#msg_vender').html(response);
            });
    }


    function delete_attract_ajax(id_attract){

        var c = confirm("ต้องการที่จะลบไฟล์ ใช่หรือไม่");

        if(c==true){

            $.post( "<?=site_url('bid/delete_attract') ?>", {
                id_attract:id_attract
            })
                .done(function( response ) {
                    alert(response);
                    show_attract_ajax();
                    $('#show_attract').focus();
                });

        }

    }

    function delete_vender_ajax(id_vender){
        var c = confirm("ต้องการที่จะลบบริษัท ใช่หรือไม่");
        if(c==true){
            $.post( "<?=site_url('bid/delete_vender') ?>", {
                id_vender:id_vender
            })
                .done(function( response ) {
                    alert(response);
                    show_sub_bid();
                    show_vender_ajax();
                    $('#button_vender').focus();
                });
        }
    }



    function show_modal_vender(no_sub) {
        reset_form();
        $('#model_vender').modal('show');
        $('#id_sub').val(no_sub) ;

    }


    function add_vender() {

        var name_vender = $('#name_vender').val();
        var sender = $('#sender_vender').val();
        var pricebid = $('#pricebid').val();
        var win = $('#win_vender').val();
        var price_under_bid = $('#price_under_bid').val();
        var no_bid = $('#no_bid').val();
        var date_sender_vender = $('#date_sender_vender').val();
        var cost_bid = $('#cost_bid').val();
        var id_sub = $('#id_sub').val();
        var pass_vender = $('#pass_vendeer').val();


        if(no_bid==''){
            alert("กรุณาใส่เลขบันทึกสังเกตุการณ์ ก่อนเพิ่มบริษัท");
            $('#no_bid').focus();
        }else{
            if(name_vender==""){
                alert("กรุณาใส่ชื่อบริษัทที่ซื้อซอง");
                $('#name_vender').focus();
            }else{
                if(sender==""){
                    alert('กรุณาเลือกสถานะการยื่นเสนอราคา');
                    $('#sender_vender').focus();
                }else{


                    var form_data = new FormData();
                    form_data.append('name_vender',name_vender);
                    form_data.append('sender',sender);
                    form_data.append('pricebid',pricebid);
                    form_data.append('win',win);
                    form_data.append('price_under_bid',price_under_bid);
                    form_data.append('no_bid',no_bid);
                    form_data.append('date_sender_vender',date_sender_vender);
                    form_data.append('cost_bid',cost_bid);
                    form_data.append('id_sub',id_sub);
                    form_data.append('pass_vender',pass_vender);

                    $.ajax({
                        url: '<?php echo base_url("/index.php/bid/add_vender"); ?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            show_sub_bid();
                            show_vender_ajax();

                            //$('#msg_vender').html(response); // display success response from the server
                        },
                        error: function (response) {
                            alert('save error');
                            //$('#msg_vender').html(response); // display error response from the server
                        }
                    });

                    $('#name_vender').val('');
                    $('#sender_vender').val('0');
                    $('#date_sender_vender').val('');
                    $('#pricebid').val('');
                    $('#win_vender').val('0');
                    $('#price_under_bid').val('');

                    document.getElementById("pricebid").style.display = "none";
                    document.getElementById("date_sender_vender").style.display = "none";
                    document.getElementById("send").style.display = "none";
                    document.getElementById("win").style.display = "none";
                    document.getElementById("price_under_bid").style.display = "none";

                }
            }
        }
    }


    function reset_form() {
        $('#name_vender').val('');
        $('#sender_vender').val('0');
        $('#date_sender_vender').val('');
        $('#pricebid').val('');
        $('#win_vender').val('0');
        $('#price_under_bid').val('');
        $('#pass_vender').val('0');

        document.getElementById("pass").style.display = "none";
        document.getElementById("pricebid").style.display = "none";
        document.getElementById("date_sender_vender").style.display = "none";
        document.getElementById("send").style.display = "none";
        document.getElementById("win").style.display = "none";
        document.getElementById("price_under_bid").style.display = "none";
    }


    $(document).ready(function (e) {

        $('#sender_vender').click(function() {

            var value_sender = $('#sender_vender').val();

            if(value_sender == "1"){
                document.getElementById("pass").style.display = " block";
            }else{
                $('#pass_vender').val('0');
                $('#date_sender_vender').val('');
                $('#pricebid').val('');
                $('#win_vender').val('0');
                $('#price_under_bid').val('');
                document.getElementById("pass").style.display = "none";
                document.getElementById("pricebid").style.display = "none";
                document.getElementById("date_sender_vender").style.display = "none";
                document.getElementById("send").style.display = "none";
                document.getElementById("win").style.display = "none";
                document.getElementById("price_under_bid").style.display = "none";
            }
        });
    });



    $(document).ready(function (e) {

        $('#pass_vender').click(function() {
            var value_sender = $('#pass_vender').val();

            if(value_sender == "1"){
                document.getElementById("pricebid").style.display = " block";
                document.getElementById("date_sender_vender").style.display = " block";
                document.getElementById("send").style.display = " block";
                document.getElementById("win").style.display = "block";
            }else{
                $('#date_sender_vender').val('');
                $('#pricebid').val('');
                $('#win_vender').val('0');
                $('#price_under_bid').val('');
                document.getElementById("pricebid").style.display = "none";
                document.getElementById("date_sender_vender").style.display = "none";
                document.getElementById("send").style.display = "none";
                document.getElementById("win").style.display = "none";
                document.getElementById("price_under_bid").style.display = "none";
            }
        });
    });


    $(document).ready(function (e) {

        $('#win_vender').click(function() {

            var value_win = $('#win_vender').val();

            if(value_win == "1"){
                document.getElementById("price_under_bid").style.display = " block";
            }else{
                $('#price_under_bid').val('');
                document.getElementById("price_under_bid").style.display = "none";
            }
        });
    });

    $(document).ready(function (e) {

        $("#devide_bid").change(function () {
            var devide = $("#devide_bid").val();
            if(devide=="ไม่แยก"){
                $('#td_asset').hide();
                $('#td_ma').hide();
            }else {
                $('#td_asset').show();
                $('#td_ma').show();
            }
        });
    });



    $(document).ready(function (e) {
        $('#button_no_bid').click(function() {
            var no_bid = $('#no_bid').val();
            if(no_bid==""){
                var year = prompt("ปีบัญชี", "2562");

                $.post( "<?=site_url('bid/check_no_bid') ?>", {
                    no_bid:no_bid,
                    year:year
                })
                    .done(function( response ) {
                       $('#no_bid').val(response);
                        $('#year_bid').val(response.substring(0,4));
                    });
            }else{
                $.post( "<?=site_url('bid/check_no_bid') ?>", {
                    no_bid:no_bid
                })
                    .done(function( response ) {
                        $('#no_bid').val(response);
                        $('#year_bid').val(response.substring(0,4));
                    });
            }
        });
    });

    $(document).ready(function (e) {
        $('#no_bid').blur(function() {
            var no_bid = $('#no_bid').val();

            if(no_bid==""){

            }else{

                $.post( "<?=site_url('bid/check_no_bid') ?>", {
                    no_bid:no_bid
                })
                    .done(function( response ) {

                        $('#no_bid').val(response);
                        $('#year_bid').val(response.substring(0,4));
                        show_sub_bid();
                        show_vender_ajax();
                        show_attract_ajax()
                    });
            }
        });
    });

    $(document).ready(function (e) {
        $('#year_bid').change(function() {
            var year = $('#year_bid').val();

            $.post( "<?=site_url('bid/check_no_bid') ?>", {
                no_bid:"",
                year:year
            })
                .done(function( response ) {
                    $('#no_bid').val(response);
                });
        });
    });


    function show_modal_sub() {
        reset_sub_form();

        $('#modal_sub').modal('show');


    }


    $(document).ready(function (e) {

        $('#devide_sub').click(function() {
            var value_devide = $('#devide_sub').val();

            if(value_devide == "แยก"){
                document.getElementById("row_list_devide_sub").style.display = " block";
            }else{
                $('#price_under_bid').val('');
                document.getElementById("row_list_devide_sub").style.display = "none";
            }
        });
    });

    function reset_sub_form() {

        $('#name_sub').val('');
        $('#cost_sub').val('');
        $('#issues_sub').val('ไม่มีประเด็น');
        $('#condition_sub').val('ไม่มีข้อตกลงเพิ่มเติม');
        $('#status_sub').val('สำเร็จ');
        $('#devide_sub').val('ไม่แยก');
        $('#asset_sub').val('');
        $('#ma_sub').val('');

        document.getElementById("row_list_devide_sub").style.display = "none";

    }


    function add_sub_bid() {
       var name_sub =  $('#name_sub').val();
       var cost_sub = $('#cost_sub').val();
       var issues_sub =$('#issues_sub').val();
       var condition_sub = $('#condition_sub').val();
       var status_sub = $('#status_sub').val();
       var devide_sub = $('#devide_sub').val();
       var asset_sub = $('#asset_sub').val();
       var ma_sub = $('#ma_sub').val();
       var no_bid = $('#no_bid').val();


       if(no_bid==""){
           alert('กรุณาระบุเลขบันทึก ก่อนการบันทึก');
           $('#no_bid').focus();
       }else{
           if(name_sub==""){
               alert('กรุณาใส่ชื่อการประกวดราคาแยกย่อย');
               $('#name_sub').focus();
           }else{
               if(cost_sub==""){
                   alert('กรุณาใส่ราคากลาง');
                   $('#cost_sub').focus();
               }else{

                   $.post( "<?=site_url('bid/add_sub_bid') ?>", {
                       no_bid:no_bid,
                       name_sub:name_sub,
                       cost_sub:cost_sub,
                       issues_sub:issues_sub,
                       condition_sub:condition_sub,
                       status_sub:status_sub,
                       devide_sub:devide_sub,
                       asset_sub:asset_sub,
                       ma_sub:ma_sub
                   })
                       .done(function( response ) {
                           alert(response);
                           //$('#msg_sub_bid').html(response);
                           show_sub_bid();
                       });
               }
           }
       }
    }
    
    function show_sub_bid() {
        var no_bid = $('#no_bid').val();
        if(no_bid==""){
            alert('กรุณาระบุเลขบันทึก ก่อนการบันทึก');
            $('#no_bid').focus();
        }else{
            $.post( "<?=site_url('bid/show_sub_bid') ?>", {
                no_bid:no_bid
            })
                .done(function( response ) {
                    $('#msg_sub_bid').html(response);
                });
        }
    }

    function delete_sub_bid(){
        var id_sub_bid = $('#id_sub_bid').val();

            $.post( "<?=site_url('bid/delete_sub_bid') ?>", {
                id_sub_bid:id_sub_bid
            })
                .done(function( response ) {
                    alert(response);
                    show_sub_bid()
                });


    }






</script>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-save fa-2x text-gray-300"></i> บันทึกการสังเกตุการณ์จัดซื้อจัดจ้างพัสดุ</h1>
    <p class="mb-4">บันทึกการสังเกตุการณ์จัดซื้อจัดจ้างพัสดุ หลังจากที่ทำรายงาน และออกเลขหนังสือแล้ว</p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">บันทึก การสังเกตการณ์</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td></td>
                        <td>
                            <label for="status_bid">สถานนะการประกวดราคา</label>
                            <select  id="status_bid" name="status_bid" class="form-control">
                                <option value="สำเร็จ">สำเร็จ</option>
                                <option value="ล้ม">ยกเลิก เนื่องจากไม่มีผู้ยื่นซอง</option>
                                <option value="ล้ม">ยกเลิก เนื่องจากเหลือผู้ยื่นซองประกวดราคารายเดียว</option>
                                <option value="ล้ม">ยกเลิก เนื่องจากตกคุณสมบัติ</option>
                                <option value="ล้ม">ยกเลิก เนื่องจากพบข้อประเด็นธุจริต ในการประกวดราคา</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="year_bid">ปีบัญชี <p style="color: tomato;">*</p></label>
                            <select  id="year_bid" name="year_bid" class="form-control">
                                <option value="">เลือกปีบัญชี......</option>
                                <option value="2560">ปีบัญชี 2560 </option>
                                <option value="2561">ปีบัญชี 2561 </option>
                                <option value="2562">ปีบัญชี 2562 </option>
                                <option value="2563">ปีบัญชี 2563 </option>
                                <option value="2564">ปีบัญชี 2564 </option>
                                <option value="2565">ปีบัญชี 2565 </option>
                                <option value="2566">ปีบัญชี 2566 </option>
                                <option value="2567">ปีบัญชี 2567 </option>
                                <option value="2568">ปีบัญชี 2568 </option>
                                <option value="2569">ปีบัญชี 2569 </option>
                            </select>
                        </td>
                        <td>
                            <label for="tri_bid">ไตรมาส</label>
                            <select  id="tri_bid" name="tri_bid" class="form-control">
                                <option value="">เลือกไตรมาส......</option>
                                <option value="1">ไตรมาส 1</option>
                                <option value="2">ไตรมาส 2</option>
                                <option value="3">ไตรมาส 3</option>
                                <option value="4">ไตรมาส 4</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <label for="no_bid">บันทึกเลขที่ </label>
                            <div class="input-group mb-3">
                                <span class="input-group-text">ICT/</span>
                            <input type="text" id="no_bid"  name="no_bid" maxlength="7"  placeholder="" class="form-control"/>
                                <input type="button" id="button_no_bid" class="btn btn-outline-secondary" value="เรียงเลขที่บันทึก">
                            </div>
                        </td>
                        <td width="50%">
                            <label for="date_bid">วันที่ประกวดราคา</label>
                            <input type="date" id="date_bid" name="date_bid" placeholder="" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  >
                            <p>
                            <label for="subject_bid">ชื่อโครงการ</label>
                            <input type="text" id="subject_bid" name="subject_bid" placeholder="" class="form-control"/>
                            </p>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="type_bid"  >ประเภทการประกวดราคา</label>
                            <select  id="type_bid" name="type_bid" class="form-control">
                                <option value="">เลือกประเภทการประกวดราคา......</option>
                                <option value="ประกวดราคา">ประกวดราคา</option>
                                <option value="e-bidding">e-bidding</option>
                                <option value="วิธีคัดเลือก">วิธีคัดเลือก</option>
                                <option value="เฉพาะเจาะจง">เฉพาะเจาะจง</option>
                                <option value="วิธีคัดเลือก">วิธีคัดเลือก</option>
                            </select>
                        </td>
                        <td>
                            <label for="cost_bid">ราคากลาง</label>
                            <input type="number" id="cost_bid" name="cost_bid" placeholder="จำนวนเงิน " value="0" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="issues_bid">ประเด็นที่การจัดซื้อจัดจ้างพัสดุ</label>
                            <textarea class="form-control" rows="5" id="issues_bid" name="issues_bid" >ไม่มีประเด็น</textarea>
                        </td>
                        <td>
                            <label for="condition_bid">ข้อตกลงเพิ่มเติม</label>
                            <textarea class="form-control" rows="5" id="condition_bid" name="condition_bid">ไม่มีข้อตกลงเพิ่มเติม</textarea>
                        </td>
                    </tr>
                    <script>
                        function devide() {
                            var devide_bid = $('#devide_bid').val();
                            if (devide_bid=="ไม่แยก"){
                                $('#td_asset').hide();
                                $('#td_ma').hide();
                            }else {
                                $('#td_asset').show();
                                $('#td_ma').show();
                            }
                        }
                    </script>
                    <tr>
                        <td>
                            <label for="devide_bid"  >แยกจ่ายตามทรัพย์สิน และMA</label>
                            <select  id="devide_bid" name="devide_bid" class="form-control" onchange="devide()">
                                <option value="ไม่แยก" selected>ไม่แยก</option>
                                <option value="แยก">แยก</option>
                            </select>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="tr_asset"  >
                        <td >
                            <div id="td_asset" style="display: none">
                                <label for="price_asset"  >ราคาทรัพย์สิน</label>
                                <input type="number" id="price_asset" value="" class="form-control">
                            </div>
                        </td>
                        <td >
                            <div id="td_ma"  style="display: none">
                                <label for="price_ma"  >ราคา MA</label>
                                <input type="number" id="price_ma" value="" class="form-control">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <label for="period_bid">วันที่หมดสัญญา</label>
                            <input type="date" id="period_bid" name="period_bid" placeholder="ระยะเวลา" class="form-control"/>
                        </td>
                        <td>
                            <label for="no_contract">เลขที่สัญญา</label>
                            <input type="text" id="no_contract" name="no_contract" class="form-control" >
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนที่มาการแยกประกวดราคาย่อย</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <button id="button_vender" class="btn btn-primary" data-toggle="modal"  onclick="show_modal_sub()" >เพิ่มการประกวดราคาแยกย่อย</button>
                <div id="show_sub">

                    <!-- Divider -->
                    <br>
                    <hr class="sidebar-divider my-0">
                    <br>
                    <!-- END Divider -->

                    <div id="msg_sub_bid">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนบริษัทที่ซื้อซอง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <label for="vender" >บริษัท ที่ซื้อซอง</label>
                            <button id="button_vender" class="btn btn-primary" data-toggle="modal" onclick="show_modal_vender('0')">เพิ่มรายละเอียดการซื้อซอง</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="msg_vender" class="col-12"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนเอกสารแนบ</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <div id="msg_vender" class="col-12"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="attachment_bid" name="attachment_bid" accept=".pdf,.doc,.docx" aria-describedby="ddd">
                                    <label class="custom-file-label" for="attachment_bid">เลือกเอกสารแนบ</label>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="ddd">อัพโหลด</button>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div id="msg" style="color:red" class="alert alert-warning" role="alert">
                                <div  >***** ยังไม่มีการอัพโหลด</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="show_attract">


                            </div>
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนผู้ตรวจสอบ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="member_audit1">ผู้ตรวจสอบ</label>
                            <select  id="member_audit1" name="member_audit1" class="form-control">
                                <option value="">ระบุผู้ตรวจสอบ1</option>
                                <option value="นายนิคม คุ้มตลอด">นายนิคม คุ้มตลอด</option>
                                <option value="นายประมุข ปาประโคน">นายประมุข ปาประโคน</option>
                                <option value="นายศานติ วิทยาเวช">นายศานติ วิทยาเวช</option>
                                <option value="นายณัฐพงศ์ ภู่ระย้า">นายณัฐพงศ์ ภู่ระย้า</option>
                                <option value="นายสุธนัตต์ มงคลชาติ">นายสุธนัตต์ มงคลชาติ</option>
                                <option value="นายอาดัม จิกิตศิลปิน">นายอาดัม จิกิตศิลปิน</option>
                            </select>
                        </td>
                        <td>
                            <label for="member_audit2"></label>
                            <select  id="member_audit2" name="member_audit2" class="form-control">
                                <option value="">ระบุผู้ตรวจสอบ2</option>
                                <option value="นายนิคม คุ้มตลอด">นายนิคม คุ้มตลอด</option>
                                <option value="นายประมุข ปาประโคน">นายประมุข ปาประโคน</option>
                                <option value="นายศานติ วิทยาเวช">นายศานติ วิทยาเวช</option>
                                <option value="นายณัฐพงศ์ ภู่ระย้า">นายณัฐพงศ์ ภู่ระย้า</option>
                                <option value="นายสุธนัตต์ มงคลชาติ">นายสุธนัตต์ มงคลชาติ</option>
                                <option value="นายอาดัม จิกิตศิลปิน">นายอาดัม จิกิตศิลปิน</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select  id="member_audit3" name="member_audit3" class="form-control">
                                <option value="">ระบุผู้ตรวจสอบ3</option>
                                <option value="นายนิคม คุ้มตลอด">นายนิคม คุ้มตลอด</option>
                                <option value="นายประมุข ปาประโคน">นายประมุข ปาประโคน</option>
                                <option value="นายศานติ วิทยาเวช">นายศานติ วิทยาเวช</option>
                                <option value="นายณัฐพงศ์ ภู่ระย้า">นายณัฐพงศ์ ภู่ระย้า</option>
                                <option value="นายสุธนัตต์ มงคลชาติ">นายสุธนัตต์ มงคลชาติ</option>
                                <option value="นายอาดัม จิกิตศิลปิน">นายอาดัม จิกิตศิลปิน</option>
                            </select>
                        </td>
                        <td>
                            <select  id="member_audit4" name="member_audit4" class="form-control">
                                <option value="">ระบุผู้ตรวจสอบ4</option>
                                <option value="นายนิคม คุ้มตลอด">นายนิคม คุ้มตลอด</option>
                                <option value="นายประมุข ปาประโคน">นายประมุข ปาประโคน</option>
                                <option value="นายศานติ วิทยาเวช">นายศานติ วิทยาเวช</option>
                                <option value="นายณัฐพงศ์ ภู่ระย้า">นายณัฐพงศ์ ภู่ระย้า</option>
                                <option value="นายสุธนัตต์ มงคลชาติ">นายสุธนัตต์ มงคลชาติ</option>
                                <option value="นายอาดัม จิกิตศิลปิน">นายอาดัม จิกิตศิลปิน</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="date_audit">วันที่สังเกตการณ์</label>
                            <input type="date" id="date_audit" name="date_audit" placeholder="" class="form-control"/>
                        </td>
                    </tr>

                </table >
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button type="submit" class="btn btn-success" onclick="ajax_add_bid()">บันทึก</button>
                            <button type="reset" class="btn btn-danger" onclick="location.reload()">ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->






<!-- start modal vender form ####################################################################################-->

<div id="model_vender" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title">บริษัทที่ซื้อซอง </h4>
            </div>

            <div class="modal-body" id="myModalBody">

                <form id="all_vender" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="id_sub">รหัสการประกวดราคาย่อย</label>
                                </div>
                                <input type="text" class="form-control" id="id_sub" name="id_sub" >
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group">

                                <input list="vender_list" id="name_vender" name="name_vender" placeholder="ชื่อบริษัท" class="form-control">
                                <datalist id="vender_list">
                                    <?php $all_vender = $this->session->userdata('vender');
                                    foreach ($all_vender as $row) {
                                        echo '<option value="'.$row->name_vender.'">';
                                    }?>
                                </datalist>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="sender_vender">สถานะการยื่นเสนอราคา</label>
                            </div>
                            <select class="custom-select" id="sender_vender" name="sender_vender" >
                                <option value="1" >ยื่นเสนอราคา</option>
                                <option value="0"  selected>ไม่ยื่นเสนอราคา</option>
                            </select>
                        </div><div class="input-group mb-3" id="pass" style="display: none">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="pass_vender">คุณสมบัติ</label>
                                <select class="custom-select" id="pass_vender" name="pass_vender" >
                                    <option value="1" >ผ่าน</option>
                                    <option value="0"  selected>ไม่ผ่าน</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3" id="send" style="display: none">
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="date_sender_vender">วันที่ยื่น</label>
                                <input type="date" id="date_sender_vender" name="date_sender_vender" class="form-control">
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="number" style="display: none" min="1" step="any" id="pricebid" name="pricebid" placeholder="ราคาที่ยื่นเสนอ" class="form-control" />
                        </div>
                        <div class="input-group mb-3" id="win" style="display: none" >
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="win_vender">สถานะ</label>
                                <select class="custom-select" id="win_vender" name="win_vender" >
                                    <option value="1" >ชนะการประกวด</option>
                                    <option value="0" selected>ไม่ชนะการประกวดราคา</option>
                                </select>
                            </div>

                        </div>
                        <input style="display: none"  type="number" min="1" step="any" id="price_under_bid" name="price_under_bid" placeholder="ราคาหลังการต่อรอง" class="form-control"/>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_vender" name="add_vender" onclick="add_vender()" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->


<!-- start modal sub form ####################################################################################-->

<div id="modal_sub" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">แยกประกวดราคาย่อย</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="sub" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-3">

                            <input   type="text" id="name_sub" name="name_sub" placeholder="ชื่อแยกประกวดราคาย่อย" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="number" id="cost_sub" name="cost_sub" placeholder="ราคากลาง" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text">ประเด็นที่การจัดซื้อจัดจ้างพัสดุ</span>
                            <textarea class="form-control" aria-label="With textarea" id="issues_sub" name="issues_sub" >ไม่มีประเด็น</textarea>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text">ข้อตกลงเพิ่มเติม</span>
                            <textarea class="form-control" aria-label="With textarea" id="condition_sub" name="condition_sub" >ไม่มีข้อตกลงเพิ่มเติม</textarea>
                        </div>
                        <div class="input-group mb-3" >
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="status_sub">สถานะ</label>
                                <select class="custom-select" id="status_sub" name="status_sub" >
                                    <option value="สำเร็จ"  selected >สำเร็จ</option>
                                    <option value="ล้ม">ล้ม</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3" >
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="devide_sub">แยกจ่ายตามทรัพย์สิน และMA</label>
                                <select class="custom-select" id="devide_sub" name="devide_sub" >
                                    <option value="แยก" >แยก</option>
                                    <option value="ไม่แยก" selected>ไม่แยก</option>
                                </select>
                            </div>
                        </div>
                        <div id="row_list_devide_sub" style="display: none">
                            <div class="row" >
                                <div class="input-group col-md-6 col-sm-12 ml-auto">
                                    <input   type="number" id="asset_sub" name="asset_sub" placeholder="ราคาทรัพย์สิน" class="form-control">
                                </div>
                                <div class="input-group col-md-6 col-sm-12 ml-auto">
                                    <input   type="number" id="ma_sub" name="ma_sub" placeholder="ราคา MA" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_sub" name="add_sub" data-dismiss="modal" aria-hidden="true" onclick="add_sub_bid()">เพิ่ม</button>

                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sub_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal sub form ####################################################################################-->





