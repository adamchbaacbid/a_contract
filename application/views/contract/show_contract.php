<script>
    function delete_contract(no_bid){

        var r = confirm("ต้องการลบ รายละเอียดสัญญา จัดซื้อจัดจ้างเลขที่"+no_bid+" ใช่หรือไม่");

        if(r== true){
            $.post( "<?=site_url('index.php/contract/delete_contract') ?>", {
                no_bid:no_bid
            })
                .done(function( data ) {
                    alert( data );
                    location.reload();
                });
        }
    }

    function edit_contract(no_bid){
        $('#form_edit_contract'+no_bid).submit();
    }

    function info_contract(no_bid){
        $('#form_info_contract'+no_bid).submit();
    }

</script>


<?php


function datethai($strDate){
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}


?>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"> <i class="fas fa-table fa-2x text-gray-300"></i> สัญญาทั้งหมด</h1>
    <p class="mb-4">รายละเอียดสัญญา</p>



    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ทั้งหมด <?php echo $num_contract ; ?> โครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped " id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">เลขที่</th>
                        <th scope="col">วันที่สิ้นสุดสัญญา</th>
                        <th scope="col">จัดซื้อจัดจ้าง</th>
                        <th scope="col">ชื่อสัญญา</th>
                        <th scope="col">จำนวนวันที่สิ้นสุดสัญญา</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;

                    foreach ($info_contract as $row){
                        $dat_dif = $row->dif ;
                        if($dat_dif<=0){
                            $msg_dat_dif = "สิ้นสุดสัญญาแล้ว";
                        }else{
                            $msg_dat_dif = "เหลืออีก $dat_dif วัน";
                        }
                        echo "<tr>
                                <td scope='row' width='5%'>$i</td>
                                <td  width='10%'>$row->no_bid</td>                                
                                <td  width='15%'>".datethai($row->end_contract)."</td>
                                <td  width='50%'>$row->name_bid</td>
                                <td  width='20%'>$row->name_contract</td>
                                <td  width='20%'>$msg_dat_dif</td>
                                <td >
                                       <form id='form_info_contract$row->no_bid' action='".base_url("index.php/contract/info_contract")."' target='_blank' method='post' >
                                    <input id='info_no_bid' name='info_no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='#'  onclick='info_contract($row->no_bid)' title='รายละเอียด' class='btn btn-info '>
                                        <i class='fas fa-info-circle'></i>
                                    </a>
                                    </form>
                                </td>
                                <td>
                                    <form id='form_edit_contract$row->no_bid' action='".base_url("index.php/contract/edit_contract")."' target='_blank' method='post' >
                                    <input id='no_bid' name='no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='# ' onclick='edit_contract($row->no_bid)' title='แก้ไข' class='btn btn-success '  >
                                        <i class='fas fa-edit'></i>
                                    </a>
                                    </form>
                                </td>
                                <td>
                                    <a href='#' title='ลบ' onclick='delete_contract($row->no_bid)' class='btn btn-danger '>
                                        <i class='fas fa-trash'></i>
                                    </a>
                                </td>
                             </tr>" ;
                        $i++;

                    }


                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




