<?php
    $data = $this->session->userdata('data');
    $name = $data['name'];

    $vender = $info_vender['0'];

?>


<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript">



    function edit_vender_ajax(id_vender){
        var name_vender = $('#name_vender').val();
        var capital_vender = $('#capital_vender').val();


                $.post( "<?=site_url('index.php/purchase/edit_vender_ajax') ?>", {
                    name_vender:name_vender,
                    capital_vender:capital_vender,
                    id_vender : id_vender
                })
                    .done(function( response ) {
                        alert(response);
                        if(response=="แก้ไขเรียบร้อย"){
                            location.reload();
                        }

                    });

    }


</script>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-save fa-2x text-gray-300"></i> ลงทะเบียนบริษัท</h1>
    <p class="mb-4">ลงทะเบียนบริษัทที่มาร่วมการจัดซื้อจัดจ้าง</p>





    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_vender"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ลงทะเบียนบริษัท</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div class="form-group">
                    <div class="input-group mb-2"  >
                        <div class="input-group-prepend" >
                            <label class="input-group-text" for="name_sign_up_vender">ชื่อบริษัท</label>
                            <input type="text" id="name_vender" name="name_vender" value="<?php echo $vender->name_vender;?>" class="form-control">
                        </div>
                    </div>
                    <div class="input-group mb-2"  >
                        <div class="input-group-prepend" >
                            <label class="input-group-text" for="capital_vender">ทุนการจดทะเบียน</label>
                            <input type="number" id="capital_vender" name="capital_vender" value="<?php echo $vender->capital_vender;?>" class="form-control">
                            <span class="input-group-text">บาท</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="add_vender" name="add_vender" onclick="edit_vender_ajax(<?php echo $vender->id_vender ; ?>)" >แก้ไข</button>
                    <button  class="btn btn-danger" onclick="window.close()" >ปิด</button>
                </div>
            </div>
        </div>
    </div>




</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->








