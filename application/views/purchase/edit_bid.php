<?php
$member = $this->session->userdata('data');
$name = $member['name'];


$arr = $bid['bid'];
$data = $arr['0'];

$name_member = $bid['name_member'];

$link_edit_bid_button = site_url('index.php/purchase/edit_bid_button') ;
$link_show_committee_bid_ajax = site_url('index.php/purchase/show_committee_bid_ajax');
$link_add_committee_bid_ajax = site_url('index.php/purchase/add_committee_bid_ajax') ;
$link_delete_committee_bid_ajax  = site_url('index.php/purchase/delete_committee_bid_ajax') ;
$link_show_name_vender = site_url('index.php/purchase/show_name_vender');
$link_sign_up_vender =site_url('index.php/purchase/sign_up_vender');
$link_show_vender_ajax = site_url('index.php/purchase/show_vender_ajax');
$link_delete_active_vender = site_url('index.php/purchase/delete_active_vender');
$link_add_vender = base_url("/index.php/purchase/add_vender");


$link_js_bootstrap_datepicker = site_url('application/third_party/calendar/js/bootstrap-datepicker.js');
$link_js_bootstrap_datepicker_th = site_url('application/third_party/calendar/js/bootstrap-datepicker-thai.js');
$link_js_bootstrap_datepicker_th_th = site_url('application/third_party/calendar/js/locales/bootstrap-datepicker.th.js');
$link_js_jquery = site_url('application/third_party/calendar/js/jquery.js');
$link_js_prettify = site_url('application/third_party/calendar/js/prettify.js');

$link_css_bootstrap_datepicker_th_th = site_url('application/third_party/calendar/css/datepicker.css');
$link_css_bootstrap = site_url('application/third_party/calendar/css/bootstrap.css');
$link_css_prettify = site_url('application/third_party/calendar/css/prettify.css');
$link_css_bootstrap_responsive = site_url('application/third_party/calendar/css/bootstrap-responsive.css');

//$link_upload_attract = site_url('application/');


function date_db_to_date_thai($date_db){
    $pattern_date_thai = "/^[0-9]{2}[\/]/";
    $pattern_date_db = "/^[0-9]{4}[-]/";

    if(preg_match($pattern_date_db, $date_db)==1){
        
        $date_db = strval($date_db);
        $Y = substr($date_db,0,4);
        $M = substr($date_db,5,2);
        $D = substr($date_db,8,2);
        if($Y!=0000){
            $Y = $Y+543;
        }
        
        $Y = strval($Y);

        $date_thai = $D."/".$M."/".$Y ;
    }elseif(preg_match($pattern_date_thai, $date_db)==1){
        $date_thai = $date_db;
    }        

    return $date_thai;
}
?>
<!-- <link href="<?php  echo $link_css_bootstrap ; ?>" rel="stylesheet" > -->
<link href="<?php  echo $link_css_bootstrap_datepicker_th_th ; ?>" rel="stylesheet" media="screen">
<link href="<?php  echo $link_css_prettify ; ?>" rel="stylesheet">
<!-- <link href="<?php  echo $link_css_bootstrap_responsive ; ?>" rel="stylesheet"> -->




<!-- Placed at the end of the document so the pages load faster -->


    <script src="<?php echo $link_js_jquery ; ?>"></script>
    <script src="<?php echo $link_js_prettify ; ?>"></script>


    <script src="<?php echo $link_js_bootstrap_datepicker ; ?>"></script>
    <script src="<?php echo $link_js_bootstrap_datepicker_th ; ?>"></script>
    <script src="<?php echo $link_js_bootstrap_datepicker_th_th ; ?>"></script>

    <script id="example_script"  type="text/javascript">
      function demo() {
        $('.datepicker').datepicker();
      }
    </script>

    <script type="text/javascript">
      $(function(){
        $('pre[data-source]').each(function(){
          var $this = $(this),
            $source = $($this.data('source'));

          var text = [];
          $source.each(function(){
            var $s = $(this);
            if ($s.attr('type') == 'text/javascript'){
              text.push($s.html().replace(/(\n)*/, ''));
            } else {
              text.push($s.clone().wrap('<div>').parent().html()
                .replace(/(\"(?=[[{]))/g,'\'')
                .replace(/\]\"/g,']\'').replace(/\}\"/g,'\'') // javascript not support lookbehind
                .replace(/\&quot\;/g,'"'));
            }
          });
          
          $this.text(text.join('\n\n').replace(/\t/g, '    '));
        });

        prettyPrint();
        demo();
      });
    </script>




<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('application/js/purchase/edit_bid.js'); ?>"></script>
<script>

window.onload = function(){
   

    $('#price_base_bid').val(format_price(<?php echo $data->price_base_bid; ?>));
    $('#cost_bid').val(format_price(<?php echo $data->cost_bid; ?>));
    $('#price_bid').val(format_price(<?php echo $data->price_bid; ?>));
    $('#cost_project').val(format_price(<?php echo $data->cost_project; ?>));

     //show_attract_ajax();   ////Disable Attract function
     show_vender_ajax();
    show_committee_bid_ajax();
       
};


</Script>



<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-save fa-2x text-gray-300"></i> บันทึกการจัดซื้อจัดจ้างพัสดุ</h1>
    <p class="mb-4">บันทึกัดซื้อจัดจ้างพัสดุ หลังจากที่ทำรายงาน และออกเลขหนังสือแล้ว</p>



    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_submit" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">โครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <pre><?php //print_r($data) ;  ?></pre>
                            <label for="input_name_project">ชื่อโครงการ  </label>
                            <div class="input-group mb-3">
                                <input  type="text" id="input_name_project" value="<?php echo $data->name_project; ?>"  name="input_name_project"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="cost_project" >งบประมาณโครงการ</label>
                            <input type="text" id="cost_project" name="cost_project" value="<?php echo $data->cost_project; ?> " class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="owner_project" >ผู้รับผิดชอบโครงการ</label>
                            <input type="text" id="owner_project" name="owner_project" value="<?php echo $data->owner_project; ?> " class="form-control" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="objective_project">วัตถุประสงค์โครงการ</label>
                            <textarea id="objective_project" name="objective_project" class="form-control" disabled ><?php echo $data->objective_project; ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div id="card_bid" class="card shadow mb-4" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">บันทึก การจัดซื้อจัดจ้าง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td></td>
                        <td>
                            <label for="status_bid">สถานนะการดำเนินการ</label>
                            <select  id="status_bid" name="status_bid" class="form-control">
                                <option value="<?php echo $data->status_bid; ?>"><?php echo $data->status_bid; ?></option>
                                <option value="ขออนุมัติจัดซื้อจัดจ้าง">ขออนุมัติจัดซื้อจัดจ้าง</option>
                                <option value="ขายเอกสาร">ขายเอกสาร</option>
                                <option value="จัดทำหนังสือเชิญชวน">จัดทำหนังสือเชิญชวน</option>
                                <option value="ชะลอการจ้าง">ชะลอการจ้าง</option>
                                <option value="แต่งตั้งคณะกรรมการ">แต่งตั้งคณะกรรมการ</option>
                                <option value="ดำเนินการประกวดราคา">ดำเนินการประกวดราคา</option>
                                <option value="ประกาศผู้ชนะการเสนอราคา">ประกาศผู้ชนะการเสนอราคา</option>
                                <option value="พิจารณาข้อเสนอ">พิจารณาข้อเสนอ</option>
                                <option value="ยกเลิกโครงการ">ยกเลิกโครงการ</option>
                                <option value="ลงนามสัญญาแล้ว">ลงนามสัญญาแล้ว</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="year_bid">ปีบัญชี</label>
                            <input type="text"  id="year_bid" name="year_bid" class="form-control" value="ปีบัญชี <?php echo $data->fiscal_year_bid; ?>" disabled>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <label for="no_bid">เลขอ้างอิงจัดซื้อจัดจ้าง</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text">เลขที่</span>
                                <input type="text" id="no_bid"  name="no_bid" maxlength="7" value="<?php echo $data->no_bid; ?>"  placeholder="" class="form-control" disabled/>

                            </div>
                        </td>
                        <td width="50%">
                            <label for="date_bid">วันที่จัดซื้อจัดจ้าง</label>
                            <div class="input-group mb-3">
                                <input type="text" id="date_bid" name="date_bid" placeholder="" data-provide="datepicker" data-date-language="th-th" value="<?php echo date_db_to_date_thai($data->date_bid); ?>" class="form-control"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  >
                            <p>
                                <label for="subject_bid">ชื่อการจัดซื้อจัดจ้าง</label>
                                <input type="text" id="subject_bid" name="subject_bid" placeholder="" value="<?php echo $data->name_bid; ?>" class="form-control"/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_announce">เลขที่ประกาศ</label>
                            <input type="text" id="no_announce" name="no_announce" value="<?php echo $data->no_announce_bid; ?>" class="form-control" >
                        </td>
                        <td >
                            <label for="date_announce">ลงวันที่ประกาศ</label>
                            <input type="text" id="date_announce" name="date_announce" placeholder="" data-provide="datepicker" data-date-language="th-th" value="<?php echo date_db_to_date_thai($data->date_announce_bid); ?>" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="type_bid"  >วิธีจัดหา</label>
                            <div class="input-group mb-3">
                                <select  id="type_bid" name="type_bid" class="form-control">
                                    <option value="<?php echo $data->type_bid; ?>"><?php echo $data->type_bid; ?></option>
                                    <option value="ประกวดราคา">ประกวดราคา</option>
                                    <option value="e-bidding">e-bidding</option>
                                    <option value="วิธีคัดเลือก">วิธีคัดเลือก</option>
                                    <option value="เฉพาะเจาะจง">เฉพาะเจาะจง</option>
                                    <option value="เปรียบเทียบ">เปรียบเทียบ</option>
                                    <option value="พิเศษ">พิเศษ</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <label for="cost_bid">งบประมาณ</label>
                            <div class="input-group mb-3">
                                
                                <input type="text" id="cost_bid" name="cost_bid" placeholder="จำนวนเงิน "  data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" value="<?php echo $data->price_base_bid; ?>" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="price_base_bid">ราคากลาง</label>
                            <div class="input-group mb-3">
                                <input type="text" id="price_base_bid" name="price_base_bid" placeholder="จำนวนเงิน " data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </td>
                        <td>
                            <label for="price_bid">ราคาจริง</label>
                            <div class="input-group mb-3">
                                <input type="text" id="price_bid" name="price_bid" placeholder="จำนวนเงิน "  data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="month_send_draft_contract">เดือน ส่งจัดทำร่างสัญญา</label>
                            <input type="text" id="month_send_draft_contract" name="month_send_draft_contract" placeholder="" data-provide="datepicker" data-date-language="th-th" value="<?php echo date_db_to_date_thai($data->send_draft_contract); ?>" class="form-control"/>
                        </td>
                        <td>
                            <label for="month_approve_bid">เดือน อนุมัติจัดซื้อจัดจ้าง</label> 
                            <input type="text" id="month_approve_bid" name="month_approve_bid" placeholder="" data-provide="datepicker" data-date-language="th-th" value="<?php echo date_db_to_date_thai($data->approve_bid); ?>" class="form-control"/>

                        </td>
                    </tr>
                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td style="display: none;">
                            <label for="no_approve_bid">เลขที่สัญญาการอนุมัติ</label>
                            <input type="text" id="no_approve_bid" name="no_approve_bid" placeholder="" value="<?php echo $data->no_approve_bid; ?>" class="form-control"/>
                        </td>
                        <td>
                            <label for="no_egp">e-gp</label>
                            <input type="text" id="no_egp" name="no_egp" placeholder="" value="<?php echo $data->no_egp; ?>"  class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_contract">เลขที่สัญญา</label>
                            <input type="text" id="no_contract" name="no_contract" value="<?php echo $data->no_contract; ?>" class="form-control" >
                        </td>
                        <td >
                            <label for="date_contract">ลงวันที่สัญญา</label>
                            <input type="text" id="date_contract" name="date_contract" placeholder="" data-provide="datepicker" data-date-language="th-th" value="<?php echo date_db_to_date_thai($data->date_contract); ?>" class="form-control"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_committee"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">คณะกรรมการประกวดราคา</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td >
                            <label for="no_direct_committee">คำสั่ง ที่</label>
                            <input type="text" id="no_direct_committee" name="no_direct_committee" value="<?php echo $data->no_direct_committee; ?>" class="form-control" >
                            <input type="text" id="show_committee_bid_ajax" name="show_committee_bid_ajax" value="<?php echo $link_show_committee_bid_ajax; ?>" class="form-control" hidden>
                            <input type="text" id="delete_committee_bid_ajax" name="delete_committee_bid_ajax" value="<?php echo $link_delete_committee_bid_ajax; ?>" class="form-control" hidden>
                        </td>
                        <td >
                            <label for="date_direct_committee">ลงวันที่</label>
                            <input type="text" id="date_direct_committee" name="date_direct_committee" data-provide="datepicker" data-date-language="th-th" value="<?php echo date_db_to_date_thai($data->date_direct_committee); ?>" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="subject_direct_committee">เรื่อง</label>
                            <input type="text" id="subject_direct_committee" name="subject_direct_committee" value="<?php echo $data->subject_direct_committee; ?>" class="form-control" >

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button id="button_committee_bid" class="btn btn-primary" data-toggle="modal" onclick="show_modal_committee_bid()">เพิ่มคณะกรรมการประกวดราคา</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="show_committee">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_vender" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนบริษัทที่ซื้อซอง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <label for="vender" >บริษัท ที่ซื้อซอง</label>
                            <button id="button_vender" class="btn btn-primary" data-toggle="modal" onclick="show_modal_vender()">เพิ่มรายละเอียดการซื้อซอง</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div  class="col-12">
                                <table class='table' width='100%' id="msg_vender">
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <!-- <div class="card shadow mb-4" id="card_attach" style="display: none;">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนเอกสารแนบ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">

                    <tr>
                        <td colspan="2">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="attachment_bid" name="attachment_bid" accept=".pdf,.doc,.docx" aria-describedby="ddd">
                                    <label class="custom-file-label" for="attachment_bid">เลือกเอกสารแนบ</label>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="ddd">อัพโหลด</button>
                                </div>

                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div id="show_attract">
                                <div  style="color:red" class="alert alert-warning" role="alert" >***** ยังไม่มีการอัพโหลด</div>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div> -->


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_submit" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ผู้รับผิดชอบ การจัดซื้อจัดจ้าง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="member" >ผู้รับผิดชอบ</label>
                            <input type="text" id="member" name="member" value="<?php echo $name_member; ?> " class="form-control" disabled/>
                        </td>
                        <td>
                            <label for="date_receive_bid">วันที่รับงาน</label>
                            <input type="text" id="date_receive_bid" name="date_receive_bid" data-provide="datepicker" data-date-language="th-th" value="<?php echo date_db_to_date_thai($data->date_receive_bid); ?>" class="form-control"/>
                        </td>
                    </tr>

                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <button type="submit" class="btn btn-success" onclick="ajax_edit_bid('<?php echo $link_edit_bid_button; ?>')">แก้ไข</button>
                            <button type="reset" class="btn btn-danger" onclick="window.close()">ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->






<!-- start modal vender form ####################################################################################-->

<div id="model_vender" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title">บริษัทที่ซื้อซอง </h4>
            </div>

            <div class="modal-body" id="myModalBody">

                <form id="all_vender" role="form" >
                    <div class="form-group">

                        <div class="input-group mb-3">
                            <div class="input-group">
                                <input list="vender_list" id="name_vender" name="name_vender" placeholder="ชื่อบริษัท" class="form-control">
                                <datalist id="vender_list">
                                </datalist>
                                <input type="button" class="btn btn-primary" onclick="show_modal_sign_up_vender()" value="ลงทะเบียน บริษัท">
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="sender_vender">สถานะการยื่นเสนอราคา</label>
                            </div>
                            <select class="custom-select" id="sender_vender" name="sender_vender" >
                                <option value="1" >ยื่นเสนอราคา</option>
                                <option value="0"  selected>ไม่ยื่นเสนอราคา</option>
                            </select>
                        </div>
                        <div class="input-group mb-3" id="pass" style="display: none">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="pass_vender">คุณสมบัติ</label>
                                <select class="custom-select" id="pass_vender" name="pass_vender" >
                                    <option value="1" >ผ่าน</option>
                                    <option value="0"  selected>ไม่ผ่าน</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3" style="display: none"  id="pricebid_div">
                            <div class="input-group-prepend">
                                <input type="number"  min="1" step="any" id="pricebid" name="pricebid" placeholder="ราคาที่ยื่นเสนอ" class="form-control" />
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                        <div class="input-group mb-3" id="win" style="display: none" >
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="win_vender">สถานะ</label>
                                <select class="custom-select" id="win_vender" name="win_vender" >
                                    <option value="1" >ชนะการประกวด</option>
                                    <option value="0" selected>ไม่ชนะการประกวดราคา</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3" style="display: none"  id="price_under_bid_div">
                            <div class="input-group-prepend">
                                <input  type="number" min="1" step="any" id="price_under_bid" name="price_under_bid" placeholder="ราคาหลังการต่อรอง" class="form-control"/>
                                <input  type="text" id="show_name_vender" name="show_name_vender" value="<?php echo $link_show_name_vender; ?>" class="form-control" hidden />

                                <input  type="text" id="show_vender_ajax" name="show_vender_ajax" value="<?php echo $link_show_vender_ajax; ?>" class="form-control" hidden />
                                
                                <input  type="text" id="delete_active_vender" name="delete_active_vender" value="<?php echo $link_delete_active_vender; ?>" class="form-control" hidden />
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_vender" name="add_vender" onclick="add_vender('<?php echo $link_add_vender; ?>')" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->





<!-- start modal sig up vender form ####################################################################################-->

<div id="model_sign_up_vender" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ลงทะเบียนบริษัท </h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="sign_up_vender_form" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="name_sign_up_vender">ชื่อบริษัท</label>
                                <input type="text" id="name_sign_up_vender" name="name_sign_up_vender" class="form-control">
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="capital_vender">ทุนการจดทะเบียน</label>
                                <input type="number" id="capital_vender" name="capital_vender" class="form-control">
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="sign_up_vender" name="sign_up_vender" onclick="sign_up_vender('<?php echo $link_sign_up_vender; ?>')" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sign_up_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->

<!-- start modal sig up vender form ####################################################################################-->
<div id="model_committee" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">คณะกรรมการประกวดราคา</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="committee_bid_form" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="name_committee_bid">ชื่อ</label>
                                <input type="text" id="name_committee_bid" name="name_committee_bid" class="form-control">
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="position_committee_bid">ตำแหน่งคณะกรรมการ</label>
                                <select  id="position_committee_bid" name="position_committee_bid" class="form-control">
                                    <option value="กรรมการ">กรรมการ</option>
                                    <option value="ประธานกรรมการ">ประธานกรรมการ</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="status_committee_bid">สถานะวันประกวดราคา</label>
                                <select  id="status_committee_bid" name="status_committee_bid" class="form-control">
                                    <option value="ๅ" selected>มา</option>
                                    <option value="0">ไม่มา</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_committee_bid" name="add_committee_bid" onclick="add_committee_bid('<?php echo $link_add_committee_bid_ajax ; ?>')" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_committee_bid_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->





