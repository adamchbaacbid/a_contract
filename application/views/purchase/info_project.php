<?php
    $data = $this->session->userdata('data');
    $name = $data['name'];
    $info_project = $info_project['0'];

?>


<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript">




    function add_project_ajax() {
       var name_project =  $('#name_project').val();
        var name =  $('#name_project').val();
       var no_project = $('#no_project').val();
       var date_project =$('#date_project').val();
       var owner_project = $('#owner_project').val();
       var cost_project = $('#cost_project').val();
       var objective_project = $('#objective_project').val();

       if(name_project==""){
           alert('กรุณาระบุชื่อโครงการ');
           $('#name_project').focus();
       }else{
           if(owner_project==""){
               alert('กรุณาระบุเจ้าของโครงการ');
               $('#owner_project').focus();
           }else{
               if(cost_project==""){
                   alert('กรุณาระบุงบประมาณโครงการ');
                   $('#cost_project').focus();
               }else{
                   if(objective_project==""){
                       alert('กรุณาระบุวัตถุประสงค์ของโครงการ');
                       $('#objective_project').focus();
                   }else {

                       $.post("<?=site_url('index.php/purchase/add_project_ajax') ?>", {
                           name_project: name_project,
                           no_project: no_project,
                           date_project: date_project,
                           owner_project: owner_project,
                           cost_project: cost_project,
                           objective_project: objective_project
                       })
                           .done(function (response) {
                               alert(response);

                               //show_sub_bid();
                               if(response=='เพิ่มสำเร็จ'){
                                   $('#input_project').val(name);
                               }
                           });
                   }
               }

           }
       }
    }



</script>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-save fa-2x text-gray-300"></i> ข้อมูลโครงการ </h1>
    <p class="mb-4">เพิ่มโครงการการจัดซื้อจัดจ้าง</p>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_vender"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เพิ่มโครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <form id="project" role="form" >
                    <div class="form-group">
                        <label for="name_project">ชื่อโครงการ</label>
                        <div class="input-group mb-3">
                            <input   type="text" id="name_project" name="name_project"  value="<?php echo $info_project->name_project ; ?>" class="form-control" disabled/>
                        </div>
                        <label for="no_project">เลขที่โครงการ</label>
                        <div class="input-group mb-3">
                            <input   type="text" id="no_project" name="no_project"  value="<?php echo $info_project->no_project ; ?>" class="form-control" disabled/>
                        </div>
                        <label for="date_project">วันที่</label>
                        <div class="input-group mb-3">
                            <input   type="text" id="date_project" name="date_project" value="<?php echo $info_project->date_project ; ?>" class="form-control"disabled/>
                        </div>
                        <label for="owner_project">เจ้าของโครงการ</label>
                        <div class="input-group mb-3">
                            <input   type="text" id="owner_project" name="owner_project"  value="<?php echo $info_project->owner_project ; ?>" class="form-control"disabled/>
                        </div>
                        <label for="cost_project">งบประมาณโครงการ</label>
                        <div class="input-group mb-3">
                            <input   type="number" id="cost_project" name="cost_project"  value="<?php echo $info_project->cost_project ; ?>" class="form-control"disabled/>
                            <span class="input-group-text">บาท</span>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text">วัตถุประสงค์ของโครงการ</span>
                            <textarea class="form-control" aria-label="With textarea" id="objective_project" name="objective_project" disabled><?php echo $info_project->objective_project ; ?></textarea>
                        </div>
                        <div class="modal-footer">
                            <button  class="btn btn-danger" onclick="window.close()" >ปิด</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>




</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->








