<?php
$data = $this->session->userdata('data');
$name = $data['name'];
$member_receive = $data['member'];

$link_show_vender = site_url('index.php/purchase/show_vender_ajax');
$link_delete_vender = site_url('index.php/purchase/delete_active_vender');
$link_sign_up_vender =  site_url('index.php/purchase/sign_up_vender');
$url_addbid = site_url('index.php/purchase/add_bid_button'); 
$url_check_member_add_bid = site_url('index.php/purchase/check_member_add_bid'); 
$link_show_committee_bid_ajax = site_url('index.php/purchase/show_committee_bid_ajax');
$link_add_committee_bid_ajax = site_url('index.php/purchase/add_committee_bid_ajax') ;
$link_delete_committee_bid_ajax = site_url('index.php/purchase/delete_committee_bid_ajax') ;
$link_show_name_member_purchase = site_url('index.php/purchase/show_name_member_purchase');

$link_js_bootstrap_datepicker = site_url('application/third_party/calendar/js/bootstrap-datepicker.js');
$link_js_bootstrap_datepicker_th = site_url('application/third_party/calendar/js/bootstrap-datepicker-thai.js');
$link_js_bootstrap_datepicker_th_th = site_url('application/third_party/calendar/js/locales/bootstrap-datepicker.th.js');
$link_js_jquery = site_url('application/third_party/calendar/js/jquery.js');
$link_js_prettify = site_url('application/third_party/calendar/js/prettify.js');

$link_css_bootstrap_datepicker_th_th = site_url('application/third_party/calendar/css/datepicker.css');
$link_css_bootstrap = site_url('application/third_party/calendar/css/bootstrap.css');
$link_css_prettify = site_url('application/third_party/calendar/css/prettify.css');
$link_css_bootstrap_responsive = site_url('application/third_party/calendar/css/bootstrap-responsive.css');

//$link_upload_attract = site_url('application/');
?>







<!-- <link href="<?php  echo $link_css_bootstrap ; ?>" rel="stylesheet" > -->
<link href="<?php  echo $link_css_bootstrap_datepicker_th_th ; ?>" rel="stylesheet" media="screen">
<link href="<?php  echo $link_css_prettify ; ?>" rel="stylesheet">
<!-- <link href="<?php  echo $link_css_bootstrap_responsive ; ?>" rel="stylesheet"> -->


<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo $link_js_jquery ; ?>"></script>
<script src="<?php echo $link_js_prettify ; ?>"></script>

<script src="<?php echo $link_js_bootstrap_datepicker ; ?>"></script>
<script src="<?php echo $link_js_bootstrap_datepicker_th ; ?>"></script>
<script src="<?php echo $link_js_bootstrap_datepicker_th_th ; ?>"></script>

<script id="example_script"  type="text/javascript">
    function demo() {
    $('.datepicker').datepicker();
    }
</script>

<script type="text/javascript">
    $(function(){
    $('pre[data-source]').each(function(){
        var $this = $(this),
        $source = $($this.data('source'));

        var text = [];
        $source.each(function(){
        var $s = $(this);
        if ($s.attr('type') == 'text/javascript'){
            text.push($s.html().replace(/(\n)*/, ''));
        } else {
            text.push($s.clone().wrap('<div>').parent().html()
            .replace(/(\"(?=[[{]))/g,'\'')
            .replace(/\]\"/g,']\'').replace(/\}\"/g,'\'') // javascript not support lookbehind
            .replace(/\&quot\;/g,'"'));
        }
        });
        
        $this.text(text.join('\n\n').replace(/\t/g, '    '));
    });

    prettyPrint();
    demo();
    });
</script>

<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('application/js/purchase/add_bid.js'); ?>"></script>




<script>
    window.onload = function(){
        gen_ref_id()
    };





   /*  $(document).ready(function (e) {   ////////////////// CLOSE Funsion Attract
        $('#ddd').on('click', function () {
            var no_bid =  $('#no_bid').val();
            var att = $('#attachment_bid').val();
            if(att==""){
                alert("กรุณาเลือกไฟล์ ก่อนอัพโหลด");
            }else {
                var file_data = $('#attachment_bid').prop('files')[0];
                var form_data = new FormData();

                if(no_bid == ""){
                    alert("กรุณาใส่เลขบันทึกสังเกตุการณ์ ก่อนอัพโหลดไฟล์");
                    $('#no_bid').focus();
                }else {
                    form_data.append('file', file_data);
                    form_data.append('no_bid', no_bid);

                    $.ajax({
                        url: '<?php echo base_url("/index.php/purchase/upload_file"); ?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',

                        success: function (response) {

                            $('#msg').html(response); // display success response from the server
                            show_attract_ajax('<?php echo $link_upload_attract ; ?>');
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                            show_attract_ajax('<?php echo $link_upload_attract ; ?>');
                        }
                    });
                }
            }
        });
    }); */

    $(document).ready(function (e) {
        $('#name_vender').focus(function() {
            $("#vender_list").empty();
            $.post("<?=site_url('index.php/purchase/show_name_vender') ?>", {
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    //alert(response);
                    $.each(arr, function(id,name) {
                        var option = $('<option value="'+name+'"></option>');
                        $('#vender_list').append(option);
                    });
                });
        });
    });

    

    $(document).ready(function (e) {
        $('#price_base_bid').blur(function() {
            show_vender_ajax('<?php echo $link_show_vender ; ?>','<?php echo $link_delete_vender ; ?>');
        });
    });

    $(document).ready(function (e) {
        $('#sender_vender').click(function() {
            var value_sender = $('#sender_vender').val();
            if(value_sender == "1"){
                document.getElementById("pass").style.display = " block";
            }else{
                $('#pass_vender').val('0');
                $('#pricebid').val('');
                $('#win_vender').val('0');
                $('#price_under_bid').val('');
                document.getElementById("pass").style.display = "none";
                document.getElementById("pricebid_div").style.display = "none";
                document.getElementById("win").style.display = "none";
                document.getElementById("price_under_bid_div").style.display = "none";
            }
        });
    });

    $(document).ready(function (e) {
        $('#pass_vender').click(function() {
            var value_sender = $('#pass_vender').val();
            if(value_sender == "1"){
                document.getElementById("pricebid_div").style.display = " block";
                document.getElementById("win").style.display = "block";
            }else{
                $('#pricebid').val('');
                $('#win_vender').val('0');
                $('#price_under_bid').val('');
                document.getElementById("pricebid_div").style.display = "none";
                document.getElementById("win").style.display = "none";
                document.getElementById("price_under_bid_div").style.display = "none";
            }
        });
    });

    $(document).ready(function (e) {
        $('#win_vender').click(function() {
            var value_win = $('#win_vender').val();
            if(value_win == "1"){
                document.getElementById("price_under_bid_div").style.display = " block";
            }else{
                $('#price_under_bid').val('');
                document.getElementById("price_under_bid_div").style.display = "none";
            }
        });
    });





    

    function gen_ref_id(){
        var no_bid = $('#no_bid').val();
            if(no_bid==""){                
                $.post( "<?=site_url('index.php/purchase/check_no_bid2') ?>", {                    
                })
                    .done(function( response ) {
                        $('#no_bid').val(response);
                        //$('#year_bid').val(response.substring(0,4));
                        show_vender_ajax('<?php echo $link_show_vender ; ?>','<?php echo $link_delete_vender ; ?>');
                        //show_attract_ajax();
                        show_committee_bid_ajax();
                    });
            }else{
                $.post( "<?=site_url('index.php/purchase/check_no_bid2') ?>", {
                    
                })
                    .done(function( response ) {
                        $('#no_bid').val(response);
                        //$('#year_bid').val(response.substring(0,4));
                        show_vender_ajax('<?php echo $link_show_vender ; ?>','<?php echo $link_delete_vender ; ?>');
                        //show_attract_ajax();
                        show_committee_bid_ajax();
                    });
            }
    }

    

    

    $(document).ready(function (e) {
        $('#input_project').focus(function() {
            $.post("<?=site_url('index.php/purchase/show_project') ?>", {
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    //alert(response);
                    $.each(arr, function(id,name) {
                        var option = $('<option value="'+name+'"></option>');
                        $('#project_list').append(option);
                    });
                });
        });
    });

    $(document).ready(function (e) {
        $('#input_project').focusout(function() {
            $("#project_list").empty();
        });
    });

</script>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-save fa-2x text-gray-300"></i> บันทึกการจัดซื้อจัดจ้างพัสดุ</h1>
    <p class="mb-4">บันทึกจัดซื้อจัดจ้างพัสดุ หลังจากที่ทำรายงาน และออกเลขหนังสือแล้ว</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เลือกโครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td colspan="2">
                            <label for="input_project"></label>
                            <div class="input-group mb-3">
                                <input list="project_list" id="input_project" name="input_project" placeholder="" class="form-control">
                                <datalist id="project_list">
                                </datalist>
                                <?php $link_add_input_name_project = site_url('index.php/purchase/check_name_project') ; ?>
                                <input type="button" id="button_name_project" class="btn btn-outline-secondary" value="เลือกโครงการ" onclick="add_input_name_project('<?php echo $link_add_input_name_project ; ?>')">
                                <button id="button_project" class="btn btn-primary" data-toggle="modal"  onclick="show_modal_project()" >เพิ่มโครงการ</button>

                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div id="card_bid" class="card shadow mb-4"  style="display: none">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">บันทึก การจัดซื้อจัดจ้าง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5" >
                    <tr>
                        <td colspan="2">
                            <label for="input_name_project">ชื่อโครงการ </label>
                            <div class="input-group mb-3">
                                <input type="text" id="input_name_project"  name="input_name_project"  placeholder="" class="form-control" disabled/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <label for="status_bid">สถานะการดำเนินการ</label>
                            <select  id="status_bid" name="status_bid" class="form-control">
                                <option value="ขออนุมัติจัดซื้อจัดจ้าง">ขออนุมัติจัดซื้อจัดจ้าง</option>
                                <option value="ขายเอกสาร">ขายเอกสาร</option>
                                <option value="จัดทำหนังสือเชิญชวน">จัดทำหนังสือเชิญชวน</option>
                                <option value="ชะลอการจ้าง">ชะลอการจ้าง</option>
                                <option value="แต่งตั้งคณะกรรมการ">แต่งตั้งคณะกรรมการ</option>
                                <option value="ดำเนินการประกวดราคา">ดำเนินการประกวดราคา</option>
                                <option value="ประกาศผู้ชนะการเสนอราคา">ประกาศผู้ชนะการเสนอราคา</option>
                                <option value="พิจารณาข้อเสนอ">พิจารณาข้อเสนอ</option>
                                <option value="ยกเลิกโครงการ">ยกเลิกโครงการ</option>
                                <option value="ลงนามสัญญาแล้ว">ลงนามสัญญาแล้ว</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="year_bid">ปีบัญชี <sup style="color: tomato;">*</sup></label>
                            <select  id="year_bid" name="year_bid" class="form-control">
                                <option value="">เลือกปีบัญชี......</option>
                                <option value="2560">ปีบัญชี 2560 </option>
                                <option value="2561">ปีบัญชี 2561 </option>
                                <option value="2562">ปีบัญชี 2562 </option>
                                <option value="2563">ปีบัญชี 2563 </option>
                                <option value="2564">ปีบัญชี 2564 </option>
                                <option value="2565">ปีบัญชี 2565 </option>
                                <option value="2566">ปีบัญชี 2566 </option>
                                <option value="2567">ปีบัญชี 2567 </option>
                                <option value="2568">ปีบัญชี 2568 </option>
                                <option value="2569">ปีบัญชี 2569 </option>
                            </select>
                        </td>

                        <td>
                            <label for="no_bid">เลขอ้างอิงจัดซื้อจัดจ้าง  </label>
                            <div class="input-group mb-3">
                                <span class="input-group-text">เลขอ้างอิง</span>
                                <input type="text" id="no_bid"  name="no_bid" maxlength="7"  placeholder="" class="form-control" disabled/>
                                
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">

                        </td>
                        <td width="50%">
                            <label for="date_bid">วันที่จัดซื้อจัดจ้าง</label>
                            <div class="input-group mb-3">
                            <input class="form-control" type="text" id="date_bid" name="date_bid" data-provide="datepicker" data-date-language="th-th" value="">
                            </div>                           
                                
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  >
                            <p>
                                <label for="subject_bid">ชื่อการจัดซื้อจัดจ้าง <sup style="color: tomato;">*</sup></label>
                                <input type="text" id="subject_bid" name="subject_bid" placeholder="" class="form-control"/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_announce">เลขที่ประกาศ</label>
                            <input type="text" id="no_announce" name="no_announce" class="form-control" >
                        </td>
                        <td >
                            <label for="date_announce">ลงวันที่ประกาศ</label>
                            
                            <input class="form-control" type="text" id="date_announce" name="date_announce" data-provide="datepicker" data-date-language="th-th" value="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="type_bid"  >วิธีจัดหา</label>
                            <div class="input-group mb-3">
                                <select  id="type_bid" name="type_bid" class="form-control">
                                    <option value="">เลือกวิธีการจัดหา......</option>
                                    <option value="ประกวดราคา">ประกวดราคา</option>
                                    <option value="e-bidding">e-bidding</option>
                                    <option value="วิธีคัดเลือก">วิธีคัดเลือก</option>
                                    <option value="เฉพาะเจาะจง">เฉพาะเจาะจง</option>
                                    <option value="เปรียบเทียบ">เปรียบเทียบ</option>
                                    <option value="พิเศษ">พิเศษ</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <label for="cost_bid">งบประมาณ</label>
                            <div class="input-group mb-3">
                                <input type="text" id="cost_bid" name="cost_bid" placeholder="จำนวนเงิน " value="0" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$"   class="form-control" />                                
                                <span class="input-group-text">บาท</span>
                                
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="price_base_bid">ราคากลาง <sup style="color: tomato;">*</sup></label>
                            <div class="input-group mb-3">
                                <input type="text" id="price_base_bid" name="price_base_bid" placeholder="จำนวนเงิน " value="0" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </td>
                        <td>
                            <label for="price_bid">ราคาจริง</label>
                            <div class="input-group mb-3">
                                <input type="text" id="price_bid" name="price_bid" placeholder="จำนวนเงิน " value="0" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="month_send_draft_contract">เดือน ส่งจัดทำร่างสัญญา</label>
                            <input type="text" id="month_send_draft_contract" name="month_send_draft_contract" data-provide="datepicker" data-date-language="th-th" placeholder="" class="form-control"/>
                        </td>
                        <td>
                            <label for="month_approve_bid">เดือน อนุมัติจัดซื้อจัดจ้าง</label>
                            <input type="text" id="month_approve_bid" name="month_approve_bid" placeholder="" data-provide="datepicker" data-date-language="th-th" class="form-control"/>

                        </td>
                    </tr>
                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td style="display: none;">
                            <label for="no_send_draft_contract">เลขที่สัญญาการอนุมัติ</label>
                            <input type="text" id="no_send_draft_contract" name="no_send_draft_contract" placeholder="" class="form-control"/>
                        </td>
                        <td>
                            <label for="no_egp">e-gp</label>
                            <input type="text" id="no_egp" name="no_egp" placeholder=""  class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_contract">เลขที่สัญญา</label>
                            <input type="text" id="no_contract" name="no_contract" class="form-control" >
                        </td>
                        <td >
                            <label for="date_contract">ลงวันที่สัญญา</label>
                            
                            <input class="form-control" type="text" id="date_contract" name="date_contract" data-provide="datepicker" data-date-language="th-th" value="">
                            
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_committee"  style="display: none">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">คณะกรรมการประกวดราคา</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td >
                            <label for="no_direct_committee">คำสั่ง ที่</label>
                            <input type="text" id="no_direct_committee" name="no_direct_committee" class="form-control" >
                            <input type="text" id="link_show_committee_bid_ajax" name="link_show_committee_bid_ajax" value="<?php echo $link_show_committee_bid_ajax ; ?>" class="form-control" hidden >
                            <input type="text" id="link_delete_committee_bid_ajax" name="link_delete_committee_bid_ajax" value="<?php echo $link_delete_committee_bid_ajax ; ?>" class="form-control" hidden >

                        </td>
                        <td >
                            <label for="date_direct_committee">ลงวันที่</label>
                            <input type="text" id="date_direct_committee" name="date_direct_committee" class="form-control" data-provide="datepicker" data-date-language="th-th" >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label for="subject_direct_committee">เรื่อง</label>
                            <input type="text" id="subject_direct_committee" name="subject_direct_committee" class="form-control" >

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button id="button_committee_bid" class="btn btn-primary" data-toggle="modal" onclick="show_modal_committee_bid()">เพิ่มคณะกรรมการประกวดราคา</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="show_committee">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_vender"  style="display: none">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนบริษัทที่ซื้อซอง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <label for="vender" >บริษัท ที่ซื้อซอง</label>
                            <button id="button_vender" class="btn btn-primary" data-toggle="modal" onclick="show_modal_vender()">เพิ่มรายละเอียดการซื้อซอง</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div  class="col-12">
                                <table class='table' width='100%' id="msg_vender">
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <!-- DataTales Example -->
    <!-- <div class="card shadow mb-4" id="card_attach"  style="display: none">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ส่วนเอกสารแนบ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">

                    <tr>
                        <td colspan="2">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="attachment_bid" name="attachment_bid" accept=".pdf,.doc,.docx" aria-describedby="ddd">
                                    <label class="custom-file-label" for="attachment_bid">เลือกเอกสารแนบ</label>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="ddd">อัพโหลด</button>
                                </div>

                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div id="msg" style="color:red" class="alert alert-warning" role="alert">
                                <div  >***** ยังไม่มีการอัพโหลด</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="show_attract">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div> -->




    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_submit"  style="display: none">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ผู้รับผิดชอบ การจัดซื้อจัดจ้าง</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                            <label for="member" >ผู้รับผิดชอบ <sup style="color: tomato;">*</sup></label>        
                            <div class="input-group">
                                <input list="member_list" id="member" name="member" placeholder="ชื่อผู้รับผิดชอบ" class="form-control">
                                <datalist id="member_list">
                                </datalist>
                            </div>
                            <input type="text" id="link_show_name_member_purchase" name="link_show_name_member_purchase" value="<?php echo $link_show_name_member_purchase; ?>" class="form-control" hidden >
                        
                        </td>
                        <td>
                            <label for="date_receive_bid">วันที่รับงาน</label>
                            <input type="text" id="date_receive_bid" name="date_receive_bid" class="form-control" data-provide="datepicker" data-date-language="th-th" />
                        </td>
                    </tr>

                </table>
                <br>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <br>
                <table width="100%" cellpadding="5">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                        
                            <button type="submit" class="btn btn-success" onclick="ajax_add_bid('<?php echo $url_addbid ;?>','<?php echo $url_check_member_add_bid ; ?>')">บันทึก</button>
                            <button type="reset" class="btn btn-danger" onclick="location.reload()">ยกเลิก</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




<!-- <!-- start modal project -->


<!-- start modal vender form ####################################################################################-->

<div id="model_vender" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title">บริษัทที่ซื้อซอง </h4>
            </div>

            <div class="modal-body" id="myModalBody">

                <form id="all_vender" role="form" >
                    <div class="form-group">

                        <div class="input-group mb-3">
                            <div class="input-group">
                                <input list="vender_list" id="name_vender" name="name_vender" placeholder="ชื่อบริษัท" class="form-control">
                                <datalist id="vender_list">
                                </datalist>
                                <input type="button" class="btn btn-primary" onclick="show_modal_sign_up_vender()" value="ลงทะเบียน บริษัท">
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="sender_vender">สถานะการยื่นเสนอราคา</label>
                            </div>
                            <select class="custom-select" id="sender_vender" name="sender_vender" >
                                <option value="1" >ยื่นเสนอราคา</option>
                                <option value="0"  selected>ไม่ยื่นเสนอราคา</option>
                            </select>
                        </div>
                        <div class="input-group mb-3" id="pass" style="display: none">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="pass_vender">คุณสมบัติ</label>
                                <select class="custom-select" id="pass_vender" name="pass_vender" >
                                    <option value="1" >ผ่าน</option>
                                    <option value="0"  selected>ไม่ผ่าน</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3" style="display: none"  id="pricebid_div">
                            <div class="input-group-prepend">
                                <input type="text"   id="pricebid" name="pricebid" placeholder="ราคาที่ยื่นเสนอ" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$"  class="form-control" />
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                        <div class="input-group mb-3" id="win" style="display: none" >
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="win_vender">สถานะ</label>
                                <select class="custom-select" id="win_vender" name="win_vender" >
                                    <option value="1" >ชนะการประกวด</option>
                                    <option value="0" selected>ไม่ชนะการประกวดราคา</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3" style="display: none"  id="price_under_bid_div">
                            <div class="input-group-prepend">
                                <input  type="text"  id="price_under_bid" name="price_under_bid" placeholder="ราคาหลังการต่อรอง" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$"  class="form-control"/>
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?php $link_add_vender = base_url("/index.php/purchase/add_vender"); ?>
                <button type="submit" class="btn btn-success" id="add_vender" name="add_vender" onclick="add_vender('<?php echo $link_add_vender ; ?>','<?php echo $link_show_vender; ?>','<?php echo $link_delete_vender ; ?>')" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->



<div id="modal_project" class="modal fade " aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">เพิ่มโครงการ</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="project" role="form" >
                    <div class="form-group" >
                        <div class="input-group mb-3">
                            <input   type="text" id="name_project" name="name_project" placeholder="ชื่อโครงการ " class="form-control"/><b style="color: tomato;">  *</b>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="no_project" name="no_project" placeholder="เลขที่โครงการ" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="date_project" name="date_project" placeholder="ลงวันที่" data-provide="datepicker" data-date-language="th-th" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="owner_project" name="owner_project" placeholder="เจ้าของเรื่อง" value="-" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="cost_project" name="cost_project" placeholder="งบประมาณของโครงการ" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" class="form-control"/>
                            <span class="input-group-text">บาท</span>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text">วัตถุประสงค์ของโครงการ</span>
                            <textarea class="form-control" aria-label="With textarea" id="objective_project" name="objective_project" >-</textarea>
                        </div>
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?php $link_add_bid = site_url('index.php/purchase/add_project_ajax'); ?>
                <button type="submit" class="btn btn-success" id="add_sub" name="add_sub" data-dismiss="modal" aria-hidden="true" onclick="add_project('<?php echo $link_add_bid ; ?>')" >เพิ่ม</button>

                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_project()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal sub form ####################################################################################-->


<!-- start modal sig up vender form ####################################################################################-->

<div id="model_sign_up_vender" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ลงทะเบียนบริษัท </h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="sign_up_vender_form" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="name_sign_up_vender">ชื่อบริษัท</label>
                                <input type="text" id="name_sign_up_vender" name="name_sign_up_vender" class="form-control">
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="capital_vender">ทุนการจดทะเบียน</label>
                                <input type="text" id="capital_vender" name="capital_vender" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$"  class="form-control">
                                <span class="input-group-text">บาท</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-success" id="add_vender" name="add_vender" onclick="sign_up_vender('<?php echo $link_sign_up_vender ; ?>')" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_sign_up_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->


<!-- start modal sig up vender form ####################################################################################-->
<div id="model_committee" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-l">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">คณะกรรมการประกวดราคา</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <form id="committee_bid_form" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="name_committee_bid">ชื่อ</label>
                                <input type="text" id="name_committee_bid" name="name_committee_bid" class="form-control">
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="position_committee_bid">ตำแหน่งคณะกรรมการ</label>
                                <select  id="position_committee_bid" name="position_committee_bid" class="form-control">
                                    <option value="กรรมการ">กรรมการ</option>
                                    <option value="ประธานกรรมการ">ประธานกรรมการ</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-2"  >
                            <div class="input-group-prepend" >
                                <label class="input-group-text" for="status_committee_bid">สถานะวันประกวดราคา</label>
                                <select  id="status_committee_bid" name="status_committee_bid" class="form-control">
                                    <option value="1" selected>มา</option>
                                    <option value="0">ไม่มา</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="add_committee_bid" name="add_committee_bid" onclick="add_committee_bid('<?php echo $link_add_committee_bid_ajax ; ?>')" data-dismiss="modal" aria-hidden="true">เพิ่ม</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_committee_bid_form()">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>
<!-- end of modal vender form ####################################################################################-->



 
