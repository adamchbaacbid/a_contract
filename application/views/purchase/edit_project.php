<?php
    $data = $this->session->userdata('data');
    $name = $data['name'];

    $info_project = $info_project['0'];



$link_js_bootstrap_datepicker = site_url('application/third_party/calendar/js/bootstrap-datepicker.js');
$link_js_bootstrap_datepicker_th = site_url('application/third_party/calendar/js/bootstrap-datepicker-thai.js');
$link_js_bootstrap_datepicker_th_th = site_url('application/third_party/calendar/js/locales/bootstrap-datepicker.th.js');
$link_js_jquery = site_url('application/third_party/calendar/js/jquery.js');
$link_js_prettify = site_url('application/third_party/calendar/js/prettify.js');

$link_css_bootstrap_datepicker_th_th = site_url('application/third_party/calendar/css/datepicker.css');
$link_css_bootstrap = site_url('application/third_party/calendar/css/bootstrap.css');
$link_css_prettify = site_url('application/third_party/calendar/css/prettify.css');
$link_css_bootstrap_responsive = site_url('application/third_party/calendar/css/bootstrap-responsive.css');

//$link_upload_attract = site_url('application/');


function date_db_to_date_thai($date_db){
    $pattern_date_thai = "/^[0-9]{2}[\/]/";
    $pattern_date_db = "/^[0-9]{4}[-]/";

    if(preg_match($pattern_date_db, $date_db)==1){
        
        $date_db = strval($date_db);
        $Y = substr($date_db,0,4);
        $M = substr($date_db,5,2);
        $D = substr($date_db,8,2);
        if($Y!=0000){
            $Y = $Y+543;
        }
        
        $Y = strval($Y);

        $date_thai = $D."/".$M."/".$Y ;
    }elseif(preg_match($pattern_date_thai, $date_db)==1){
        $date_thai = $date_db;
    }        

    return $date_thai;
}
?>
<!-- <link href="<?php  echo $link_css_bootstrap ; ?>" rel="stylesheet" > -->
<link href="<?php  echo $link_css_bootstrap_datepicker_th_th ; ?>" rel="stylesheet" media="screen">
<link href="<?php  echo $link_css_prettify ; ?>" rel="stylesheet">
<!-- <link href="<?php  echo $link_css_bootstrap_responsive ; ?>" rel="stylesheet"> -->




<!-- Placed at the end of the document so the pages load faster -->


    <script src="<?php echo $link_js_jquery ; ?>"></script>
    <script src="<?php echo $link_js_prettify ; ?>"></script>


    <script src="<?php echo $link_js_bootstrap_datepicker ; ?>"></script>
    <script src="<?php echo $link_js_bootstrap_datepicker_th ; ?>"></script>
    <script src="<?php echo $link_js_bootstrap_datepicker_th_th ; ?>"></script>

    <script id="example_script"  type="text/javascript">
      function demo() {
        $('.datepicker').datepicker();
      }
    </script>

    <script type="text/javascript">
      $(function(){
        $('pre[data-source]').each(function(){
          var $this = $(this),
            $source = $($this.data('source'));

          var text = [];
          $source.each(function(){
            var $s = $(this);
            if ($s.attr('type') == 'text/javascript'){
              text.push($s.html().replace(/(\n)*/, ''));
            } else {
              text.push($s.clone().wrap('<div>').parent().html()
                .replace(/(\"(?=[[{]))/g,'\'')
                .replace(/\]\"/g,']\'').replace(/\}\"/g,'\'') // javascript not support lookbehind
                .replace(/\&quot\;/g,'"'));
            }
          });
          
          $this.text(text.join('\n\n').replace(/\t/g, '    '));
        });

        prettyPrint();
        demo();
      });
    </script>


<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript">

    $(document).ready(function (e) {
        $("input[data-type='currency']").on({
            keyup: function() {
            formatCurrency($(this));            
            },
            blur: function() { 
            formatCurrency($(this), "blur");            
            }
        });
    });   
    
    function formatNumber(n) {
      // format number 1000000 to 1,234,567
      return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }    
    
    function formatCurrency(input, blur) {
      // appends $ to value, validates decimal side
      // and puts cursor back in right position.
      
      // get input value
      var input_val = input.val();
      
      // don't validate empty input
      if (input_val === "") { return; }
      
      // original length
      var original_len = input_val.length;
    
      // initial caret position 
      var caret_pos = input.prop("selectionStart");
        
      // check for decimal
      if (input_val.indexOf(".") >= 0) {
    
        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
        var decimal_pos = input_val.indexOf(".");
    
        // split number by decimal point
        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);
    
        // add commas to left side of number
        left_side = formatNumber(left_side);
    
        // validate right side
        right_side = formatNumber(right_side);
        
        // On blur make sure 2 numbers after decimal
        if (blur === "blur") {
          right_side += "00";
        }
        
        // Limit decimal to only 2 digits
        right_side = right_side.substring(0, 2);
    
        // join number by .
        input_val = "" + left_side + "." + right_side;
    
      } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = formatNumber(input_val);
        input_val = "" + input_val;
        
        // final formatting
        if (blur === "blur") {
          input_val += ".00";
        }
      }
      
      // send updated string to input
      input.val(input_val);
    
      // put caret back in the right position
      var updated_len = input_val.length;
      caret_pos = updated_len - original_len + caret_pos;
      input[0].setSelectionRange(caret_pos, caret_pos);
    }




    function format_price(n) { ///////function for price
    return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }
   
   
    window.onload = function(){
        $('#cost_project').val(format_price(<?php echo $info_project->cost_project; ?>));
    };




    function edit_project_ajax(){
       var name_project =  $('#name_project').val();
       var no_project = $('#no_project').val();
       var date_project =$('#date_project').val();
       var owner_project = $('#owner_project').val();
       var cost_project = $('#cost_project').val();
       var cost_project = Number(cost_project.replace(/[^0-9.-]+/g,""));
       var objective_project = $('#objective_project').val();
       var id_project = $('#id_project').val();
        $.post("<?=site_url('index.php/purchase/edit_project_ajax') ?>", {
            name_project: name_project,
            id_project : id_project,
            no_project: no_project,
            date_project: date_project,
            owner_project: owner_project,
            cost_project: cost_project,
            objective_project: objective_project
        })
            .done(function (response) {
                alert(response);
                if(response=="แก้ไขโครงการสำเร็จ"){
                    location.reload();
                }
            });
    }



</script>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-save fa-2x text-gray-300"></i> เพิ่มโครงการ</h1>
    <p class="mb-4">เพิ่มโครงการการจัดซื้อจัดจ้าง</p>





    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_vender"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เพิ่มโครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <label for="name_project">ชื่อโครงการ</label>
                <div class="input-group mb-3">
                    <input   type="text" id="name_project" name="name_project"  value="<?php echo $info_project->name_project ; ?>" class="form-control" />
                </div>
                <label for="no_project">เลขที่โครงการ</label>
                <div class="input-group mb-3">
                    <input   type="text" id="no_project" name="no_project"  value="<?php echo $info_project->no_project ; ?>" class="form-control" />
                </div>
                <label for="date_project">วันที่</label>
                <div class="input-group mb-3">
                    <input   type="text" id="date_project" name="date_project" value="<?php echo date_db_to_date_thai($info_project->date_project); ?>" data-provide="datepicker" data-date-language="th-th" class="form-control"/>
                </div>
                <label for="owner_project">เจ้าของโครงการ</label>
                <div class="input-group mb-3">
                    <input   type="text" id="owner_project" name="owner_project"  value="<?php echo $info_project->owner_project ; ?>" class="form-control"/>
                </div>
                <label for="cost_project">งบประมาณโครงการ</label>
                <div class="input-group mb-3">
                    <input   type="text" id="cost_project" name="cost_project" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" value="<?php echo $info_project->cost_project ; ?>" class="form-control"/>
                    <input   type="text" id="id_project" name="id_project"  value="<?php echo $info_project->id_project ; ?>" class="form-control" hidden/>
                    <span class="input-group-text">บาท</span>
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text">วัตถุประสงค์ของโครงการ</span>
                    <textarea class="form-control" aria-label="With textarea" id="objective_project" name="objective_project" ><?php echo $info_project->objective_project ; ?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="edit_project" name="edit_project"  onclick="edit_project_ajax()">แก้ไข</button>

                    <button type="reset" class="btn btn-danger"  onclick="window.close()">ยกเลิก</button>
                </div>
            </div>
        </div>
    </div>




</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->








