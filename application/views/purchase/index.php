
<!-- Begin Page Content -->
<div class="container-fluid" >

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">

        <h1 class="h3 mb-0 text-gray-800">รายงานสรุป</h1>

    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a href="<?php echo base_url('index.php/audit/show_bid'); ?>">จำนวนการจัดซื้อจัดจ้างทั้งหมด</a> </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_num_bid">โครงการ</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">ราคากลางทั้งหมด</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_price_all_bid"> บาท</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">จำนวนเงินในการจัดซื้อจัดจ้าง</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_all_price">บาท </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">จำนวนการจัดซื้อจัดจ้างที่ ยกเลิกโครงการ</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_fail_bid"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-book-reader fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->

    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">กราฟแสดง จำนวนค่าใช้จ่ายแยกตามปีบัญชีต่อเดือน </h6>

                </div>

                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="chart-0"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <!-- Area Chart -->
        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">กราฟแสดง จำนวนค่าใช้จ่ายแยกตามปีบัญชีต่อไตรไตรมาส </h6>

                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pie Chart -->
        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">อัตราส่วนค่าใช้จ่าย แยกตามปีบัญชี</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <canvas id="myPieChart"></canvas>
                    </div>
                    <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <i class="fas fa-circle text-primary" ><p id="year_subtract_two"></p></i> 
                    </span>
                        <span class="mr-2">
                      <i class="fas fa-circle text-success" ><p id="year_subtract_one"></p></i> 
                    </span>
                        <span class="mr-2">
                      <i class="fas fa-circle text-info" ><p id="current_year"></p></i> 
                    </span>
                    </div>
                </div>

            </div>
        </div>
    </div>





</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->




<script >

    window.onload = function(){
        var current_y_en = new Date().getFullYear();
        var current_y_th = current_y_en+543;       

        show_num_bid();
        show_price_all_bid();
        show_fail_bid();
        show_all_price();
        area_chart(current_y_th);
        generateData1(current_y_th);
        pie_chart(current_y_th);
    };

    function format_price(n) { ///////function for price
        return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+" บาท";
    }






    function show_num_bid(){
        $.post( "<?=site_url('index.php/chart/show_num_bid') ?>", {})
            .done(function( response ) {

               $('#show_num_bid').html(response);
            });
    }
    
    function show_all_price() {
        $.post( "<?=site_url('index.php/chart/show_all_price') ?>", {})
            .done(function( response ) {
                var p = response*1;
                var price = format_price(p) ;
                $('#show_all_price').html(price);
            });
    }

    function show_price_all_bid(){
        $.post( "<?=site_url('index.php/chart/show_price_all_bid') ?>", {})
            .done(function( response ) {
                var p = response*1;
                var price = format_price(p) ;
                $('#show_price_all_bid').html(price);
            });
    }

    function show_fail_bid(){
        $.post( "<?=site_url('index.php/chart/show_fail_bid') ?>", {})
            .done(function( response ) {
                $('#show_fail_bid').html(response);
            });
    }

    /* <!-- DATA area Chart  --> */

    // Set new default font family and font color to mimic Bootstrap's default styling
    //Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
   // Chart.defaults.global.defaultFontColor = '#858796';

    function number_format(number, decimals, dec_point, thousands_sep) {
        // *     example: number_format(1234.56, 2, ',', ' ');
        // *     return: '1 234,56'
        number = (number + '').replace(',', '').replace(' ', '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }






    function area_chart(year_th) {
        var current_year = year_th;
        var year_subtract_one = year_th-1;
        var year_subtract_two = year_th-2;


        $.ajax({
            type: "POST",
            url: "<?=site_url('index.php/chart/show_area_chart') ?>",
            data:  {year_th:year_th} ,
            success: function(result) {
                var result_year_subtract_two = result.y3;
                var tri1_year_subtract_two = result_year_subtract_two.total_tri_1;
                var tri2_year_subtract_two = result_year_subtract_two.total_tri_2;
                var tri3_year_subtract_two = result_year_subtract_two.total_tri_3;
                var tri4_year_subtract_two = result_year_subtract_two.total_tri_4;
                var datatri_year_subtract_two = [tri1_year_subtract_two,tri2_year_subtract_two,tri3_year_subtract_two,tri4_year_subtract_two];

                var result_year_subtract_one = result.y2;
                var tri1_year_subtract_one = result_year_subtract_one.total_tri_1;
                var tri2_year_subtract_one = result_year_subtract_one.total_tri_2;
                var tri3_year_subtract_one = result_year_subtract_one.total_tri_3;
                var tri4_year_subtract_one = result_year_subtract_one.total_tri_4;
                var datatri_year_subtract_one = [tri1_year_subtract_one,tri2_year_subtract_one,tri3_year_subtract_one,tri4_year_subtract_one];

                var result_current_year = result.y1;
                var tri1_current_year = result_current_year.total_tri_1;
                var tri2_current_year = result_current_year.total_tri_2;
                var tri3_current_year = result_current_year.total_tri_3;
                var tri4_current_year = result_current_year.total_tri_4;
                var datatri_current_year = [tri1_current_year,tri2_current_year,tri3_current_year,tri4_current_year];

                var labeltri = [" ไตรไตรมาส 1","ไตรไตรมาส 2","ไตรไตรมาส 3","ไตรไตรมาส 4"]

                // Area Chart Example
                var ctx = document.getElementById("myAreaChart");
                var myLineChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: labeltri,
                        datasets: [
                            {
                            label: "ปีบัญชี "+year_subtract_two,
                            lineTension: 0.3,
                            backgroundColor: "rgba(112, 33, 225, 0.05)",
                            borderColor: "rgba(112, 33, 225, 1)",
                            pointRadius: 3,
                            pointBackgroundColor: "rgba(78, 115, 223, 1)",
                            pointBorderColor: "rgba(78, 115, 223, 1)",
                            pointHoverRadius: 3,
                            pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                            pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                            pointHitRadius: 10,
                            pointBorderWidth: 2,
                            data:datatri_year_subtract_two ,
                            },
                            {
                                label: "ปีบัญชี "+year_subtract_one,
                                lineTension: 0.3,
                                backgroundColor: "rgba(0,255,0,0.3)",
                                borderColor: "rgba(0,255,0,1)",
                                pointRadius: 3,
                                pointBackgroundColor: "rgba(0,255,0,1)",
                                pointBorderColor: "rgba(0,255,0,1)",
                                pointHoverRadius: 3,
                                pointHoverBackgroundColor: "rgba(0,255,0,1)",
                                pointHoverBorderColor: "rgba(0,255,0,1)",
                                pointHitRadius: 10,
                                pointBorderWidth: 2,
                                data:datatri_year_subtract_one ,
                            },
                            {
                                label: "ปีบัญชี "+current_year,
                                lineTension: 0.3,
                                backgroundColor: "rgba(255,0,0,0.3)",
                                borderColor: "rgba(255,0,0,1)",
                                pointRadius: 3,
                                pointBackgroundColor: "rgba(255,0,0,0.1)",
                                pointBorderColor: "rgba(255,0,0,1)",
                                pointHoverRadius: 3,
                                pointHoverBackgroundColor: "rgba(255,0,0,1)",
                                pointHoverBorderColor: "rgba(255,0,0,1)",
                                pointHitRadius: 10,
                                pointBorderWidth: 2,
                                data:datatri_current_year ,
                            }
                        ],
                    },
                    options: {
                        maintainAspectRatio: false,
                        layout: {
                            padding: {
                                left: 10,
                                right: 25,
                                top: 25,
                                bottom: 0
                            }
                        },
                        scales: {
                            xAxes: [{
                                time: {
                                    unit: 'date'
                                },
                                gridLines: {
                                    display: false,
                                    drawBorder: false
                                },
                                ticks: {
                                    maxTicksLimit: 7
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    maxTicksLimit: 5,
                                    padding: 10,
                                    // Include a dollar sign in the ticks
                                    callback: function(value, index, values) {
                                        return '‎฿' + number_format(value);
                                    }
                                },
                                gridLines: {
                                    color: "rgb(234, 236, 244)",
                                    zeroLineColor: "rgb(234, 236, 244)",
                                    drawBorder: false,
                                    borderDash: [2],
                                    zeroLineBorderDash: [2]
                                }
                            }],
                        },
                        legend: {
                            display: false
                        },
                        tooltips: {
                            backgroundColor: "rgb(255,255,255)",
                            bodyFontColor: "#858796",
                            titleMarginBottom: 10,
                            titleFontColor: '#6e707e',
                            titleFontSize: 14,
                            borderColor: '#dddfeb',
                            borderWidth: 1,
                            xPadding: 15,
                            yPadding: 15,
                            displayColors: false,
                            intersect: false,

                            caretPadding: 10,
                            callbacks: {
                                label: function(tooltipItem, chart) {
                                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                    return datasetLabel + ': ' + number_format(tooltipItem.yLabel)+' บาท';
                                }
                            }
                        }
                    }
                });



            }
        });
    }
    /* <!-- END of area Chart  --> */







    /* <!-- DATA of pie Chart  --> */

    // Pie Chart Example
    function pie_chart(year_th) {
        var current_year = year_th;
        var year_subtract_one = year_th-1;
        var year_subtract_two = year_th-2;
        $('#current_year').replaceWith("ปีบัญชี "+current_year);
        $('#year_subtract_one').replaceWith("ปีบัญชี "+year_subtract_one);
        $('#year_subtract_two').replaceWith("ปีบัญชี "+year_subtract_two);

        $.ajax({
            type: "POST",
            url: "<?=base_url('index.php/chart/show_area_chart_by_month') ?>",
            data:  {year_th:year_th} ,
            success: function(result) {

                var result_year_subtract_two = result.y3;
                var april_year_subtract_two = result_year_subtract_two.total_april;
                var may_year_subtract_two = result_year_subtract_two.total_may;
                var june_year_subtract_two = result_year_subtract_two.total_june;
                var july_year_subtract_two = result_year_subtract_two.total_july;
                var august_year_subtract_two = result_year_subtract_two.total_august;
                var september_year_subtract_two = result_year_subtract_two.total_september;
                var october_year_subtract_two = result_year_subtract_two.total_october;
                var november_year_subtract_two = result_year_subtract_two.total_november;
                var december_year_subtract_two = result_year_subtract_two.total_december;
                var january_year_subtract_two = result_year_subtract_two.total_january;
                var february_year_subtract_two = result_year_subtract_two.total_february;
                var march_year_subtract_two = result_year_subtract_two.total_march;
                var y_year_subtract_two = april_year_subtract_two+may_year_subtract_two+june_year_subtract_two+july_year_subtract_two+august_year_subtract_two+september_year_subtract_two+october_year_subtract_two+november_year_subtract_two+december_year_subtract_two+january_year_subtract_two+february_year_subtract_two+march_year_subtract_two;


                var result_year_subtract_one = result.y2;
                var april_year_subtract_one = result_year_subtract_one.total_april;
                var may_year_subtract_one = result_year_subtract_one.total_may;
                var june_year_subtract_one = result_year_subtract_one.total_june;
                var july_year_subtract_one = result_year_subtract_one.total_july;
                var august_year_subtract_one = result_year_subtract_one.total_august;
                var september_year_subtract_one = result_year_subtract_one.total_september;
                var october_year_subtract_one = result_year_subtract_one.total_october;
                var november_year_subtract_one = result_year_subtract_one.total_november;
                var december_year_subtract_one = result_year_subtract_one.total_december;
                var january_year_subtract_one = result_year_subtract_one.total_january;
                var february_year_subtract_one = result_year_subtract_one.total_february;
                var march_year_subtract_one = result_year_subtract_one.total_march;
                var y_year_subtract_one = april_year_subtract_one+may_year_subtract_one+june_year_subtract_one+july_year_subtract_one+august_year_subtract_one+september_year_subtract_one+october_year_subtract_one+november_year_subtract_one+december_year_subtract_one+january_year_subtract_one+february_year_subtract_one+march_year_subtract_one;


                var result_current_year = result.y1;
                var april_current_year = result_current_year.total_april;
                var may_current_year = result_current_year.total_may;
                var june_current_year = result_current_year.total_june;
                var july_current_year = result_current_year.total_july;
                var august_current_year = result_current_year.total_august;
                var september_current_year = result_current_year.total_september;
                var october_current_year = result_current_year.total_october;
                var november_current_year = result_current_year.total_november;
                var december_current_year = result_current_year.total_december;
                var january_current_year = result_current_year.total_january;
                var february_current_year = result_current_year.total_february;
                var march_current_year = result_current_year.total_march;
                var y_current_year = april_current_year+may_current_year+june_current_year+july_current_year+august_current_year+september_current_year+october_current_year+november_current_year+december_current_year+january_current_year+february_current_year+march_current_year;

                var ctx = document.getElementById("myPieChart");
                var myPieChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ["ปีบัญชี "+year_subtract_two, "ปีบัญชี "+year_subtract_one, "ปีบัญชี112112 "+current_year],
                        datasets: [{
                            data: [y_year_subtract_two, y_year_subtract_one, y_current_year],
                            backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc'],
                            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
                            hoverBorderColor: "rgba(234, 236, 244, 1)",
                        }],
                    },
                    options: {
                        maintainAspectRatio: false,
                        tooltips: {
                            backgroundColor: "rgb(255,255,255)",
                            bodyFontColor: "#858796",
                            borderColor: '#dddfeb',
                            borderWidth: 1,
                            xPadding: 15,
                            yPadding: 15,
                            displayColors: false,
                            caretPadding: 10,



                        },
                        legend: {
                            display: false
                        },
                        cutoutPercentage: 80,
                    },
                });
            }
        })
    }
    /* <!-- END of pai Chart  --> */



</script>




<link rel="stylesheet" type="text/css" href="<?php echo base_url('theme/vendor/chart.js/style.css') ;?>">

<script src="<?php echo base_url('') ;?>theme/vendor/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url('') ;?>theme/vendor/chart.js/utils.js"></script>
<script src="<?php echo base_url('') ;?>theme/vendor/chart.js/analyser.js"></script>
<script>

    
    


    


    var presets = window.chartColors;
    var utils = Samples.utils;

    function generateData1(year_th) {
        var current_year = year_th;
        var year_subtract_one = year_th-1;
        var year_subtract_two = year_th-2;
        

       $.ajax({
            type: "POST",
            url: "<?=base_url('index.php/chart/show_area_chart_by_month') ?>",
            data:  {year_th:year_th} ,
            success: function(result) {                

                var result_year_subtract_two = result.y3;
                var april_year_subtract_two = result_year_subtract_two.total_april;
                var may_year_subtract_two = result_year_subtract_two.total_may;
                var june_year_subtract_two = result_year_subtract_two.total_june;
                var july_year_subtract_two = result_year_subtract_two.total_july;
                var august_year_subtract_two = result_year_subtract_two.total_august;
                var september_year_subtract_two = result_year_subtract_two.total_september;
                var october_year_subtract_two = result_year_subtract_two.total_october;
                var november_year_subtract_two = result_year_subtract_two.total_november;
                var december_year_subtract_two = result_year_subtract_two.total_december;
                var january_year_subtract_two = result_year_subtract_two.total_january;
                var february_year_subtract_two = result_year_subtract_two.total_february;
                var march_year_subtract_two = result_year_subtract_two.total_march;
                var y_year_subtract_two = [april_year_subtract_two,may_year_subtract_two,june_year_subtract_two,july_year_subtract_two,august_year_subtract_two,september_year_subtract_two,october_year_subtract_two,november_year_subtract_two,december_year_subtract_two,january_year_subtract_two,february_year_subtract_two,march_year_subtract_two];


                var result_year_subtract_one = result.y2;
                var april_year_subtract_one = result_year_subtract_one.total_april;
                var may_year_subtract_one = result_year_subtract_one.total_may;
                var june_year_subtract_one = result_year_subtract_one.total_june;
                var july_year_subtract_one = result_year_subtract_one.total_july;
                var august_year_subtract_one = result_year_subtract_one.total_august;
                var september_year_subtract_one = result_year_subtract_one.total_september;
                var october_year_subtract_one = result_year_subtract_one.total_october;
                var november_year_subtract_one = result_year_subtract_one.total_november;
                var december_year_subtract_one = result_year_subtract_one.total_december;
                var january_year_subtract_one = result_year_subtract_one.total_january;
                var february_year_subtract_one = result_year_subtract_one.total_february;
                var march_year_subtract_one = result_year_subtract_one.total_march;
                var y_year_subtract_one = [april_year_subtract_one,may_year_subtract_one,june_year_subtract_one,july_year_subtract_one,august_year_subtract_one,september_year_subtract_one,october_year_subtract_one,november_year_subtract_one,december_year_subtract_one,january_year_subtract_one,february_year_subtract_one,march_year_subtract_one];


                var result_current_year = result.y1;
                var april_current_year = result_current_year.total_april;
                var may_current_year = result_current_year.total_may;
                var june_current_year = result_current_year.total_june;
                var july_current_year = result_current_year.total_july;
                var august_current_year = result_current_year.total_august;
                var september_current_year = result_current_year.total_september;
                var october_current_year = result_current_year.total_october;
                var november_current_year = result_current_year.total_november;
                var december_current_year = result_current_year.total_december;
                var january_current_year = result_current_year.total_january;
                var february_current_year = result_current_year.total_february;
                var march_current_year = result_current_year.total_march;
                var y_current_year = [april_current_year,may_current_year,june_current_year,july_current_year,august_current_year,september_current_year,october_current_year,november_current_year,december_current_year,january_current_year,february_current_year,march_current_year];


                var data = {
                    labels: ['เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม','มกราคม','กุมภาพันธ์','มีนาคม'],
                    datasets: [ {
                        backgroundColor: utils.transparentize(presets.orange),
                        borderColor: presets.orange,
                        data: y_year_subtract_two ,
                        label: 'ปีบัญชี '+year_subtract_two,
                        fill: false
                    }, {
                        backgroundColor: utils.transparentize(presets.yellow),
                        borderColor: presets.yellow,
                        data: y_year_subtract_one,
                        label: 'ปีบัญชี '+year_subtract_one,
                        fill: false
                    }, {
                        backgroundColor: utils.transparentize(presets.green),
                        borderColor: presets.green,
                        data: y_current_year,
                        label: 'ปีบัญชี '+current_year,
                        fill: false
                    }]
                };

                var options = {
                    maintainAspectRatio: false,
                    spanGaps: false,
                    elements: {
                        line: {
                            tension: 0.000001
                        }
                    },
                    scales: {
                        xAxes: [{
                            time: {
                                unit: 'date'
                            },
                            gridLines: {
                                display: false,
                                drawBorder: false
                            },
                            ticks: {
                                maxTicksLimit: 12
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                maxTicksLimit: 5,
                                padding: 10,
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return '‎฿' + number_format(value);
                                }
                            },
                            gridLines: {
                                color: "rgb(234, 236, 244)",
                                zeroLineColor: "rgb(234, 236, 244)",
                                drawBorder: false,
                                borderDash: [2],
                                zeroLineBorderDash: [2]
                            }
                        }],
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        },
                        'samples-filler-analyser': {
                            target: 'chart-analyser'
                        }
                    },
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        titleMarginBottom: 10,
                        titleFontColor: '#6e707e',
                        titleFontSize: 14,
                        borderColor: '#dddfeb',
                        borderWidth: 1,
                        xPadding: 15,
                        yPadding: 15,
                        displayColors: false,
                        intersect: false,

                        caretPadding: 10,
                        callbacks: {
                            label: function(tooltipItem, chart) {
                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                return datasetLabel + ': ' + number_format(tooltipItem.yLabel)+' บาท';
                            }
                        }
                    }
                };

                var chart = new Chart('chart-0', {
                    type: 'line',
                    data: data,
                    options: options
                });


            }
        });




    }



    // eslint-disable-next-line no-unused-vars
    function toggleSmooth(btn) {
        var value = btn.classList.toggle('btn-on');
        chart.options.elements.line.tension = value ? 0.4 : 0.000001;
        chart.update();
    }


</script>




