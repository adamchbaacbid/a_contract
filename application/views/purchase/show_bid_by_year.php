<?php
$data = $this->session->userdata('data');
$user_member = $data['member'];

?>
<script>

    function date_thai(date){

        if(date == '0000-00-00'){
            var datethai = "ไม่ได้ระบุวันที่";
        }else {
            var day = date.substr(8,9);
            var month = date.substr(5,2);
            var year = date.substr(0,4);
            day = +day;
            year = +year;
            month = +month-1;

            now = new Date();
            var thday = new Array ("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัส","ศุกร์","เสาร์");
            var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
            var thm = new Array ("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.", "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
            var datethai = day+" "+thm[month]+" "+(year+543);
        }


        return datethai;
    }

    function format_price(n) { ///////function for price
        return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+" บาท";
    }

    function info_bid_button(no_bid){

        $('#form_info_'+no_bid).submit();
    }
    function edit_bid(no_bid){

        $('#form_edit_'+no_bid).submit();
    }

    function search_bid(year){
        $.post( "<?=site_url('index.php/purchase/show_bid_by_year_ajax') ?>", {
            year:year
        })
            .done(function( response ) {
                var arr = JSON.parse(response);
                $('#dataTable').empty();
                var msg_table = $('#dataTable'), container;
                var i = 1 ;

                msg_table.append("<thead>\n" +
                    "<tr>\n" +
                    "<th scope=\"col\">#</th>\n" +
                    "<th scope=\"col\">สถานะ</th>\n" +
                    "<th scope=\"col\">วันที่จัดซื้อจัดจ้าง</th>\n" +
                    "<th scope=\"col\">โครงการ</th>\n" +
                    "<th scope=\"col\"></th>\n" +
                    "<th scope=\"col\"></th>\n" +

                    "</tr>\n" +
                    "</thead>");

                body = $('<tbody></tbody>');
                msg_table.append(body);


                for (var key in arr) {
                    var url_edit = '<?php echo base_url('index.php/purchase/edit_bid');?>';
                    var url_info = '<?php echo base_url('index.php/purchase/info_bid');?>';
                    var member = arr[key].id_member ;
                    var user_member = '<?php echo "$user_member"; ?>';

                    if(user_member == member){
                        var td_edit = '<td  ><a href="#" id="edit'+arr[key].no_bid+'"  title="แก้ไข" onclick="edit_bid('+arr[key].no_bid+')"> <i class="fas fa-edit"></i></a></td>';
                    }else {
                        var td_edit = '<td  ></td>';
                    }




                    container = $('<tr></tr>');
                    body.append(container);
                    container.append('<form action="'+url_edit+'" id="form_edit_'+arr[key].no_bid+'" target="_blank" method="post"><input type="text" id="no_bid" name="no_bid" value="'+arr[key].no_bid+'" hidden> </form>');
                    container.append('<form action="'+url_info+'" id="form_info_'+arr[key].no_bid+'" target="_blank" method="post"><input type="text" id="info_no_bid" name="info_no_bid" value="'+arr[key].no_bid+'" hidden> </form>');
                    var d = arr[key].date_bid;
                    d = date_thai(d);

                    container.append('<td scope=\'row\' width=\'5%\'>' + i + '</td>');
                    container.append('<td  width=\'15%\'>' + arr[key].status_bid + '</td>');
                    container.append('<td  width=\'15%\'>' + d + '</td>');
                    container.append('<td  width=\'55%\'> ' + arr[key].name_bid + '</td>');


                    container.append('<td><a href="#"  title="แสดงข้อมูล" onclick="info_bid_button('+arr[key].no_bid+')"> <i class="fas fa-info-circle"></i></a></td>');
                    container.append(td_edit);
                    i++;
                }
                i=i-1;

                $('#show_name_vender').html('จำนวนการจัดซื้อจัดจ้างทั้งหมดจากปีบัญชี '+year+' (มีจำนวน '+i+' โครงการ)');
            });
    }

    function show_table_bid_cancle_by_year_ajax(year){

        $.post( "<?=site_url('index.php/purchase/show_table_bid_cancle_by_year_ajax') ?>", {
            year:year
        })
            .done(function( response ) {

                var arr = JSON.parse(response);
                $('#dataTable').empty();
                var msg_table = $('#dataTable'), container;
                var i = 1 ;

                msg_table.append("<thead>\n" +
                    "<tr>\n" +
                    "<th scope=\"col\">#</th>\n" +
                    "<th scope=\"col\">สถานะ</th>\n" +
                    "<th scope=\"col\">วันที่จัดซื้อจัดจ้าง</th>\n" +
                    "<th scope=\"col\">โครงการ</th>\n" +
                    "<th scope=\"col\"></th>\n" +
                    "<th scope=\"col\"></th>\n" +
                    "</tr>\n" +
                    "</thead>");

                body = $('<tbody></tbody>');
                msg_table.append(body);

                for (var key in arr) {
                    var url_edit = '<?php echo base_url('index.php/purchase/edit_bid');?>';
                    var url_info = '<?php echo base_url('index.php/purchase/info_bid');?>';

                    var member = arr[key].id_member ;
                    var user_member = '<?php echo "$user_member"; ?>';

                    if(user_member == member){
                        var td_edit = '<td  ><a href="#" id="edit'+arr[key].no_bid+'"  title="แก้ไข" onclick="edit_bid('+arr[key].no_bid+')"> <i class="fas fa-edit"></i></a></td>';
                    }else {
                        var td_edit = '<td  ></td>';
                    }

                    container = $('<tr></tr>');
                    body.append(container);
                    container.append('<form action="'+url_edit+'" id="form_edit_'+arr[key].no_bid+'" target="_blank" method="post"><input type="text" id="no_bid" name="no_bid" value="'+arr[key].no_bid+'" hidden> </form>');
                    container.append('<form action="'+url_info+'" id="form_info_'+arr[key].no_bid+'" target="_blank" method="post"><input type="text" id="info_no_bid" name="info_no_bid" value="'+arr[key].no_bid+'" hidden> </form>');
                    var d = arr[key].date_bid;
                    d = date_thai(d);

                    container.append('<td scope=\'row\' width=\'5%\'>' + i + '</td>');
                    container.append('<td  width=\'15%\'>' + arr[key].status_bid + '</td>');
                    container.append('<td  width=\'15%\'>' + d + '</td>');
                    container.append('<td  width=\'55%\'> ' + arr[key].name_bid + '</td>');

                    container.append('<td><a href="#" title="แสดงข้อมูล" onclick="info_bid_button('+arr[key].no_bid+')"> <i class="fas fa-info-circle"></i></a></td>');
                    container.append(td_edit);
                    i++;
                }
                i=i-1;

                $('#show_name_vender').html('โครงการที่ยกเลิก ปีบัญชี '+year+' (มีจำนวน '+i+' โครงการ)');
            });
    }

    function show_price_under_price_base_bid_by_year_ajax(year){

        $.post( "<?=site_url('index.php/purchase/show_price_under_price_base_bid_by_year_ajax') ?>", {
            year:year
        })
            .done(function( response ) {

                var arr = JSON.parse(response);
                $('#dataTable').empty();
                var msg_table = $('#dataTable'), container;
                var i = 1 ;

                msg_table.append("<thead>\n" +
                    "<tr>\n" +
                    "<th scope=\"col\">#</th>\n" +
                    "<th scope=\"col\">สถานะ</th>\n" +
                    "<th scope=\"col\">วันที่จัดซื้อจัดจ้าง</th>\n" +
                    "<th scope=\"col\">โครงการ</th>\n" +
                    "<th scope=\"col\">ลดลงจากราคากลางร้อยละ</th>\n" +
                    "<th scope=\"col\"></th>\n" +
                    "<th scope=\"col\"></th>\n" +
                    "</tr>\n" +
                    "</thead>");

                body = $('<tbody></tbody>');
                msg_table.append(body);

                for (var key in arr) {

                    var price_per = arr[key].price_per

                    if(price_per>30){
                        var per = price_per*1;
                        var url_edit = '<?php echo base_url('index.php/purchase/edit_bid');?>';
                        var url_info = '<?php echo base_url('index.php/purchase/info_bid');?>';

                        var member = arr[key].id_member ;
                        var user_member = '<?php echo "$user_member"; ?>';

                        if(user_member == member){
                            var td_edit = '<td><a href="#" id="edit'+arr[key].no_bid+'"  title="แก้ไข" onclick="edit_bid('+arr[key].no_bid+')"> <i class="fas fa-edit"></i></a></td>';
                        }else {
                            var td_edit = '<td></td>';
                        }

                        container = $('<tr></tr>');
                        body.append(container);
                        container.append('<form action="'+url_edit+'" id="form_edit_'+arr[key].no_bid+'" target="_blank" method="post"><input type="text" id="no_bid" name="no_bid" value="'+arr[key].no_bid+'" hidden> </form>');
                        container.append('<form action="'+url_info+'" id="form_info_'+arr[key].no_bid+'" target="_blank" method="post"><input type="text" id="info_no_bid" name="info_no_bid" value="'+arr[key].no_bid+'" hidden> </form>');
                        var d = arr[key].date_bid;
                        d = date_thai(d);

                        container.append('<td scope=\'row\' width=\'5%\'>' + i + '</td>');
                        container.append('<td  width=\'15%\'>' + arr[key].status_bid + '</td>');
                        container.append('<td  width=\'15%\'>' + d + '</td>');
                        container.append('<td  width=\'55%\'> ' + arr[key].name_bid + '</td>');
                        container.append('<td  width=\'55%\'> ' + per.toFixed(2) + '</td>');
                        container.append('<td><a href="#" title="แสดงข้อมูล" onclick="info_bid_button('+arr[key].no_bid+')"> <i class="fas fa-info-circle"></i></a></td>');
                        container.append(td_edit);
                        i++;
                    }
                }
                i=i-1;

                $('#show_name_vender').html('โครงการที่เสนอราคาต่ำกว่าราคากลาง มากกว่าร้อยละ30 ปีบัญชี '+year+' (มีจำนวน '+i+' โครงการ)');
            });
    }

    function info_bid(no_bid){
        $('#form_info_bid'+no_bid).submit();
    }

    function all_show() {
        var year = $('#year').val();
        search_bid(year);
        show_num_bid(year);
        show_price_base_bid_by_year(year);
        show_price_bid_by_year(year);
        show_bid_cancle_by_year(year);
        show_price_under_price_base_bid_by_year(year);

    }

    function show_num_bid(year){
        $.post( "<?=site_url('index.php/purchase/show_num_bid_by_year') ?>", {
            year:year
        })
            .done(function( response ) {
                $('#show_num_bid').text(response+' โครงการ');
                $('#label_show_num_bid').empty();
                $('#label_show_num_bid').html('<a href="#" onclick="search_bid('+year+')">จำนวนการจัดซื้อจัดจ้างทั้งหมดจากปีบัญชี '+year+'</a>');
            });
    }

    function show_price_base_bid_by_year(year){
        $.post( "<?=site_url('index.php/purchase/show_price_base_bid_by_year') ?>", {
            year:year
        })
            .done(function( response ) {
                var price_base_bid = response*1;
                var p = format_price(price_base_bid);
                $('#show_price_all_bid').text(p);
                $('#label_show_price_all_bid').empty();
                $('#label_show_price_all_bid').text('ราคากลางประจำปีบัญชี '+year);
            });
    }
    
    function show_price_bid_by_year(year){
        $.post("<?=site_url('index.php/purchase/show_price_bid_by_year') ?>",{
            year:year
        })
            .done(function (response) {
                var p = response*1;
                var price = format_price(p);
                $('#show_all_price').text(price);
                $('#label_show_all_price').empty();
                $('#label_show_all_price').text('ใช้จำนวนงเงินประจำปีบัญชี '+year);
            });
    }

    function show_bid_cancle_by_year(year) {
        $.post("<?=site_url('index.php/purchase/show_bid_cancle_by_year') ?>",{
            year:year
        })
            .done(function (response) {

                $('#show_fail_bid').text(response+' โครงการ');
                $('#label_show_fail_bid').empty();
                $('#label_show_fail_bid').html('<a href="#" onclick="show_table_bid_cancle_by_year_ajax('+year+')">โครงการที่ยกเลิก ปีบัญชี '+year+'</a>');
            });
    }

    function show_price_under_price_base_bid_by_year(year) {
        $.post("<?=site_url('index.php/purchase/show_price_under_price_base_bid_by_year') ?>",{
            year:year
        })
            .done(function (response) {

                $('#show_price_under_price_base_bid').text(response+' โครงการ');
                $('#label_show_price_under_price_base_bid').empty();
                $('#label_show_price_under_price_base_bid').html('<a href="#" onclick="show_price_under_price_base_bid_by_year_ajax('+year+')">โครงการที่เสนอราคาต่ำกว่าราคากลาง มากกว่าร้อยละ30 ปีบัญชี '+year+'</a>');

            });
    }








</script>




<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-search fa-2x text-gray-300"></i> ค้นหาการจัดซื้อจัดจ้าง</h1>
    <p class="mb-4"></p>
    <div class="card shadow mb-1">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> ค้นหาการจัดซื้อจัดจ้าง ตามปีบัญชี  </h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="input-group mb-8 col-sm-12 col-lg-6"  >
                        <label class="input-group-text" for="year">ปีบัญชี</label>
                        <select class="form-control" id="year">
                        <?php
                            $y =$years_all_bid['0'];
                            foreach ($years_all_bid as $year){
                                echo "<option>$year->fiscal_year_bid </option>" ;
                            }
                            ?>
                        </select>
                        <button type="submit" class="btn btn-primary" id="search_bid" name="search_bid" onclick="all_show()" data-dismiss="modal" aria-hidden="true">ค้นหา</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Row -->
    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4  mb-1">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1" id="label_show_num_bid"><a href="<?php echo base_url('index.php/purchase/show_bid'); ?>">จำนวนการจัดซื้อจัดจ้างทั้งหมด</a> </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_num_bid"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-4  mb-1">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1" id="label_show_fail_bid">จำนวนการจัดซื้อจัดจ้างที่ ยกเลิกโครงการ</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="show_fail_bid"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-skull-crossbones fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4  mb-1">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1" id="label_show_price_under_price_base_bid">โครงการที่เสนอราคาต่ำกว่าราคากลาง มากกว่าร้อยละ30 </div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800" id="show_price_under_price_base_bid"> </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-percent fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-6 col-md-6 mb-1">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1" id="label_show_price_all_bid">ราคากลางทั้งหมด</div>
                            <div class="h4 mb-0 font-weight-bold text-gray-800" id="show_price_all_bid"> </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-6 col-md-6 mb-1">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1" id="label_show_all_price">จำนวนเงินในการจัดซื้อจัดจ้าง</div>
                            <div class="h4 mb-0 font-weight-bold text-gray-800" id="show_all_price"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3" >

            <h6 class="m-0 font-weight-bold text-primary"  > ผลการค้นหา : <span id="show_name_vender" class="m-0 font-weight-bold text-danger"></span></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive" >
                <table class="table table-striped " id="dataTable"  width="100%" cellspacing="0" >
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">วันที่ประกวดราคา</th>
                            <th scope="col">โครงการ</th>
                            <th scope="col">สถานะ</th>

                        </tr>
                    </thead>
                    <tbody>
                      <tr><td colspan='8' align='center'>ยังไม่มีการค้นหา</td></tr>
                    </tbody>
                </table>
                <div ><pre id="test_json"></pre></div>
            </div>
        </div>
    </div>
</div>



</td>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->




