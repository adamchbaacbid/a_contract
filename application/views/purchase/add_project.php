<?php
    $data = $this->session->userdata('data');
    $name = $data['name'];
?>


<script type="text/javascript" src="<?php echo base_url('theme/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript">
     $(document).ready(function (e) {
        $("input[data-type='currency']").on({
            keyup: function() {
            formatCurrency($(this));            
            },
            blur: function() { 
            formatCurrency($(this), "blur");            
            }
        });
    });   
    
    function formatNumber(n) {
      // format number 1000000 to 1,234,567
      return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }    
    
    function formatCurrency(input, blur) {
      // appends $ to value, validates decimal side
      // and puts cursor back in right position.
      
      // get input value
      var input_val = input.val();
      
      // don't validate empty input
      if (input_val === "") { return; }
      
      // original length
      var original_len = input_val.length;
    
      // initial caret position 
      var caret_pos = input.prop("selectionStart");
        
      // check for decimal
      if (input_val.indexOf(".") >= 0) {
    
        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
        var decimal_pos = input_val.indexOf(".");
    
        // split number by decimal point
        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);
    
        // add commas to left side of number
        left_side = formatNumber(left_side);
    
        // validate right side
        right_side = formatNumber(right_side);
        
        // On blur make sure 2 numbers after decimal
        if (blur === "blur") {
          right_side += "00";
        }
        
        // Limit decimal to only 2 digits
        right_side = right_side.substring(0, 2);
    
        // join number by .
        input_val = "" + left_side + "." + right_side;
    
      } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = formatNumber(input_val);
        input_val = "" + input_val;
        
        // final formatting
        if (blur === "blur") {
          input_val += ".00";
        }
      }
      
      // send updated string to input
      input.val(input_val);
    
      // put caret back in the right position
      var updated_len = input_val.length;
      caret_pos = updated_len - original_len + caret_pos;
      input[0].setSelectionRange(caret_pos, caret_pos);
    }




    function add_project_ajax() {
       var name_project =  $('#name_project').val();
        var name =  $('#name_project').val();
       var no_project = $('#no_project').val();
       var date_project =$('#date_project').val();
       var owner_project = $('#owner_project').val();
       var cost_project = $('#cost_project').val();
       var cost_project = Number(cost_project.replace(/[^0-9.-]+/g,""));
       var objective_project = $('#objective_project').val();

       if(name_project==""){
           alert('กรุณาระบุชื่อโครงการ');
           $('#name_project').focus();
       }else{
           if(owner_project==""){
               alert('กรุณาระบุเจ้าของโครงการ');
               $('#owner_project').focus();
           }else{
               if(cost_project==""){
                   alert('กรุณาระบุงบประมาณโครงการ');
                   $('#cost_project').focus();
               }else{
                   if(objective_project==""){
                       alert('กรุณาระบุวัตถุประสงค์ของโครงการ');
                       $('#objective_project').focus();
                   }else {

                       $.post("<?=site_url('index.php/purchase/add_project_ajax') ?>", {
                           name_project: name_project,
                           no_project: no_project,
                           date_project: date_project,
                           owner_project: owner_project,
                           cost_project: cost_project,
                           objective_project: objective_project
                       })
                           .done(function (response) {
                               alert(response);

                               //show_sub_bid();
                               if(response=='เพิ่มสำเร็จ'){
                                   $('#input_project').val(name);
                               }
                           });
                   }
               }

           }
       }
    }



</script>


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-save fa-2x text-gray-300"></i> เพิ่มโครงการ</h1>
    <p class="mb-4">เพิ่มโครงการการจัดซื้อจัดจ้าง</p>





    <!-- DataTales Example -->
    <div class="card shadow mb-4" id="card_vender"  >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เพิ่มโครงการ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <form id="project" role="form" >
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input   type="text" id="name_project" name="name_project" placeholder="ชื่อโครงการ" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="no_project" name="no_project" placeholder="เลขที่โครงการ" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="date_project" name="date_project" placeholder="ลงวันที่" data-provide="datepicker" data-date-language="th-th" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="owner_project" name="owner_project" placeholder="เจ้าของเรื่อง" class="form-control"/>
                        </div>
                        <div class="input-group mb-3">
                            <input   type="text" id="cost_project" name="cost_project" data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" placeholder="งบประมาณของโครงการ" class="form-control"/>
                            <span class="input-group-text">บาท</span>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text">วัตถุประสงค์ของโครงการ</span>
                            <textarea class="form-control" aria-label="With textarea" id="objective_project" name="objective_project" ></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success" id="add_sub" name="add_sub" data-dismiss="modal" aria-hidden="true" onclick="add_project_ajax()">เพิ่ม</button>

                            <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="reset_project()">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>




</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->








