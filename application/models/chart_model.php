<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/9/2561
 * Time: 14:26
 */

class chart_model extends  CI_Model{

    function  __construct()
    {
        parent::__construct();
    }


    function model_show_num_bid(){
        $sql = "SELECT * FROM bid_tb";
        $result = $this->db->query($sql)->num_rows();

        return $result;
    }

    function model_show_all_price(){
        $sql = "SELECT * FROM bid_tb
INNER JOIN active_vender_tb ON active_vender_tb.no_bid = bid_tb.no_bid
 WHERE active_vender_tb.win_active_vender = '1'";
        $result = $this->db->query($sql)->result();

        $total = 0;
        foreach ($result as $row){
            $chaffer  = $row->chaffer_active_vender;
            $price  = $row->price_active_vender;
            if($chaffer==0){
                $total += $price ;
            }else{
                $total += $chaffer ;
            }

        }

        return $total ;


    }

    function model_show_price_all_bid(){
        $sql = "SELECT price_base_bid FROM bid_tb ";
        $result = $this->db->query($sql)->result();
        //$result = $result['0'];
        $total = 0 ;

        foreach ($result as $row){

            $total += $row->price_base_bid ;
        }

        return $total ;
    }

    function model_show_fail_bid(){
        $sql = "SELECT * FROM bid_tb WHERE status_bid = 'ยกเลิกโครงการ' ";
        $result = $this->db->query($sql)->num_rows();

        return $result ;

    }

    function model_show_area_chart($year){

        $i = 1 ;

        for ($x = 1; $x <= 3; $x++) {
            $th_year = $year;
            $en_year = $year - 543;
            $en_year1 = $en_year + 1;



            $sql = "SELECT bid_tb.date_bid,active_vender_tb.chaffer_active_vender,active_vender_tb.price_active_vender  FROM bid_tb 
                    INNER JOIN active_vender_tb 
                    ON bid_tb.no_bid = active_vender_tb.no_bid 
                    WHERE active_vender_tb.win_active_vender = '1' AND bid_tb.fiscal_year_bid = '$th_year'";

            $sql_tri_1 = $sql."AND bid_tb.date_bid BETWEEN '$en_year-04-01'  AND '$en_year-06-30' ";
            $result_tri_1 = $this->db->query($sql_tri_1)->result();
            $total_tri_1 = 0;
            if($result_tri_1){
                foreach ($result_tri_1 as $num_tri1){
                    $price_tri1c = $num_tri1->chaffer_active_vender;
                    if($price_tri1c=='0'){
                        $price_tri1 = $num_tri1->price_active_vender;
                    }else{
                        $price_tri1 = $num_tri1->chaffer_active_vender;
                    }
                    $total_tri_1 += $price_tri1;
                }
            }

            $sql_tri_2 = $sql."AND bid_tb.date_bid BETWEEN '$en_year-07-01'  AND '$en_year-09-30' ";
            $result_tri_2 = $this->db->query($sql_tri_2)->result();
            $total_tri_2 = 0;
            if($result_tri_2){
                foreach ($result_tri_2 as $num_tri2){
                    $price_tri2c = $num_tri2->chaffer_active_vender;
                    if($price_tri2c=='0'){
                        $price_tri2 = $num_tri2->price_active_vender;
                    }else{
                        $price_tri2 = $num_tri2->chaffer_active_vender;
                    }
                    $total_tri_2 += $price_tri2;
                }
            }

            $sql_tri_3 = $sql."AND bid_tb.date_bid BETWEEN '$en_year-10-01'  AND '$en_year-12-31' ";
            $result_tri_3 = $this->db->query($sql_tri_3)->result();
            $total_tri_3 = 0;
            if($result_tri_3){
                foreach ($result_tri_3 as $num_tri3){
                    $price_tri3c = $num_tri3->chaffer_active_vender;
                    if($price_tri3c=='0'){
                        $price_tri3 = $num_tri3->price_active_vender;
                    }else{
                        $price_tri3 = $num_tri3->chaffer_active_vender;
                    }
                    $total_tri_3 += $price_tri3;
                }
            }

            $sql_tri_4 = $sql."AND bid_tb.date_bid BETWEEN '$en_year1-01-01'  AND '$en_year1-03-31' ";
            $result_tri_4 = $this->db->query($sql_tri_4)->result();
            $total_tri_4 = 0;
            if($result_tri_4){
                foreach ($result_tri_4 as $num){
                    $price_tri4c = $num->chaffer_active_vender;
                    if($price_tri4c=='0'){
                        $price_tri4 = $num->price_active_vender;
                    }else{
                        $price_tri4 = $num->chaffer_active_vender;
                    }
                    $total_tri_4 += $price_tri4;
                }
            }

            header('Content-Type: application/json');
            $arr = array(
                "total_tri_1"=>$total_tri_1,
                "total_tri_2"=>$total_tri_2,
                "total_tri_3"=>$total_tri_3,
                "total_tri_4"=>$total_tri_4
            );

            $data['y'.$i] = $arr ;

            $year = $year-1 ;
            $i++;
        }

        return json_encode($data);
    }





    function model_show_area_chart_by_month($year){

        $i = 1;
        for ($x = 1; $x <= 3; $x++) {
            $th_year = $year ;
            $en_year = $year-543;

            $en_year1 = $en_year+1;


            $sql = "SELECT bid_tb.date_bid,active_vender_tb.chaffer_active_vender,active_vender_tb.price_active_vender  FROM bid_tb 
                    INNER JOIN active_vender_tb 
                    ON bid_tb.no_bid = active_vender_tb.no_bid 
                    WHERE active_vender_tb.win_active_vender = '1' AND bid_tb.fiscal_year_bid = '$th_year' ";

            $sql_april = $sql."AND bid_tb.date_bid BETWEEN '$en_year-04-01'  AND '$en_year-04-30' ";
            $result_april = $this->db->query($sql_april)->result();

            $total_april = 0;

            if($result_april){
                foreach ($result_april as $num_april){
                    $price_april1 = $num_april->chaffer_active_vender;
                    if($price_april1=='0'){
                        $price_april = $num_april->price_active_vender;
                    }else{
                        $price_april = $num_april->chaffer_active_vender;
                    }
                    $total_april += $price_april;
                }
            }


            $sql_may = $sql."AND bid_tb.date_bid BETWEEN '$en_year-05-01'  AND '$en_year-05-31' ";
            $result_may = $this->db->query($sql_may)->result();
            $total_may = 0;
            //$total_may = $this->db->query($sql_may)->num_rows();

            if($result_may){
                foreach ($result_may as $num_may){
                    $price_may1 = $num_may->chaffer_active_vender;
                    if($price_may1=='0'){
                        $price_may = $num_may->price_active_vender;
                    }else{
                        $price_may = $num_may->chaffer_active_vender;
                    }
                    $total_may += $price_may;
                }
            }


            $sql_june = $sql."AND bid_tb.date_bid BETWEEN '$en_year-06-01'  AND '$en_year-06-30' ";
            $result_june = $this->db->query($sql_june)->result();
            $total_june = 0;
            if($result_june){
                foreach ($result_june as $num_june){
                    $price_june1 = $num_june->chaffer_active_vender;
                    if($price_june1=='0'){
                        $price_june = $num_june->price_active_vender;
                    }else{
                        $price_june = $num_june->chaffer_active_vender;
                    }
                    $total_june += $price_june;
                }
            }

            $sql_july = $sql."AND bid_tb.date_bid BETWEEN '$en_year-07-01'  AND '$en_year-07-31' ";
            $result_july = $this->db->query($sql_july)->result();
            $total_july = 0;
            if($result_july){
                foreach ($result_july as $num_july){
                    $price_july1 = $num_july->chaffer_active_vender;
                    if($price_july1=='0'){
                        $price_july = $num_july->price_active_vender;
                    }else{
                        $price_july = $num_july->chaffer_active_vender;
                    }
                    $total_july += $price_july;
                }
            }

            $sql_august = $sql."AND bid_tb.date_bid BETWEEN '$en_year-08-01'  AND '$en_year-08-31' ";
            $result_august = $this->db->query($sql_august)->result();
            $total_august = 0;
            if($result_august){
                foreach ($result_august as $num_august){
                    $price_august1 = $num_august->chaffer_active_vender;
                    if($price_august1=='0'){
                        $price_august = $num_august->price_active_vender;
                    }else{
                        $price_august = $num_august->chaffer_active_vender;
                    }
                    $total_august += $price_august;
                }
            }

            $sql_september = $sql."AND bid_tb.date_bid BETWEEN '$en_year-09-01'  AND '$en_year-09-30' ";
            $result_september = $this->db->query($sql_september)->result();
            $total_september = 0;
            if($result_september){
                foreach ($result_september as $num_september){
                    $price_september1 = $num_september->chaffer_active_vender;
                    if($price_september1=='0'){
                        $price_september = $num_september->price_active_vender;
                    }else{
                        $price_september = $num_september->price_active_vender;
                    }
                    $total_september += $price_september;
                }
            }

            $sql_october = $sql."AND bid_tb.date_bid BETWEEN '$en_year-10-01'  AND '$en_year-10-31' ";
            $result_october = $this->db->query($sql_october)->result();
            $total_october = 0;
            if($result_october){
                foreach ($result_october as $num_october){
                    $price_october1 = $num_october->chaffer_active_vender;
                    if($price_october1=='0'){
                        $price_october = $num_october->price_active_vender;
                    }else{
                        $price_october = $num_october->chaffer_active_vender;
                    }
                    $total_october += $price_october;
                }
            }

            $sql_november = $sql."AND bid_tb.date_bid BETWEEN '$en_year-11-01'  AND '$en_year-11-30' ";
            $result_november = $this->db->query($sql_november)->result();
            $total_november = 0;
            if($result_november){
                foreach ($result_november as $num_november){
                    $price_november1 = $num_november->chaffer_active_vender;
                    if($price_november1=='0'){
                        $price_november = $num_november->price_active_vender;
                    }else{
                        $price_november = $num_november->chaffer_active_vender;
                    }
                    $total_november += $price_november;
                }
            }

            $sql_december = $sql."AND bid_tb.date_bid BETWEEN '$en_year-12-01'  AND '$en_year-12-31' ";
            $result_december = $this->db->query($sql_december)->result();
            $total_december = 0;
            if($result_december){
                foreach ($result_december as $num_december){
                    $price_december1 = $num_december->chaffer_active_vender;
                    if($price_december1=='0'){
                        $price_december = $num_december->price_active_vender;
                    }else{
                        $price_december = $num_december->chaffer_active_vender;
                    }
                    $total_december += $price_december;
                }
            }

            $sql_january = $sql."AND bid_tb.date_bid BETWEEN '$en_year1-01-01'  AND '$en_year1-01-31' ";
            $result_january = $this->db->query($sql_january)->result();
            $total_january = 0;
            if($result_january){
                foreach ($result_january as $num_january){
                    $price_january1 = $num_january->chaffer_active_vender;
                    if($price_january1=='0'){
                        $price_january = $num_january->price_active_vender;
                    }else{
                        $price_january = $num_january->chaffer_active_vender;
                    }
                    $total_january += $price_january;
                }
            }

            $sql_february = $sql."AND bid_tb.date_bid BETWEEN '$en_year1-02-01'  AND '$en_year1-02-28' ";
            $result_february = $this->db->query($sql_february)->result();
            $total_february = 0;
            if($result_february){
                foreach ($result_february as $num_february){
                    $price_february1 = $num_february->chaffer_active_vender;
                    if($price_february1=='0'){
                        $price_february = $num_february->price_active_vender;
                    }else{
                        $price_february = $num_february->chaffer_active_vender;
                    }
                    $total_february += $price_february;
                }
            }

            $sql_march = $sql."AND bid_tb.date_bid BETWEEN '$en_year1-03-01'  AND '$en_year1-03-31' ";
            $result_march = $this->db->query($sql_march)->result();
            $total_march = 0;
            if($result_march){
                foreach ($result_march as $num_march){
                    $price_march1 = $num_march->chaffer_active_vender;
                    if($price_march1=='0'){
                        $price_march = $num_march->price_active_vender;
                    }else{
                        $price_march = $num_march->chaffer_active_vender;
                    }
                    $total_march += $price_march;
                }
            }


            header('Content-Type: application/json');
            $arr = array(
                "total_april"=>$total_april,
                "total_may"=>$total_may,
                "total_june"=>$total_june,
                "total_july"=>$total_july,
                "total_august"=>$total_august,
                "total_september"=>$total_september,
                "total_october"=>$total_october,
                "total_november"=>$total_november,
                "total_december"=>$total_december,
                "total_january"=>$total_january,
                "total_february"=>$total_february,
                "total_march"=>$total_march,
                "year"=>$th_year
            );

            

            $data['y'.$i] = $arr ;

            $year = $year-1 ;
            $i++;

        }

        return json_encode($data);
    }

    function model_show_bar_chart($year){
        $year = $year-543;
        $sql = "SELECT bid_tb.date_bid,vender_tb.price_under_vender,vender_tb.price_vender  
                FROM bid_tb   
                INNER JOIN vender_tb ON bid_tb.no_bid = vender_tb.no_bid 
                WHERE vender_tb.win_vender = '1'";
        $sql_april = $sql." AND bid_tb.date_bid BETWEEN '".$year."-04-01'  AND '".$year."-04-30' ";
        $result_april = $this->db->query($sql_april)->result();
        $total_april = 0;
        if($result_april){
            foreach ($result_april as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_april += $price;
            }
        }

        $sql_may = $sql."AND bid_tb.date_bid BETWEEN '".$year."-05-01'  AND '".$year."-05-31' ";
        $result_may = $this->db->query($sql_may)->result();
        $total_may = 0;
        if($result_may){
            foreach ($result_may as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_may += $price;
            }
        }

        $sql_june = $sql."AND bid_tb.date_bid BETWEEN '".$year."-06-01'  AND '".$year."-06-30' ";
        $result_june = $this->db->query($sql_june)->result();
        $total_june = 0;
        if($result_june){
            foreach ($result_june as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_june += $price;
            }
        }

        $sql_july = $sql."AND bid_tb.date_bid BETWEEN '".$year."-07-01'  AND '".$year."-07-31' ";
        $result_july = $this->db->query($sql_july)->result();
        $total_july = 0;
        if($result_july){
            foreach ($result_july as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_july += $price;
            }
        }

        $sql_august = $sql."AND bid_tb.date_bid BETWEEN '".$year."-08-01'  AND '".$year."-08-31' ";
        $result_august = $this->db->query($sql_august)->result();
        $total_august = 0;
        if($result_august){
            foreach ($result_august as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_august += $price;
            }
        }

        $sql_september = $sql."AND bid_tb.date_bid BETWEEN '".$year."-09-01'  AND '".$year."-09-30' ";
        $result_september = $this->db->query($sql_september)->result();
        $total_september = 0;
        if($result_september){
            foreach ($result_september as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_september += $price;
            }
        }

        $sql_october = $sql."AND bid_tb.date_bid BETWEEN '".$year."-10-01'  AND '".$year."-10-31' ";
        $result_october = $this->db->query($sql_october)->result();
        $total_october = 0;
        if($result_october){
            foreach ($result_october as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_october += $price;
            }
        }

        $sql_november = $sql."AND bid_tb.date_bid BETWEEN '".$year."-11-01'  AND '".$year."-11-30' ";
        $result_november = $this->db->query($sql_november)->result();
        $total_november = 0;
        if($result_november){
            foreach ($result_november as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_november += $price;
            }
        }

        $sql_december = $sql."AND bid_tb.date_bid BETWEEN '".$year."-12-01'  AND '".$year."-12-31' ";
        $result_december = $this->db->query($sql_december)->result();
        $total_december = 0;
        if($result_december){
            foreach ($result_december as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_december += $price;
            }
        }

        $sql_january = $sql."AND bid_tb.date_bid BETWEEN '".($year+1)."-01-01'  AND '".($year+1)."-01-31' ";
        $result_january = $this->db->query($sql_january)->result();
        $total_january = 0;
        if($result_january){
            foreach ($result_january as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_january += $price;
            }
        }

        $sql_february = $sql."AND bid_tb.date_bid BETWEEN '".($year+1)."-02-01'  AND '".($year+1)."-02-28' ";
        $result_february = $this->db->query($sql_february)->result();
        $total_february = 0;
        if($result_february){
            foreach ($result_february as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_february += $price;
            }
        }

        $sql_march = $sql."AND bid_tb.date_bid BETWEEN '".($year+1)."-03-01'  AND '".($year+1)."-03-31' ";
        $result_march = $this->db->query($sql_march)->result();
        $total_march = 0;
        if($result_march){
            foreach ($result_march as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_march += $price;
            }
        }

        $arr = array(
            "total_january"=>$total_january,
            "total_february"=>$total_february,
            "total_march"=>$total_march,
            "total_april"=>$total_april,
            "total_may"=>$total_may,
            "total_june"=>$total_june,
            "total_july"=>$total_july,
            "total_august"=>$total_august,
            "total_september"=>$total_september,
            "total_october"=>$total_october,
            "total_november"=>$total_november,
            "total_december"=>$total_december
        );


        return json_encode($arr);
    }


}
