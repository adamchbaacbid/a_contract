<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/9/2561
 * Time: 14:26
 */

class login_model extends  CI_Model{

    function __construct()
    {
        parent::__construct();
        $this->load->model('log_model');

    }

    function model_check_login($user,$pass){////////////////////////////////////////////////
        $password = base64_encode($pass);//encode pass

        $sql_check_member_active = "SELECT  user_member,status_member,name_member,privilege_member FROM member_tb WHERE user_member = '$user'  AND privilege_member = 'Active'";
        $result_check_member_active = $this->db->query($sql_check_member_active)->num_rows();//check user active

        if($result_check_member_active==0){
            $msg = "not";
            $data['result'] = "ไม่พบผู้ใช้งานในระบบ ";
            $data['member'] = "";
            $data['msg'] =$msg;

            ////sent status Login Fail to LOG
            $event = 'Login Fail';
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;

        }else{
            $sql = "SELECT  user_member,status_member,name_member,privilege_member FROM member_tb WHERE user_member = '$user' AND password_member = '$password' AND privilege_member = 'Active'";
        $result = $this->db->query($sql)->num_rows();//check user on db

        if($result==0){ //not have user
            $msg = "not";
            $data['result'] = "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง  ";
            $data['member'] = "";
            $data['msg'] =$msg;

            ////sent status Login Fail to LOG
            $event = 'Login Fail';
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;

        }else{ //have user

            $active = $this->check_active_member($user);
            if($active==0){    ////// 0 is not change password 
                $msg = "change_pass";
                $data['member'] = "";
                $data['msg'] =$msg;

            }else{

                $msg = "pass";
                $member = $this->db->query($sql)->result();
                $data2=$member['0'];

                $data['msg'] =$msg;

                ////sent status login complete to LOG
                $event = 'Login complete';
                $msg_log = $this->log_model->set_log($event,$user);
                $data['msg_log'] =$msg_log;
                $data['member'] = $data2->user_member;
                $data['name'] = $data2->name_member;
                $data['privilege_member'] = $data2->privilege_member;
                $ses_data = $data;
                $arr_data['data'] = $ses_data;
                $data['status_member'] = $data2->status_member;

                ///// set data session
                $time = 5;
                $this->session->set_userdata($arr_data);
                $this->session->mark_as_temp('member',$time);
                $this->session->mark_as_temp('msg',$time);

            }

        }

        }


        
        return $data ;
    }

    function check_active_member($user){
        $sql_check_active = "SELECT active_member FROM member_tb WHERE user_member = '$user'";
        $result_check_active = $this->db->query($sql_check_active)->result();
        $result_check_active=$result_check_active['0'];
        $data= $result_check_active->active_member;

        return $data;
    }

    function model_logout(){
        $data = $this->session->userdata('data');
        $user = $data['member'];
        $de = $this->session->sess_destroy();

        if($de == false){
            $event = "Logout complete";

            ////sent status logout complete to LOG
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
        }else{
            $event = "Logout Fail";

            ////sent status logout Fail to LOG
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
        }
        return $event;
    }

    function check_session_for_login(){//////////////////////////////////////////
        $data = $this->session->userdata('data');
         
        if(!isset($data['member'])){
            $this->load->view('login/login');
        }else{
            $user = $data['member'];
            $sql = "SELECT * FROM member_tb WHERE user_member = '$user'";
            $num_result = $this->db->query($sql)->num_rows();//check user on db
    
            if($num_result==0){ //dont have user
                $this->load->view('login/login');
            }else {
                $result = $this->db->query($sql)->result();
                $ob_data = $result[0];
                $status_member = $ob_data->status_member;
    
                if ($status_member == "admin") { //check status memb
                    $url = base_url('index.php/login_controller/admin');
                    header( "location:$url" );
                } elseif ($status_member == "audit user") {
                    $url =  base_url('index.php/login_controller/audit');
                    header( "location:$url" );
                } elseif ($status_member == "contract user") {
                    $url = base_url('index.php/login_controller/contract');
                    header( "location:$url" );
                } elseif ($status_member == "purchase user") {
                    $url = base_url('index.php/login_controller/purchase');
                    header( "location:$url" );
                } elseif ($status_member == "procurement user") {
                    $url = base_url('index.php/login_controller/procurement');
                    header("location:$url");
                }
            }
        }       
        
    }

    function check_session_for_admin(){//////////////////////////////////////////
        $data = $this->session->userdata('data');
        $user = $data['member'];
        $sql = "SELECT * FROM member_tb WHERE user_member = '$user'";
        $num_result = $this->db->query($sql)->num_rows();//check user on db

        if($num_result==0){ //dont have user
            $this->load->view('login/login');
        }else {
            $result = $this->db->query($sql)->result();
            $ob_data = $result[0];
            $status_member = $ob_data->status_member;
            if ($status_member != "admin") { //check status admin

                if ($status_member == "audit user") {
                    $url =  base_url('index.php/login_controller/audit');
                    header( "location:$url" );
                } elseif ($status_member == "contract user") {
                    $url = base_url('index.php/login_controller/contract');
                    header( "location:$url" );
                } elseif ($status_member == "purchase user") {
                    $url = base_url('index.php/login_controller/purchase');
                    header( "location:$url" );
                } elseif ($status_member == "procurement user") {
                    $url = base_url('index.php/login_controller/procurement');
                    header( "location:$url" );
                }
            }
        }
    }

    function check_session_for_audit(){//////////////////////////////////////////
        $data = $this->session->userdata('data');
        $user = $data['member'];
        $sql = "SELECT * FROM member_tb WHERE user_member = '$user'";
        $num_result = $this->db->query($sql)->num_rows();//check user on db

        if($num_result==0){ //dont have user
            $this->load->view('login/login');
        }else {
            $result = $this->db->query($sql)->result();
            $ob_data = $result[0];
            $status_member = $ob_data->status_member;
            if ($status_member != "audit user") { //check status admin

                if ($status_member == "admin") {
                    $url =  base_url('index.php/login_controller/admin');
                    header( "location:$url" );
                } elseif ($status_member == "contract user") {
                    $url = base_url('index.php/login_controller/contract');
                    header( "location:$url" );
                } elseif ($status_member == "purchase user") {
                    $url = base_url('index.php/login_controller/purchase');
                    header( "location:$url" );
                } elseif ($status_member == "procurement user") {
                    $url = base_url('index.php/login_controller/procurement');
                    header( "location:$url" );
                }
            }
        }
    }

    function check_session_for_contract(){//////////////////////////////////////////
        $data = $this->session->userdata('data');
        $user = $data['member'];
        $sql = "SELECT * FROM member_tb WHERE user_member = '$user'";
        $num_result = $this->db->query($sql)->num_rows();//check user on db

        if($num_result==0){ //dont have user
            $this->load->view('login/login');
        }else {
            $result = $this->db->query($sql)->result();
            $ob_data = $result[0];
            $status_member = $ob_data->status_member;
            if ($status_member != "contract user") { //check status admin

                if ($status_member == "audit user") {
                    $url =  base_url('index.php/login_controller/audit');
                    header( "location:$url" );
                } elseif ($status_member == "admin") {
                    $url = base_url('index.php/login_controller/admin');
                    header( "location:$url" );
                } elseif ($status_member == "purchase user") {
                    $url = base_url('index.php/login_controller/purchase');
                    header( "location:$url" );
                } elseif ($status_member == "procurement user") {
                    $url = base_url('index.php/login_controller/procurement');
                    header( "location:$url" );
                }
            }
        }
    }

    function check_session_for_purchase(){//////////////////////////////////////////
        $data = $this->session->userdata('data');
        $user = $data['member'];
        $sql = "SELECT * FROM member_tb WHERE user_member = '$user'";
        $num_result = $this->db->query($sql)->num_rows();//check user on db

        if($num_result==0){ //dont have user
            $this->load->view('login/login');
        }else {
            $result = $this->db->query($sql)->result();
            $ob_data = $result[0];
            $status_member = $ob_data->status_member;
            if ($status_member != "purchase user") { //check status admin

                if ($status_member == "audit user") {
                    $url =  base_url('index.php/login_controller/audit');
                    header( "location:$url" );
                } elseif ($status_member == "contract user") {
                    $url = base_url('index.php/login_controller/contract');
                    header( "location:$url" );
                } elseif ($status_member == "admin") {
                    $url = base_url('index.php/login_controller/admin');
                    header( "location:$url" );
                } elseif ($status_member == "procurement user") {
                    $url = base_url('index.php/login_controller/procurement');
                    header( "location:$url" );
                }
            }
        }
    }

function check_session_for_procurement(){//////////////////////////////////////////
    $data = $this->session->userdata('data');
    $user = $data['member'];
    $sql = "SELECT * FROM member_tb WHERE user_member = '$user'";
    $num_result = $this->db->query($sql)->num_rows();//check user on db

    if($num_result==0){ //dont have user
        $this->load->view('login/login');
    }else {
        $result = $this->db->query($sql)->result();
        $ob_data = $result[0];
        $status_member = $ob_data->status_member;
        if ($status_member != "procurement user") { //check status admin

            if ($status_member == "audit user") {
                $url =  base_url('index.php/login_controller/audit');
                header( "location:$url" );
            } elseif ($status_member == "contract user") {
                $url = base_url('index.php/login_controller/contract');
                header( "location:$url" );
            } elseif ($status_member == "admin") {
                $url = base_url('index.php/login_controller/admin');
                header( "location:$url" );
            } elseif ($status_member == "purchase user") {
                $url = base_url('index.php/login_controller/purchase');
                header( "location:$url" );
            }
        }
    }
}





    function model_edit_member_ajax($id_member,$name_member,$position_member,$username,$agency_member,$email_member,$tell_member){
        $sql = "UPDATE member_tb 
                SET name_member = '$name_member',
                employee_id_member = '$id_member', 
                position_member = '$position_member',
                agency_member = '$agency_member',
                email_member = '$email_member' ,
                tell_member = '$tell_member' 
                WHERE user_member = '$username'";
        $result = $this->db->query($sql);
        if($result){
            $msg = "ทำการแก้ไขเรียบร้อยแล้ว";
            //////////// LOG ////////////////////////
            $event = "$username Edit Profile Successful ";
            $data = $this->session->userdata('data');
            $user = $data['member'];
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
            ////////////////////////////////////////
            $data['name']=$name_member;
            $ses_data['data'] = $data;
            $this->session->set_userdata($ses_data);
        }else{
            $msg = "ไม่สามารถแก้ไขได้";
            //////////// LOG ////////////////////////
            $event = "$username Edit Profile Unsuccessful ";
            $data = $this->session->userdata('data');
            $user = $data['member'];
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
            ////////////////////////////////////////
        }
        return $msg ;
    }

    function model_pass_member_old($pass){
        $data = $this->session->userdata('data');
        $user_member = $data['member'];

        $password = base64_encode($pass);///// converse pass

        $sql = "SELECT user_member,password_member FROM member_tb WHERE user_member = '$user_member'AND password_member = '$password'";
        $result  = $this->db->query($sql)->num_rows();

        if($result>=1){
            $msg = "pass";
        }else{
            $msg = "not";
        }
        return $msg ;
    }

    function model_edit_password($pass_member_new,$user_member){

        $new_pass = base64_encode($pass_member_new);
        $sql ="UPDATE member_tb SET password_member = '$new_pass'WHERE user_member = '$user_member' ";
        $result = $this->db->query($sql);
        if($result){
            $msg = "เปลี่ยนพาสเวิดเรียบร้อยแล้ว กรุณาเข้าสู่ระบบอีกครั้ง";
            $this->session->sess_destroy();
            //////////////// LOG /////////////////////////////

            //$data =  $this->session->userdata('data');
            $user = $user_member;
            
            

            $event = "Edit Password user $user Successful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }else{
            $msg = "เปลี่ยนรหัสผ่านไม่สำเร็จ";
            //////////////// LOG /////////////////////////////
            //$data =  $this->session->userdata('data');
            $user = $user_member;
            $event = "Edit Password user $user Unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }
        return $msg;
    }

    function active_member($user_member){
        $sql = "UPDATE member_tb SET active_member = '1' WHERE user_member ='$user_member' ";
        $result = $this->db->query($sql);
        if($result){
            $msg = "ยืนยันตัวตนสำเร็จ";
            //////////////// LOG /////////////////////////////

            $user = $user_member;
            $event = "Active password $user successful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }else{
            $msg = "ยืนยันตัวตนไม่สำเร็จ";
            //////////////// LOG /////////////////////////////

            $user = $user_member;
            $event = "Active password $user Unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }
        return $msg;
    }


    function model_check_contract(){
        $sql = "SELECT  * ,DATEDIFF(STR_TO_DATE((end_contract),'%Y-%m-%d'),NOW()) AS dif FROM contract_tb ";
        $result = $this->db->query($sql)->result();

        return $result ;
    }

    function model_forgot_password($email){
        
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'mygmail@gmail.com';
        $config['smtp_pass']    = '*******';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'text'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      
    
        $this->email->initialize($config);
    
    
        $this->email->from('mygmail@gmail.com', 'myname');
        $this->email->to('target@gmail.com');
    
        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');  
    
        $this->email->send();
    
        echo $this->email->print_debugger();
    
         $this->load->view('email_view');

        

    }





}