<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/9/2561
 * Time: 14:26
 */

class function_model extends  CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    public function datethai($strDate){
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

    public function long_datethai($strDate){
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strMonthCut = Array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

    public function get_client_ip() {
        $ipaddress = '';
        if($_SERVER['REMOTE_ADDR']) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        }else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    function random_password($len){
        srand((double)microtime()*10000000);
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@";
        $ret_str = "";
        $num = strlen($chars);
        for($i = 0; $i < $len; $i++) {
            $ret_str.= $chars[rand()%$num];
            $ret_str.="";
        }
        return $ret_str;
    }


    function date_thai_to_date_db($date_thai){
        $pattern_date_thai = "/^[0-9]{2}[\/]/";
        $pattern_date_db = "/^[0-9]{4}[-]/";
        
        if(preg_match($pattern_date_thai, $date_thai)==1){
            $date_thai = strval($date_thai);
            $D = substr($date_thai,0,2);
            $M =  substr($date_thai,3,2);
            $Y =  substr($date_thai,6,4);
            if($Y!=0000){
                $Y = $Y-543;
            }
            $Y = strval($Y);

           $date_db = $Y."-".$M."-".$D ;
            
        }elseif(preg_match($pattern_date_db, $date_thai)==1){
            $date_db  =  $date_thai;
            
        }else{
            $date_db = "0000-00-01";
        }
        
        return $date_db ;
    }


    function date_db_to_date_thai($date_db){
        $pattern_date_thai = "/^[0-9]{2}[\/]/";
        $pattern_date_db = "/^[0-9]{4}[-]/";

        if(preg_match($pattern_date_db, $date_db)==1){
            $date_db = strval($date_db);
            $Y = substr($date_db,0,4);
            $M = substr($date_db,5,2);
            $D = substr($date_db,8,2);
            if($Y!=0000){
                $Y = $Y+543;
            }
            $Y = strval($Y);
    
            $date_thai = $D."/".$M."/".$Y ;
        }elseif(preg_match($pattern_date_thai, $date_db)==1){
            $date_thai = $date_db;
        }        

        return $date_thai;
    }

    





}