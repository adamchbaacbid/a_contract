<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/9/2561
 * Time: 14:26
 */

class member_model extends  CI_Model{

    function __construct()
    {
        parent::__construct();
        $this->load->model('log_model');
    }

    function model_add_member_ajax($employee_id_member,$name_member,$position_member,$user_member,$status_member,$email_member,$tel_member,$agency_member){
        $sql_check_member = "SELECT employee_id_member FROM member_tb WHERE employee_id_member = '$employee_id_member'";
        $result_check_member = $this->db->query($sql_check_member)->num_rows();
        if($result_check_member>0){
            $msg = "มีชื่อผู้ใช้งานนนี้แล้ว";
        }else{
            $password_default = base64_encode("$employee_id_member");
            $member = $this->session->userdata('data');
            $add_by_member = $member['member'];

            $status_first_active = "0";
            $status_member_first_login = "Active";

            $sql = "INSERT INTO member_tb (employee_id_member,name_member,position_member,user_member,password_member,status_member,add_by_member,add_date_member,email_member,tell_member,agency_member,active_member,privilege_member)
                VALUES ('$employee_id_member','$name_member','$position_member','$user_member','$password_default','$status_member','$add_by_member',NOW(),'$email_member','$tel_member','$agency_member','$status_first_active','$status_member_first_login')
                ";
            $result = $this->db->query($sql);
            if($result){
                $msg = "เพิ่มผู้ใช้งาน สำเร็จ";
                //////////////// LOG /////////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "add member $employee_id_member successful By $user";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////////////////
            }else{
                $msg = "เพิ่มผู้ใช้งาน ไม่สำเร็จ";
                //////////////// LOG /////////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "add member $employee_id_member Unsuccessful By $user ";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////////////////
            }
        }


        return $msg ;
    }

    function model_show_all_member(){
        $sql = "SELECT name_member,id_member,position_member,status_member,add_by_member,add_date_member FROM member_tb";
        $result = $this->db->query($sql)->result();
        return $result ;
    }

    function model_check_admin(){
        $user = $this->session->userdata('member');

        $status_member = $user->status_member ;

        if($status_member!='admin'){
            redirect(base_url());
        }
    }

    function model_delete_member($username){
        $sql1 = "SELECT user_member FROM member_tb WHERE user_member = '$username' ";
        $result_num = $this->db->query($sql1)->num_rows(); 
        if($result_num ==1){
            
            $sql = "UPDATE member_tb SET privilege_member = 'Inactive',active_member = '0' WHERE user_member = '$username' ";
            $result2 = $this->db->query($sql); 
            if($result2){
                $msg = "ลบ Username $username สำเร็จ" ;
                
                //////////////// LOG /////////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete member Username $username successful By $user";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////////////////
            }else{
                $msg = "ลบ Username $username ไม่สำเร็จ" ;
                //////////////// LOG /////////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete member Username $username unsuccess By $user";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////////////////
            }
        }else{
            $msg = "ไม่พบผู้ใช้งาน".$username ;
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "delete member Username $username unsuccess By $user";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }
        return $msg ;

        
    }


}