<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/9/2561
 * Time: 14:26
 */

class bid_model extends  CI_Model{

    function  __construct()
    {
        parent::__construct();
    }



    function model_add_vender($no_bid,$name_vender,$sender,$win,$pricebid,$price_under_bid,$date_vender_sender,$id_sub,$pass_vender){

        $sql = "INSERT INTO vender_tb (no_bid,no_sub_bid,name_vender,sender_vender,price_vender,price_under_vender,win_vender,pass_vender,date_sender_vender)
                VALUES ('$no_bid','$id_sub','$name_vender','$sender','$pricebid','$price_under_bid','$win','$pass_vender','$date_vender_sender')";
        $result = $this->db->query($sql);

        if($result){
            $msg['msg']="success";
        }else{
            $msg['msg'] = "unsuccessful";
        }
        return $msg;
    }


    function model_add_attract_file($old_name,$new_name,$path,$no_bid){

        $sql1 = "SELECT * FROM attract_tb where newname_attract='$new_name'";
        $result1 = $this->db->query($sql1);
        $row = $result1->num_rows();

        if($row > 0){
            $msg= "have";
        }else{
            $sql = "INSERT INTO attract_tb (no_bid,newname_attract,oldname_attract,path_attract)
                  VALUES ('$no_bid','$new_name','$old_name','$path')";
            $result = $this->db->query($sql);
            if($result){
                $msg = "success";
            }else{
                $msg = "unsuccessful";
            }
        }
        return $msg ;
    }
    
    function model_show_attract_by_no_bid($no_bid){
       $sql = "SELECT * FROM attract_tb WHERE no_bid = '$no_bid'";
       $result = $this->db->query($sql)->result();
        return $result;
    }


    function model_show_vender_by_no_bid($no_bid){
        $sql = "SELECT * FROM vender_tb WHERE no_bid = '$no_bid'AND no_sub_bid = 0";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function model_show_bid_by_no_bid($no_bid){
        $sql = "SELECT * FROM bid_tb
                INNER JOIN audit_tb ON bid_tb.no_bid = audit_tb.no_bid 
                WHERE bid_tb.no_bid = '$no_bid'";
        $result = $this->db->query($sql)->result();
        return $result;
    }


    function model_add_bid($no_bid,$date_bid,$year_bid,$subject_bid,$type_bid,$tri_bid,$cost_bid,$date_audit,$issues_bid,$condition_bid,$period_bid,$member_audit1,$member_audit2,$member_audit3,$member_audit4,$devide_bid,$price_asset,$price_ma,$status_bid,$no_contract){
        $sub_sql = "SELECT * FROM sub_bid_tb WHERE no_bid = '$no_bid' ";
        $result_sub = $this->db->query($sub_sql)->num_rows();
        if($result_sub=='0'){
            $divide_sub_bid = "ไม่มีแยกประกวดราคา";
        }else{
            $divide_sub_bid = "แยกประกวดราคา";
        }
        $sql = "INSERT INTO bid_tb (no_bid,date_bid,fiscal_year_bid,subject_bid,type_bid,tri_bid,cost_bid,date_audit,issues_bid,condition_bid,period_bid,no_contract,devide_bid,price_asset,price_ma,status_bid,divide_sub_bid)
                VALUES ('$no_bid','$date_bid','$year_bid','$subject_bid','$type_bid','$tri_bid','$cost_bid','$date_audit','$issues_bid','$condition_bid','$period_bid','$no_contract','$devide_bid','$price_asset','$price_ma','$status_bid','$divide_sub_bid')
";
        $result = $this->db->query($sql);
        if($result){
            $sql2 = "INSERT INTO audit_tb (no_bid,name1_audit,name2_audit,name3_audit,name4_audit) 
                      VALUES ('$no_bid','$member_audit1','$member_audit2','$member_audit3','$member_audit4')
";
            $result2 = $this->db->query($sql2);
            if($result2){
                $msg = "success";
            }else{
                $msg = "audit_tb unsuccessful";
            }
        }else{
            $msg = "bid_tb unsuccessful";
        }
        return $msg ;
    }


    function model_edit_bid($no_bid,$date_bid,$year_bid,$subject_bid,$type_bid,$tri_bid,$cost_bid,$date_audit,$issues_bid,$condition_bid,$period_bid,$member_audit1,$member_audit2,$member_audit3,$member_audit4,$devide_bid,$price_asset,$price_ma,$status_bid,$no_contract){

        $sql ="UPDATE bid_tb
                SET date_bid = '$date_bid', fiscal_year_bid = '$year_bid' , subject_bid = '$subject_bid' , type_bid = '$type_bid', tri_bid = '$tri_bid' , cost_bid = '$cost_bid', date_audit = '$date_audit', issues_bid = '$issues_bid',condition_bid = '$condition_bid', period_bid = '$period_bid',no_contract='$no_contract',devide_bid = '$devide_bid',price_asset = '$price_asset' , price_ma = '$price_ma',status_bid = '$status_bid'
                WHERE no_bid = '$no_bid'";

        $sql2 = "UPDATE audit_tb 
                  SET  name1_audit = '$member_audit1',name2_audit = '$member_audit2', name3_audit = '$member_audit3', name4_audit = '$member_audit4'
                  WHERE no_bid = '$no_bid' ";

        $result = $this->db->query($sql);
        $result2 = $this->db->query($sql2);
        if($result){
            if($result2){
                $msg = "success";
            }else{
                $msg = "unsuccessful error audit_tb";
            }
        }else{
            $msg = "unsuccessful error bid_tb";
        }
        return $msg ;
    }


    function model_show_bid(){

        $sql= "SELECT * FROM bid_tb ORDER BY bid_tb.date_audit DESC  ";
        $result = $this->db->query($sql)->result();
        return $result;
    }


    function model_delete_bid($no_bid){

        $sql2="SELECT * FROM attract_tb WHERE no_bid ='$no_bid'";
        $result2 = $this->db->query($sql2)->result();
        $msg = "ไม่มีไฟล์";

        foreach ($result2 as $loop ){
            $del = unlink('file_upload/'.$loop->newname_attract);  //DELETE FILE IN DIRECTORY
            if($del){
                $msg = "ลบไฟล์สำเร็จ";
            }else{
                $msg = "ลบไฟล์ไม่สำเร็จ";
            }
        }

        $sql_sub_bid = "SELECT * FROM sub_bid_tb WHERE no_bid = '$no_bid'";
        $result_num_sub_bid = $this->db->query($sql_sub_bid)->num_rows();
        if($result_num_sub_bid!=0){
            $sql_del_sub_bid = "DELETE FROM sub_bid_tb WHERE no_bid = '$no_bid' ";
            $result_delete_sub_bid = $this->db->query($sql_del_sub_bid);
            if($result_delete_sub_bid){
                $msg = $msg." + ลบประกวดราคาแยกย่อย สำเร็จ";
            }else{
                $msg = $msg." + ลบประกวดราคาแยกย่อย ไม่สำเร็จ";
            }
        }else{
            $msg = $msg." + ไม่พบประกวดราคาแยกย่อย";
        }

        $sql_delete_attract = "DELETE FROM attract_tb WHERE no_bid = '$no_bid'";
        $result_delete_attract = $this->db->query($sql_delete_attract);
        if($result_delete_attract){
            $msg = $msg." + ลบข้อมูลไฟล์สำเร็จ";
            $sql_delete_vender = "DELETE FROM vender_tb WHERE no_bid = '$no_bid'";
            $result_delete_vender = $this->db->query($sql_delete_vender);
            if($result_delete_vender){
                $msg = $msg." + ลบข้อมูลบริษัทสำเร็จ";
                $sql_delete_bid = "DELETE bid_tb,audit_tb  
                FROM bid_tb                  
                INNER JOIN audit_tb                
                WHERE  audit_tb.no_bid= '$no_bid'  AND  bid_tb.no_bid = '$no_bid'";
                $result_dele_bid = $this->db->query($sql_delete_bid);
                if($result_dele_bid){
                    $msg = $msg." + ลบข้อมูลการสังเกตุการณ์สำเร็จ";
                }else{
                    $msg = $msg." + ลบข้อมูลการสังเกตุการณ์ ไม่สำเร็จ";
                }

            }else{
                $msg = $msg." + ลบข้อมูลบริษัท ไม่สำเร็จ";
            }
        }else{
            $msg = $msg." + ลบข้อมูลไฟล์ ไม่สำเร็จ";
        }

        return $msg ;
    }


    function model_delete_attract($id_attract){
        $sql2="SELECT * FROM attract_tb WHERE id_attract ='$id_attract'";
        $result2 = $this->db->query($sql2)->result();
        $row = $result2['0'];
        $path_attract = $row->path_attract;
        $del = unlink($path_attract);
        if($del){
            $sql = "DELETE FROM attract_tb WHERE id_attract = '$id_attract'";
            $result = $this->db->query($sql);
            if($result){
                $msg = "success";
            }else{
                $msg = "unsuccessful";
            }
        }else{
            $msg = "file unsuccessful";
        }
        return $msg;
    }



    function model_delete_vender($id_vender){

        $sql = "DELETE FROM vender_tb WHERE id_vender = '$id_vender'";
        $result = $this->db->query($sql);
        if($result){
            $msg = "success";
        }else{
            $msg = "unsuccessful";
        }
        return $msg;
    }


    function model_check_vender_by_no_bid($no_bid){
        $sql = "SELECT COUNT(no_bid) AS count FROM vender_tb WHERE no_bid ='$no_bid'";
        $row = $this->db->query($sql)->result();
        return $row;
    }


    function model_show_edit_bid($no_bid){
        $sql = "SELECT * FROM bid_tb
                INNER JOIN audit_tb ON bid_tb.no_bid = audit_tb.no_bid
                where bid_tb.no_bid = '$no_bid';         
         ";

        $result = $this->db->query($sql)->result();
        return $result;
    }

    function model_show_all_vender(){
        $sql = "SELECT DISTINCT name_vender FROM vender_tb";
        $vender = $this->db->query($sql)->result();
        $data['vender'] = $vender;

        $this->session->set_userdata($data);
    }

    function model_show_win_vender(){
        $sql = "SELECT DISTINCT name_vender FROM vender_tb WHERE win_vender = '1'";
        $vender = $this->db->query($sql)->result();
        $data['win_vender'] = $vender;

        $this->session->set_userdata($data);
    }


    function model_show_bid_by_vender($name_vender,$before,$after){
        if($before==''&& $after==''){
            $sql = "SELECT * FROM vender_tb
                INNER JOIN bid_tb ON vender_tb.no_bid = bid_tb.no_bid
                WHERE vender_tb.name_vender = '$name_vender'AND vender_tb.win_vender = '1'
              ";
        }elseif ($before==''&& $after!=''){
            $sql = "SELECT * FROM vender_tb
                INNER JOIN bid_tb ON vender_tb.no_bid = bid_tb.no_bid
                WHERE vender_tb.name_vender = '$name_vender'AND vender_tb.win_vender = '1' AND date_bid <= '$after'
              ";
        }elseif ($before!=''&& $after==''){
            $sql = "SELECT * FROM vender_tb
                INNER JOIN bid_tb ON vender_tb.no_bid = bid_tb.no_bid
                WHERE vender_tb.name_vender = '$name_vender'AND vender_tb.win_vender = '1' AND date_bid >= '$before'
              ";
        }elseif ($before!=''&& $after!=''){
            $sql = "SELECT * FROM vender_tb
                INNER JOIN bid_tb ON vender_tb.no_bid = bid_tb.no_bid
                WHERE vender_tb.name_vender = '$name_vender'AND vender_tb.win_vender = '1' AND date_bid BETWEEN '$before' AND '$after'
              ";
        }

        $data['result'] = $this->db->query($sql)->result();
        $data['num'] = $this->db->query($sql)->num_rows();

        return $data;
    }

    function model_show_sub_bid_by_nio_sub_bid($no_sub_bid,$no_bid){
        $sql = "SELECT * FROM sub_bid_tb WHERE no_sub_bid = '$no_sub_bid' AND no_bid = '$no_bid'";
        $result = $this->db->query($sql)->result();

        return $result;

    }

    function model_check_no_bid($no_bid,$year){
        if($no_bid==""&&$year!=""){
            $sql1 = "SELECT no_bid FROM bid_tb WHERE fiscal_year_bid = '$year'";
            $result1= $this->db->query($sql1)->num_rows();
            if($result1=='1'){
                $sql = "SELECT MAX(no_bid) as max FROM bid_tb WHERE fiscal_year_bid ='$year'";
                $result = $this->db->query($sql)->result();
                $new_no_bid = $result['0'];
                $msg =($new_no_bid->max)+1;
            }else{
                if($year<=2559 ){
                    $y = date("Y");
                    $year = $y+543;
                }elseif($year>=2570){
                    $y = date("Y");
                    $year = $y+543;
                }
                $no_bid =($year*1000)+1;

                $sql = "SELECT no_bid FROM bid_tb WHERE no_bid = '$no_bid'";
                $result = $this->db->query($sql)->num_rows();
                if($result=='1'){
                    $sql = "SELECT MAX(no_bid) as max FROM bid_tb WHERE no_bid LIKE '".$year."%'";
                    $result = $this->db->query($sql)->result();
                    $new_no_bid = $result['0'];
                    $msg =($new_no_bid->max)+1;
                }else{
                    $msg = $no_bid;
                }
            }
        }else{
            ////Check repeatedly
            $sql = "SELECT no_bid FROM bid_tb WHERE no_bid = '$no_bid'";
            $result = $this->db->query($sql)->num_rows();

            if($result=='1'){
                $year = substr($no_bid,0,4);
                $sql = "SELECT MAX(no_bid) as max FROM bid_tb WHERE no_bid LIKE '".$year."%'";
                $result = $this->db->query($sql)->result();
                $new_no_bid = $result['0'];
                $msg =($new_no_bid->max)+1;
            }else{
                $year = substr($no_bid,0,4);
                if($year<=2559 ){
                    $y = date("Y");
                    $year = $y+543;
                }elseif($year>=2570){
                    $y = date("Y");
                    $year = $y+543;
                }
                $no_bid =($year*1000)+1;

                $sql = "SELECT no_bid FROM bid_tb WHERE no_bid = '$no_bid'";
                $result = $this->db->query($sql)->num_rows();
                if($result=='1'){
                    $sql = "SELECT MAX(no_bid) as max FROM bid_tb WHERE no_bid LIKE '".$year."%'";
                    $result = $this->db->query($sql)->result();
                    $new_no_bid = $result['0'];
                    $msg =($new_no_bid->max)+1;
                }else{
                    $msg = $no_bid;
                }
            }
        }
        return $msg;
    }


    function model_show_num_bid(){
        $sql = "SELECT * FROM bid_tb";
        $result = $this->db->query($sql)->num_rows();

        return $result;
    }

    function model_show_price_all_bid(){
        $sql = "SELECT * FROM vender_tb WHERE win_vender = '1'";
        $result = $this->db->query($sql)->result();
        $vender = $result;
        $total = 0;
        $totalsub = 0;
        $sql_sub = "SELECT * FROM sub_bid_tb 
                    INNER JOIN vender_tb ON sub_bid_tb.no_bid = vender_tb.no_bid AND sub_bid_tb.no_sub_bid = vender_tb.no_sub_bid
                    WHERE  vender_tb.win_vender = '1'";
        $result_sub = $this->db->query($sql_sub)->result();
        foreach($result_sub as $numbid){
            $pricesub = $numbid->price_under_vender;
            if($pricesub=='0'){
                $pricesub = $numbid->price_vender;
            }
            $totalsub += $pricesub;
        }
        foreach ($vender as $num){
            $price = $num->price_under_vender;
            if($price=='0'){
                $price = $num->price_vender;
            }
            $total += $price;
        }
        $to =$total+$totalsub;
        return $to;
    }

    function model_show_per_status_bid(){
        $sql = "SELECT * FROM bid_tb ";
        $sql_cancel = $sql."WHERE status_bid = 'ล้ม'";

        $total = $this->db->query($sql)->num_rows();
        $cancel = $this->db->query($sql_cancel)->num_rows();

        $number = ($cancel*100)/$total;
        $per = number_format($number, 2, '.', '');

        $msg = '        
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">'.$per.'%</div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: '.$per.'%" aria-valuenow="'.$per.'" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
        
        ';
        return $msg;
    }

    function model_show_area_chart($year){

            $year = $year-543;
            $sql = "SELECT bid_tb.date_bid,vender_tb.price_under_vender,vender_tb.price_vender  FROM bid_tb INNER JOIN vender_tb ON bid_tb.no_bid = vender_tb.no_bid WHERE vender_tb.win_vender = '1'";
            $sql_tri_1 = $sql."AND bid_tb.date_bid BETWEEN '".$year."-04-01'  AND '".$year."-06-30' ";
            $result_tri_1 = $this->db->query($sql_tri_1)->result();
            $total_tri_1 = 0;
            if($result_tri_1){
                foreach ($result_tri_1 as $num){
                    $price = $num->price_under_vender;
                    if($price=='0'){
                        $price = $num->price_vender;
                    }
                    $total_tri_1 += $price;
                }
            }

            $sql_tri_2 = $sql."AND bid_tb.date_bid BETWEEN '".$year."-07-01'  AND '".$year."-09-30' ";
            $result_tri_2 = $this->db->query($sql_tri_2)->result();
            $total_tri_2 = 0;
            if($result_tri_2){
                foreach ($result_tri_2 as $num){
                    $price = $num->price_under_vender;
                    if($price=='0'){
                        $price = $num->price_vender;
                    }
                    $total_tri_2 += $price;
                }
            }

            $sql_tri_3 = $sql."AND bid_tb.date_bid BETWEEN '".$year."-10-01'  AND '".$year."-12-31' ";
            $result_tri_3 = $this->db->query($sql_tri_3)->result();
            $total_tri_3 = 0;
            if($result_tri_3){
                foreach ($result_tri_3 as $num){
                    $price = $num->price_under_vender;
                    if($price=='0'){
                        $price = $num->price_vender;
                    }
                    $total_tri_3 += $price;
                }
            }

            $sql_tri_4 = $sql."AND bid_tb.date_bid BETWEEN '".($year+1)."-01-01'  AND '".($year+1)."-03-31' ";
            $result_tri_4 = $this->db->query($sql_tri_4)->result();
            $total_tri_4 = 0;
            if($result_tri_4){
                foreach ($result_tri_4 as $num){
                    $price = $num->price_under_vender;
                    if($price=='0'){
                        $price = $num->price_vender;
                    }
                    $total_tri_4 += $price;
                }
            }

        header('Content-Type: application/json');
        $arr = array("total_tri_1"=>$total_tri_1,
            "total_tri_2"=>$total_tri_2,
            "total_tri_3"=>$total_tri_3,
            "total_tri_4"=>$total_tri_4
        );

        return json_encode($arr);
    }

    function model_show_bar_chart($year){
        $year = $year-543;
        $sql = "SELECT bid_tb.date_bid,vender_tb.price_under_vender,vender_tb.price_vender  
                FROM bid_tb   
                INNER JOIN vender_tb ON bid_tb.no_bid = vender_tb.no_bid 
                WHERE vender_tb.win_vender = '1'";
        $sql_april = $sql." AND bid_tb.date_bid BETWEEN '".$year."-04-01'  AND '".$year."-04-30' ";
        $result_april = $this->db->query($sql_april)->result();
        $total_april = 0;
        if($result_april){
            foreach ($result_april as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_april += $price;
            }
        }

        $sql_may = $sql."AND bid_tb.date_bid BETWEEN '".$year."-05-01'  AND '".$year."-05-31' ";
        $result_may = $this->db->query($sql_may)->result();
        $total_may = 0;
        if($result_may){
            foreach ($result_may as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_may += $price;
            }
        }

        $sql_june = $sql."AND bid_tb.date_bid BETWEEN '".$year."-06-01'  AND '".$year."-06-30' ";
        $result_june = $this->db->query($sql_june)->result();
        $total_june = 0;
        if($result_june){
            foreach ($result_june as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_june += $price;
            }
        }

        $sql_july = $sql."AND bid_tb.date_bid BETWEEN '".$year."-07-01'  AND '".$year."-07-31' ";
        $result_july = $this->db->query($sql_july)->result();
        $total_july = 0;
        if($result_july){
            foreach ($result_july as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_july += $price;
            }
        }

        $sql_august = $sql."AND bid_tb.date_bid BETWEEN '".$year."-08-01'  AND '".$year."-08-31' ";
        $result_august = $this->db->query($sql_august)->result();
        $total_august = 0;
        if($result_august){
            foreach ($result_august as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_august += $price;
            }
        }

        $sql_september = $sql."AND bid_tb.date_bid BETWEEN '".$year."-09-01'  AND '".$year."-09-30' ";
        $result_september = $this->db->query($sql_september)->result();
        $total_september = 0;
        if($result_september){
            foreach ($result_september as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_september += $price;
            }
        }

        $sql_october = $sql."AND bid_tb.date_bid BETWEEN '".$year."-10-01'  AND '".$year."-10-31' ";
        $result_october = $this->db->query($sql_october)->result();
        $total_october = 0;
        if($result_october){
            foreach ($result_october as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_october += $price;
            }
        }

        $sql_november = $sql."AND bid_tb.date_bid BETWEEN '".$year."-11-01'  AND '".$year."-11-30' ";
        $result_november = $this->db->query($sql_november)->result();
        $total_november = 0;
        if($result_november){
            foreach ($result_november as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_november += $price;
            }
        }

        $sql_december = $sql."AND bid_tb.date_bid BETWEEN '".$year."-12-01'  AND '".$year."-12-31' ";
        $result_december = $this->db->query($sql_december)->result();
        $total_december = 0;
        if($result_december){
            foreach ($result_december as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_december += $price;
            }
        }

        $sql_january = $sql."AND bid_tb.date_bid BETWEEN '".($year+1)."-01-01'  AND '".($year+1)."-01-31' ";
        $result_january = $this->db->query($sql_january)->result();
        $total_january = 0;
        if($result_january){
            foreach ($result_january as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_january += $price;
            }
        }

        $sql_february = $sql."AND bid_tb.date_bid BETWEEN '".($year+1)."-02-01'  AND '".($year+1)."-02-28' ";
        $result_february = $this->db->query($sql_february)->result();
        $total_february = 0;
        if($result_february){
            foreach ($result_february as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_february += $price;
            }
        }

        $sql_march = $sql."AND bid_tb.date_bid BETWEEN '".($year+1)."-03-01'  AND '".($year+1)."-03-31' ";
        $result_march = $this->db->query($sql_march)->result();
        $total_march = 0;
        if($result_march){
            foreach ($result_march as $num){
                $price = $num->price_under_vender;
                if($price=='0'){
                    $price = $num->price_vender;
                }
                $total_march += $price;
            }
        }

        $arr = array(
            "total_january"=>$total_january,
            "total_february"=>$total_february,
            "total_march"=>$total_march,
            "total_april"=>$total_april,
            "total_may"=>$total_may,
            "total_june"=>$total_june,
            "total_july"=>$total_july,
            "total_august"=>$total_august,
            "total_september"=>$total_september,
            "total_october"=>$total_october,
            "total_november"=>$total_november,
            "total_december"=>$total_december
        );
        return json_encode($arr);
    }

    function model_add_sub_bid($name_sub,$cost_sub,$issues_sub,$condition_sub,$status_sub,$devide_sub,$asset_sub,$ma_sub,$no_bid){

        $sql_check_no_sub_bid = "SELECT * FROM sub_bid_tb WHERE no_bid = '$no_bid'";
        $result_check_no_sub_bid = $this->db->query($sql_check_no_sub_bid)->num_rows();

        if($result_check_no_sub_bid==0){
            $no_sub_bid = 1;
        }else{
            $sql_max_no_sub_bid = "SELECT MAX(no_sub_bid)as max_no_sub_bid FROM sub_bid_tb WHERE no_bid = '$no_bid'";
            $result_max_no_sub_bid = $this->db->query($sql_max_no_sub_bid)->result();
            $row = $result_max_no_sub_bid['0'];
            $max_no_sub_bid = $row->max_no_sub_bid ;
            $no_sub_bid = $max_no_sub_bid+1;
        }

        $sql = "INSERT INTO sub_bid_tb (no_bid,no_sub_bid,name_sub_bid,cost_sub_bid,issues_sub_bid,condition_sub_bid,devide_sub_bid,price_asset_sub_bid,price_ma_sub_bid,status_sub_bid)
                VALUES ('$no_bid','$no_sub_bid','$name_sub','$cost_sub','$issues_sub','$condition_sub','$devide_sub','$asset_sub','$ma_sub','$status_sub')";
        $result = $this->db->query($sql);
        if($result){
           $msg = "successful";
        }else{
            $msg ="unsuccessful";
        }

        return $msg ;
    }

    function model_show_sub_bid_by_no_bid($no_bid){
        $sql = "SELECT * FROM sub_bid_tb WHERE no_bid = '$no_bid'";
        $result = $this->db->query($sql)->num_rows();

        if($result==0){
            $result = false;
        }else {
            $result = $this->db->query($sql)->result();
        }

        return $result ;
    }

    function model_show_vender_by_no_sub_bid($no_bid,$no_sub_bid){

        $sql = "SELECT * FROM vender_tb WHERE no_bid = '$no_bid' AND no_sub_bid = '$no_sub_bid'";
        $result = $this->db->query($sql)->num_rows();

        if($result==0){
            $result = false;
        }else {
            $result = $this->db->query($sql)->result();
        }
        return $result ;
    }


    function model_show_vender_sub_bid_by_no_bid($no_bid){
        $sql = "SELECT * FROM sub_bid_tb 
                INNER JOIN vender_tb ON sub_bid_tb.no_bid = vender_tb.no_bid AND sub_bid_tb.no_sub_bid = vender_tb.no_sub_bid
                WHERE sub_bid_tb.no_bid = '$no_bid'";
        $result = $this->db->query($sql)->num_rows();

        if($result==0){
            $result = false;
        }else {
            $result = $this->db->query($sql)->result();
        }
        return $result ;
    }


    function model_delete_sub_bid($id_sub_bid){
        $sql_sub_bid = "SELECT * FROM sub_bid_tb WHERE id_sub_bid = '$id_sub_bid'";
        $result_bid = $this->db->query($sql_sub_bid)->result();
        $row = $result_bid['0'];
        $no_bid = $row->no_bid;
        $no_sub_bid = $row->no_sub_bid;

        $sql_delete_vender = "DELETE FROM vender_tb WHERE no_bid = '$no_bid'AND no_sub_bid = '$no_sub_bid' ";
        $result_deletevender = $this->db->query($sql_delete_vender);
        if($result_deletevender){
            $msg['delete_vender']= "delete vender successful";
            $sql_delete = "DELETE FROM sub_bid_tb WHERE id_sub_bid = '$id_sub_bid' ";
            $result_delete = $this->db->query($sql_delete);
            if($result_delete){
                $msg['delete_sub_bid']= "delete sub_bid successful";
            }else{
                $msg['delete_sub_bid']= "delete sub_bid unsuccessful";
            }
        }else{
            $msg['delete_vender']= "delete vender successful";
            $msg['delete_sub_bid']= "delete sub_bid not execute";
        }

        return $msg ;
    }

    function model_end_contract(){
        $sql = "SELECT  * ,DATEDIFF(STR_TO_DATE((period_bid),'%Y-%m-%d'),NOW())/'30' AS dif FROM bid_tb ";
        $result = $this->db->query($sql)->result();

       return $result;

    }












}
