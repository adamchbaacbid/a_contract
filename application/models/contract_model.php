<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/9/2561
 * Time: 14:26
 */

class contract_model extends  CI_Model{

    function  __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('contract_model');
        $this->load->model('function_model');
        $this->load->model('login_model');
        $this->login_model->check_session_for_contract();
    }

    function model_show_bid_for_add_contract(){
        $sql= "SELECT bid_tb.status_bid,bid_tb.date_bid,project_tb.name_project,project_tb.owner_project,bid_tb.no_bid FROM bid_tb 
                INNER JOIN project_tb  
                WHERE  bid_tb.id_project = project_tb.id_project 
					
					AND bid_tb.no_bid NOT IN (SELECT no_bid FROM contract_tb )
                ORDER BY id_bid DESC   ";
        $result = $this->db->query($sql)->result();
        return $result;
    }


    function model_show_info_bid_byno_bid($no_bid){
        $sql = "SELECT * FROM bid_tb
                  INNER JOIN project_tb
                  WHERE bid_tb.id_project = project_tb.id_project 
                  
                  AND bid_tb.no_bid = '$no_bid'";
        $result = $this->db->query($sql)->result();
        $data['bid'] = $result;
        $user = $result['0'];
        $user_name= $user->id_member ;
        $sql_member = "SELECT name_member FROM member_tb WHERE user_member = '$user_name'";
        $result_member = $this->db->query($sql_member)->result();
        $member = $result_member['0'];
        $data['name_member'] = $member->name_member;

        return $data ;
    }


    function model_show_attract_by_no_bid($no_bid){
        $sql = "SELECT * FROM attract_tb WHERE no_bid = '$no_bid'";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function model_show_vender_by_no_bid($no_bid){
        $sql = "SELECT * FROM active_vender_tb INNER JOIN vender_tb WHERE active_vender_tb.no_bid = '$no_bid'AND active_vender_tb.id_vender = vender_tb.id_vender";
        $result = $this->db->query($sql);
        $num = $result->num_rows();
        if($num==0){
            $data = $this->db->query($sql)->result();
        }else{
            $data = $this->db->query($sql)->result();
        }

        $myJSON = json_encode($data);
        return $myJSON ;
    }

    function model_add_contract_ajax($no_bid,$name_contract,$period_contract,$start_contract,$end_contract,$description_contract,$member_contract){
        $sql_num_bid = "SELECT no_bid FROM contract_tb WHERE no_bid = '$no_bid'";
        $result_num_bid = $this->db->query($sql_num_bid)->num_rows();

        if($result_num_bid==0){
            $sql = "INSERT INTO contract_tb (no_bid,name_contract,period_contract,start_contract,end_contract,description_contract,member_contract)
                VALUES ('$no_bid','$name_contract','$period_contract','$start_contract','$end_contract','$description_contract','$member_contract')";
            $result = $this->db->query($sql);

            if($result){
                $msg = "เพิ่มรายละเอียดสัญญา สำเร็จ ";
                //////////// LOG ////////////////////////
                $event = "Add contract $no_bid Successful";
                $data = $this->session->userdata('data');
                $user = $data['member'];
                $msg_log = $this->log_model->set_log($event,$user);
                $data['msg_log'] =$msg_log;
                ////////////////////////////////////////

            }else{
                $msg = "ไม่สามารถเพิ่มรายละเอียดสัญญาได้";
                //////////// LOG ////////////////////////
                $event = "Add contract $no_bid Unsuccessful";
                $data = $this->session->userdata('data');
                $user = $data['member'];
                $msg_log = $this->log_model->set_log($event,$user);
                $data['msg_log'] =$msg_log;
                ////////////////////////////////////////
            }
        }else{
            $msg = "การจัดซือจัดจ้างนี้ ทำการเพิ่มรายละเอียดสัญญาแล้ว";
            //////////// LOG ////////////////////////
            $event = "Add contract $no_bid Successful.This audit already exists on database";
            $data = $this->session->userdata('data');
            $user = $data['member'];
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
            ////////////////////////////////////////
        }
        return $msg ;
    }

    function model_show_contract(){
        $sql= "SELECT bid_tb.no_bid,bid_tb.name_bid,contract_tb.*,DATEDIFF(STR_TO_DATE((end_contract),'%Y-%m-%d'),NOW()) AS dif  FROM bid_tb 
                INNER JOIN project_tb  
                INNER JOIN contract_tb
                WHERE  bid_tb.id_project = project_tb.id_project 					 
					AND bid_tb.no_bid = contract_tb.no_bid
					
                ORDER BY id_bid DESC  ";
        $result['contract'] = $this->db->query($sql)->result();
        $result['num'] = $this->db->query($sql)->num_rows();

        return $result;
    }

    function model_info_contract_by_no_bid($no_bid){//////////////////////////////////////////////////////////////////// ค้างโชวชื่อ ตรวจสอบทั้งสี่คน
        $sql = "SELECT bid_tb.*,project_tb.*,contract_tb.*,member_tb.name_member FROM bid_tb 
                INNER JOIN project_tb  
                INNER JOIN contract_tb
                INNER JOIN member_tb
                WHERE  bid_tb.id_project = project_tb.id_project
                AND contract_tb.member_contract = member_tb.employee_id_member                 					 
				AND bid_tb.no_bid = '$no_bid'
				AND contract_tb.no_bid = '$no_bid'
                 ";
        $result = $this->db->query($sql)->result();

        return $result;
    }

    function model_delete_contract($no_bid){
        $sql = "DELETE FROM contract_tb WHERE no_bid = '$no_bid'";
        $result = $this->db->query($sql);
        if($result){
            $msg = "ลบรายละเอียดสัญญาสำเร็จ";
            //////////// LOG ////////////////////////
            $event = "Delete contract $no_bid Success";
            $data = $this->session->userdata('data');
            $user = $data['member'];
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
            ////////////////////////////////////////
        }else{
            $msg = "ไม่สามารถลบรายละเอียดสัญญา";
            //////////// LOG ////////////////////////
            $event = "Delete contract $no_bid Unsuccessful";
            $data = $this->session->userdata('data');
            $user = $data['member'];
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
            ////////////////////////////////////////
        }
        return $msg ;
    }

    function model_show_committee_bid_by_no_bid($no_bid){
        $sql = "SELECT * FROM committee_bid_tb WHERE no_bid = '$no_bid' order by position_committee_bid DESC ";
        $result = $this->db->query($sql)->result();
        return $result ;
    }

    function model_edit_contract_ajax($no_bid,$name_contract,$period_contract,$start_contract,$end_contract,$description_contract){
        $sql = "UPDATE contract_tb SET  name_contract='$name_contract',period_contract='$period_contract',start_contract='$start_contract',end_contract='$end_contract',description_contract='$description_contract'
                WHERE no_bid = '$no_bid'";
        $result = $this->db->query($sql);
        if($result){
            $msg = "แก้ไขรายละเอียดสัญญา สำเร็จ ";
            //////////// LOG ////////////////////////
            $event = "Edit contract $no_bid Success";
            $data = $this->session->userdata('data');
            $user = $data['member'];
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
            ////////////////////////////////////////
        }else{
            $msg = "ไม่สามารถแก้ไขรายละเอียดสัญญาได้";
            //////////// LOG ////////////////////////
            $event = "Edit contract $no_bid Unsuccessful";
            $data = $this->session->userdata('data');
            $user = $data['member'];
            $msg_log = $this->log_model->set_log($event,$user);
            $data['msg_log'] =$msg_log;
            ////////////////////////////////////////
        }
        return $msg ;
    }

    function model_show_contract_by_dif_date($max_days,$min_days){

        if($min_days==""){
            $sql= "SELECT bid_tb.no_bid,bid_tb.name_bid,contract_tb.*,DATEDIFF(STR_TO_DATE((end_contract),'%Y-%m-%d'),NOW()) AS dif  FROM bid_tb 
                INNER JOIN project_tb  
                INNER JOIN contract_tb
                WHERE  bid_tb.id_project = project_tb.id_project 					 
				  AND bid_tb.no_bid = contract_tb.no_bid
				  AND DATEDIFF(STR_TO_DATE((end_contract),'%Y-%m-%d'),NOW()) < '$max_days'
				  			
                ORDER BY dif ASC  ";
        }else{
            $sql= "SELECT bid_tb.no_bid,bid_tb.name_bid,contract_tb.*,DATEDIFF(STR_TO_DATE((end_contract),'%Y-%m-%d'),NOW()) AS dif  FROM bid_tb 
                INNER JOIN project_tb  
                INNER JOIN contract_tb
                WHERE  bid_tb.id_project = project_tb.id_project 					 
				  AND bid_tb.no_bid = contract_tb.no_bid
				  AND DATEDIFF(STR_TO_DATE((end_contract),'%Y-%m-%d'),NOW()) < '$max_days'
				  AND DATEDIFF(STR_TO_DATE((end_contract),'%Y-%m-%d'),NOW()) > '$min_days'			
                ORDER BY dif ASC  ";
        }


        $result['contract'] = $this->db->query($sql)->result();
        $result['num'] = $this->db->query($sql)->num_rows();

        return $result;
    }

    function show_end_day_by_no_bid($no_bid){
        $sql = "SELECT DATEDIFF(STR_TO_DATE((end_contract),'%Y-%m-%d'),NOW()) AS dif FROM contract_tb
                WHERE no_bid  = '$no_bid'";
        $result = $this->db->query($sql)->result();

        return $result ;
    }

    function show_profile(){
        $data = $this->session->userdata('data');
        $user = $data['member'];
        $sql = "SELECT * FROM member_tb WHERE user_member = '$user'";
        $result = $this->db->query($sql)->result();
        $user_member = $result['0'];
        $member['employee_id_member'] = $user_member->employee_id_member;
        $member['name_member'] = $user_member->name_member;
        $member['position_member']=$user_member->position_member ;
        $member['email_member']=$user_member->email_member;
        $member['tell_member']=$user_member->tell_member;
        $member['agency_member']=$user_member->agency_member;
        $member['user_member']=$user_member->user_member;
        $member['status_member']=$user_member->status_member;

        return $member ;
    }










































    function model_info_member_by_name($name){
        $sql = "SELECT employee_id_member,position_member FROM member_tb WHERE name_member = '$name'";
        $result = $this->db->query($sql)->result();

        return $result;

    }








































    function model_show_bid(){
        $sql= "SELECT bid_tb.status_bid,bid_tb.date_bid,project_tb.name_project,project_tb.owner_project,bid_tb.no_bid FROM bid_tb 
                INNER JOIN project_tb
                WHERE  bid_tb.id_project = project_tb.id_project 
                ORDER BY id_bid DESC  ";
        $result['bid'] = $this->db->query($sql)->result();
        $result['num'] = $this->db->query($sql)->num_rows();
        return $result;
    }





    function model_add_project($name_project,$no_project,$date_project,$owner_project,$cost_project,$objective_project){
        $sql_check_name_project= "SELECT * FROM project_tb WHERE name_project = '$name_project'";
        $result_check_name_project = $this->db->query($sql_check_name_project)->num_rows();
        if($result_check_name_project==0){
            $sql = "INSERT INTO project_tb (no_project,date_project,name_project,owner_project,cost_project,objective_project)
                VALUES ('$no_project','$date_project','$name_project','$owner_project','$cost_project','$objective_project')";
            $result = $this->db->query($sql);
            if($result){
                $msg = "successful";
                //////////// LOG ////////////////////////
                $event = "Creat Project $no_project Success";
                $data = $this->session->userdata('data');
                $user = $data['member'];
                $msg_log = $this->log_model->set_log($event,$user);
                $data['msg_log'] =$msg_log;
                ////////////////////////////////////////
            }else{
                $msg ="unsuccessful";
                //////////// LOG ////////////////////////
                $event = "Creat Project $no_project unsuccessful";
                $data = $this->session->userdata('data');
                $user = $data['member'];
                $msg_log = $this->log_model->set_log($event,$user);
                $data['msg_log'] =$msg_log;
                //////////////////////////////////////////
            }
        }else{
            $msg ="มีชื่อโครงการนี้แล้ว";
        }
        return $msg ;
    }

    function model_show_project(){
        $sql = "SELECT name_project,id_project FROM project_tb";
        $result = $this->db->query($sql)->result();
        foreach ($result as $row){
            $id = $row->id_project;
            $arr[$id] = $row->name_project;
        }

        $myJSON = json_encode($arr);
        return $myJSON ;
    }

    function model_check_name_project($name_project){
        $sql = "SELECT name_project FROM project_tb WHERE name_project = '$name_project' ";
        $result = $this->db->query($sql)->num_rows();
        if($result=="0"){
            $data = "no";
        }else{
            $data = "yes";
        }
        return $data ;
    }

    function model_check_no_bid($no_bid,$year){
        if($no_bid==""&&$year!=""){
            $sql1 = "SELECT no_bid FROM bid_tb WHERE fiscal_year_bid = '$year'";
            $result1= $this->db->query($sql1)->num_rows();
            if($result1=='1'){
                $sql = "SELECT MAX(no_bid) as max FROM bid_tb WHERE fiscal_year_bid ='$year'";
                $result = $this->db->query($sql)->result();
                $new_no_bid = $result['0'];
                $msg =($new_no_bid->max)+1;
            }else{
                if($year<=2559 ){
                    $y = date("Y");
                    $year = $y+543;
                }elseif($year>=2570){
                    $y = date("Y");
                    $year = $y+543;
                }
                $no_bid =($year*1000)+1;

                $sql = "SELECT no_bid FROM bid_tb WHERE no_bid = '$no_bid'";
                $result = $this->db->query($sql)->num_rows();
                if($result=='1'){
                    $sql = "SELECT MAX(no_bid) as max FROM bid_tb WHERE no_bid LIKE '".$year."%'";
                    $result = $this->db->query($sql)->result();
                    $new_no_bid = $result['0'];
                    $msg =($new_no_bid->max)+1;
                }else{
                    $msg = $no_bid;
                }
            }
        }else{
            ////Check repeatedly
            $sql = "SELECT no_bid FROM bid_tb WHERE no_bid = '$no_bid'";
            $result = $this->db->query($sql)->num_rows();

            if($result=='1'){
                $year = substr($no_bid,0,4);
                $sql = "SELECT MAX(no_bid) as max FROM bid_tb WHERE no_bid LIKE '".$year."%'";
                $result = $this->db->query($sql)->result();
                $new_no_bid = $result['0'];
                $msg =($new_no_bid->max)+1;
            }else{
                $year = substr($no_bid,0,4);
                if($year<=2559 ){
                    $y = date("Y");
                    $year = $y+543;
                }elseif($year>=2570){
                    $y = date("Y");
                    $year = $y+543;
                }
                $no_bid =($year*1000)+1;

                $sql = "SELECT no_bid FROM bid_tb WHERE no_bid = '$no_bid'";
                $result = $this->db->query($sql)->num_rows();
                if($result=='1'){
                    $sql = "SELECT MAX(no_bid) as max FROM bid_tb WHERE no_bid LIKE '".$year."%'";
                    $result = $this->db->query($sql)->result();
                    $new_no_bid = $result['0'];
                    $msg =($new_no_bid->max)+1;
                }else{
                    $msg = $no_bid;
                }
            }
        }
        return $msg;
    }

    function model_add_attract_file($old_name,$new_name,$path,$no_bid){

        $sql1 = "SELECT * FROM attract_tb where newname_attract='$new_name'";
        $result1 = $this->db->query($sql1);
        $row = $result1->num_rows();

        if($row > 0){
            $msg= "have";
        }else{
            $sql = "INSERT INTO attract_tb (no_bid,newname_attract,oldname_attract,path_attract)
                  VALUES ('$no_bid','$new_name','$old_name','$path')";
            $result = $this->db->query($sql);
            if($result){
                $msg = "success";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "add attract $old_name on bid number $no_bid success";
                $this->log_model->set_log($event,$user);
                /////////////////////////////////////////

            }else{
                $msg = "unsuccessful";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "add attract $old_name on bid number $no_bid unsuccessful";
                $this->log_model->set_log($event,$user);
                ////////////////////////////////////////
            }
        }
        return $msg ;
    }



    function model_delete_attract($id_attract){
        $sql2="SELECT * FROM attract_tb WHERE id_attract ='$id_attract'";
        $result2 = $this->db->query($sql2)->result();
        $row = $result2['0'];
        $path_attract = $row->path_attract;
        $no_bid = $row->no_bid;
        $old_name = $row->oldname_attract ;

        $del = unlink($path_attract);
        if($del){
            $sql = "DELETE FROM attract_tb WHERE id_attract = '$id_attract'";
            $result = $this->db->query($sql);
            if($result){
                $msg = "success";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete attract $old_name on bid number $no_bid success";
                $log = $this->log_model->set_log($event,$user);
                /////////////////////////////////////////
            }else{
                $msg = "unsuccessful";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete attract $old_name on bid number $no_bid unsuccessful";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////
            }
        }else{
            $msg = "file unsuccessful";
            //////////// LOG ////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "delete attract $id_attract on bid number $no_bid unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            /////////////////////////////////////////
        }
        return $msg;
    }



    function model_show_bid_by_no_bid($no_bid){////////////////////////////////////////////////////////////////////////
        $sql = "SELECT * FROM bid_tb
                INNER JOIN audit_tb ON bid_tb.no_bid = audit_tb.no_bid 
                WHERE bid_tb.no_bid = '$no_bid'";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function model_show_all_vender(){
        $sql = "SELECT DISTINCT name_vender FROM vender_tb";
        $vender = $this->db->query($sql)->result();
        $data['vender'] = $vender;
        $this->session->set_userdata($data);
    }

    function model_delete_active_vender($id_vender){

        $sql_vender = "SELECT name_vender FROM active_vender_tb WHERE id_active_vender = '$id_vender'";
        $result_vender = $this->db->query($sql_vender)->result();
        $vender = $result_vender['0'];
        $name_vender = $vender->name_vender;

        $sql = "DELETE FROM active_vender_tb WHERE id_active_vender = '$id_vender'";
        $result = $this->db->query($sql);


        if($result){
            $msg = "success";
            //////////// LOG ////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "delete active vender $name_vender success";
            $log = $this->log_model->set_log($event,$user);
            /////////////////////////////////////////
        }else{
            $msg = "unsuccessful";
            //////////// LOG ////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "delete active vender $name_vender unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////
        }
        return $msg;
    }


    function model_add_vender($no_bid,$name_vender,$sender,$win,$pricebid,$price_under_bid,$pass_vender){

        $sql_get_id_vender = "SELECT id_vender FROM vender_tb WHERE name_vender = '$name_vender'";
        $result_get_id_vender  = $this->db->query($sql_get_id_vender);
        $num_vender = $result_get_id_vender->num_rows();
        if($num_vender!=0){
            $get_id_vender  = $this->db->query($sql_get_id_vender)->result();
            $data = $get_id_vender['0'];
            $id_vender = $data->id_vender;

            $sql = "INSERT INTO active_vender_tb (no_bid,name_vender,id_vender,sender_active_vender,price_active_vender,chaffer_active_vender,win_active_vender,pass_active_vender)
                VALUES ('$no_bid','$name_vender','$id_vender','$sender','$pricebid','$price_under_bid','$win','$pass_vender')";
            $result = $this->db->query($sql);

            if($result){
                $msg['msg']="success";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "add vender $name_vender on bid number $no_bid success";
                $log = $this->log_model->set_log($event,$user);
                ///////////////////////////////////////////

            }else{
                $msg['msg'] = "unsuccessful";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "add vender $name_vender on bid number $no_bid unsuccessful";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////
            }
        }else{
            $msg['msg'] = "ไม่พบชื่อบริษัทในทะเบียนบริษัท";
        }
        return $msg;
    }


    function model_show_name_vender(){
        $sql = "SELECT name_vender,id_vender FROM vender_tb";
        $result = $this->db->query($sql)->result();
        foreach ($result as $row){
            $id = $row->id_vender;
            $arr[$id] = $row->name_vender;
        }
        $myJSON = json_encode($arr);
        return $myJSON ;
    }

    function model_sign_up_vender($name_sign_up_vender,$capital_vender){
        $sql = "SELECT * FROM vender_tb WHERE name_vender = '$name_sign_up_vender'";
        $result = $this->db->query($sql)->num_rows();
        if($result==0){
            $sql_insert = "INSERT INTO vender_tb (name_vender,capital_vender) VALUES ('$name_sign_up_vender','$capital_vender')";
            $result_insert = $this->db->query($sql_insert);

            if($result_insert){
                $msg = "success";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "sign up vender $name_sign_up_vender success";
                $log = $this->log_model->set_log($event,$user);
                /////////////////////////////////////////
            }else{
                $msg = "ลงทะเบียน $name_sign_up_vender ไม่สำเร็จ";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "sign up vender $name_sign_up_vender unsuccessful";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////
            }
        }else{
            $msg = "$name_sign_up_vender ทำการลงทะเบียนแล้ว";
        }
        return $msg ;
    }


    function model_add_bid($input_name_project,$status_bid,$year_bid,$no_bid,$date_bid,$subject_bid,$no_announce,$date_announce,$type_bid,$cost_bid,$price_base_bid,$price_bid,$month_send_draft_contract,$month_approve_bid,$no_send_draft_contract,$no_egp,$date_contract,$no_contract,$member,$date_receive_bid,$no_direct_committee,$date_direct_committee,$subject_direct_committee){

        $sql_get_id_project = "SELECT id_project FROM project_tb WHERE name_project = '$input_name_project'";
        $result_get_id_project = $this->db->query($sql_get_id_project)->result();
        $project = $result_get_id_project['0'];
        $id_project = $project->id_project ;

        $sql_get_id_member = "SELECT id_member FROM member_tb WHERE name_member = '$member'";
        $result_get_id_member = $this->db->query($sql_get_id_member)->result();
        $membder_get = $result_get_id_member['0'];
        $id_member = $membder_get->id_member ;

        $sql_insert = "INSERT INTO bid_tb (id_project,no_bid,name_bid,cost_bid,no_announce_bid,date_announce_bid,type_bid,date_bid,fiscal_year_bid,price_base_bid,price_bid,no_contract,date_contract,status_bid,no_approve_bid,no_egp,send_draft_contract,approve_bid,id_member,date_receive_bid,no_direct_committee,date_direct_committee,subject_direct_committee) 
                VALUES ('$id_project','$no_bid','$subject_bid','$cost_bid','$no_announce','$date_announce','$type_bid','$date_bid','$year_bid','$price_base_bid','$price_bid','$no_contract','$date_contract','$status_bid','$no_send_draft_contract','$no_egp','$month_send_draft_contract','$month_approve_bid','$id_member','$date_receive_bid','$no_direct_committee','$date_direct_committee','$subject_direct_committee')";

        $result_insert = $this->db->query($sql_insert);
        if($result_insert){
            $msg = "success";
            //////////// LOG ////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "add bid number $no_bid successful";
            $log = $this->log_model->set_log($event,$user);
            /////////////////////////////////////////

        }else{
            $msg = "unsuccessful";
            //////////// LOG ////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "add bid number $no_bid unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////
        }
        return $msg ;

    }


    function model_edit_bid($input_name_project,$status_bid,$no_bid,$date_bid,$subject_bid,$no_announce,$date_announce,$type_bid,$cost_bid,$price_base_bid,$price_bid,$month_send_draft_contract,$month_approve_bid,$no_approve_bid,$no_egp,$date_contract,$no_contract,$member,$date_receive_bid,$no_direct_committee,$date_direct_committee,$subject_direct_committee){

        $sql_update = "UPDATE bid_tb 
                        SET  name_bid = '$subject_bid',cost_bid = '$cost_bid',no_announce_bid = '$no_announce',date_announce_bid = '$date_announce',type_bid = '$type_bid', date_bid = '$date_bid', price_base_bid = '$price_base_bid',price_bid = '$price_bid', no_contract = '$no_contract', date_contract ='$date_contract', status_bid='$status_bid', no_approve_bid ='$no_approve_bid', no_egp='$no_egp',send_draft_contract ='$month_send_draft_contract',approve_bid='$month_approve_bid',date_receive_bid='$date_receive_bid',no_direct_committee='$no_direct_committee',date_direct_committee='$date_direct_committee',subject_direct_committee='$subject_direct_committee'
                        WHERE no_bid = '$no_bid'";

        $result_insert = $this->db->query($sql_update);
        if($result_insert){
            $msg = "success";
            //////////// LOG ////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "edit bid number $no_bid successful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////
        }else{
            $msg = "unsuccessful";
            //////////// LOG ////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "edit bid number $no_bid unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            /////////////////////////////////////////
        }
        return $msg ;
    }

    function model_delete_bid($no_bid){

        $sql2="SELECT * FROM attract_tb WHERE no_bid ='$no_bid'";
        $result2 = $this->db->query($sql2)->result();
        $msg = "ไม่มีไฟล์";

        foreach ($result2 as $loop ){
            $name_file = $loop->oldname_attract;
            $del = unlink('file_upload/'.$loop->newname_attract);  //DELETE FILE IN DIRECTORY
            if($del){
                $msg = "ลบไฟล์สำเร็จ";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete file $name_file successful";
                $log = $this->log_model->set_log($event,$user);
                ///////////////////////////////////////////

            }else{
                $msg = "ลบไฟล์ไม่สำเร็จ";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete file $name_file unsuccessful";
                $log = $this->log_model->set_log($event,$user);
                /////////////////////////////////////////
            }
        }

        $sql_delete_attract = "DELETE FROM attract_tb WHERE no_bid = '$no_bid'";
        $result_delete_attract = $this->db->query($sql_delete_attract);
        if($result_delete_attract){
            $msg = $msg." + ลบข้อมูลไฟล์สำเร็จ";

            //////////// LOG ////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "delete file bid number $no_bid successful";
            $log = $this->log_model->set_log($event,$user);
            /////////////////////////////////////////

            $sql_delete_vender = "DELETE FROM active_vender_tb WHERE no_bid = '$no_bid'";
            $result_delete_vender = $this->db->query($sql_delete_vender);
            if($result_delete_vender){
                $msg = $msg." + ลบข้อมูลบริษัทสำเร็จ";

                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete vender bid number $no_bid successful";
                $log = $this->log_model->set_log($event,$user);
                ////////////////////////////////////////////

                $sql_delete_audit = "DELETE audit_tb,committee_bid_tb FROM audit_tb INNER JOIN committee_bid_tb WHERE audit_tb.no_bid = committee_bid_tb.no_bid AND audit_tb.no_bid = '$no_bid'";
                $result_delete_audit = $this->db->query($sql_delete_audit);
                if($result_delete_audit){
                    $msg = $msg." + ลบข้อมูลตรวจสอบสำเร็จ";
                    //////////// LOG ////////////////////////
                    $data =  $this->session->userdata('data');
                    $user = $data['member'];
                    $event = "delete Audit bid number $no_bid successful";
                    $log = $this->log_model->set_log($event,$user);
                    /////////////////////////////////////////
                    $sql_delete_bid = "DELETE  FROM bid_tb WHERE  no_bid= '$no_bid'";
                    $result_dele_bid = $this->db->query($sql_delete_bid);
                    if($result_dele_bid){
                        $msg = $msg." + ลบการจัดซื้อจัดจ้างสำเร็จ";
                        //////////// LOG ////////////////////////
                        $data =  $this->session->userdata('data');
                        $user = $data['member'];
                        $event = "delete bid number $no_bid successful";
                        $log = $this->log_model->set_log($event,$user);
                        /////////////////////////////////////////
                    }else{
                        $msg = $msg." + ลบการจัดซื้อจัดจ้าง ไม่สำเร็จ";
                        //////////// LOG ////////////////////////
                        $data =  $this->session->userdata('data');
                        $user = $data['member'];
                        $event = "delete bid number $no_bid unsuccessful";
                        $log = $this->log_model->set_log($event,$user);
                        /////////////////////////////////////////
                    }
                }else{
                    $msg = $msg." + ลบข้อมูลตรวจสอบ ไม่สำเร็จ";
                    //////////// LOG ////////////////////////
                    $data =  $this->session->userdata('data');
                    $user = $data['member'];
                    $event = "delete Audit bid number $no_bid unsuccessful";
                    $log = $this->log_model->set_log($event,$user);
                    /////////////////////////////////////////
                }
            }else{
                $msg = $msg." + ลบข้อมูลบริษัท ไม่สำเร็จ";
                //////////// LOG ////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete vender bid number $no_bid unsuccessful";
                $log = $this->log_model->set_log($event,$user);
                /////////////////////////////////////////
            }
        }else{
            $msg = $msg." + ลบข้อมูลไฟล์ ไม่สำเร็จ";

            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "delete file bid number $no_bid unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }

        return $msg ;

    }

    function model_show_bid_by_vender_ajax($vender,$before,$after){
        $sql = "SELECT * FROM active_vender_tb 
                  INNER JOIN bid_tb
                  INNER JOIN project_tb
                  WHERE project_tb.name_project = bid_tb.name_bid AND  active_vender_tb.no_bid = bid_tb.no_bid AND active_vender_tb.name_vender = '$vender'AND active_vender_tb.win_active_vender ='1'";

        if($before!=""OR $after!=""){
            if($before==""){
                $before = date("Y-m-d");
            }elseif ($after==""){
                $after = date("Y-m-d");
            }
            $sql = $sql." AND bid_tb.date_bid BETWEEN '$before' AND '$after'";
        }
        $result = $this->db->query($sql)->result();
        $myJSON = json_encode($result);
        return $myJSON ;

    }



    function model_show_all_project(){
        $sql = "SELECT * FROM project_tb ";

        $result['project'] = $this->db->query($sql)->result();
        $result['num'] = $this->db->query($sql)->num_rows();

        return $result;
    }

    function model_delete_project($id_project){
        $sql_check_bid = "SELECT * FROM bid_tb WHERE id_project = '$id_project'";
        $resul_check_bid = $this->db->query($sql_check_bid)->num_rows();


        $sql1 = "SELECT * FROM project_tb WHERE id_project = '$id_project'";
        $result1 = $this->db->query($sql1)->result();
        $project = $result1['0'];
        $name_project = $project->name_project;

        if($resul_check_bid=='0'){


            $sql = "DELETE FROM project_tb WHERE id_project ='$id_project' ";
            $result = $this->db->query($sql);
            if($result){
                $msg = "ทำการลบโครงการสำเร็จ";
                //////////////// LOG /////////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete Project  $name_project Successful";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////////////////
            }else{
                $msg = "ลบโครงการไม่สำเร็จ";
                //////////////// LOG /////////////////////////////
                $data =  $this->session->userdata('data');
                $user = $data['member'];
                $event = "delete Project  $name_project Unsuccessful";
                $log = $this->log_model->set_log($event,$user);
                //////////////////////////////////////////////////////
            }
        }else{
            $msg = "ต้องทำการลบ การจัดซื้อจัดจ้างก่อนการลบ โครงการ";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "delete Project  $name_project Unsuccessful.This have bid not delete.";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////

        }



        return $msg;
    }


    function model_info_project($info_id_project){
        $sql = "SELECT * FROM project_tb WHERE id_project = '$info_id_project'";
        $result = $this->db->query($sql)->result();

        return $result;
    }


    function model_edit_project_ajax($name_project,$id_project,$no_project,$date_project,$owner_project,$cost_project,$objective_project){
        $sql = "UPDATE project_tb 
                SET name_project = '$name_project',
                    no_project = '$no_project' , 
                    date_project = '$date_project' , 
                    owner_project = '$owner_project' , 
                    cost_project = '$cost_project' , 
                    objective_project = '$objective_project' 
                WHERE id_project = $id_project";

        $result = $this->db->query($sql);

        if($result){
            $msg = "แก้ไขโครงการสำเร็จ";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Edit Project  $name_project Successful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }else{
            $msg = "แก้ไขโครงการไม่สำเร็จ";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Edit Project  $name_project Unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }

        return $msg;
    }

    function model_show_vender(){
        $sql = "SELECT * FROM vender_tb ";
        $result['vender'] = $this->db->query($sql)->result();
        $result['num'] = $this->db->query($sql)->num_rows();
        return $result;
    }

    function model_delete_vender($id_vender,$name_vender){
        $sql = "DELETE FROM vender_tb WHERE id_vender = '$id_vender'";
        $result = $this->db->query($sql);
        if($result){
            $msg = "ลบเรียบร้อยแล้ว";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Delete vender $name_vender Successful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }else{
            $msg = "ไม่สามารถลบได้";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Delete vender $name_vender Unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }
        return $msg ;
    }

    function model_info_vender($id_vender){
        $sql = "SELECT * FROM vender_tb WHERE id_vender = '$id_vender'";
        $result = $this->db->query($sql)->result();

        return $result;
    }

    function model_edit_vender_ajax($name_vender,$capital_vender,$id_vender){
        $sql = "UPDATE vender_tb SET name_vender = '$name_vender' , capital_vender = '$capital_vender' WHERE id_vender = '$id_vender'";
        $result = $this->db->query($sql);

        if($result){
            $msg = "แก้ไขเรียบร้อย";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Edit vender $name_vender Successful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }else{
            $msg = "ไม่สามารถแก้ไขได้";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Edit vender $name_vender Unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }
        return $msg ;
    }




    function model_add_committee_bid_ajax($no_bid,$name_committee_bid,$position_committee_bid,$status_committee_bid){
        $sql = "INSERT INTO committee_bid_tb (no_bid,name_committee_bid,position_committee_bid,status_committee_bid)
                VALUES  ('$no_bid','$name_committee_bid','$position_committee_bid','$status_committee_bid')";
        $result = $this->db->query($sql);
        if($result){
            $msg = "เพิ่มสำเร็จ";

            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Add committee $name_committee_bid successful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }else{
            $msg = "เพิ่มไม่สำเร็จ";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Add committee $name_committee_bid Unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }
        return $msg ;
    }

    function model_delete_committee_bid_ajax($id_committee,$name_committee_bid){
        $sql = "DELETE FROM committee_bid_tb WHERE id_committee_bid = '$id_committee'";
        $result = $this->db->query($sql);

        if($result){
            $msg = "ลบ $name_committee_bid สำเร็จ";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Delete committee $name_committee_bid successful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }else{
            $msg = "ลบ $name_committee_bid ไม่สำเร็จ";
            //////////////// LOG /////////////////////////////
            $data =  $this->session->userdata('data');
            $user = $data['member'];
            $event = "Delete committee $name_committee_bid Unsuccessful";
            $log = $this->log_model->set_log($event,$user);
            //////////////////////////////////////////////////////
        }
        return $msg ;
    }


    function converse_nobid($no_bid){

        $b = substr($no_bid,4);
        $n = "2561";
        $sql = "UPDATE active_vender_tb SET no_bid = '$n$b' WHERE no_bid = '$no_bid' ";//////$no_bid
        $result = $this->db->query($sql);
        $msg = " เริ่ม ";

        if($result){
            $msg .= " เปลี่ยน active_vender_tb สำเร็จ ";
            $attract = "SELECT * FROM attract_tb WHERE no_bid = '$no_bid' ";//////////$no_bid
            $attract_result = $this->db->query($attract)->result();


            foreach ($attract_result as $row){
                $newname = $row->newname_attract;
                $newnwme_b = substr($newname,4);

                $path = $row->path_attract;
                $path_f = substr($path,0,12);
                $path_b = substr($path,16);

                $id_attract = $row->id_attract ;

                $attract_sql = "UPDATE attract_tb SET no_bid = '$n$b',newname_attract = '$n$newnwme_b',path_attract = '$path_f$n$path_b' WHERE id_attract = '$id_attract'";
                $result_attract = $this->db->query($attract_sql);
            }

            if($result_attract){
                $msg .= " เปลี่ยน attract_tb สำเร็จ ";

                $committee  = "UPDATE committee_bid_tb SET no_bid = '$n$b' WHERE no_bid = '$no_bid'";/////////$no_bid
               $result_committee = $this->db->query($committee);

                if($result_committee){
                    $msg .= "เปลี่ยน committee_tb สำเร็จ";

                    $bid = "UPDATE bid_tb SET no_bid = '$n$b',fiscal_year_bid = '2561'WHERE no_bid='$no_bid'";
                    $result_bid = $this->db->query($bid);
                    if($result_bid){
                        $msg .= " เปลี่ยน bid_tb สำเร็จ";
                    }else{
                        $msg .= " เปลี่ยน bid_tb ไม่สำเร็จ";
                    }

                }else{
                    $msg .= "เปลี่ยน committee_tb ไม่สำเร็จ";
                }

            }else{
                $msg .= " เปลี่ยน attract_tb ไม่สำเร็จ ";
            }



        }else{
            $msg .=" เปลี่ยน active_vender_tb ไม่สำเร็จ ";
        }

        return $msg;
    }










}
