<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/9/2561
 * Time: 14:26
 */

class phpword_model extends  CI_Model{

    function  __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->login_model->check_session_for_audit();
    }

    function model_info_audit_by_no_bid($no_bid){//////////////////////////////////////////////////////////////////// ค้างโชวชื่อ ตรวจสอบทั้งสี่คน
        $sql = "SELECT bid_tb.*,project_tb.*,audit_tb.*,member_tb.name_member,member_tb.position_member FROM bid_tb 
                INNER JOIN project_tb  
                INNER JOIN audit_tb
                INNER JOIN member_tb
                WHERE  bid_tb.id_project = project_tb.id_project
                AND audit_tb.name1_audit = member_tb.employee_id_member                 					 
				AND bid_tb.no_bid = '$no_bid'
				AND audit_tb.no_bid = '$no_bid'
                 ";
        $result = $this->db->query($sql)->result();

        return $result;
    }


    function model_show_vender_by_no_bid($no_bid){
        $sql = "SELECT * FROM active_vender_tb INNER JOIN vender_tb WHERE active_vender_tb.no_bid = '$no_bid'AND active_vender_tb.id_vender = vender_tb.id_vender";
        $result = $this->db->query($sql);
        $num = $result->num_rows();
        if($num==0){
            $data = $this->db->query($sql)->result();
        }else{
            $data = $this->db->query($sql)->result();
        }

        $myJSON = json_encode($data);
        return $myJSON ;
    }

    function model_info_member_by_name($name){
        $sql = "SELECT employee_id_member,position_member FROM member_tb WHERE name_member = '$name'";
        $result = $this->db->query($sql)->result();

        return $result;

    }




}
