<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/9/2561
 * Time: 14:26
 */

class public_model extends  CI_Model{

    function  __construct()
    {
        parent::__construct();
        $this->load->model('log_model');


    }

    function model_ajax_add_car_rental_step1($branch,$select_step1,$description_step1,$name_employee_step1,$year_procurement,$time_procurement){
        $id_procurement_type = '1';
        $step_procurement = '1';

        $sql1= "SELECT * FROM procurement_description_tb WHERE year_procurement = '$year_procurement'AND time_procurement = '$time_procurement'AND id_procurement_type =  '$id_procurement_type' AND step_procurement = '$step_procurement' AND name_branch = '$branch'";
        $result1 = $this->db->query($sql1)->num_rows();
        if($result1>0){
            $msg = "ไม่สามารถบันทึกได้ เนื่องจากมีอยู่แล้ว";
        }else{
            $sqlname = "SELECT id_branch FROM procurement_branch_tb WHERE name_branch = '$branch'";
            $result_id = $this->db->query($sqlname)->result();
            $id_branch = $result_id['0'];
            $id_branch2 = $id_branch->id_branch;

            $sql_type_name = "SELECT name_procurement_type FROM procurement_type_tb WHERE id_procurement_type = '$id_procurement_type'";
            $result_type_name = $this->db->query($sql_type_name)->result();
            $name_prosurement_type = $result_type_name['0'];
            $name_type = $name_prosurement_type->name_procurement_type;


            $sql = "INSERT INTO procurement_description_tb (id_branch,name_branch,id_procurement_type,name_procurement_type,year_procurement,time_procurement,step_procurement,condition_procurement,procurement_description,name_employees,date_procurement)
                    VALUES ('$id_branch2','$branch','$id_procurement_type','$name_type','$year_procurement','$time_procurement','$step_procurement','$select_step1','$description_step1','$name_employee_step1',NOW())";
            $result = $this->db->query($sql);

            if($result){
                $msg = "บันทึกสำเร็จ";
            }else{
                $msg = "ไม่สามารถบันทึกได้";
            }
        }
        return $msg;
    }

    function model_ajax_add_car_rental_step2($branch,$select_step2,$description_step2,$name_employee_step2,$year_procurement,$time_procurement){
        $id_procurement_type = '1';
        $step_procurement = '2';



        $sql1= "SELECT * FROM procurement_description_tb WHERE year_procurement = '$year_procurement'AND time_procurement = '$time_procurement'AND id_procurement_type =  '$id_procurement_type' AND step_procurement = '$step_procurement'AND id_branch ='$branch'";
        $result1 = $this->db->query($sql1)->num_rows();
        if($result1>0){
            $msg = "ไม่สามารถบันทึกได้ เนื่องจากมีอยู่แล้ว";
        }else{
            $sqlname = "SELECT name_branch FROM procurement_branch_tb WHERE id_branch = '$branch'";
            $result_name = $this->db->query($sqlname)->result();
            $name_branch = $result_name['0'];
            $name_branch2 = $name_branch->name_branch;

            $sql_type_name = "SELECT name_procurement_type FROM procurement_type_tb WHERE id_procurement_type = '$id_procurement_type'";
            $result_type_name = $this->db->query($sql_type_name)->result();
            $name_prosurement_type = $result_type_name['0'];
            $name_type = $name_prosurement_type->name_procurement_type;

            $sql = "INSERT INTO procurement_description_tb (id_branch,name_branch,id_procurement_type,name_procurement_type,year_procurement,time_procurement,step_procurement,condition_procurement,procurement_description,name_employees,date_procurement)
                                                VALUES ('$branch','$name_branch2','$id_procurement_type','$name_type','$year_procurement','$time_procurement','$step_procurement','$select_step2','$description_step2','$name_employee_step2',NOW())";
            $result = $this->db->query($sql);

            if($result){
                $msg = "บันทึกสำเร็จ";
            }else{
                $msg = "ไม่สามารถบันทึกได้";
            }
        }
        return $msg;
    }

    function model_ajax_add_car_rental_step($branch,$description_step,$name_employee_step,$year_procurement,$time_procurement,$step){
        $id_procurement_type = '1';
        $step_procurement = $step;



        $sql1= "SELECT * FROM procurement_description_tb WHERE year_procurement = '$year_procurement'AND time_procurement = '$time_procurement'AND id_procurement_type =  '$id_procurement_type' AND step_procurement = '$step_procurement'AND id_branch ='$branch'";
        $result1 = $this->db->query($sql1)->num_rows();
        if($result1>0){
            $msg = "ไม่สามารถบันทึกได้ เนื่องจากมีอยู่แล้ว";
        }else{
            $sqlname = "SELECT name_branch FROM procurement_branch_tb WHERE id_branch = '$branch'";
            $result_name = $this->db->query($sqlname)->result();
            $name_branch = $result_name['0'];
            $name_branch2 = $name_branch->name_branch;

            $sql_type_name = "SELECT name_procurement_type FROM procurement_type_tb WHERE id_procurement_type = '$id_procurement_type'";
            $result_type_name = $this->db->query($sql_type_name)->result();
            $name_prosurement_type = $result_type_name['0'];
            $name_type = $name_prosurement_type->name_procurement_type;

            $sql = "INSERT INTO procurement_description_tb (id_branch,name_branch,id_procurement_type,name_procurement_type,year_procurement,time_procurement,step_procurement,procurement_description,name_employees,date_procurement)
                                                VALUES ('$branch','$name_branch2','$id_procurement_type','$name_type','$year_procurement','$time_procurement','$step_procurement','$description_step','$name_employee_step',NOW())";
            $result = $this->db->query($sql);

            if($result){
                $msg = "บันทึกสำเร็จ";
            }else{
                $msg = "ไม่สามารถบันทึกได้";
            }
        }
        return $msg;
    }


    function model_ajax_add_atc_step1($branch,$year_procurement,$time_procurement){
        $id_procurement_type = '2';
        $step_procurement = '1';

        $sql1= "SELECT * FROM procurement_description_tb WHERE year_procurement = '$year_procurement'AND time_procurement = '$time_procurement'AND id_procurement_type =  '$id_procurement_type' AND step_procurement = '$step_procurement' AND name_branch = '$branch'";
        $result1 = $this->db->query($sql1)->num_rows();
        if($result1>0){
            $msg = "ไม่สามารถบันทึกได้ เนื่องจากมีอยู่แล้ว";
        }else{
            $sqlname = "SELECT id_branch FROM procurement_branch_tb WHERE name_branch = '$branch'";
            $result_id = $this->db->query($sqlname)->result();
            $id_branch = $result_id['0'];
            $id_branch2 = $id_branch->id_branch;

            $sql_type_name = "SELECT name_procurement_type FROM procurement_type_tb WHERE id_procurement_type = '$id_procurement_type'";
            $result_type_name = $this->db->query($sql_type_name)->result();
            $name_procurement_type = $result_type_name['0'];
            $name_type = $name_procurement_type->name_procurement_type;


            $sql = "INSERT INTO procurement_description_tb (id_branch,name_branch,id_procurement_type,name_procurement_type,year_procurement,time_procurement,step_procurement,date_procurement)
                    VALUES ('$id_branch2','$branch','$id_procurement_type','$name_type','$year_procurement','$time_procurement','$step_procurement',NOW())";
            $result = $this->db->query($sql);

            if($result){
                $msg = "บันทึกสำเร็จ";
            }else{
                $msg = "ไม่สามารถบันทึกได้";
            }
        }
        return $msg;
    }

    function model_ajax_add_atc_step2($branch,$select_step2,$description_step2,$name_employee_step2,$year_procurement,$time_procurement){
        $id_procurement_type = '2';
        $step_procurement = '2';

        $sql1= "SELECT * FROM procurement_description_tb WHERE year_procurement = '$year_procurement'AND time_procurement = '$time_procurement'AND id_procurement_type =  '$id_procurement_type' AND step_procurement = '$step_procurement'AND id_branch ='$branch'";
        $result1 = $this->db->query($sql1)->num_rows();
        if($result1>0){
            $msg = "ไม่สามารถบันทึกได้ เนื่องจากมีอยู่แล้ว";
        }else{
            $sqlname = "SELECT name_branch FROM procurement_branch_tb WHERE id_branch = '$branch'";
            $result_name = $this->db->query($sqlname)->result();
            $name_branch = $result_name['0'];
            $name_branch2 = $name_branch->name_branch;

            $sql_type_name = "SELECT name_procurement_type FROM procurement_type_tb WHERE id_procurement_type = '$id_procurement_type'";
            $result_type_name = $this->db->query($sql_type_name)->result();
            $name_prosurement_type = $result_type_name['0'];
            $name_type = $name_prosurement_type->name_procurement_type;

            $sql = "INSERT INTO procurement_description_tb (id_branch,name_branch,id_procurement_type,name_procurement_type,year_procurement,time_procurement,step_procurement,condition_procurement,procurement_description,name_employees,date_procurement)
                                                VALUES ('$branch','$name_branch2','$id_procurement_type','$name_type','$year_procurement','$time_procurement','$step_procurement','$select_step2','$description_step2','$name_employee_step2',NOW())";
            $result = $this->db->query($sql);

            if($result){
                $msg = "บันทึกสำเร็จ";
            }else{
                $msg = "ไม่สามารถบันทึกได้";
            }
        }
        return $msg;
    }

    function model_ajax_add_atc_step3($branch,$select_step2,$description_step2,$name_employee_step2,$year_procurement,$time_procurement){
        $id_procurement_type = '2';
        $step_procurement = '3';

        $sql1= "SELECT * FROM procurement_description_tb WHERE year_procurement = '$year_procurement'AND time_procurement = '$time_procurement'AND id_procurement_type =  '$id_procurement_type' AND step_procurement = '$step_procurement'AND id_branch ='$branch'";
        $result1 = $this->db->query($sql1)->num_rows();
        if($result1>0){
            $msg = "ไม่สามารถบันทึกได้ เนื่องจากมีอยู่แล้ว";
        }else{
            $sqlname = "SELECT name_branch FROM procurement_branch_tb WHERE id_branch = '$branch'";
            $result_name = $this->db->query($sqlname)->result();
            $name_branch = $result_name['0'];
            $name_branch2 = $name_branch->name_branch;

            $sql_type_name = "SELECT name_procurement_type FROM procurement_type_tb WHERE id_procurement_type = '$id_procurement_type'";
            $result_type_name = $this->db->query($sql_type_name)->result();
            $name_prosurement_type = $result_type_name['0'];
            $name_type = $name_prosurement_type->name_procurement_type;

            $sql = "INSERT INTO procurement_description_tb (id_branch,name_branch,id_procurement_type,name_procurement_type,year_procurement,time_procurement,step_procurement,condition_procurement,procurement_description,name_employees,date_procurement)
                                                VALUES ('$branch','$name_branch2','$id_procurement_type','$name_type','$year_procurement','$time_procurement','$step_procurement','$select_step2','$description_step2','$name_employee_step2',NOW())";
            $result = $this->db->query($sql);

            if($result){
                $msg = "บันทึกสำเร็จ";
            }else{
                $msg = "ไม่สามารถบันทึกได้";
            }
        }
        return $msg;
    }

    function model_ajax_add_atc_step($branch,$description_step,$name_employee_step,$year_procurement,$time_procurement,$step){
        $id_procurement_type = '2';
        $step_procurement = $step;



        $sql1= "SELECT * FROM procurement_description_tb WHERE year_procurement = '$year_procurement'AND time_procurement = '$time_procurement'AND id_procurement_type =  '$id_procurement_type' AND step_procurement = '$step_procurement'AND id_branch ='$branch'";
        $result1 = $this->db->query($sql1)->num_rows();
        if($result1>0){
            $msg = "ไม่สามารถบันทึกได้ เนื่องจากมีอยู่แล้ว";
        }else{
            $sqlname = "SELECT name_branch FROM procurement_branch_tb WHERE id_branch = '$branch'";
            $result_name = $this->db->query($sqlname)->result();
            $name_branch = $result_name['0'];
            $name_branch2 = $name_branch->name_branch;

            $sql_type_name = "SELECT name_procurement_type FROM procurement_type_tb WHERE id_procurement_type = '$id_procurement_type'";
            $result_type_name = $this->db->query($sql_type_name)->result();
            $name_prosurement_type = $result_type_name['0'];
            $name_type = $name_prosurement_type->name_procurement_type;

            $sql = "INSERT INTO procurement_description_tb (id_branch,name_branch,id_procurement_type,name_procurement_type,year_procurement,time_procurement,step_procurement,procurement_description,name_employees,date_procurement)
                                                VALUES ('$branch','$name_branch2','$id_procurement_type','$name_type','$year_procurement','$time_procurement','$step_procurement','$description_step','$name_employee_step',NOW())";
            $result = $this->db->query($sql);

            if($result){
                $msg = "บันทึกสำเร็จ";
            }else{
                $msg = "ไม่สามารถบันทึกได้";
            }
        }
        return $msg;
    }



    function model_show_car_rental($id_branch,$id_procurement_type){
        if($id_branch==""){
            $sql ="SELECT  DISTINCT id_branch,year_procurement,time_procurement FROM procurement_description_tb WHERE  id_procurement_type='1'";

        }else{
            $sql ="SELECT  DISTINCT id_branch,year_procurement,time_procurement FROM procurement_description_tb WHERE id_branch= '$id_branch'AND id_procurement_type='1'";

        }
        $result = $this->db->query($sql)->result();
        $num = $this->db->query($sql)->num_rows();
        if($num==0){
            $arr = "";
        }else{
            foreach ($result as $row){
                $id_branch = $row->id_branch;
                $year = $row->year_procurement;
                $time = $row->time_procurement;
                $sql2 = "SELECT procurement_description_tb.*,procurement_branch_tb.province_branch FROM procurement_description_tb
                          INNER JOIN procurement_branch_tb
                          ON procurement_description_tb.id_branch = procurement_branch_tb.id_branch
                          WHERE procurement_description_tb.id_branch='$id_branch' AND procurement_description_tb.year_procurement = '$year' AND procurement_description_tb.time_procurement = '$time' AND procurement_description_tb.id_procurement_type='1' GROUP BY procurement_description_tb.step_procurement DESC LIMIT 1";
                $result2 = $this->db->query($sql2)->result();

                $arr[]=$result2;
            }
        }


        //$myJSON = json_encode($arr);
        return $arr ;
    }


    function model_show_atc($id_branch,$id_procurement_type){
        if($id_branch==""){
            $sql ="SELECT  DISTINCT id_branch,year_procurement,time_procurement FROM procurement_description_tb WHERE  id_procurement_type='2'";
        }else{
            $sql ="SELECT  DISTINCT id_branch,year_procurement,time_procurement FROM procurement_description_tb WHERE id_branch= '$id_branch'AND id_procurement_type='2'";
        }
        $result = $this->db->query($sql)->result();
        $num = $this->db->query($sql)->num_rows();
        if($num==0){
            $arr = "";
        }else{
            foreach ($result as $row){
                $id_branch = $row->id_branch;
                $year = $row->year_procurement;
                $time = $row->time_procurement;
                $sql2 = "SELECT procurement_description_tb.*,procurement_branch_tb.province_branch FROM procurement_description_tb
                          INNER JOIN procurement_branch_tb
                          ON procurement_description_tb.id_branch = procurement_branch_tb.id_branch
                          WHERE procurement_description_tb.id_branch='$id_branch' AND procurement_description_tb.year_procurement = '$year' AND procurement_description_tb.time_procurement = '$time' AND procurement_description_tb.id_procurement_type='2' GROUP BY procurement_description_tb.step_procurement DESC LIMIT 1";
                $result2 = $this->db->query($sql2)->result();

                $arr[]=$result2;
            }
        }

        //$myJSON = json_encode($arr);
        return $arr ;
    }

    function model_show_name_branch(){
        $sql = "SELECT * FROM procurement_branch_tb";
        $result = $this->db->query($sql)->result();
        foreach ($result as $row){
            $id_branch = $row->id_branch;
            $arr[$id_branch] = $row->name_branch;
        }

        $myJSON = json_encode($arr);
        return $myJSON ;
    }

    function model_check_name_branch($name_branch){
        $sql = "SELECT * FROM procurement_branch_tb WHERE name_branch = '$name_branch'";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function model_info_procurement($id_branch,$id_procurement_type,$year_procurement,$time_procurement){
        $sql = "SELECT * FROM procurement_description_tb WHERE id_branch = '$id_branch' AND id_procurement_type = '$id_procurement_type ' AND year_procurement = '$year_procurement'AND time_procurement = '$time_procurement'";
        $result = $this->db->query($sql)->result();

        return $result ;

    }

    function model_ajax_show_info_procurement($id_branch,$id_procurement_type,$year_procurement,$time_procurement){
         $sql = "SELECT procurement_description_tb.*,procurement_branch_tb.province_branch FROM procurement_description_tb
                  INNER JOIN procurement_branch_tb
                  ON procurement_description_tb.id_branch = procurement_branch_tb.id_branch
                  WHERE procurement_description_tb.id_branch = '$id_branch' AND procurement_description_tb.id_procurement_type = '$id_procurement_type ' AND procurement_description_tb.year_procurement = '$year_procurement'AND procurement_description_tb.time_procurement = '$time_procurement'";
        $result = $this->db->query($sql)->result();

        $json = json_encode($result);
        return $json ;
    }

    function model_name_to_id_branch($name_branch){
        $sqlname = "SELECT id_branch FROM procurement_branch_tb WHERE name_branch = '$name_branch'";
        $result_id = $this->db->query($sqlname)->result();
        $id_branch = $result_id['0'];
        $id_branch2 = $id_branch->id_branch;
        return $id_branch2 ;
    }



    function model_num_branch_car_rental(){
        $sql = "SELECT distinct (procurement_branch_tb.province_branch)FROM procurement_description_tb
                INNER JOIN procurement_branch_tb
                ON procurement_description_tb.id_branch = procurement_branch_tb.id_branch
                WHERE procurement_description_tb.id_procurement_type = '1'";
        $result = $this->db->query($sql)->num_rows();
        return $result ;
    }

    function model_num_branch_no_car_rental(){
        $sql = " SELECT DISTINCT( b.province_branch )FROM (SELECT distinct (procurement_branch_tb.province_branch)FROM procurement_description_tb
                INNER JOIN procurement_branch_tb
                ON procurement_description_tb.id_branch = procurement_branch_tb.id_branch
                WHERE procurement_description_tb.id_procurement_type = '1') a
      RIGHT JOIN procurement_branch_tb b
      ON a.province_branch =b.province_branch
      WHERE a.province_branch IS NULL";
        $result = $this->db->query($sql)->num_rows();
        return $result ;
    }

    function model_branch_no_car_rental(){
        $sql = " SELECT DISTINCT( b.province_branch )FROM (SELECT distinct (procurement_branch_tb.province_branch)FROM procurement_description_tb
                INNER JOIN procurement_branch_tb
                ON procurement_description_tb.id_branch = procurement_branch_tb.id_branch
                WHERE procurement_description_tb.id_procurement_type = '1') a
      RIGHT JOIN procurement_branch_tb b
      ON a.province_branch =b.province_branch
      WHERE a.province_branch IS NULL";
        $result = $this->db->query($sql)->result();


        $myJSON = json_encode($result);
        return $myJSON ;
    }

































}
