function format_price(n) { ///////function for price
    return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}


$(document).ready(function (e) {
    $("input[data-type='currency']").on({
        keyup: function() {
        formatCurrency($(this));            
        },
        blur: function() { 
        formatCurrency($(this), "blur");            
        }        
    });
});   

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}    

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === ""){
       return; 
    }
  
    // original length
    var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {
    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}




function ajax_edit_bid($link_edit_bid_button) {
    var input_name_project = $('#input_name_project').val();
    var status_bid =$('#status_bid').val();

    var no_bid = $('#no_bid').val();
    var date_bid = $('#date_bid').val();
    var subject_bid =$('#subject_bid').val();
    var no_announce =$('#no_announce').val();
    var date_announce =$('#date_announce').val();
    var type_bid =$('#type_bid').val();
    var cost_bid =$('#cost_bid').val();
    var cost_bid = Number(cost_bid.replace(/[^0-9.-]+/g,""));

    var price_base_bid =$('#price_base_bid').val();
    var price_base_bid = Number(price_base_bid.replace(/[^0-9.-]+/g,""));

    var price_bid =$('#price_bid').val();
    var price_bid = Number(price_bid.replace(/[^0-9.-]+/g,""));

    var month_send_draft_contract =$('#month_send_draft_contract').val();
    var month_approve_bid =$('#month_approve_bid').val();
    var no_approve_bid = $('#no_approve_bid').val();
    var no_egp = $('#no_egp').val();
    var no_contract = $('#no_contract').val();
    var date_contract = $('#date_contract').val();
    var member = $('#member').val();
    var date_receive_bid = $('#date_receive_bid').val();

    var no_direct_committee = $('#no_direct_committee').val();
    var date_direct_committee = $('#date_direct_committee').val();
    var subject_direct_committee = $('#subject_direct_committee').val();

    $.post( $link_edit_bid_button, {
        input_name_project:input_name_project,
        status_bid:status_bid,

        no_bid:no_bid,
        date_bid:date_bid,
        subject_bid:subject_bid,
        no_announce:no_announce,
        date_announce:date_announce,
        type_bid:type_bid,
        cost_bid:cost_bid,
        price_base_bid:price_base_bid,
        price_bid:price_bid,
        month_send_draft_contract:month_send_draft_contract,
        month_approve_bid:month_approve_bid,
        no_approve_bid:no_approve_bid,
        no_egp:no_egp,
        date_contract:date_contract,
        no_contract:no_contract,
        member:member,
        date_receive_bid:date_receive_bid,
        no_direct_committee:no_direct_committee,
        date_direct_committee:date_direct_committee,
        subject_direct_committee:subject_direct_committee
    }).done(function( test ) {
        alert( test );
        location.reload();
    });
}





function show_committee_bid_ajax(){
    var no_bid =  $('#no_bid').val();
    var show_committee_bid_ajax =  $('#show_committee_bid_ajax').val();
    $.post( show_committee_bid_ajax, {
        no_bid:no_bid
    })
        .done(function( response ) {
            $('#show_committee').html(response);
        });
}

function show_modal_committee_bid() {
    reset_committee_bid_form();
    $('#model_committee').modal('show');
    show_committee_bid_ajax()
}

function reset_committee_bid_form() {
    $('#name_committee_bid').val('');
    $('#position_committee_bid').val('');
    show_committee_bid_ajax()
}

function add_committee_bid(link_add_committee_bid_ajax) {
    var name_committee_bid =  $('#name_committee_bid').val();
    var position_committee_bid =  $('#position_committee_bid').val();
    var no_bid =  $('#no_bid').val();
    var status_committee_bid = $('#status_committee_bid').val();
    
    if(no_bid==""){
        alert('กรุณาระบุเลขที่จัดซื้อจัดจ้าง');
        $('#no_bid').focus();
    }else{
        if(name_committee_bid==""){
            alert('กรุณาระบุชื่อคณะกรรมการ');
            $('#name_committee_bid').focus();
        }else{
            if(position_committee_bid==""){
                alert('กรุณาระบุตำแหน่ง');
                $('#position_committee_bid').focus();
            }else{
                $.post(link_add_committee_bid_ajax, {
                    name_committee_bid: name_committee_bid,
                    position_committee_bid: position_committee_bid,
                    no_bid: no_bid,
                    status_committee_bid:status_committee_bid
                })
                    .done(function (response) {
                        alert(response);

                        if(response=='เพิ่มสำเร็จ'){
                            show_committee_bid_ajax();
                        }
                    });
            }
        }
    }
}

function delete_committee_bid_ajax(id_committee_bid ,name_committee_bid) {
   

    var link_delete_committee_bid_ajax =  $('#delete_committee_bid_ajax').val();
    var c = confirm("ต้องการที่จะลบ "+name_committee_bid+"  ใช่หรือไม่");
    if(c==true){
        $.post( link_delete_committee_bid_ajax, {
            id_committee_bid:id_committee_bid,
            name_committee_bid:name_committee_bid
        })
            .done(function( response ) {
                alert(response);
                show_committee_bid_ajax();
                //$('#show_committee').focus();
            });
    }
}



/* $(document).ready(function (e) {   //////////////  Disable function Attract
    $('#ddd').on('click', function () {
        var no_bid =  $('#no_bid').val();
        var att = $('#attachment_bid').val();
        if(att==""){
            alert("กรุณาเลือกไฟล์ ก่อนอัพโหลด");
        }else {
            var file_data = $('#attachment_bid').prop('files')[0];
            var form_data = new FormData();

            if(no_bid == ""){
                alert("กรุณาใส่เลขบันทึกสังเกตุการณ์ ก่อนอัพโหลดไฟล์");
                $('#no_bid').focus();
            }else {
                form_data.append('file', file_data);
                form_data.append('no_bid', no_bid);

                $.ajax({
                    url: '<?php echo base_url("/index.php/purchase/upload_file"); ?>', // point to server-side controller method
                    dataType: 'text', // what to expect back from the server
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',

                    success: function (response) {
                        $('#msg').html(response); // display success response from the server
                        show_attract_ajax();
                    },
                    error: function (response) {
                        $('#msg').html(response); // display error response from the server
                        show_attract_ajax();
                    }
                });
            }
        }
    });
});

function show_attract_ajax(){
    var no_bid =  $('#no_bid').val();
    $.post( "<?=site_url('index.php/purchase/show_attract_ajax') ?>", {
        no_bid:no_bid
    })
        .done(function( response ) {
            $('#show_attract').html(response);
        });
}

function delete_attract_ajax(id_attract){
    var c = confirm("ต้องการที่จะลบไฟล์ ใช่หรือไม่");
    if(c==true){
        $.post( "<?=site_url('index.php/purchase/delete_attract') ?>", {
            id_attract:id_attract
        })
            .done(function( response ) {
                alert(response);
                show_attract_ajax();
                $('#attachment_bid').focus();
            });
    }
} */


$(document).ready(function (e) {
    $('#name_vender').focus(function() {
        var show_name_vender = $('#show_name_vender').val();
        $.post(show_name_vender, {
        })
            .done(function (response) {
                var arr = JSON.parse(response);
                //alert(response);
                $.each(arr, function(id,name) {
                    var option = $('<option value="'+name+'"></option>');
                    $('#vender_list').append(option);
                });
            });
    });
});

$(document).ready(function (e) {
    $('#name_vender').focusout(function() {
        $("#vender_list").empty();
    });
});

function show_modal_sign_up_vender() {
    reset_sign_up_form();
    $('#model_sign_up_vender').modal('show');
}

function reset_sign_up_form() {
    $('#name_sign_up_vender').val('');
    $('#capital_vender').val('');
}

function sign_up_vender(link_sign_up_vender){
    var name_sign_up_vender = $('#name_sign_up_vender').val();
    var capital_vender = $('#capital_vender').val();

    if(name_sign_up_vender==""){
        alert('กรุณาระบุชื่อบริษัท');
        $('#name_sign_up_vender').focus();
    }else {
        $.post( link_sign_up_vender, {
            name_sign_up_vender:name_sign_up_vender,
            capital_vender:capital_vender
        })
            .done(function( response ) {
                //$('#msg_vender').html(response);
                alert(response);
                $('#model_sign_up_vender').modal('hide');
            });
    }
}


function show_vender_ajax(){
    $('#msg_vender').empty();
    var no_bid =  $('#no_bid').val();
    var price_base_bid = $('#price_base_bid').val();
   
    var price_base_bid = Number(price_base_bid.replace(/[^0-9.-]+/g,""));
    

    var show_vender_ajax = $('#show_vender_ajax').val();
    if(no_bid==""){
        alert('ไม่พบ เลขที่จัดซื้อจัดจ้าง');
        $('#no_bid').focus();
    }else {
        //alert(no_bid);
        $.post(show_vender_ajax, {
            no_bid: no_bid
        })
            .done(function (response) {
                var arr = JSON.parse(response);                    

                var i = 1;
                var status = "";
                var msg_vender = $('#msg_vender'), container;

                msg_vender.append("<thead><tr><th>ลำดับ</th><th>บริษัท</th><th>ราคาที่ยืน</th><th>ราคาหลังการต่อรอง</th><th>จำนวนที่ลดลง</th><th>ลดจากราคากลางร้อยละ</th><th>สถานะ</th><th></th></tr></thead>");
                for (var key in arr) {
                    var send = arr[key].sender_active_vender;
                    var pass = arr[key].pass_active_vender;
                    var win = arr[key].win_active_vender;


                    var price_active_vender = arr[key].price_active_vender;
                    var price = arr[key].chaffer_active_vender;

                    if(price==0){
                        var un = price_base_bid - price_active_vender*1;
                        var sale = ((price_base_bid - price_active_vender)*100)/price_base_bid ;
                    }else{
                        var un = price_base_bid - price*1;
                        var sale = ((price_base_bid - price)*100)/price_base_bid ;
                    }
                    price = price*1;
                    price_active_vender = price_active_vender*1;
                    un = un * 1;

                    var p_a_v = "-" ;
                    var p = "-";
                    var u = "-";
                    var s = "-";

                    if (send == 0 && pass == 0 && win == 0) {
                        status = "ไม่ได้ยื่นซอง";
                        var p_a_v = "-" ;
                        var p = "-";
                        var u = "-";
                        var s = "-";
                    } else {
                        if (send == 1 && pass == 0 && win == 0) {
                            status = "คุณสมบัติไม่ผ่าน";
                            var p_a_v = "-" ;
                            var p = "-";
                            var u = "-";
                            var s = "-";
                        } else {
                            if (send == 1 && pass == 1 && win == 0) {
                                status = "ไม่ชนะ";

                                var p_a_v = format_price(price_active_vender) ;
                                var p = "-";
                                var u = "-";
                                var s = "-";
                            } else {
                                if (send == 1 && pass == 1 && win == 1) {
                                    status = "ชนะ";

                                    var p_a_v = format_price(price_active_vender) ;
                                    var p = format_price(price);
                                    var u = format_price(un);
                                    var s = sale.toFixed(2);

                                }
                            }
                        }
                    }


                    container = $('<tr></tr>');
                    msg_vender.append(container);
                    container.append('<td>' + i + '</td>');
                    container.append('<td>' + arr[key].name_vender + '</td>');
                    container.append('<td>' + p_a_v + '</td>');
                    container.append('<td>' + p + '</td>');
                    container.append('<td>' + u +'</td>');
                    container.append('<td>' + s + '</td>');
                    container.append('<td>' + status + '</td>');
                    container.append('<td><a href="#" onclick="delete_active_vender_ajax('+arr[key].id_active_vender+')" class="btn btn-danger"><i class="fas fa-trash"></i></td>');
                    i++;
                }
            });
    }
}

$(document).ready(function (e) {
    $('#price_base_bid').blur(function() {
        show_vender_ajax();
    });
});

function delete_active_vender_ajax(id_vender){
    var link_delete_active_vender = $('#delete_active_vender').val();
    var c = confirm("ต้องการที่จะลบบริษัท ใช่หรือไม่");
    if(c==true){
        $.post( link_delete_active_vender, {
            id_vender:id_vender
        })
            .done(function( response ) {
                alert(response);
                show_vender_ajax();
                $('#button_vender').focus();
            });
    }
}



function show_modal_vender() {
    reset_form();
    $('#model_vender').modal('show');
}


function add_vender(link_add_vender) {
    var name_vender = $('#name_vender').val();
    var sender = $('#sender_vender').val();
    var pricebid = $('#pricebid').val();
    var win = $('#win_vender').val();
    var price_under_bid = $('#price_under_bid').val();
    var no_bid = $('#no_bid').val();
    var pass_vender = $('#pass_vender').val();

    if(no_bid==''){
        alert("กรุณาใส่เลขที่จัดซื้อจัดจ้าง");
        $('#no_bid').focus();
    }else{
        if(name_vender==""){
            alert("กรุณาใส่ชื่อบริษัทที่ซื้อซอง");
            $('#name_vender').focus();
        }else{
            if(sender==""){
                alert('กรุณาเลือกสถานะการยื่นเสนอราคา');
                $('#sender_vender').focus();
            }else{
                var form_data = new FormData();
                form_data.append('name_vender',name_vender);
                form_data.append('sender',sender);
                form_data.append('pricebid',pricebid);
                form_data.append('win',win);
                form_data.append('price_under_bid',price_under_bid);
                form_data.append('no_bid',no_bid);
                form_data.append('pass_vender',pass_vender);

                $.ajax({
                    url: link_add_vender, // point to server-side controller method
                    dataType: 'text', // what to expect back from the server
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (response) {
                        alert(response);
                        show_vender_ajax();
                        //$('#msg_vender').html(response); // display success response from the server
                    },
                    error: function (response) {
                        alert('save error');
                        //$('#msg_vender').html(response); // display error response from the server
                    }
                });
                reset_form();
            }
        }
    }
}

function reset_form() {
    $('#name_vender').val('');
    $('#sender_vender').val('0');
    $('#pricebid').val('');
    $('#win_vender').val('0');
    $('#price_under_bid').val('');
    $('#pass_vender').val('0');
    document.getElementById("pass").style.display = "none";
    document.getElementById("pricebid_div").style.display = "none";
    document.getElementById("win").style.display = "none";
    document.getElementById("price_under_bid_div").style.display = "none";
}

$(document).ready(function (e) {
    $('#sender_vender').click(function() {
        var value_sender = $('#sender_vender').val();
        if(value_sender == "1"){
            document.getElementById("pass").style.display = " block";
        }else{
            $('#pass_vender').val('0');
            $('#pricebid').val('');
            $('#win_vender').val('0');
            $('#price_under_bid').val('');
            document.getElementById("pass").style.display = "none";
            document.getElementById("pricebid_div").style.display = "none";
            document.getElementById("win").style.display = "none";
            document.getElementById("price_under_bid_div").style.display = "none";
        }
    });
});

$(document).ready(function (e) {
    $('#pass_vender').click(function() {
        var value_sender = $('#pass_vender').val();
        if(value_sender == "1"){
            document.getElementById("pricebid_div").style.display = " block";
            document.getElementById("win").style.display = "block";
        }else{
            $('#pricebid').val('');
            $('#win_vender').val('0');
            $('#price_under_bid').val('');
            document.getElementById("pricebid_div").style.display = "none";
            document.getElementById("win").style.display = "none";
            document.getElementById("price_under_bid_div").style.display = "none";
        }
    });
});

$(document).ready(function (e) {
    $('#win_vender').click(function() {
        var value_win = $('#win_vender').val();
        if(value_win == "1"){
            document.getElementById("price_under_bid_div").style.display = " block";
        }else{
            $('#price_under_bid').val('');
            document.getElementById("price_under_bid_div").style.display = "none";
        }
    });
});

/* $(document).ready(function (e) {
    $('#member').focus(function() {
        $("#member_list").empty();
        var link_show_name_member_purchase = $('#link_show_name_member_purchase').val();
        
        $.post(link_show_name_member_purchase, {
        })
            .done(function (response) {
                var arr = JSON.parse(response);
                //alert(response);
                $.each(arr, function(id,name) {
                    var option = $('<option value="'+name+'"></option>');
                    $('#member_list').append(option);
                });
            });
    });
}); */



