    function format_price(n) { ///////function for price
        return n.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+" บาท";
    }


    $(document).ready(function (e) {
        $("input[data-type='currency']").on({
            keyup: function() {
            formatCurrency($(this));            
            },
            blur: function() { 
            formatCurrency($(this), "blur");            
            }
        });
    });   
    
    function formatNumber(n) {
      // format number 1000000 to 1,234,567
      return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }    
    
    function formatCurrency(input, blur) {
      // appends $ to value, validates decimal side
      // and puts cursor back in right position.
      
      // get input value
      var input_val = input.val();
      
      // don't validate empty input
      if (input_val === "") { return; }
      
      // original length
      var original_len = input_val.length;
    
      // initial caret position 
      var caret_pos = input.prop("selectionStart");
        
      // check for decimal
      if (input_val.indexOf(".") >= 0) {
    
        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
        var decimal_pos = input_val.indexOf(".");
    
        // split number by decimal point
        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);
    
        // add commas to left side of number
        left_side = formatNumber(left_side);
    
        // validate right side
        right_side = formatNumber(right_side);
        
        // On blur make sure 2 numbers after decimal
        if (blur === "blur") {
          right_side += "00";
        }
        
        // Limit decimal to only 2 digits
        right_side = right_side.substring(0, 2);
    
        // join number by .
        input_val = "" + left_side + "." + right_side;
    
      } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = formatNumber(input_val);
        input_val = "" + input_val;
        
        // final formatting
        if (blur === "blur") {
          input_val += ".00";
        }
      }
      
      // send updated string to input
      input.val(input_val);
    
      // put caret back in the right position
      var updated_len = input_val.length;
      caret_pos = updated_len - original_len + caret_pos;
      input[0].setSelectionRange(caret_pos, caret_pos);
    }



    function ajax_add_bid(url_addbid,url_checkmember_add_bid) {
        
        var input_name_project = $('#input_name_project').val();
        var status_bid =$('#status_bid').val();
        var year_bid =$('#year_bid').val();

        var no_bid = $('#no_bid').val();
        var date_bid = $('#date_bid').val();
        var subject_bid =$('#subject_bid').val();
        var no_announce =$('#no_announce').val();
        var date_announce =$('#date_announce').val();
        var type_bid =$('#type_bid').val();
        var cost_bid =$('#cost_bid').val();
        var cost_bid = Number(cost_bid.replace(/[^0-9.-]+/g,""));
        
        var price_base_bid =$('#price_base_bid').val();
        var price_base_bid = Number(price_base_bid.replace(/[^0-9.-]+/g,""));

        var price_bid =$('#price_bid').val();
        var price_bid = Number(price_bid.replace(/[^0-9.-]+/g,""));

        var month_send_draft_contract =$('#month_send_draft_contract').val();
        var month_approve_bid =$('#month_approve_bid').val();
        var no_send_draft_contract = $('#no_send_draft_contract').val();
        var no_egp = $('#no_egp').val();
        var no_contract = $('#no_contract').val();
        var date_contract = $('#date_contract').val();
        var member = $('#member').val();
        var date_receive_bid = $('#date_receive_bid').val();

        var no_direct_committee = $('#no_direct_committee').val();
        var date_direct_committee = $('#date_direct_committee').val();
        var subject_direct_committee = $('#subject_direct_committee').val();
        

        
               
        
        if(no_bid==""){
            alert("กรุณาเลือกปีบัญชี ก่อนบันทึก");
            $('#year_bid').focus();
        }else{
            if(subject_bid==""){
                alert("กรุณาระบุชื่อการจัดซื้อจัดจ้าง ก่อนบันทึก");
                $('#subject_bid').focus();
            }else{
                if(price_base_bid=="0"){
                    alert("กรุณาระบุราคากลาง ก่อนบันทึก");
                    $('#price_base_bid').focus();
                }else{
                    if(member==""){                        
                        alert("กรุณาระบุผู้รับผิดชอบ ก่อนบันทึก");
                        $('#member').focus();

                    }else{

                        $.post( url_checkmember_add_bid, {                            
                            member:member                      
                        }).done(function( test ) {
                            

                            if(test!=0){
                                $.post( url_addbid, {
                                    input_name_project:input_name_project,
                                    status_bid:status_bid,
                                    year_bid:year_bid,
                                    no_bid:no_bid,
                                    date_bid:date_bid,
                                    subject_bid:subject_bid,
                                    no_announce:no_announce,
                                    date_announce:date_announce,
                                    type_bid:type_bid,
                                    cost_bid:cost_bid,
                                    price_base_bid:price_base_bid,
                                    price_bid:price_bid,
                                    month_send_draft_contract:month_send_draft_contract,
                                    month_approve_bid:month_approve_bid,
                                    no_send_draft_contract:no_send_draft_contract,
                                    no_egp:no_egp,
                                    date_contract:date_contract,
                                    no_contract:no_contract,
                                    member:member,
                                    date_receive_bid:date_receive_bid,
                                    no_direct_committee:no_direct_committee,
                                    date_direct_committee:date_direct_committee,
                                    subject_direct_committee:subject_direct_committee
                    
                                }).done(function( test ) {
                                    alert( test );
                                    location.reload();
                                });
                            }else{
                                alert("ไม่พบผู้รับผิดชอบในระบบ");
                                $('#member').focus();
                            }
                            
                        });
                    }
                }
            }            
        }
    }






    function show_committee_bid_ajax(){
        var no_bid =  $('#no_bid').val();
        var link_show_committee_bid_ajax =  $('#link_show_committee_bid_ajax').val();
        $.post( link_show_committee_bid_ajax, {
            no_bid:no_bid
        })
            .done(function( response ) {
                $('#show_committee').html(response);
            });
    }

    function show_modal_committee_bid() {
        reset_committee_bid_form();
        $('#model_committee').modal('show');
        show_committee_bid_ajax()
    }

    function reset_committee_bid_form() {
        $('#name_committee_bid').val('');
        $('#position_committee_bid').val('');
        show_committee_bid_ajax()
    }

    function add_committee_bid(link_add_committee_bid_ajax) {
        var name_committee_bid =  $('#name_committee_bid').val();
        var position_committee_bid =  $('#position_committee_bid').val();
        var no_bid =  $('#no_bid').val();
        var status_committee_bid = $('#status_committee_bid').val();

        if(no_bid==""){
            alert('กรุณาระบุเลขที่จัดซื้อจัดจ้าง');
            $('#no_bid').focus();
        }else{
            if(name_committee_bid==""){
                alert('กรุณาระบุชื่อคณะกรรมการ');
                $('#name_committee_bid').focus();
            }else{
                if(position_committee_bid==""){
                    alert('กรุณาระบุตำแหน่ง');
                    $('#position_committee_bid').focus();
                }else{
                    $.post(link_add_committee_bid_ajax, {
                        name_committee_bid: name_committee_bid,
                        position_committee_bid: position_committee_bid,
                        no_bid: no_bid,
                        status_committee_bid:status_committee_bid
                    })
                        .done(function (response) {
                            alert(response);

                            if(response=='เพิ่มสำเร็จ'){
                                show_committee_bid_ajax();
                            }
                        });
                }
            }
        }
    }

    function delete_committee_bid_ajax(id_committee_bid ,name_committee_bid) {
        var link_delete_committee_bid_ajax =  $('#link_delete_committee_bid_ajax').val();

        var c = confirm("ต้องการที่จะลบ "+name_committee_bid+"  ใช่หรือไม่");
        if(c==true){
            $.post( link_delete_committee_bid_ajax, {
                id_committee_bid:id_committee_bid,
                name_committee_bid:name_committee_bid
            })
                .done(function( response ) {
                    alert(response);
                    show_committee_bid_ajax();
                    //$('#show_committee').focus();
                });
        }
    }





    /////////////////////////////////////////////////////////////// disable Function attract
    /* function show_attract_ajax(link_upload_attract){
        var no_bid =  $('#no_bid').val();
        alert(no_bid);
        $.post(link_upload_attract , {
            no_bid:no_bid
        })
            .done(function( response ) {
                alert(response);
                $('#show_attract').html(response);
            });
    }

    function delete_attract_ajax(id_attract){
        var c = confirm("ต้องการที่จะลบไฟล์ ใช่หรือไม่");
        if(c==true){
            $.post( "<?=site_url('index.php/purchase/delete_attract') ?>", {
                id_attract:id_attract
            })
                .done(function( response ) {
                    alert(response);
                    show_attract_ajax();
                    $('#attachment_bid').focus();
                });
        }
    }
 */

    function show_modal_sign_up_vender() {
        reset_sign_up_form();
        $('#model_sign_up_vender').modal('show');
    }



    function reset_sign_up_form() {
        $('#name_sign_up_vender').val('');
        $('#capital_vender').val('');
    }

    function sign_up_vender(link_sign_up_vender){
        var name_sign_up_vender = $('#name_sign_up_vender').val();
        var capital_vender = $('#capital_vender').val();
        var capital_vender = Number(capital_vender.replace(/[^0-9.-]+/g,""));

        if(name_sign_up_vender==""){
            alert('กรุณาระบุชื่อบริษัท');
            $('#name_sign_up_vender').focus();
        }else {
            if(capital_vender==""){
                alert('กรุณาระบุชื่อบริษัท');
                $('#capital_vender').focus();
            }else{
                $.post( link_sign_up_vender , {
                    name_sign_up_vender:name_sign_up_vender,
                    capital_vender:capital_vender
                })
                    .done(function( response ) {
                        //$('#msg_vender').html(response);
                        alert(response);
                        $('#model_sign_up_vender').modal('hide');
                    });
            }
        }
    }


    function show_vender_ajax(link_show_vender,link_delete_vender){
        $('#msg_vender').empty();
        var no_bid =  $('#no_bid').val();
        if(no_bid==""){
            alert('ไม่พบ เลขอ้างอิงจัดซื้อจัดจ้าง');
            $('#no_bid').focus();
        }else {
            //alert(no_bid);
            $.post(link_show_vender, {
                no_bid: no_bid
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    var price_base_bid = $('#price_base_bid').val();
                    var price_base_bid = Number(price_base_bid.replace(/[^0-9.-]+/g,""));
                    
                    var i = 1;
                    var status = "";
                    var msg_vender = $('#msg_vender'), container;

                    msg_vender.append("<thead><tr><th>ลำดับ</th><th>บริษัท</th><th>ราคาที่ยืน</th><th>ราคาหลังการต่อรอง</th><th>จำนวนที่ลดลง</th><th>ลดจากราคากลางร้อยละ</th><th>สถานะ</th><th></th></tr></thead>");
                    for (var key in arr) {
                        var send = arr[key].sender_active_vender;
                        var pass = arr[key].pass_active_vender;
                        var win = arr[key].win_active_vender;

                        var price_active_vender = arr[key].price_active_vender;
                        var price = arr[key].chaffer_active_vender;

                        if(price==0){
                            var un = price_base_bid - price_active_vender*1;
                            var sale = ((price_base_bid - price_active_vender)*100)/price_base_bid ;
                        }else{
                            var un = price_base_bid - price*1;
                            var sale = ((price_base_bid - price)*100)/price_base_bid ;
                        }
                        price = price*1;
                        price_active_vender = price_active_vender*1;
                        un = un * 1;

                        var p_a_v = "-" ;
                        var p = "-";
                        var u = "-";
                        var s = "-";

                        if (send == 0 && pass == 0 && win == 0) {
                            status = "ไม่ได้ยื่นซอง";
                            var p_a_v = "-" ;
                            var p = "-";
                            var u = "-";
                            var s = "-";
                        } else {
                            if (send == 1 && pass == 0 && win == 0) {
                                status = "คุณสมบัติไม่ผ่าน";
                                var p_a_v = "-" ;
                                var p = "-";
                                var u = "-";
                                var s = "-";
                            } else {
                                if (send == 1 && pass == 1 && win == 0) {
                                    status = "ไม่ชนะ";

                                    var p_a_v = format_price(price_active_vender) ;
                                    var p = "-";
                                    var u = format_price(un);
                                    var s = sale.toFixed(2);
                                } else {
                                    if (send == 1 && pass == 1 && win == 1) {
                                        status = "ชนะ";

                                        var p_a_v = format_price(price_active_vender) ;
                                        var p = format_price(price);
                                        var u = format_price(un);
                                        var s = sale.toFixed(2);

                                    }
                                }
                            }
                        }

                        container = $('<tr></tr>');
                        msg_vender.append(container);
                        container.append('<td>' + i + '</td>');
                        container.append('<td>' + arr[key].name_vender + '</td>');
                        container.append('<td>' + p_a_v + '</td>');
                        container.append('<td>' + p + '</td>');
                        container.append('<td>' + u +'</td>');
                        container.append('<td>' + s + '</td>');
                        container.append('<td>' + status + '</td>');
                        container.append('<td><a href="#"  onclick="delete_active_vender_ajax(\' '+arr[key].id_active_vender+'\',\' '+link_delete_vender+'\' ,\' '+link_show_vender+'\')" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>');
                         i++;
                    }
                });
        }
    }

    function delete_active_vender_ajax(id_vender,link_delete_vender,link_show_vender){

        var c = confirm("ต้องการที่จะลบบริษัท ใช่หรือไม่");
        if(c==true){
            $.post( link_delete_vender, {
                id_vender:id_vender
            })
                .done(function( response ) {
                    alert(response);
                    show_vender_ajax(link_show_vender,link_delete_vender);
                    $('#button_vender').focus();
                });
        }
    }







    function show_modal_vender() {
        var price_base_bid = $('#price_base_bid').val();
        if(price_base_bid=="0" || price_base_bid =="0.00"){
            alert("กรุณาระบุราคากลาง");
            $('#price_base_bid').focus();
        }else{
            reset_form();
            $('#model_vender').modal('show');
        }
        
    }


    function add_vender(link_add_vender,link_show_vender,link_delete_vender) {
        var name_vender = $('#name_vender').val();
        var sender = $('#sender_vender').val();
        var pricebid = $('#pricebid').val();
        var pricebid = Number(pricebid.replace(/[^0-9.-]+/g,""));
        
        var win = $('#win_vender').val();
        var price_under_bid = $('#price_under_bid').val();
        var price_under_bid = Number(price_under_bid.replace(/[^0-9.-]+/g,""));
        
        var no_bid = $('#no_bid').val();
        var pass_vender = $('#pass_vender').val();

        if(no_bid==''){
            alert("กรุณาใส่เลขอ้างอิงจัดซื้อจัดจ้าง");
            $('#no_bid').focus();
        }else{
            if(name_vender==""){
                alert("กรุณาใส่ชื่อบริษัทที่ซื้อซอง");
                $('#name_vender').focus();
            }else{
                if(sender==""){
                    alert('กรุณาเลือกสถานะการยื่นเสนอราคา');
                    $('#sender_vender').focus();
                }else{
                    var form_data = new FormData();
                    form_data.append('name_vender',name_vender);
                    form_data.append('sender',sender);
                    form_data.append('pricebid',pricebid);
                    form_data.append('win',win);
                    form_data.append('price_under_bid',price_under_bid);
                    form_data.append('no_bid',no_bid);
                    form_data.append('pass_vender',pass_vender);

                    $.ajax({
                        url: link_add_vender, // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            alert(response);
                            show_vender_ajax(link_show_vender,link_delete_vender);
                            //$('#msg_vender').html(response); // display success response from the server
                        },
                        error: function (response) {
                            alert('save error');
                            //$('#msg_vender').html(response); // display error response from the server
                        }
                    });
                    reset_form();
                }
            }
        }
    }

    function reset_form() {
        $('#name_vender').val('');
        $('#sender_vender').val('0');
        $('#pricebid').val('');
        $('#win_vender').val('0');
        $('#price_under_bid').val('');
        $('#pass_vender').val('0');
        document.getElementById("pass").style.display = "none";
        document.getElementById("pricebid_div").style.display = "none";
        document.getElementById("win").style.display = "none";
        document.getElementById("price_under_bid_div").style.display = "none";
    }







    function show_modal_project() {
        reset_project();
        $('#modal_project').modal('show');
    }

    function reset_project() {
        $('#name_project').val('');
        $('#no_project').val('');
        $('#date_project').val('');
        $('#owner_project').val('');
        $('#cost_project').val('');
        $('#objective_project').val('');
    }

    function add_project(link) {

        var name_project =  $('#name_project').val();
        var name =  $('#name_project').val();
        var no_project = $('#no_project').val();
        var date_project =$('#date_project').val();
        var owner_project = $('#owner_project').val();
        var cost_project = $('#cost_project').val();
        var cost_project = Number(cost_project.replace(/[^0-9.-]+/g,""));
        var objective_project = $('#objective_project').val();


        if(name_project==""){
            alert('กรุณาระบุชื่อโครงการ');
            $('#name_project').focus();
        }else{
            $.post(link, {
                name_project: name_project,
                no_project: no_project,
                date_project: date_project,
                owner_project: owner_project,
                cost_project: cost_project,
                objective_project: objective_project
            })

                .done(function (response) {
                    alert(response);

                    //show_sub_bid();
                    if(response=='เพิ่มสำเร็จ'){
                        $('#input_project').val(name);
                    }
                });
        }
    }

     function add_input_name_project(link) {
        var input_project = $('#input_project').val();
        if(input_project==""){
            alert('กรุณาระบุชื่อโครงการ หรือเพิ่มโครงการ ก่อนการเลือกโครงการ');
        }else{
            $.post(link, {
                input_project:input_project
            })
                .done(function (response) {
                    if(response=="yes"){
                        $('#input_name_project').val(input_project);
                        $('#subject_bid').val(input_project);
                        $('#input_project').val("");
                        //$('#card_bid').show();
                        document.getElementById("card_bid").style.display = "block";
                        document.getElementById("card_vender").style.display = "block";
                        //document.getElementById("card_attach").style.display = "none";
                        document.getElementById("card_submit").style.display = "block";
                        document.getElementById("card_committee").style.display = "block";
                    }else{
                        alert('ไม่พบชื่อโครงการ กรุณาเพิ่มโครงการ หรือเลือกโครงการ');
                        $('#button_project').focus();
                    }
                });
        }
    } 

    $(document).ready(function (e) {
        $('#member').focus(function() {
            $("#member_list").empty();
            var link_show_name_member_purchase = $('#link_show_name_member_purchase').val();
            
            $.post(link_show_name_member_purchase, {
            })
                .done(function (response) {
                    var arr = JSON.parse(response);
                    //alert(response);
                    $.each(arr, function(id,name) {
                        var option = $('<option value="'+name+'"></option>');
                        $('#member_list').append(option);
                    });
                });
        });
    });

