<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class chart extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('chart_model');
        $this->load->model('function_model');
    }


    public function show_num_bid(){
       $result = $this->chart_model->model_show_num_bid();
       echo $result." โครงการ";
    }

    public function show_all_price(){
        $result = $this->chart_model->model_show_all_price();
        echo $result ;
    }

    public function show_price_all_bid(){
        $result = $this->chart_model->model_show_price_all_bid();
        echo $result ;
    }

    public function show_fail_bid(){
         $result = $this->chart_model->model_show_fail_bid();
         echo $result." โครงการ";
    }

    public function show_area_chart_by_month(){
        $year_th = $_POST['year_th'];

        echo $result = $this->chart_model->model_show_area_chart_by_month($year_th);

    }




    public function show_area_chart(){
        $year_th = $_POST['year_th'];
        echo $result = $this->chart_model->model_show_area_chart($year_th);
    }

    public function show_bar_chart(){
        $year = $_POST['year'];
       echo $result = $this->chart_model->model_show_bar_chart($year);
    }




}