<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bid extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->output->enable_profiler(true);
        $this->load->model('bid_model');
        $this->load->model('function_model');
    }

    public function index(){

        $this->load->view('from/head');
        $this->load->view('index');
        $this->load->view('from/footer');
    }

    public function show_bid(){

        $data['data'] = $this->bid_model->model_show_bid();
        $data['num_bid'] = $this->bid_model->model_show_num_bid();

        $this->load->view('from/head');
        $this->load->view('show_bid',$data);
        $this->load->view('from/footer');
    }

    public function add_bid(){
        $this->bid_model->model_show_all_vender();
        $this->load->view('from/head');
        $this->load->view('add_bid');
        $this->load->view('from/footer');
    }


    public function edit_bid(){
        $no_bid = $_POST['no_bid'];

        $data['bid'] = $this->bid_model->model_show_edit_bid($no_bid);
        $data['vender_all_bid'] = $this->bid_model->model_show_vender_by_no_bid($no_bid);
        $data['attract'] = $this->bid_model->model_show_attract_by_no_bid($no_bid);
        $data['sub_bid'] = $this->bid_model->model_show_sub_bid_by_no_bid($no_bid);
        $data['vender_sub_bid'] = $this->bid_model->model_show_vender_sub_bid_by_no_bid($no_bid);

        $this->bid_model->model_show_all_vender();

        $this->load->view('from/head');
        $this->load->view('edit_bid',$data);
        $this->load->view('from/footer');
    }

    public function show_bid_by_vender(){
        $this->bid_model->model_show_win_vender();

        $this->load->view('from/head');
        $this->load->view('show_bid_by_vender');
        $this->load->view('from/footer');
    }

    public function end_contract_1year(){
        $data['data'] = $this->bid_model->model_end_contract();
        $this->load->view('from/head');
        $this->load->view('end_contract_1year',$data);
        $this->load->view('from/footer');
    }

    public function end_contract_6month(){
        $data['data'] = $this->bid_model->model_end_contract();
        $this->load->view('from/head');
        $this->load->view('end_contract_6month',$data);
        $this->load->view('from/footer');
    }

    public function end_contract_3month(){
        $data['data'] = $this->bid_model->model_end_contract();
        $this->load->view('from/head');
        $this->load->view('end_contract_3month',$data);
        $this->load->view('from/footer');
    }

    public function ended_contract(){
        $data['data'] = $this->bid_model->model_end_contract();
        $this->load->view('from/head');
        $this->load->view('ended_contract',$data);
        $this->load->view('from/footer');
    }





    public function info_bid(){

        $no_bid = $_POST['no_bid'];
        $data['bid'] = $this->bid_model->model_show_bid_by_no_bid($no_bid);
        $data['vender_all_bid'] = $this->bid_model->model_show_vender_by_no_bid($no_bid);
        $data['attract'] = $this->bid_model->model_show_attract_by_no_bid($no_bid);
        $data['sub_bid'] = $this->bid_model->model_show_sub_bid_by_no_bid($no_bid);
        $data['vender_sub_bid'] = $this->bid_model->model_show_vender_sub_bid_by_no_bid($no_bid);

        $this->load->view('from/head');
        $this->load->view('info_bid',$data);
        $this->load->view('from/footer');
    }

    public function upload_file() {

        //upload file
        $file_name = $_POST['no_bid']."_1" ;
        $config['file_name']=$file_name;
        $config['upload_path'] = 'file_upload/';
        $config['allowed_types'] = '*';
        $config['max_filename'] = '1000';
        $config['encrypt_name'] = false;
        //$config['max_size'] = '1024'; //1 MB
        $old_name = $_FILES['file']['name'];
        $type = strrchr($old_name,".");
        $new_name = $file_name.$type;
        $no_bid = $_POST['no_bid'] ;
        $path = 'file_upload/' . $file_name.$type;
        $i = "1";

        while($result=$this->bid_model->model_add_attract_file($old_name,$new_name,$path,$no_bid)=="have"){
            $file_name = substr($file_name,0,-1).$i;
            $new_name = $file_name.$type;
            $path = 'file_upload/' . $file_name.$type;
            $config['file_name'] = $file_name ;
            $i++;
        }

        if (isset($_FILES['file']['name'])) {
            if (0 < $_FILES['file']['error']) {
                echo 'ไม่สามารถอัพโหลดได้' . $_FILES['file']['error'];
            }else{
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        echo $this->upload->display_errors();
                    }else{
                        echo "อัพโหลดเรียบร้อยแล้ว";
                    }
            }
        } else {
            echo 'กรุณาเลือกไฟล์ที่ อัพโหลด';
        }
        if($result=="unsuccessful"){
            echo 'ไม่สามารถอัพโหลดไฟล์ '.$_FILES['file']['name'].' ได้';
        }
    }



    public function show_bid_by_vender_ajax(){
        $this->bid_model->model_show_all_vender();

            $name_vender = $_POST['name_vender'];
            $before = $_POST['before'];
            $after = $_POST['after'];

            $data = $this->bid_model->model_show_bid_by_vender($name_vender,$before,$after);
            $num=$data['num'];
            $result = $data['result'];

            echo"
            <table class=\"table table-striped \"  width=\"100%\" cellspacing=\"0\" >
                    <thead>
                        <tr>
                            <th colspan='7'><h6 class=\"m-0 font-weight-bold text-primary\"  >$name_vender : จำนวน $num โครงการ</h6></th>
                        </tr>
                        <tr>
                            <th scope=\"col\">#</th>
                            <th scope=\"col\">วันที่ประกวดราคา</th>                            
                            <th scope=\"col\">โครงการ</th>                            
                            <th scope=\"col\">ราคากลาง</th>
                            <th scope=\"col\">ราคาเสนอครั้งสุดท้าย</th>
                            <th scope=\"col\">ลดลง</th>
                            
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
            ";
            $i = 1;
            foreach ($result as $row){

                $last_price = $row->price_under_vender;

                if($row->price_under_vender=="0"){
                    $last_price = $row->price_vender;
                }
                $cost = $row->cost_bid;

                $devide = $row->devide_bid;
                $no_sub_bid = $row->no_sub_bid ;
                $no_bid = $row->no_bid ;
                $msg_devide = "";
                if($devide=="แยก"){

                    $result_sub_bid = $this->bid_model->model_show_sub_bid_by_nio_sub_bid($no_sub_bid,$no_bid);
                    $r = $result_sub_bid['0'];
                    $cost = $r->cost_sub_bid;
                    $msg_devide = "(".$r->name_sub_bid.")";
                }
                $dif = $cost-$last_price;

                echo "              
                
                      <tr>
                        <td >$i</td>
                        <td>".$this->function_model->datethai($row->date_bid)."</td>                        
                        <td>".$row->subject_bid." ".$msg_devide."</td>
                        <td>".number_format($cost,2)."</td>
                        <td>".number_format($last_price,2)."</td>
                        <td>".number_format($dif,2)."</td>
                        <td>
                          <form id='form_info_bid$row->no_bid' action='".base_url("index.php/bid/info_bid")."' method='post' >
                                    <input id='no_bid' name='no_bid' value='$row->no_bid' type='hidden' >
                                    <a href='#'  onclick='info_bid($row->no_bid)' title='รายละเอียด' class='btn btn-info '>
                                        <i class='fas fa-info-circle'></i>
                                    </a>
                                    </form>
                        </td>                        
                      </tr>           
                ";
                $i++;
            }

            echo "
             </tbody>
                </table>
            
            ";


    }

    public function show_attract_ajax(){

        $no_bid = $_POST['no_bid'];
        $result = $this->bid_model->model_show_attract_by_no_bid($no_bid);
        $i=1;

        echo "<table cellpadding='5' class='table col-6' >";
        echo "
            <tr>
                <td>ลำดับ</td>
                <td>ชื่อไฟล์</td>
                <td></td>            
            </tr>
        ";

        foreach ($result as $row){
            echo "
                <tr>
                    <td>$i</td>
                    <td>$row->oldname_attract</td> 
                    <td><a href='#' title='ลบ' onclick='delete_attract_ajax($row->id_attract)' class='btn btn-danger '>
                        <i class='fas fa-trash'></i></a>
                    </td>                
                </tr>      
            ";
            $i++;
        }
        echo "</table>";
    }


    public function show_vender_ajax(){

        $no_bid = $_POST['no_bid'];

        $data = $this->bid_model->model_show_vender_by_no_bid($no_bid);

        if(isset($_POST['cost_bid'])){
            $cost_bid = $_POST['cost_bid'];

        }else{
            $result = $this->bid_model->model_show_bid_by_no_bid($no_bid);
            $bid = $result['0'];
            $cost_bid = $bid->cost_bid;
        }

        $this->bid_model->model_show_all_vender();

        echo "
            <table class='table' width='100%'>
                <thead>
                    <tr><th>ลำดับ</th>
                        <th>บริษัท</th>
                        <th>วันที่ยื่น</th>
                        <th>ราคาที่ยืน</th>
                        <th>ราคาหลังการต่อรอง</th>
                        <th>ลดจากราคากลาง</th>
                    </tr>
                </thead>
                <tbody>          
        ";
        $i=1;

        foreach ($data as $row) {
            $name_vender = $row->name_vender;
            $price_vender = $row->price_vender;
            $price_under_vender = $row->price_under_vender;
            $no_vender = $row->id_vender;

            if($price_under_vender=='0'){
                $sub_price = ($cost_bid - $price_vender);
            }else{
                $sub_price = ($cost_bid - $price_under_vender);
            }
            if($cost_bid==0){
                $per = 0;
            }else{
                $per = ($sub_price*100)/$cost_bid;
            }

            if($row->win_vender==1){
                $status = "ชนะการประกวดราคา";
            }elseif ($row->sender_vender==1&&$row->price_vender==0&&$row->pass_vender==0){
                $status = "ไม่ผ่านคุณสมบัติ";
            }elseif ($row->sender_vender==1&&$row->price_vender!=0){
                $status = "ยื่นซอง";
            }elseif($row->sender_vender==0){
                $status = "ไม่ได้ยื่นซอง";
            }

            echo "
                    <tr>
                        <td>$i</td>
                        <td width='40%'>$name_vender</td>
                        <td width='15%'>$status</td>
                        <td width='15%'>".number_format($price_vender,2)." บาท</td>
                        <td width='15%'>".number_format($price_under_vender,2)." บาท</td>
                        <td width='15%'>".number_format($sub_price,2)." บาท".number_format($per, 2, '.', '')."%</td>                                               
                        <td><a href='#' title='ลบ' onclick='delete_vender_ajax($no_vender)' class='btn btn-danger '>
                        <i class='fas fa-trash'></i></a></td>
                    </tr>            
            ";
            $i++;
        }
        echo "</tbody>
            </table>
        ";
    }



    public function delete_attract(){
        $id_attract = $_POST['id_attract'];
        $result = $this->bid_model->model_delete_attract($id_attract);

        if($result=="success"){
            echo "ทำการลบไฟล์แนบเรียบร้อยแล้ว";
        }else{
            echo "ไม่สามารถลบไฟล์แนบได้";
        }
    }


    public function delete_vender(){
        $id_vender = $_POST['id_vender'];
        $result = $this->bid_model->model_delete_vender($id_vender);
        $this->bid_model->model_show_all_vender();

        if($result=="success"){
            echo "ทำการลบบริษัทเรียบร้อยแล้ว";
        }else{
            echo "ไม่สามารถลบบริษัทได้";
        }
    }

    public function add_vender(){

        $no_bid = $_POST['no_bid'];
        $name_vender = $_POST['name_vender'];
        $sender = $_POST['sender'];
        $win = $_POST['win'];
        $pricebid = $_POST['pricebid'];
        $price_under_bid = $_POST['price_under_bid'];
        $date_sender_vender = $_POST['date_sender_vender'];
        $id_sub = $_POST['id_sub'];
        $pass_vender = $_POST['pass_vender'];

        $cost_bid = $_POST['cost_bid'];

        $result = $this->bid_model->model_add_vender($no_bid,$name_vender,$sender,$win,$pricebid,$price_under_bid,$date_sender_vender,$id_sub,$pass_vender);

        $msg = $result['msg'];
        //$msg="success";

        if($msg=="success"){

            echo $msg;
        }else{
            echo "ไม่พบข้อมูลบริษัท";
        }
    }



    public function add_bid_button(){

        $no_bid  = $_POST['no_bid'];
        $date_bid  = $_POST['date_bid'];
        $year_bid  = $_POST['year_bid'];
        $subject_bid  = $_POST['subject_bid'];
        $type_bid  = $_POST['type_bid'];
        $tri_bid  = $_POST['tri_bid'];
        $cost_bid  = $_POST['cost_bid'];
        $date_audit  = $_POST['date_audit'];
        $issues_bid  = $_POST['issues_bid'];
        $condition_bid  = $_POST['condition_bid'];
        $period_bid  = $_POST['period_bid'];
        $member_audit1 = $_POST['member_audit1'];
        $member_audit2 = $_POST['member_audit2'];
        $member_audit3 = $_POST['member_audit3'];
        $member_audit4 = $_POST['member_audit4'];
        $devide_bid = $_POST['devide_bid'];
        $price_asset = $_POST['price_asset'];
        $price_ma = $_POST['price_ma'];
        $status_bid = $_POST['status_bid'];
        $no_contract = $_POST['no_contract'];

      $result = $this->bid_model->model_add_bid($no_bid,$date_bid,$year_bid,$subject_bid,$type_bid,$tri_bid,$cost_bid,$date_audit,$issues_bid,$condition_bid,$period_bid,$member_audit1,$member_audit2,$member_audit3,$member_audit4,$devide_bid,$price_asset,$price_ma,$status_bid,$no_contract);

        if($result=="success"){
            echo "บันทึกการสังเกตุการณ์ สำเร็จ";
        }else{
            echo "บันทึกสังเกตุการณ์ ไม่สำเร็จ";
        }
    }

    public function delete_bid(){
        $no_bid = $_POST['no_bid'];
        $result = $this->bid_model->model_delete_bid($no_bid);

        echo $result;
    }



    public function  check_vender_by_no_bid(){
        $this->bid_model->model_show_all_vender();
        $no_bid = $_POST['no_bid'];
        $result = $this->bid_model->model_check_vender_by_no_bid($no_bid);
        $num = $result['0'];
        echo $data = $num->count;
     }


    public function edit_bid_button(){
        $no_bid  = $_POST['no_bid'];
        $date_bid  = $_POST['date_bid'];
        $year_bid  = $_POST['year_bid'];
        $subject_bid  = $_POST['subject_bid'];
        $type_bid  = $_POST['type_bid'];
        $tri_bid  = $_POST['tri_bid'];
        $cost_bid  = $_POST['cost_bid'];
        $date_audit  = $_POST['date_audit'];
        $issues_bid  = $_POST['issues_bid'];
        $condition_bid  = $_POST['condition_bid'];
        $period_bid  = $_POST['period_bid'];
        $member_audit1 = $_POST['member_audit1'];
        $member_audit2 = $_POST['member_audit2'];
        $member_audit3 = $_POST['member_audit3'];
        $member_audit4 = $_POST['member_audit4'];
        $devide_bid = $_POST['devide_bid'];
        $price_asset = $_POST['price_asset'];
        $price_ma = $_POST['price_ma'];
        $status_bid = $_POST['status_bid'];
        $no_contract = $_POST['no_contract'];

        $result = $this->bid_model->model_edit_bid($no_bid,$date_bid,$year_bid,$subject_bid,$type_bid,$tri_bid,$cost_bid,$date_audit,$issues_bid,$condition_bid,$period_bid,$member_audit1,$member_audit2,$member_audit3,$member_audit4,$devide_bid,$price_asset,$price_ma,$status_bid,$no_contract);

        if($result=="success"){
            echo "แก้ไขการสังเกตุการณ์ สำเร็จ";
        }else{
            echo "แก้ไขสังเกตุการณ์ ไม่สำเร็จ".$result ;
        }
    }

    public function show_num_bid(){
       $result = $this->bid_model->model_show_num_bid();
       echo $result." รายการ";
    }



    public function show_price_all_bid(){
        $result = $this->bid_model->model_show_price_all_bid();
        echo number_format($result)." บาท";
    }

    public function show_per_status_bid(){
        echo $result = $this->bid_model->model_show_per_status_bid();
    }

    public function show_area_chart(){
        $year = $_POST['year'];
        echo $result = $this->bid_model->model_show_area_chart($year);
    }

    public function show_bar_chart(){
        $year = $_POST['year'];
       echo $result = $this->bid_model->model_show_bar_chart($year);
    }

    public function check_no_bid(){
        $no_bid = $_POST['no_bid'];
        if(isset($_POST['year'])){
            $year = $_POST['year'];
        }else{
            $year = "";
        }
        echo $result = $this->bid_model->model_check_no_bid($no_bid,$year);
    }

    public function add_sub_bid(){
        $name_sub = $_POST['name_sub'];
        $cost_sub = $_POST['cost_sub'];
        $issues_sub = $_POST['issues_sub'];
        $condition_sub = $_POST['condition_sub'];
        $status_sub = $_POST['status_sub'];
        $devide_sub = $_POST['devide_sub'];
        $asset_sub = $_POST['asset_sub'];
        $ma_sub = $_POST['ma_sub'];
        $no_bid = $_POST['no_bid'];

         $result = $this->bid_model->model_add_sub_bid($name_sub,$cost_sub,$issues_sub,$condition_sub,$status_sub,$devide_sub,$asset_sub,$ma_sub,$no_bid);
         if($result=="successful"){
             echo "เพิ่มสำเร็จ";
         }else{
             echo "เพิ่มไม่สำเร็จ";
         }
    }

    public function show_sub_bid(){

        $no_bid = $_POST['no_bid'];

        $result = $this->bid_model->model_show_sub_bid_by_no_bid($no_bid);
        if($result){
            foreach ($result as $row) {
                $cost_sub = $row->cost_sub_bid;
                $no_sub_bid = $row->no_sub_bid;

                echo '
                    <div class="card shadow mb-4">
                            <div class="card-header py-2">
                            <div class="row">
                                <div class="col-sm-11"><h6 class="m-0 font-weight-bold text-black-50">'.$row->no_sub_bid .' '.$row->name_sub_bid.'<input id="id_sub_bid" value="'.$row->id_sub_bid.'" hidden> </h6></div>
                                <div class="col-sm-1 align-content-sm-end"><a href=\'#\' title=\'ลบ\' onclick=\'delete_sub_bid()\' ><i class=\'fas fa-trash\'></i></a></div>
                                 
                            </div>
                            </div>
                            <div class="card-body">
                                <div  align="center">
                                    <table class="table-responsive" width="100%">
                                        <tr>
                                            <td align="right" width="40%">ราคากลาง : </td>
                                            <td width="60%">'.number_format($cost_sub, 2).' บาท</td>
                                        </tr>
                                        <tr>
                                            <td align="right"  valign="top">ประเด็นที่การจัดซื้อจัดจ้างพัสดุ : </td>
                                            <td  valign="top">'.$row->issues_sub_bid.'</td>
                                        </tr>
                                        <tr>
                                            <td align="right"  valign="top">ข้อตกลงเพิ่มเติม : </td>
                                            <td  valign="top">'.$row->condition_sub_bid.'</td>
                                        </tr>
                                        <tr>
                                            <td align="right">สถานะ : </td>
                                            <td>'.$row->status_sub_bid.'</td>
                                        </tr>
                                        <tr>
                                            <td align="right">แยกตามค่าอุปกรณื และ MA : </td>
                                            <td>'.$row->devide_sub_bid.'</td>
                                        </tr>
                                    </table>
                                </div>
                                <br>
                                <div class="col-12">
                                    <label for="vender" >บริษัท ที่ยื่นซอง'.$row->name_sub_bid.'</label>
                                    <button id="button_vender" class="btn btn-primary" data-toggle="modal" onclick="show_modal_vender('.$no_sub_bid.')">เพิ่มรายละเอียดการซื้อซอง</button>
                                    <table class=\'table table-bordered\'>
                                             <thead >
                                                <tr >
                                                  <th >บริษัท</th>
                                                  <th>สถานะ</th>
                                                  <th >ราคาที่ยืน</th>
                                                  <th >ราคาหลังการต่อรอง</th>
                                                  <th >ลดจากราคากลาง</th>
                                                  <th >ลดลง</th>
                                                  <th ></th>
                                                </tr>
                                             </thead>
                                             <tbody >
                                    
                                    
                                    ';

                $result_vender = $this->bid_model->model_show_vender_by_no_sub_bid($no_bid,$no_sub_bid);

                if($result_vender){
                    foreach ($result_vender as $vender){
                        $price_vender = $vender->price_vender;
                        $price = $cost_sub-$price_vender;
                        $per = ($price*100)/$cost_sub;
                        $no_vender = $vender->id_vender;
                        if($vender->win_vender==1){
                            $status = "ชนะการประกวดราคา";
                        }elseif ($vender->sender_vender==1&&$vender->price_vender==0&&$vender->pass_vender==0){
                            $status = "ไม่ผ่านคุณสมบัติ";
                        }elseif ($vender->sender_vender==1&&$vender->price_vender!=0){
                            $status = "ยื่นซอง";
                        }elseif($vender->sender_vender==0){
                            $status = "ไม่ได้ยื่นซอง";
                        }

                        echo "
                                 <tr >
                                   <td >".$vender->name_vender."</td>
                                   <td>$status</td>
                                   <td >".number_format($vender->price_vender, 2)."</td>
                                   <td >".number_format($vender->price_under_vender, 2)."</td>
                                   <td >".number_format($price, 2)."</td>
                                   <td >".number_format($per, 2)."%</td>
                                   <td ><a href='#' title='ลบ' onclick='delete_vender_ajax($no_vender)' class='btn btn-danger '><i class='fas fa-trash'></i></a></td>
                                 </tr>
                        ";
                    }
                }else{
                    echo "
                                 <tr >
                                   <td colspan='7'>ไม่พบบริษัท</td>
                                 </tr>
                    ";
                }

                echo '
                                                </tbody>
                                          </table>
                                </div>
                                <!-- Divider -->
                                <br>
                                <hr class="sidebar-divider my-0">
                                <br>
                                <!-- END Divider -->
                            </div>
                        </div>            
                ';
            }
        }else{
            echo "ไม่สามารถเพิ่มได้";
        }
    }

    public function delete_sub_bid(){
        $id_sub_bid = $_POST['id_sub_bid'];

         $result = $this->bid_model->model_delete_sub_bid($id_sub_bid);
         echo $result['delete_vender']."<br>".$result['delete_sub_bid'];
    }



}