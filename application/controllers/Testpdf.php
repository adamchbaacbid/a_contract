<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Name:  Testpdf
*
* Version: 1.0.0
*
* Author: Pedro Ruiz Hidalgo
*		  ruizhidalgopedro@gmail.com
*         @pedroruizhidalg
*
* Location: application/controllers/Testpdf.php
*
* Created:  208-02-27
*
* Description:  This demonstrates pdf library is working.
*
* Requirements: PHP5 or above
*
*/


class Testpdf extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->add_package_path( APPPATH . 'third_party/fpdf');
        $this->load->library('pdf');
        $this->load->model('audit_model');
    }

	public function index()
	{
        $this->pdf = new Pdf();

        $this->pdf->Add_Page('P','A4',0);
        $this->pdf->AddFont('THSarabun','','THSarabun.php');
        $this->pdf->SetFont('THSarabun','',16);
       // $this->pdf->AliasNbPages();

        
        $this->pdf->Output( 'page.pdf' , 'I' );
	}


	public function audit_report(){

        $result = $this->audit_model->model_info_audit_by_no_bid('2562001');
        $audit = $result['0'];



        $text = iconv('UTF-8','cp874', "ข้อความทดสอบ");


        $this->pdf = new Pdf();
        $this->pdf->Add_Page('P','A4',0);
        $this->pdf->SetMargins(22, 10,15);
        $this->pdf->SetAutoPageBreak(true, 10);

        $this->pdf->AddFont('angsana','','angsa.php');//
        $this->pdf->AddFont('THSarabunNew','','THSarabunNew.php');//
        $this->pdf->AddFont('angsana','b','angsab.php');//
        $this->pdf->AddFont('THSarabunNew','b','THSarabunNew Bold.php');//



        $this->pdf->Ln(8);
        $this->pdf->SetFont('THSarabunNew','b',16);
        $this->pdf->Cell(0,-30,iconv('UTF-8','cp874', "บันทึก"),0,1,'C');


        $this->pdf->Ln(25);
        $this->pdf->SetFont('THSarabunNew','b',16);
        $this->pdf->Cell(0,-5,iconv('UTF-8','cp874', "ส่วนงาน "));
        $this->pdf->Cell(-155);
        $this->pdf->SetFont('THSarabunNew','',16);
        $this->pdf->Cell(0,-5,iconv('UTF-8','cp874', "กลุ่มงานตรวจสอบการบริหารงานเทคโนโลยีและสารสนเทศ  สำนักตรวจสอบเทคโนโลยีและสารสนเทศ"));

        $this->pdf->Ln(5);
        $this->pdf->Cell(0,0,iconv('UTF-8','cp874', "โทร 4933 , 4940"));

        $this->pdf->Ln(8);
        $this->pdf->SetFont('THSarabunNew','b',16);
        $this->pdf->Cell(0,0,iconv('UTF-8','cp874', "ที่  "));
        $this->pdf->Cell(-160);
        $this->pdf->SetFont('THSarabunNew','',16);
        $this->pdf->Cell(0,0,iconv('UTF-8','cp874', "สตท/"));
        $this->pdf->Cell(-100);
        $this->pdf->Cell(0,0,iconv('UTF-8','cp874', "วันที่ "));

        $this->pdf->Ln(5);
        $this->pdf->SetFont('THSarabunNew','b',16);
        $this->pdf->MultiCell(0,5,iconv('UTF-8','cp874', "เรื่อง"));
        $this->pdf->Ln(-5);
        //$this->pdf->Cell(12);
        $this->pdf->SetXY(10,202);
        $this->pdf->SetFont('THSarabunNew','',16);
        $this->pdf->MultiCell(0,5,iconv('UTF-8','cp874', "รายงานผลการสังเกตการณ์การ".$audit->name_bid));


        $this->pdf->Line(22,68,192,68);



        $this->pdf->Ln(15);
        $this->pdf->SetFont('THSarabunNew','',16);
        $this->pdf->MultiCell(0,5,iconv('UTF-8','cp874', "รายงานผลการสังเกตการณ์การ".$audit->name_bid));



        $this->pdf->Ln(15);
        $this->pdf->SetFont('THSarabunNew','',16);
        $this->pdf->MultiCell(0,5,iconv('UTF-8','cp874', "รายงานผลการสังเกตการณ์การ".$audit->name_bid),1);


        $this->pdf->Output();

	}









}

/*
* application/controllers/Testpdf.php
*/
