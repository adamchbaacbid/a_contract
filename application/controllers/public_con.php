<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class public_con extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('public_model');
        $this->load->model('function_model');
    }



    public function index(){
        $this->load->view('from_public/head');
        $this->load->view('from_public/index');
        $this->load->view('from_public/footer');
    }

    public function admin(){
        if(!isset($_POST['branch'])){
            $id_branch = "";
            $id_procurement_type = "";
            $data['data'] = $this->public_model-> model_show_car_rental($id_branch,$id_procurement_type);
            if($data['data']==""){
                echo "<script>alert('ไม่พบข้อมูลในฐานข้อมูล');window.history.back();</script>";
                $this->load->view('from_public/head');
                $this->load->view('from_public/car_rental/add_show_car_rental');
                $this->load->view('from_public/footer');
            }else{
                $this->load->view('from_public/head');
                $this->load->view('from_public/admin/index',$data);
                $this->load->view('from_public/footer');
            }

        }else{
            $id_branch = $_POST['branch'];
            $id_procurement_type = $_POST['id_procurement_type'];
            $data['data'] = $this->public_model-> model_show_car_rental($id_branch,$id_procurement_type);

            if($data['data']==""){
                echo "<script>alert('ไม่พบข้อมูลในฐานข้อมูล');window.history.back();</script>";
            }else{
                $this->load->view('from_public/head');
                $this->load->view('from_public/admin/index',$data);
                $this->load->view('from_public/footer');
            }
        }
    }

    public function inventory(){
        $this->load->view('from_public/head');
        $this->load->view('from_public/inventory');
        $this->load->view('from_public/footer');
    }

    public function general(){
        $this->load->view('from_public/head');
        $this->load->view('from_public/general');
        $this->load->view('from_public/footer');
    }

    public function show_car_rental(){

        if(!isset($_POST['branch'])){
            $id_branch = "";
            $id_procurement_type = "";
            $data['data'] = $this->public_model-> model_show_car_rental($id_branch,$id_procurement_type);
            if($data['data']==""){
                echo "<script>alert('ไม่พบข้อมูลในฐานข้อมูล');window.history.back();</script>";
                $this->load->view('from_public/head');
                $this->load->view('from_public/car_rental/add_show_car_rental');
                $this->load->view('from_public/footer');
            }else{
                $this->load->view('from_public/head');
                $this->load->view('from_public/car_rental/show_car_rental',$data);
                $this->load->view('from_public/footer');
            }

        }else{
            $id_branch = $_POST['branch'];
            $id_procurement_type = $_POST['id_procurement_type'];
            $data['data'] = $this->public_model-> model_show_car_rental($id_branch,$id_procurement_type);

            if($data['data']==""){
                echo "<script>alert('ไม่พบข้อมูลในฐานข้อมูล');window.history.back();</script>";
            }else{
                $this->load->view('from_public/head');
                $this->load->view('from_public/car_rental/show_car_rental',$data);
                $this->load->view('from_public/footer');
            }
        }
    }

    public function show_atc(){
        if(!isset($_POST['branch'])){
            $id_branch = "";
            $id_procurement_type = "";
            $data['data'] = $this->public_model->model_show_atc($id_branch,$id_procurement_type);
            if($data['data']==""){
                echo "<script>alert('ไม่พบข้อมูลในฐานข้อมูล');</script>";
                $this->load->view('from_public/head');
                $this->load->view('from_public/atc/add_atc');
                $this->load->view('from_public/footer');
            }else{
                $this->load->view('from_public/head');
                $this->load->view('from_public/atc/show_atc',$data);
                $this->load->view('from_public/footer');
            }
        }else{
            $id_branch = $_POST['branch'];
            $id_procurement_type = $_POST['id_procurement_type'];
            $data['data'] = $this->public_model->model_show_atc($id_branch,$id_procurement_type);

            if($data['data']==""){
                echo "<script>alert('ไม่พบข้อมูลในฐานข้อมูล');window.history.back();</script>";
            }else{
                $this->load->view('from_public/head');
                $this->load->view('from_public/atc/show_atc',$data);
                $this->load->view('from_public/footer');
            }
        }
    }

    public function info_procurement(){

        if(!isset($_POST['id_branch'])){
            echo "<script>alert('ไม่สามารถแสดงข้อมูลได้');window.history.back();</script>";
        }else{
            $data['id_branch'] = $_POST['id_branch'];
            $data['id_procurement_type'] = $_POST['id_procurement_type'];
            $data['year_procurement'] = $_POST['year_procurement'];
            $data['time_procurement'] = $_POST['time_procurement'];

            //$data = $this->public_model->model_info_procurement($id_branch,$id_procurement_type,$year_procurement,$time_procurement);
            $this->load->view('from_public/head');
            $this->load->view('from_public/car_rental/info_procurement',$data);
            $this->load->view('from_public/footer');
        }
    }

    public function info_procurement1(){

        if(!isset($_POST['id_branch'])){
            echo "<script>alert('ไม่สามารถแสดงข้อมูลได้');window.history.back();</script>";
        }else{
            $name_branch = $_POST['id_branch'];

            $data['id_branch'] = $this->public_model->model_name_to_id_branch($name_branch);

            $data['id_procurement_type'] = $_POST['id_procurement_type'];
            $data['year_procurement'] = $_POST['year_procurement'];
            $data['time_procurement'] = $_POST['time_procurement'];

            //$data = $this->public_model->model_info_procurement($id_branch,$id_procurement_type,$year_procurement,$time_procurement);
            $this->load->view('from_public/head');
            $this->load->view('from_public/car_rental/info_procurement',$data);
            $this->load->view('from_public/footer');
        }
    }

    public function info_procurement_atc(){

        if(!isset($_POST['id_branch'])){
            echo "<script>alert('ไม่สามารถแสดงข้อมูลได้');window.history.back();</script>";
        }else{
            $data['id_branch'] = $_POST['id_branch'];
            $data['id_procurement_type'] = $_POST['id_procurement_type'];
            $data['year_procurement'] = $_POST['year_procurement'];
            $data['time_procurement'] = $_POST['time_procurement'];

            $this->load->view('from_public/head');
            $this->load->view('from_public/atc/info_procurement_atc',$data);
            $this->load->view('from_public/footer');
        }
    }

    public function info_procurement_atc1(){

        if(!isset($_POST['id_branch'])){
            echo "<script>alert('ไม่สามารถแสดงข้อมูลได้');window.history.back();</script>";
        }else{
            $name_branch = $_POST['id_branch'];
            $data['id_branch'] = $this->public_model->model_name_to_id_branch($name_branch);

            $data['id_procurement_type'] = $_POST['id_procurement_type'];
            $data['year_procurement'] = $_POST['year_procurement'];
            $data['time_procurement'] = $_POST['time_procurement'];

            $this->load->view('from_public/head');
            $this->load->view('from_public/atc/info_procurement_atc',$data);
            $this->load->view('from_public/footer');
        }
    }










    public function car_rental(){
        $this->load->view('from_public/head');
        $this->load->view('from_public/car_rental');
        $this->load->view('from_public/footer');
    }

    public function add_car_rental(){
        $this->load->view('from_public/head');
        $this->load->view('from_public/car_rental/add_car_rental');
        $this->load->view('from_public/footer');
    }

    public function add_atc(){
        $this->load->view('from_public/head');
        $this->load->view('from_public/atc/add_atc');
        $this->load->view('from_public/footer');
    }










    public function ajax_add_car_rental_step1(){
        if(!isset($_POST['branch'])){
            echo 'ไม่พบสาขา ไม่สามารบันทึกได้';
        }else{
            $branch = $_POST['branch'];
            $select_step1 = $_POST['select_step1'];
            $description_step1 = $_POST['description_step1'];
            $name_employee_step1 = $_POST['name_employee_step1'];
            $year_procurement = $_POST['year_procurement'];
            $time_procurement = $_POST['time_procurement'];
            $result = $this->public_model->model_ajax_add_car_rental_step1($branch,$select_step1,$description_step1,$name_employee_step1,$year_procurement,$time_procurement);
            echo $result ;
        }
    }

    public function ajax_add_car_rental_step2(){
        if(!isset($_POST['branch'])){
            echo 'ไม่พบสาขา ไม่สามารบันทึกได้';
        }else{
            $branch = $_POST['branch'];
            $select_step2 = $_POST['select_step2'];
            $description_step2 = $_POST['description_step2'];
            $name_employee_step2 = $_POST['name_employee_step2'];
            $year_procurement = $_POST['year_procurement'];
            $time_procurement = $_POST['time_procurement'];
            $result = $this->public_model->model_ajax_add_car_rental_step2($branch,$select_step2,$description_step2,$name_employee_step2,$year_procurement,$time_procurement);
            echo $result ;
        }
    }

    public function ajax_add_car_rental_step(){
        if(!isset($_POST['branch'])){
            echo 'ไม่พบสาขา ไม่สามารบันทึกได้';
        }else{
            $branch = $_POST['branch'];
            $description_step = $_POST['description_step'];
            $name_employee_step = $_POST['name_employee_step'];
            $year_procurement = $_POST['year_procurement'];
            $time_procurement = $_POST['time_procurement'];
            $step = $_POST['step'];
            $result = $this->public_model->model_ajax_add_car_rental_step($branch,$description_step,$name_employee_step,$year_procurement,$time_procurement,$step);
            echo $result ;
        }
    }

    public function ajax_add_atc_step1(){
        if(!isset($_POST['branch'])){
            echo 'ไม่พบสาขา ไม่สามารบันทึกได้';
        }else{
            $branch = $_POST['branch'];
            $year_procurement = $_POST['year_procurement'];
            $time_procurement = $_POST['time_procurement'];
            $result = $this->public_model->model_ajax_add_atc_step1($branch,$year_procurement,$time_procurement);
            echo $result ;
        }
    }

    public function ajax_add_atc_step2(){
        if(!isset($_POST['branch'])){
            echo 'ไม่พบสาขา ไม่สามารบันทึกได้';
        }else{
            $branch = $_POST['branch'];
            $select_step2 = $_POST['select_step2'];
            $description_step2 = $_POST['description_step2'];
            $name_employee_step2 = $_POST['name_employee_step2'];
            $year_procurement = $_POST['year_procurement'];
            $time_procurement = $_POST['time_procurement'];
            $result = $this->public_model->model_ajax_add_atc_step2($branch,$select_step2,$description_step2,$name_employee_step2,$year_procurement,$time_procurement);
            echo $result ;
        }
    }

    public function ajax_add_atc_step3(){
        if(!isset($_POST['branch'])){
            echo 'ไม่พบสาขา ไม่สามารบันทึกได้';
        }else{
            $branch = $_POST['branch'];
            $select_step2 = $_POST['select_step3'];
            $description_step2 = $_POST['description_step3'];
            $name_employee_step2 = $_POST['name_employee_step3'];
            $year_procurement = $_POST['year_procurement'];
            $time_procurement = $_POST['time_procurement'];
            $result = $this->public_model->model_ajax_add_atc_step3($branch,$select_step2,$description_step2,$name_employee_step2,$year_procurement,$time_procurement);
            echo $result ;
        }
    }

    public function ajax_add_atc_step(){
        if(!isset($_POST['branch'])){
            echo 'ไม่พบสาขา ไม่สามารบันทึกได้';
        }else{
            $branch = $_POST['branch'];
            $description_step = $_POST['description_step'];
            $name_employee_step = $_POST['name_employee_step'];
            $year_procurement = $_POST['year_procurement'];
            $time_procurement = $_POST['time_procurement'];
            $step = $_POST['step'];
            $result = $this->public_model->model_ajax_add_atc_step($branch,$description_step,$name_employee_step,$year_procurement,$time_procurement,$step);
            echo $result ;
        }
    }










    public function show_name_branch(){
        $result = $this->public_model->model_show_name_branch();
        echo $result;
    }

    public function check_name_branch(){
        $name_branch = $_POST['branch'];
        $result = $this->public_model->model_check_name_branch($name_branch);
        echo $result ;
    }

    public function ajax_show_info_procurement(){
        $id_branch = $_POST['id_branch'];
        $id_procurement_type = $_POST['id_procurement_type'];
        $year_procurement = $_POST['year_procurement'];
        $time_procurement = $_POST['time_procurement'];

        $result = $this->public_model->model_ajax_show_info_procurement($id_branch,$id_procurement_type,$year_procurement,$time_procurement);
        echo $result ;

    }



    public function num_branch_car_rental(){
        $result = $this->public_model->model_num_branch_car_rental();
        echo $result ;
    }

    public function num_branch_no_car_rental(){
        $result = $this->public_model->model_num_branch_no_car_rental();
        echo $result ;
    }

    public function branch_no_car_rental(){
        $result = $this->public_model->model_branch_no_car_rental();
        echo $result ;
    }










}
