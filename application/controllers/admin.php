<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('admin_model');
        $this->load->model('member_model');
        $this->load->model('function_model');
        $this->load->model('login_model');
        $this->login_model->check_session_for_admin();
    }

    public function index(){
        $this->load->view('admin/head');
        $this->load->view('admin/index');
        $this->load->view('admin/footer');
    }

    public function add_member(){
        $this->load->view('admin/head');
        $this->load->view('admin/add_member');
        $this->load->view('admin/footer');
    }

    public function show_all_member(){
        $data['member']= $this->admin_model->show_all_member_model();
        $this->load->view('admin/head');
        $this->load->view('admin/show_member',$data);
        $this->load->view('admin/footer');
    }

    public function edit_profile(){
        $data['member']=$this->admin_model->show_profile();

        $this->load->view('admin/head');
        $this->load->view('admin/edit_profile',$data);
        $this->load->view('admin/footer');
    }

    public function edit_password(){
        $this->load->view('admin/head');
        $this->load->view('admin/edit_password');
        $this->load->view('admin/footer');
    }

    public function show_log(){
        $data['log'] = $this->admin_model->show_log_model();
        $this->load->view('admin/head');
        $this->load->view('admin/show_log',$data);
        $this->load->view('admin/footer');
    }

    public function password_policy(){
        $data['policy'] = $this->admin_model->password_policy_model();
        $this->load->view('admin/head');
        $this->load->view('admin/password_policy',$data);
        $this->load->view('admin/footer');
    } 
    
    public function delete_member(){
        $username = $_POST['username'];
        $result = $this->member_model->model_delete_member($username);
        echo $result ; 
    }





}