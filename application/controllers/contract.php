<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class contract extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('contract_model');
        $this->load->model('function_model');
        $this->load->model('login_model');
        $this->login_model->check_session_for_contract();
    }

    public function index(){
        $this->load->view('contract/head');
        $this->load->view('contract/index');
        $this->load->view('contract/footer');
    }

    public function add_contract(){
        $result = $this->contract_model->model_show_bid_for_add_contract();
        $data['bid'] = $result;
        $this->load->view('contract/head');
        $this->load->view('contract/add_contract',$data);
        $this->load->view('contract/footer');
    }

    public function edit_profile(){
        $data['member']=$this->contract_model->show_profile();

        $this->load->view('contract/head');
        $this->load->view('contract/edit_profile',$data);
        $this->load->view('contract/footer');
    }

    public function edit_password()
    {
        $this->load->view('contract/head');
        $this->load->view('contract/edit_password');
        $this->load->view('contract/footer');
    }



    public function add_contract2(){
        if(!isset($_POST['info_no_bid'])){
            echo "<script>alert('ไม่พบเลขที่จัดซื้อจัดจ้าง ไม่สามารถเข้าดูข้อมูลได้');window.history.back();</script>";
        }else{

            $no_bid = $_POST['info_no_bid'];
            $data['bid'] = $this->contract_model->model_show_info_bid_byno_bid($no_bid);

            $this->load->view('contract/head');
            $this->load->view('contract/add_contract2',$data);
            $this->load->view('contract/footer');
        }
    }

    public function show_contract(){
        $result = $this->contract_model->model_show_contract();
        $data['info_contract'] = $result['contract'];
        $data['num_contract'] = $result['num'];

        $this->load->view('contract/head');
        $this->load->view('contract/show_contract',$data);
        $this->load->view('contract/footer');
    }

    public function show_contract_by_end_contract(){
        $result = $this->contract_model->model_show_contract_by_dif_date(30,"");
        $data['info_contract'] = $result['contract'];
        $data['num_contract'] = $result['num'];


        $this->load->view('contract/head');
        $this->load->view('contract/show_contract_by_end_contract',$data);
        $this->load->view('contract/footer');
    }


    public function info_contract(){
        if(!isset($_POST['info_no_bid'])){
            echo "<script>alert('ไม่สามารถแสดงรายะเอียดสัญญาได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['info_no_bid'];
            $result = $this->contract_model->model_info_contract_by_no_bid($no_bid);
            $data['contract'] = $result;

            $this->load->view('contract/head');
            $this->load->view('contract/info_contract',$data);
            $this->load->view('contract/footer');

        }
    }

    public function edit_contract(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแก้ไขได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $result = $this->contract_model->model_info_contract_by_no_bid($no_bid);
            $data['contract'] = $result;


            $this->load->view('contract/head');
            $this->load->view('contract/edit_contract',$data);
            $this->load->view('contract/footer');
        }
    }




    public function add_contract_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถเพิ่มการสังเกตการณ์');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $name_contract = $_POST['name_contract'];
            $period_contract = $_POST['period_contract'];
            $start_contract = $_POST['start_contract'];
            $end_contract = $_POST['end_contract'];
            $description_contract = $_POST['description_contract'];
            $member_contract = $_POST['member_contract'];

            $result = $this->contract_model->model_add_contract_ajax($no_bid,$name_contract,$period_contract,$start_contract,$end_contract,$description_contract,$member_contract);
            echo $result;
        }

    }

    public function edit_contract_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแก้ไขรายละเอียดสัญญาได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $name_contract = $_POST['name_contract'];
            $period_contract = $_POST['period_contract'];
            $start_contract = $_POST['start_contract'];
            $end_contract = $_POST['end_contract'];
            $description_contract = $_POST['description_contract'];


            $result = $this->contract_model->model_edit_contract_ajax($no_bid,$name_contract,$period_contract,$start_contract,$end_contract,$description_contract);
            echo $result;
        }
    }


    public function delete_contract(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถลบได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $result = $this->contract_model->model_delete_contract($no_bid);

            echo $result;
        }
    }

    public function show_attract_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแสดงเอกสารแนบได้');window.history.back();</script>";
        }else{

            $no_bid = $_POST['no_bid'];
            $result = $this->contract_model->model_show_attract_by_no_bid($no_bid);
            $i=1;

            echo "<table cellpadding='5' class='table col-6' >";
            echo "
            <tr>
                <td>ลำดับ</td>
                <td>ชื่อไฟล์</td>
                           
            </tr>
        ";

            foreach ($result as $row){
                $path_attract = $row->path_attract ;
                $url = base_url($path_attract);
                echo "
                <tr>
                    <td>$i</td>
                    <td><a href='$url' target='_blank'>$row->oldname_attract</a></td> 
                                   
                </tr>      
            ";
                $i++;
            }
            echo "</table>";
        }
    }

    public function show_vender_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแสดงบริษัท');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $data = $this->contract_model->model_show_vender_by_no_bid($no_bid);
            echo $data ;
        }
    }

    public function show_committee_bid_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแสดงคณะกรรมการประกวดราคา');window.history.back();</script>";
        }else{
            if(!isset($_POST['edit'])){
                $edit = "";
            }else{
                $edit = "style='display: none'";
            }
            $no_bid = $_POST['no_bid'];
            $result = $this->contract_model->model_show_committee_bid_by_no_bid($no_bid);

            $i=1;

            echo "<table cellpadding='5' class='table col-9' >";
            echo "
            <tr>
                <td align='center'>ลำดับ</td>
                <td>ชื่อ</td>
                <td>ตำแหน่ง</td>   
                <td align='center'>สถานะวันประกวดราคา</td>   
                <td $edit></td>      
            </tr>
        ";

            foreach ($result as $row){

                $id_committee_bid =$row->id_committee_bid ;
                $name_committee_bid = $row->name_committee_bid ;
                $position_committee_bid = $row->position_committee_bid;
                $status_committee_bid = $row->status_committee_bid;
                if($status_committee_bid==0){
                    $status_committee_bid = "ไม่มา";
                }else{
                    $status_committee_bid = "มา";
                }


                echo "
                <tr>
                    <td align='center'>$i</td>
                    <td>$name_committee_bid</td>
                    <td>$position_committee_bid</td>     
                    <td align='center'>$status_committee_bid</td>                           
                    <td $edit ><a href='#' id='delete_committee_bid' title='ลบ' onclick='delete_committee_bid_ajax($id_committee_bid , \"$name_committee_bid\")' class='btn btn-danger '>
                        <i class='fas fa-trash'></i></a>
                    </td>
                </tr>      
            ";
                $i++;
            }
            echo "</table>";
        }
    }


   




}