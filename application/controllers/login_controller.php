<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('login_model');
        $this->load->model('function_model');
    }

    public function login(){///////////////////////////////////////
        $this->login_model->check_session_for_login();
    }

    public function edit_profile(){
        $this->load->view('from/head');
        $this->load->view('member/edit_profile');
        $this->load->view('from/footer');
    }

    public function edit_password(){
        $this->load->view('from/head');
        $this->load->view('member/edit_password');
        $this->load->view('from/footer');
    }

    public function index(){//////////////////////////////
        $this->login_model->check_session_for_login();
        //$this->load->view('login/login');
    }

    public function audit(){////////////////////////////
        $this->login_model->check_session_for_audit();
        $this->load->view('audit/head');
        $this->load->view('audit/index');
        $this->load->view('audit/footer');
    }

    public function contract(){/////////////////////////
        $this->login_model->check_session_for_contract();
        //$this->check_contract();//alert end of contract
        $this->load->view('contract/head');
        $this->load->view('contract/index');
        $this->load->view('contract/footer');
    }

    public function purchase(){////////////////////////
        $this->login_model->check_session_for_purchase();
        $this->load->view('purchase/head');
        $this->load->view('purchase/index');
        $this->load->view('purchase/footer');
    }

    public function procurement(){////////////////////////
        $this->login_model->check_session_for_procurement();
        $this->load->view('procurement/head');
        $this->load->view('procurement/index');
        $this->load->view('procurement/footer');
    }

    public function admin(){//////////////////////////////
        $this->login_model->check_session_for_admin();
        $this->load->view('admin/head');
        $this->load->view('admin/index');
        $this->load->view('admin/footer');
    }

    function forgot_password(){
        //$this->load->view('login/head');
        $this->load->view('login/forgot_password');
        // $this->load->view('login/footer');
    }

    public function forgot_password_ajax(){
        if(!isset($_POST['email'])){
            $email =$_POST['email'];
            echo "<script>alert('ไม่สามารถแสดงข้อมูลได้ $email');window.history.back();</script>" ;
        }else{
            $email = $_POST['email'] ;
            $result = $this->login_model->model_forgot_password($email);
            if($result == "successful"){
                echo "<script>alert('ทำการ Reset password เรียบร้อยแล้ว และส่งรหัสชั่วคราวไปที่อีเมล์ของท่าน เรียบร้อยแล้ว');window.history.back();</script>" ;
            }else{
                echo "<script>alert(' Reset password ไม่สำเร็จ');</script>" ;
            }
        }
    }

    public function edit_member_ajax(){

        if(!isset($_POST['id_member'])){
          echo "<script>alert('ไม่สามารถแสดงข้อมูลได้');window.history.back();</script>" ;
        }else{
            $id_member = $_POST['id_member'];
            $name_member = $_POST['name_member'];
            $position_member = $_POST['position_member'];
            $username = $_POST['username'];
            $agency_member = $_POST['agency_member'];
            $email_member = $_POST['email_member'];
            $tell_member = $_POST['tell_member'];

            $result = $this->login_model->model_edit_member_ajax($id_member,$name_member,$position_member,$username,$agency_member,$email_member,$tell_member);
            echo $result;
        }

    }



    public function do_login(){
        if(!isset($_POST['username'])||!isset($_POST['pass'])){
            echo "<script>alert('ไม่สามารถเข้าสู่ระบบได้');window.history.back();</script>";
        }else{
            $user = $_POST['username'];
            $pass = $_POST['pass'];

            if($user==""){

                echo "<script>alert('กรุณาใส่ชื่อผู้ใช้ ในการเข้าสู่ระบบ');</script>";
                $this->login();
            }elseif($pass==""){
                echo "<script>alert('กรุณาใส่รหัสผ่าน ในการเข้าสู่ระบบ');</script>";
                $this->login();
            }else{
                $result = $this->login_model->model_check_login($user,$pass);
                if($result['msg']=="pass"){  //check user login pass
                    $this->login_model->check_session_for_login();
                }elseif($result['msg']=="not"){
                    $msg = $result['result'] ;
                    echo "<script>alert('$msg');</script>";
                    $this->login();
                }elseif($result['msg']=="change_pass"){
                    $data['user_member'] = $user ;
                    $this->load->view('login/change_pass',$data);
                }
            }
        }


    }

    public function logout(){///////////////////////////////////////////

        $logout = $this->login_model->model_logout();
        if($logout == "Logout complete"){
            redirect(base_url());
        }else if($logout == "Logout Fail"){
            echo "<script>alert('ไม่สามารถ ออกจากระบบได้');</script>";
            redirect(base_url());
        }
    }

    public function check_old_password_ajax(){
        if(!isset($_POST['pass_member_old'])){
            echo "<script>alert('ไม่สามรถตรวจสอบได้');window.history.back();</script>";
        }else{
            //echo "pass";
            $pass = $_POST['pass_member_old'];
            $result = $this->login_model->model_pass_member_old($pass);
            echo $result;
        }
    }

    public function edit_password_ajax(){
        if(!isset($_POST['pass_member_new'])){
            echo "<script>alert('ไม่สามารถเปลี่ยนรหัสผ่านได้');window.history.back();</script>";
        }else{
            $pass_member_new = $_POST['pass_member_new'];
            $data = $this->session->userdata('data');
            $user_member = $data['member'];
            $result = $this->login_model->model_edit_password($pass_member_new,$user_member);
            echo $result ;
        }

    }

    function first_chang_pass(){
        if(!isset($_POST['new_password']) AND !isset($_POST['user_member'])){
            echo "<script>alert('ไม่สามารถเปลี่ยนรหัสผ่านได้');window.history.back();</script>";
        }else{
            $pass_member_new = $_POST['new_password'];
            $user_member = $_POST['user_member'];
            $result = $this->login_model->model_edit_password($pass_member_new,$user_member);

            if($result=='เปลี่ยนพาสเวิดเรียบร้อยแล้ว กรุณาเข้าสู่ระบบอีกครั้ง'){
                echo "<script>alert('$result');</script>";
                $active = $this->login_model->active_member($user_member);
                if($active=='ยืนยันตัวตนสำเร็จ'){
                    echo "<script>alert('$active');</script>";
                    $this->login();
                    

                }
            }
        }
    }

    

    /*
    public function check_contract(){
        $result = $this->login_model->model_check_contract();
        $y=0;
        $m6=0;
        $m3=0;
        $end=0;
         foreach ($result as $row){
             $dif = $row->dif;
             $period = $row->period_bid;
             if($dif<='12'&&$dif>'6'){
                 $y++;
             }elseif ($dif<='6'&&$dif>'3'){
                 $m6++;
             }elseif ($dif<='3'&&$dif>'0'){
                 $m3++;
             }elseif ($dif<='0'&&$period!='0000-00-00'){
                 $end++;
             }
         }

         if($y!='0'){
             echo "<script>alert('มีโครงการที่จะหมดสัญญา ภายใน 1ปี $y โครงการ');</script>";
         }
         if ($m6!='0'){
             echo "<script>alert('มีโครงการที่จะหมดสัญญา ภายใน 6เดือน $m6 โครงการ');</script>";
         }
         if ($m3!='0'){
             echo "<script>alert('มีโครงการที่จะหมดสัญญา ภายใน 3เดือน $m3 โครงการ');</script>";
         }
         if($end!='0'){
             echo "<script>alert('มีโครงการที่หมดสัญญา  $end โครงการ');</script>";
         }
    }
    */




}