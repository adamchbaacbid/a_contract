<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');
class PHPWord_Text extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('word');
        $this->load->model('phpword_model');
        $this->load->model('function_model');
    }



    function index() {
        $no_bid = "2562011";


        $data = $this->phpword_model-> model_info_audit_by_no_bid($no_bid);
        $audit = $data['0'];

        $vender_info = $this->phpword_model->model_show_vender_by_no_bid($no_bid);
        $vender = json_decode($vender_info);

        $PHPWord = $this->word; // New Word Document
        $section = $PHPWord->createSection(); // New portrait section

        $PHPWord->setDefaultFontName('TH SarabunPSK');
        $PHPWord->setDefaultFontSize(16);

        /*
        // Ads styles tab
        $PHPWord->addParagraphStyle(
            'multipleTab',
            array(
                'tabs' => array(
                    new \PhpOffice\PhpWord\Style\Tab('left', 1550),
                    new \PhpOffice\PhpWord\Style\Tab('center', 3200),
                    new \PhpOffice\PhpWord\Style\Tab('right', 5300),
                )
            )
        );

        $PHPWord->addParagraphStyle(
            'rightTab',
            array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('right', 9090)))
        );

        $PHPWord->addParagraphStyle(
            'centerTab',
            array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('center', 4680)))
        );
        */

       /* // Add header
        $header = $section->createHeader();
        $table = $header->addTable();
        $table->addRow();
        $table->addCell(4500)->addText('');
        $table->addCell(4500)->addText('');
        $table->addCell(4500)->addText('This is the header.',array('bold'=>true, 'size'=>16 ,'name'=>'TH SarabunPSK'),array('align'=>'right'));
        */

        // Add text elements
        $PHPWord->addFontStyle('F_title', array('bold'=>true, 'size'=>16 ,'name'=>'TH SarabunPSK'));
        $PHPWord->addParagraphStyle('P_title', array('align'=>'center'));
        $section->addText('บันทึก', 'F_title', 'P_title');

        $textrun = $section->createTextRun();
        $textrun->addText('ส่วนงาน  ' ,array('name'=>'TH SarabunPSK','size'=>16 ,'bold'=>true ));
        $textrun->addText('กลุ่มงานตรวจสอบงานบริหารเทคโนโลยีและสารสนเทศ  สำนักตรวจสอบเทคโนโลยีและสารสนเทศ' ,array('name'=>'TH SarabunPSK','size'=>16  ));

        $textrun2 = $section->createTextRun();
        $textrun2->addText('โทร.  4933, 4940');

        $textrun2 = $section->createTextRun();
        $textrun2->addText('ที่   ', 'F_title');
        $textrun2->addText('    สตท /');

        $textrun2 = $section->createTextRun();
        $textrun2->addText('เรื่อง   ', 'F_title');
        $textrun2->addText('การสังเกตการณ์การ'.$audit->name_bid);

        $textrun2 = $section->createTextRun();
        $textrun2->addText('--------------------------------------------------------------------------------------------------------------------------------');

        $textrun2 = $section->createTextRun();
        $textrun2->addText('เรียน   ');
        $textrun2->addText(' ผอ.สตท. ผ่าน ผช.สตท. (นางสิริพร เริงสมัย');



        $date_announce_bid = $this->function_model->long_datethai($audit->date_announce_bid);

        $textrun2 = $section->createTextRun();
        $textrun2->addText('           ตามที่ สตท. มอบหมายให้ข้าพเจ้า '.$audit->name_member.' ตำแหน่ง'.$audit->position_member.' ได้เข้าสังเกตการณ์ประกวดราคาตามบันทึกที่ '.$audit->no_announce_bid.' ลงวันที่ '.$date_announce_bid.' เรื่องการ'.$audit->name_bid.' โดยมีรายละเอียด ดังนี้');

        $textrun2 = $section->createTextRun();
        $textrun2->addText('   ', 'F_title');
        $textrun2->addText('          1. การยื่นซอง/เปิดซอง ข้อเสนอทางด้านเทคนิค', 'F_title');

        $date_audit = $this->function_model->long_datethai($audit->date_audit);

        $textrun2 = $section->createTextRun();
        $textrun2->addText('                   ข้าพเจ้าฯ ได้เข้าสังเกตการณ์การพิจารณาผลการประกวดราคาอิเล็กทรอนิกส์ เมื่อวันที่ '.$date_audit.' เวลา 10.00 น. ณ '.$audit->place_audit.' สรุปผล ดังนี้ ');


        $textrun2 = $section->createTextRun();
        $textrun2->addText('                        1.1	แต่งตั้งคณะกรรมการประกวดราคาตามคำสั่งธนาคารที่ เลขที่ '.$audit->no_direct_committee.' ลงวันที่              30 พฤษภาคม 2562  เรื่องแต่งตั้งคณะกรรมพิจารณาผลการประกวดราคา และคณะกรรมการตรวจรับพัสดุสำหรับการซื้อและจ้างบำรุงรักษาโครงการพัฒนาประสิทธิภาพระบบเครือข่ายสื่อสารระยะไกล (Wide Area Network : WAN) สำหรับสาขาทั่วประเทศ ชุดที่ 1 เครือข่ายสื่อสารและระบบรักษาความปลอดภัย โดยวิธีประกวดราคาราคา โดยวิธีประกวดราคา (เอกสารแนบ 1) ประกอบด้วย  ');











        // Define table style arrays
        $styleTable = array('borderSize'=>6);
       // $styleFirstRow = array();

        // Define cell style arrays
        $styleCell = array('valign'=>'center');
        //$styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);

        // Define font style for first row
        $fontStyle = array('bold'=>true, 'align'=>'center','name'=>'TH SarabunPSK','size'=>16);
        $fontStyle2 = array( 'align'=>'center','name'=>'TH SarabunPSK','size'=>16);

        // Add table style
        $PHPWord->addTableStyle('myOwnTableStyle', $styleTable);

        // Add table
        $table = $section->addTable('myOwnTableStyle');

        // Add row
        $table->addRow();
        // Add cells
        $table->addCell(50, $styleCell)->addText('ลำดับ', $fontStyle,'P_title');
        $table->addCell(5000, $styleCell)->addText('ชื่อผู้ยื่นข้อเสนอ', $fontStyle,'P_title');
        $table->addCell(1500, $styleCell)->addText('ราคาที่เสนอครั้งสุดท้าย(บาท)', $fontStyle,'P_title');
        $table->addCell(1500, $styleCell)->addText('ราคาที่ลดลง/เพิ่มขึ้น(บาท)', $fontStyle,'P_title');
        $table->addCell(1500, $styleCell)->addText('ลดลง/เพิ่มขึ้นร้อยละจากราคากลาง', $fontStyle,'P_title');

        $i=1;
        foreach ($vender as $row){
            $table->addRow();
            $table->addCell(10, $styleCell)->addText($i, $fontStyle2,'P_title');
            $table->addCell(10, $styleCell)->addText($row->name_vender , $fontStyle2);
            $table->addCell(10, $styleCell)->addText($row->price_active_vender, $fontStyle2,'P_title');
            $table->addCell(10, $styleCell)->addText($row->chaffer_active_vender, $fontStyle2,'P_title');
            $table->addCell(10, $styleCell)->addText('', $fontStyle2,'P_title');

            $i++;
        }



		
        $filename='การสังเกตการณ์การ'.$audit->name_bid.'.docx'; //save our document as this file name
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        $objWriter->save('php://output');
    }

    function temp(){

        $PHPWord = new PHPWord();

        $document = $PHPWord->loadTemplate('/Template.docx');

        $document->setValue('Value1', 'Sun');
        $document->setValue('Value2', 'Mercury');
        $document->setValue('Value3', 'Venus');
        $document->setValue('Value4', 'Earth');
        $document->setValue('Value5', 'Mars');
        $document->setValue('Value6', 'Jupiter');
        $document->setValue('Value7', 'Saturn');
        $document->setValue('Value8', 'Uranus');
        $document->setValue('Value9', 'Neptun');
        $document->setValue('Value10', 'Pluto');

        $document->setValue('weekday', date('l'));
        $document->setValue('time', date('H:i'));

        $document->save('Solarsystem.docx');
    }



}
?>
