<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class audit extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('audit_model');
        $this->load->model('function_model');
        $this->load->model('login_model');
        $this->login_model->check_session_for_audit();

    }

    public function index(){
        $this->load->view('audit/head');
        $this->load->view('audit/index');
        $this->load->view('audit/footer');
    }

    public function add_audit(){
        $result = $this->audit_model->model_show_bid_for_add_audit();
        $data['bid'] = $result;
        $this->load->view('audit/head');
        $this->load->view('audit/add_audit',$data);
        $this->load->view('audit/footer');
    }



    public function add_audit2(){
        if(!isset($_POST['info_no_bid'])){
            echo "<script>alert('ไม่พบเลขที่จัดซื้อจัดจ้าง ไม่สามารถเข้าดูข้อมูลได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['info_no_bid'];
            $data['bid'] = $this->audit_model->model_show_info_bid_byno_bid($no_bid);
            $this->load->view('audit/head');
            $this->load->view('audit/add_audit2',$data);
            $this->load->view('audit/footer');
        }
    }

    public function show_audit(){ ////////////////////////////////////ค้างแสดงชื่อ ผู้ตรวจสอบ
        $result = $this->audit_model->model_show_audit();
        $data['info_audit'] = $result['audit'];
        $data['num_audit'] = $result['num'];



        $this->load->view('audit/head');
        $this->load->view('audit/show_audit',$data);
        $this->load->view('audit/footer');
    }


    public function info_audit(){
        if(!isset($_POST['info_no_bid'])){
            echo "<script>alert('ไม่สามารถแสดงการสังเกตการณ์ได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['info_no_bid'];
            $result = $this->audit_model->model_info_audit_by_no_bid($no_bid);
            $data['audit'] = $result;
            $audit = $result['0'];
            $name1 = $audit->name1_audit ;
            $name2 = $audit->name2_audit ;
            $name3 = $audit->name3_audit ;
            $name4 = $audit->name4_audit ;
            $data['name1_member'] = $this->audit_model->model_show_audit_name($name1);
            $data['name2_member'] = $this->audit_model->model_show_audit_name($name2);
            $data['name3_member'] = $this->audit_model->model_show_audit_name($name3);
            $data['name4_member'] = $this->audit_model->model_show_audit_name($name4);

            $this->load->view('audit/head');
            $this->load->view('audit/info_audit',$data);
            $this->load->view('audit/footer');

        }
    }

    public function edit_audit(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแก้ไขได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $result = $this->audit_model->model_info_audit_by_no_bid($no_bid);
            $data['audit'] = $result;
            $audit = $result['0'];
            $name1 = $audit->name1_audit ;
            $name2 = $audit->name2_audit ;
            $name3 = $audit->name3_audit ;
            $name4 = $audit->name4_audit ;
            $data['name1_member'] = $this->audit_model->model_show_audit_name($name1);
            $data['name2_member'] = $this->audit_model->model_show_audit_name($name2);
            $data['name3_member'] = $this->audit_model->model_show_audit_name($name3);
            $data['name4_member'] = $this->audit_model->model_show_audit_name($name4);


            $this->load->view('audit/head');
            $this->load->view('audit/edit_audit',$data);
            $this->load->view('audit/footer');
        }
    }

    public function add_bid(){
        $this->load->view('audit/head');
        $this->load->view('audit/add_bid');
        $this->load->view('audit/footer');

    }


    public function add_audit_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถเพิ่มการสังเกตการณ์');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $date_audit = $_POST['date_audit'];
            $place_audit = $_POST['place_audit'];
            $name_audit1 = $_POST['name_audit1'];
            $name_audit2 = $_POST['name_audit2'];
            $name_audit3 = $_POST['name_audit3'];
            $name_audit4 = $_POST['name_audit4'];
            $issue_audit = $_POST['issue_audit'];
            $condition_audit = $_POST['condition_audit'];

            $result = $this->audit_model->model_add_audit_ajax($no_bid,$date_audit,$place_audit,$name_audit1,$name_audit2,$name_audit3,$name_audit4,$issue_audit,$condition_audit);
            echo $result;
        }

    }

    public function edit_audit_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแก้ไขการสังเกตการณ์');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $date_audit = $_POST['date_audit'];
            $name_audit1 = $_POST['name_audit1'];
            $name_audit2 = $_POST['name_audit2'];
            $name_audit3 = $_POST['name_audit3'];
            $name_audit4 = $_POST['name_audit4'];
            $issue_audit = $_POST['issue_audit'];
            $condition_audit = $_POST['condition_audit'];

            $result = $this->audit_model->model_edit_audit_ajax($no_bid,$date_audit,$name_audit1,$name_audit2,$name_audit3,$name_audit4,$issue_audit,$condition_audit);
            echo $result;
        }
    }


    public function delete_audit(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถลบได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $result = $this->audit_model->model_delete_audit($no_bid);

            echo $result;
        }
    }


    public function toword(){////////////////////////////////////////////////////////////ค้างไว้ก่อน

        $no_bid = "2562001";
        $data['audit'] = $this->audit_model->model_info_audit_by_no_bid($no_bid);
        $this->load->view('audit/word',$data);

    }













    public function add_project(){
        $this->load->view('audit/head');
        $this->load->view('audit/add_project');
        $this->load->view('audit/footer');
    }

    public function test(){

        $this->load->view('audit/test');

    }



    public function show_bid_by_vender(){

        $this->load->view('audit/head');
        $this->load->view('audit/show_bid_by_vender');
        $this->load->view('audit/footer');
    }

    public function edit_profile(){
        $data['member']=$this->audit_model->show_profile();

        $this->load->view('audit/head');
        $this->load->view('audit/edit_profile',$data);
        $this->load->view('audit/footer');
    }

    public function edit_password(){
        $this->load->view('audit/head');
        $this->load->view('audit/edit_password');
        $this->load->view('audit/footer');
    }

    public function show_bid(){
        $result = $this->audit_model->model_show_bid();
        $data['data'] = $result['bid'];
        $data['num_bid'] = $result['num'];
        $this->load->view('audit/head');
        $this->load->view('audit/show_bid',$data);
        $this->load->view('audit/footer');
    }

    public function info_bid(){
        if(!isset($_POST['info_no_bid'])){
            echo "<script>alert('ไม่พบเลขที่จัดซื้อจัดจ้าง ไม่สามารถเข้าดูข้อมูลได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['info_no_bid'];
            $data['bid'] = $this->audit_model->model_show_info_bid_byno_bid($no_bid);
            $this->load->view('audit/head');
            $this->load->view('audit/info_bid',$data);
            $this->load->view('audit/footer');
        }
    }

    public function edit_bid(){

        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่พบเลขที่จัดซื้อจัดจ้าง ไม่สามารถแก้ไขข้อมูลได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $data['bid'] = $this->audit_model->model_show_info_bid_byno_bid($no_bid);
            $this->load->view('audit/head');
            $this->load->view('audit/edit_bid',$data);
            $this->load->view('audit/footer');
        }
    }

    public function show_all_project(){
        $result = $this->audit_model->model_show_all_project();
        $data['all_project'] = $result['project'];
        $data['num'] = $result['num'];
        $this->load->view('audit/head');
        $this->load->view('audit/show_all_project',$data);
        $this->load->view('audit/footer');

    }

    public function info_project(){
        if(!isset($_POST['info_id_project'])){
            echo "<script>alert('ไม่พบเลขที่โครงการจัดซื้อจัดจ้าง ไม่สามารถเข้าดูข้อมูลได้');window.history.back();</script>";
        }else{
            $info_id_project = $_POST['info_id_project'];
            $result = $this->audit_model->model_info_project($info_id_project);
            $data['info_project'] = $result;
            $this->load->view('audit/head');
            $this->load->view('audit/info_project',$data);
            $this->load->view('audit/footer');
        }
    }

    public function edit_project(){
        if(!isset($_POST['id_project'])){
            echo "<script>alert('ไม่พบเลขที่โครงการจัดซื้อจัดจ้าง ไม่สามารถแก้ไขข้อมูลได้');window.history.back();</script>";
        }else{
            $id_project = $_POST['id_project'];
            $result = $this->audit_model->model_info_project($id_project);
            $data['info_project'] = $result;
            $this->load->view('audit/head');
            $this->load->view('audit/edit_project',$data);
            $this->load->view('audit/footer');
        }
    }

    public function register_vender(){
        $this->load->view('audit/head');
        $this->load->view('audit/register_vender');
        $this->load->view('audit/footer');
    }

    public function show_all_vender(){
        $result = $this->audit_model->model_show_vender();
        $data['all_vender'] = $result['vender'];
        $data['numshow_committee_bid_ajax'] = $result['num'];
        $this->load->view('audit/head');
        $this->load->view('audit/show_all_vender',$data);
        $this->load->view('audit/footer');

    }




















    public function add_project_ajax(){

        if(!isset($_POST['name_project'])){
            echo "<script>alert('ไม่สามารถเพิ่มโครงการได้');window.history.back();</script>";
        }else{
            $name_project = $_POST['name_project'];
            $no_project = $_POST['no_project'];
            $date_project = $_POST['date_project'];
            $owner_project = $_POST['owner_project'];
            $cost_project = $_POST['cost_project'];
            $objective_project = $_POST['objective_project'];

            $result = $this->audit_model->model_add_project($name_project,$no_project,$date_project,$owner_project,$cost_project,$objective_project);
            if($result=="successful"){
                echo "เพิ่มสำเร็จ";
            }else{
                echo $result ;
            }
        }

    }

    public function delete_project(){

        if(!isset($_POST['id_project'])){
            echo "<script>alert('ไม่สามารถลบโครงการได้');window.history.back();</script>";
        }else{
            $id_project = $_POST['id_project'];
            $result = $this->audit_model->model_delete_project($id_project);
            echo $result;
        }

    }

    public function edit_project_ajax(){
        if(!isset($_POST['id_project'])){
            echo "<script>alert('ไม่สามารถแก้ไขโครงการได้');window.history.back();</script>";
        }else{
            $name_project = $_POST['name_project'];
            $id_project = $_POST['id_project'];
            $no_project = $_POST['no_project'];
            $date_project = $_POST['date_project'];
            $owner_project = $_POST['owner_project'];
            $cost_project = $_POST['cost_project'];
            $objective_project = $_POST['objective_project'];

            $result = $this->audit_model->model_edit_project_ajax($name_project,$id_project,$no_project,$date_project,$owner_project,$cost_project,$objective_project);

            echo $result;
        }
    }

    public function show_project(){
        $result = $this->audit_model->model_show_project();
        echo $result;
    }

    public function check_name_project(){
        $name_project = $_POST['input_project'];
        $result = $this->audit_model->model_check_name_project($name_project);
        echo $result;
    }



    public function check_no_bid(){
        $no_bid = $_POST['no_bid'];
        if(isset($_POST['year'])){
            $year = $_POST['year'];
        }else{
            $year = "";
        }
        echo $result = $this->audit_model->model_check_no_bid($no_bid,$year);
    }

    public function upload_file() {
        //upload file
        $file_name = $_POST['no_bid']."_1" ;
        $config['file_name']=$file_name;
        $config['upload_path'] = 'file_upload/';
        $config['allowed_types'] = '*';
        $config['max_filename'] = '1000';
        $config['encrypt_name'] = false;
        //$config['max_size'] = '1024'; //1 MB
        $old_name = $_FILES['file']['name'];
        $type = strrchr($old_name,".");
        $new_name = $file_name.$type;
        $no_bid = $_POST['no_bid'] ;
        $path = 'file_upload/' . $file_name.$type;
        $i = "1";

        while($result=$this->audit_model->model_add_attract_file($old_name,$new_name,$path,$no_bid)=="have"){
            $file_name = substr($file_name,0,-1).$i;
            $new_name = $file_name.$type;
            $path = 'file_upload/' . $file_name.$type;
            $config['file_name'] = $file_name ;
            $i++;
        }

        if (isset($_FILES['file']['name'])) {
            if (0 < $_FILES['file']['error']) {
                echo 'ไม่สามารถอัพโหลดได้' . $_FILES['file']['error'];
            }else{
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('file')) {
                    echo $this->upload->display_errors();
                }else{
                    echo "อัพโหลดเรียบร้อยแล้ว";
                }
            }
        } else {
            echo 'กรุณาเลือกไฟล์ที่ อัพโหลด';
        }
        if($result=="unsuccessful"){
            echo 'ไม่สามารถอัพโหลดไฟล์ '.$_FILES['file']['name'].' ได้';
        }
    }

    public function show_attract_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแสดงเอกสารแนบได้');window.history.back();</script>";
        }else{
            if(!isset($_POST['info'])){
                $info = "";
            }else{
                $info = "style='display: none'";
            }
            $no_bid = $_POST['no_bid'];
            $result = $this->audit_model->model_show_attract_by_no_bid($no_bid);
            $i=1;

            echo "<table cellpadding='5' class='table col-6' >";
            echo "
            <tr>
                <td>ลำดับ</td>
                <td>ชื่อไฟล์</td>
                <td $info ></td>            
            </tr>
        ";

            foreach ($result as $row){
                $path_attract = $row->path_attract ;
                $url = base_url($path_attract);
                echo "
                <tr>
                    <td>$i</td>
                    <td><a href='$url' target='_blank'>$row->oldname_attract</a></td> 
                    <td $info><a href='#' id='delete_attract' title='ลบ' onclick='delete_attract_ajax($row->id_attract)' class='btn btn-danger '>
                        <i class='fas fa-trash'></i></a>
                    </td>                
                </tr>      
            ";
                $i++;
            }
            echo "</table>";
        }
    }

    public function delete_attract(){
        if(!isset($_POST['id_attract'])){
            echo "<script>alert('ไม่พบรหัสเอกสารแนบ ไม่สามารถลบเอกสารแนบได้');window.history.back();</script>";
        }else{
            $id_attract = $_POST['id_attract'];
            $result = $this->audit_model->model_delete_attract($id_attract);

            if($result=="success"){
                echo "ทำการลบไฟล์แนบเรียบร้อยแล้ว";
            }else{
                echo "ไม่สามารถลบไฟล์แนบได้";
            }
        }
    }

    public function show_vender_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแสดงบริษัท');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $data = $this->audit_model->model_show_vender_by_no_bid($no_bid);
            echo $data ;
        }
    }

    public function delete_active_vender(){

        if(!isset($_POST['id_vender'])){
            echo "<script>alert('ไม่สามารถลบบริษัทได้');window.history.back();</script>";
        }else{
            $id_vender = $_POST['id_vender'];
            $result = $this->audit_model->model_delete_active_vender($id_vender);

            if($result=="success"){
                echo "ทำการลบบริษัทเรียบร้อยแล้ว";
            }else{
                echo "ไม่สามารถลบบริษัทได้";
            }
        }
    }

    public function add_vender(){

        if(!isset($_POST['no_bid'])||!isset($_POST['name_vender'])){
            echo "<script>alert('ไม่สามารถเพิ่มบริษัทได้');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $name_vender = $_POST['name_vender'];
            $sender = $_POST['sender'];
            $win = $_POST['win'];
            $pricebid = $_POST['pricebid'];
            $price_under_bid = $_POST['price_under_bid'];
            $pass_vender = $_POST['pass_vender'];
            $result = $this->audit_model->model_add_vender($no_bid,$name_vender,$sender,$win,$pricebid,$price_under_bid,$pass_vender);
            $msg = $result['msg'];
            if($msg=="success"){
                echo $msg;
            }else{
                echo $msg;
            }
        }
    }

    public function show_name_vender(){
        $result = $this->audit_model->model_show_name_vender();
        echo $result;
    }

    public function sign_up_vender(){
        $name_sign_up_vender = $_POST['name_sign_up_vender'];
        $capital_vender = $_POST['capital_vender'];
        $result= $this->audit_model->model_sign_up_vender($name_sign_up_vender,$capital_vender);
        if($result=="success"){
            echo "ทำการลงทะเบียนสำเร็จ";
        }else{
            echo $result;
        }
    }

    public function add_bid_button(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถเพิ่มการจัดซื้อจัดจ้าง');window.history.back();</script>";
        }else{
            $input_name_project  = $_POST['input_name_project'];
            $status_bid  = $_POST['status_bid'];
            $year_bid  = $_POST['year_bid'];
            $no_bid  = $_POST['no_bid'];
            $date_bid  = $_POST['date_bid'];
            $subject_bid  = $_POST['subject_bid'];
            $no_announce  = $_POST['no_announce'];
            $date_announce  = $_POST['date_announce'];
            $type_bid  = $_POST['type_bid'];
            $cost_bid = $_POST['cost_bid'];
            $price_base_bid = $_POST['price_base_bid'];
            $price_bid = $_POST['price_bid'];
            $month_send_draft_contract = $_POST['month_send_draft_contract'];
            $month_approve_bid = $_POST['month_approve_bid'];
            $no_send_draft_contract = $_POST['no_send_draft_contract'];
            $no_egp = $_POST['no_egp'];
            $date_contract = $_POST['date_contract'];
            $no_contract = $_POST['no_contract'];
            $member = $_POST['member'];
            $date_receive_bid = $_POST['date_receive_bid'];
            $no_direct_committee = $_POST['no_direct_committee'];
            $date_direct_committee = $_POST['date_direct_committee'];
            $subject_direct_committee = $_POST['subject_direct_committee'];

            $result = $this->audit_model->model_add_bid($input_name_project,$status_bid,$year_bid,$no_bid,$date_bid,$subject_bid,$no_announce,$date_announce,$type_bid,$cost_bid,$price_base_bid,$price_bid,$month_send_draft_contract,$month_approve_bid,$no_send_draft_contract,$no_egp,$date_contract,$no_contract,$member,$date_receive_bid,$no_direct_committee,$date_direct_committee,$subject_direct_committee);

            if($result=="success"){
                echo "บันทึกการจัดซื้อจัดจ้าง สำเร็จ";
            }else{
                echo "บันทึกการจัดซื้อจัดจ้างสำเร็จ ไม่สำเร็จ";
            }
        }
    }

    public function edit_bid_button(){

        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารแก้ไขการจัดซื้อจัดจ้าง');window.history.back();</script> ";
        }else{
            $input_name_project  = $_POST['input_name_project'];
            $status_bid  = $_POST['status_bid'];

            $no_bid  = $_POST['no_bid'];
            $date_bid  = $_POST['date_bid'];
            $subject_bid  = $_POST['subject_bid'];
            $no_announce  = $_POST['no_announce'];
            $date_announce  = $_POST['date_announce'];
            $type_bid  = $_POST['type_bid'];
            $cost_bid = $_POST['cost_bid'];
            $price_base_bid = $_POST['price_base_bid'];
            $price_bid = $_POST['price_bid'];
            $month_send_draft_contract = $_POST['month_send_draft_contract'];
            $month_approve_bid = $_POST['month_approve_bid'];
            $no_approve_bid = $_POST['no_approve_bid'];
            $no_egp = $_POST['no_egp'];
            $date_contract = $_POST['date_contract'];
            $no_contract = $_POST['no_contract'];
            $member = $_POST['member'];
            $date_receive_bid = $_POST['date_receive_bid'];
            $no_direct_committee = $_POST['no_direct_committee'];
            $date_direct_committee = $_POST['date_direct_committee'];
            $subject_direct_committee = $_POST['subject_direct_committee'];


            $result = $this->audit_model->model_edit_bid($input_name_project,$status_bid,$no_bid,$date_bid,$subject_bid,$no_announce,$date_announce,$type_bid,$cost_bid,$price_base_bid,$price_bid,$month_send_draft_contract,$month_approve_bid,$no_approve_bid,$no_egp,$date_contract,$no_contract,$member,$date_receive_bid,$no_direct_committee,$date_direct_committee,$subject_direct_committee);

            if($result=="success"){
                echo "แก้ไขการจัดซื้อจัดจ้าง สำเร็จ";
            }else{
                echo "แก้ไขการจัดซื้อจัดจ้างสำเร็จ ไม่สำเร็จ";
            }
        }
    }

    public function delete_bid(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถลบการจัดซื้อจัดจ้าง');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $data = $this->audit_model->model_delete_bid($no_bid);
            echo $data;
        }
    }

    public function show_bid_by_vender_ajax(){
        if(!isset( $_POST['name_vender'])){
            echo  "<script>alert('ไม่สามารถแสดงราชื่อบริษัทได้');window.history.back();</script>";
        }else{
            $vender = $_POST['name_vender'];
            $before = $_POST['before'];
            $after = $_POST['after'];
            $result = $this->audit_model->model_show_bid_by_vender_ajax($vender,$before,$after);
            echo $result;
        }
    }


    public function delete_vender(){
        if(!isset($_POST['id_vender'])){
            echo  "<script>alert('ไม่สามารถลบบริษัทได้');window.history.back();</script>";
        }else{
            $id_vender = $_POST['id_vender'];
            $name_vender = $_POST['name_vender'];
            $result = $this->audit_model->model_delete_vender($id_vender,$name_vender);
            echo $result ;
        }
    }


    public function edit_vender(){
        if(!isset($_POST['id_vender'])){
            echo "<script>alert('ไม่สามารถแก้ไขได้');window.history.back();</script>";
        }else{
            $id_vender = $_POST['id_vender'];
            $result = $this->audit_model->model_info_vender($id_vender);
            $data['info_vender'] = $result;
            $this->load->view('audit/head');
            $this->load->view('audit/edit_vender',$data);
            $this->load->view('audit/footer');
        }
    }

    public function edit_vender_ajax(){
        if(!isset($_POST['name_vender'])){
            echo "<script>alert('ไม่สามารถแก้ไขได้');window.history.back();</script>";
        }else{
            $name_vender = $_POST['name_vender'];
            $capital_vender = $_POST['capital_vender'];
            $id_vender = $_POST['id_vender'];
            $result  = $this->audit_model->model_edit_vender_ajax($name_vender,$capital_vender,$id_vender);
            echo $result;
        }
    }


    public function show_committee_bid_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถแสดงคณะกรรมการประกวดราคา');window.history.back();</script>";
        }else{
            if(!isset($_POST['edit'])){
                $edit = "";
            }else{
                $edit = "style='display: none'";
            }
            $no_bid = $_POST['no_bid'];
            $result = $this->audit_model->model_show_committee_bid_by_no_bid($no_bid);

            $i=1;

            echo "<table cellpadding='5' class='table col-9' >";
            echo "
            <tr>
                <td align='center'>ลำดับ</td>
                <td>ชื่อ</td>
                <td>ตำแหน่ง</td>   
                <td align='center'>สถานะวันประกวดราคา</td>   
                <td $edit></td>      
            </tr>
        ";

            foreach ($result as $row){

                $id_committee_bid =$row->id_committee_bid ;
                $name_committee_bid = $row->name_committee_bid ;
                $position_committee_bid = $row->position_committee_bid;
                $status_committee_bid = $row->status_committee_bid;
                if($status_committee_bid==0){
                    $status_committee_bid = "ไม่มา";
                }else{
                    $status_committee_bid = "มา";
                }


                echo "
                <tr>
                    <td align='center'>$i</td>
                    <td>$name_committee_bid</td>
                    <td>$position_committee_bid</td>     
                    <td align='center'>$status_committee_bid</td>                           
                    <td $edit ><a href='#' id='delete_committee_bid' title='ลบ' onclick='delete_committee_bid_ajax($id_committee_bid , \"$name_committee_bid\")' class='btn btn-danger '>
                        <i class='fas fa-trash'></i></a>
                    </td>
                </tr>      
            ";
                $i++;
            }
            echo "</table>";
        }
    }



    public function add_committee_bid_ajax(){
        if(!isset($_POST['no_bid'])){
            echo "<script>alert('ไม่สามารถเพิ่มคณะกรรมการ');window.history.back();</script>";
        }else{
            $no_bid = $_POST['no_bid'];
            $name_committee_bid = $_POST['name_committee_bid'];
            $position_committee_bid = $_POST['position_committee_bid'];
            $status_committee_bid = $_POST['status_committee_bid'];

            $result = $this->audit_model->model_add_committee_bid_ajax($no_bid,$name_committee_bid,$position_committee_bid,$status_committee_bid);
            echo $result ;
        }
    }

    public function delete_committee_bid_ajax(){
        if(!isset($_POST['id_committee_bid'])){
            echo "<script>alert('ไม่สามารถลบได้');window.history.back();</script>";
        }else{
            $id_committee_bid = $_POST['id_committee_bid'];
            $name_committee_bid = $_POST['name_committee_bid'];
            $result = $this->audit_model->model_delete_committee_bid_ajax($id_committee_bid,$name_committee_bid);
            echo  $result;
        }
    }








    public function converse_nobid_ajax(){
        $no_bid = $_POST['no_bid'];
        echo $no_bid ;
        $result = $this->audit_model->converse_nobid($no_bid);
        echo $result ;
    }
















}