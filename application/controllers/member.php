<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class member extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('member_model');
        $this->load->model('login_model');
        $this->login_model->check_session_for_admin();

    }

    public function show_all_member(){
        $this->member_model->model_check_admin();
        $result = $this->member_model->model_show_all_member();
        $data['member'] = $result;
        $this->load->view('from/head');
        $this->load->view('member/show_member',$data);
        $this->load->view('from/footer');
    }

    public function add_member(){
        $this->member_model->model_check_admin();
        $this->load->view('from/head');
        $this->load->view('member/add_member');
        $this->load->view('from/footer');
    }

    public function add_member_ajax(){
        if(!isset($_POST['employee_id_member'])){
            echo "<script>alert('ไม่สามารถเพิ่มผู้ใช้งานได้');</script>";
        }else{
            $employee_id_member = $_POST['employee_id_member'];
            $name_member = $_POST['name_member'];
            $position_member = $_POST['position_member'];
            $user_member = $_POST['employee_id_member'];
            $status_member = $_POST['status_member'];
            $email_member = $_POST['email_member'];
            $tel_member = $_POST['tel_member'];
            $agency_member = $_POST['agency_member'];

            $result = $this->member_model->model_add_member_ajax($employee_id_member,$name_member,$position_member,$user_member,$status_member,$email_member,$tel_member,$agency_member);
            echo $result;
        }

    }
    
}